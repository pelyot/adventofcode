import sys

ing = []
for l in sys.stdin.readlines():
    name, charac = l.rstrip().split(': ')
    w = charac.split(', ')
    c = [int(w[i].split()[1]) for i in range(4)]
    ing.append(c)
    print name,c

m = 0
for r in range(0,100):
    for s in range(r+1,100):
        for t in range(s+1,100):
            n = [r, s-r, t-s, 100-t]
            #print '%s%s%s%s' % ('r'*n[0], 's'*n[1], 't'*n[2], 'u'*n[3])
            #print sum(n)
            val = \
            reduce(lambda acc, x: acc * x,
                   map(lambda x: x if x > 0 else 0,
                       map(sum,
                           zip(*(map(lambda x: n[i] * x, ing[i]) for i in range(4)))
                          )
                       )
                   )
            if val > m:
                m = val 
                print val
    

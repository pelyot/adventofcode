use std::io::BufRead;

type BoolMatrix = Vec<Vec<bool>>;

struct Grid {
  data: BoolMatrix
}
const N : i32 = 100;

impl Grid {
  fn print(&self) {
    for line in &self.data {
      println!("{}",line.iter().map(|&e| if e {'#'} else {'.'}).collect::<String>());
    }
    println!("");
  }
  fn is_on(&self, l: i32, c: i32) -> bool {
    match l {
      l if l < 0 || l >= N => return false,
      l => match c {
        c if c < 0 || c >= N => return false,
        c => self.data[l as usize][c as usize]
      }
    }
  }
  fn new(data: BoolMatrix) -> Grid {
    Grid {data: data}
  }
  fn clone(&self) -> Grid {
    Grid {data: self.data.clone()}
  }
  fn evolve(&self, other: &mut Grid) {
    let n = N as usize;
    for l in 0..n {
      for c in 0..n {
        let nb = self.neighbors(l as i32,c as i32);
        if nb == 3 || (self.data[l][c] && nb == 2) {
          other.data[l][c] = true
        } else {
          other.data[l][c] = false
        }
      }
    }
    other.data[0][0] = true;
    other.data[0][n-1] = true;
    other.data[n-1][0] = true;
    other.data[n-1][n-1] = true;
  }
  fn neighbors(&self, l: i32, c: i32) -> i32 {
    let mut count = 0;
    for li in l-1..l+2 {
      for ci in c-1..c+2 {
        if (li,ci) != (l,c) && self.is_on(li,ci) {
          count += 1
        }
      }
    }
    count
  }
}

fn main() {
  let fin = std::fs::File::open("input.txt").unwrap();
  let reader = std::io::BufReader::new(fin);
  let data: BoolMatrix = reader.lines().map(|line| line.unwrap().chars().map(|c| c == '#').collect()).collect();
  let mut g1 = Grid::new(data);
  let mut g2 = g1.clone();
  for _ in 0..50 {
    g1.evolve(&mut g2);
    g2.evolve(&mut g1);
  }
  println!("{}", g1.data.iter().fold(0, |acc, ref el| acc + el.iter().filter(|&e| *e).count()))
}

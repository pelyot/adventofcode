import string

repl = []
values = []
molecules = set()
with open("day19/input_mod.txt", "r") as f:
  for line in f.readlines():
    if ' => ' in line:
      a, b = line.rstrip().split(' => ')
      repl.append((a, b.rstrip()))
    elif len(line) > 1:
      base = line.rstrip()

count = 0
print base
while True:
  for i,(k,v) in enumerate(repl):
    if v in base:
      print i,':',k,'<=',v
  which = int(raw_input('Which repl? '))
  a,b = repl[which]
  print a,'<=',b
  occ = base.count(b)
  if occ == 0:
    print("No match!")
  else:
    print 'Found',occ
    iocc = raw_input('Which occurence? (0..%i) ' % (occ-1))
    print base
    if iocc == '':
      base = base.replace(b,a)
      count += occ
    else:
      index = base.find(b,0)
      for i in range(0,int(iocc)):
        index = base.find(b,index+1)
      base = base[0:index]+a+base[index+len(b):]
      count += 1
    print base,'@',count,'\n'

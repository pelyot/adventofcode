const ROW: usize = 2981;
const COL: usize = 3075;

fn u(row: usize, col: usize) -> usize {
    // let u(r,c) the number at row = r, col = c
    // to reach u(r,c), we first get u(1,c), the from there go to u(r,c)
    let u_1_c = col * (col + 1) / 2;
    let mut u = u_1_c;
    for r in 1..row {
        u += r + col - 1;
    }
    u
}

fn next(nb: usize) -> usize {
    (252533 * nb) % 33554393
}
fn main() {
    let nb_iter = u(ROW, COL);
    let mut nb = 20151125;
    for _ in 1..nb_iter {
        nb = next(nb)
    }
    //28423277 => too high
    //7447965 => too low
    println!("{}", nb);
}

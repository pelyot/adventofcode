import string
import random
import sys

repl = []
replv = []
values = []
molecules = set()
with open("day19/input.txt", "r") as f:
  for line in f.readlines():
    if ' => ' in line:
      a, b = line.rstrip().split(' => ')
      repl.append((a, b.rstrip()))
      replv.append((b.rstrip(), a))
    elif len(line) > 1:
      base = line.rstrip()

l = len(replv)

big_loop = 0
while True:
    s = base
    count = 0
    cont = True
    big_loop += 1
    print('- {}'.format(big_loop))
    while cont:
        cont = False
        random.shuffle(replv)
        print('{} {}'.format(len(s),s))
        for ra, rb in replv:
            if ra in s:
                s = s.replace(ra, rb, 1)
                count += 1
                cont = True
                break
    if len(s) < 50:
        print(s)
    if s == 'e':
        print('>>>>>>>>: {}'.format(count))
        sys.exit()

use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;

#[derive(Debug)]
enum Register {
    A, B
}

#[derive(Debug)]
enum Instruction {
    Hlf(Register),
    Tpl(Register),
    Inc(Register),
    Jmp(i32),
    Jie(Register, i32),
    Jio(Register, i32)
}

#[derive(Debug)]
struct Program {
    instructions: Vec<Instruction>,
    reg_a: i32,
    reg_b: i32,
    program_counter: i32
}

impl Program {
    fn FromFile(name: &str) -> Program {
        let f = File::open(name).expect("Cannot open input file");
        let reader = BufReader::new(f);
        let mut instructions = Vec::<Instruction>::new();
        Program {
            instructions: reader.lines().map(|l| Program::DecodeInstruction(&l.unwrap())).collect(),
            reg_a: 1,
            reg_b: 0,
            program_counter: 0
        }
    }
    fn DecodeInstruction(line: &str) -> Instruction {
        let mut words = line.split_whitespace();
        match words.next().unwrap() {
            "hlf" => Instruction::Hlf(Program::DecodeRegister(words.next().unwrap())),
            "tpl" => Instruction::Tpl(Program::DecodeRegister(words.next().unwrap())),
            "inc" => Instruction::Inc(Program::DecodeRegister(words.next().unwrap())),
            "jmp" => Instruction::Jmp(words.next().unwrap().parse::<i32>().unwrap()),
            "jie" => {
                let r = Program::DecodeRegister(words.next().unwrap());
                let offset = words.next().unwrap().parse::<i32>().unwrap();
                Instruction::Jie(r, offset)
            },
            "jio" => {
                let r = Program::DecodeRegister(words.next().unwrap());
                let offset = words.next().unwrap().parse::<i32>().unwrap();
                Instruction::Jio(r, offset)
            },
            _ => unreachable!()
        }
    }
    fn DecodeRegister(word: &str) -> Register {
        match word {
            "a" => Register::A,
            "a," => Register::A,
            "b" => Register::B,
            "b," => Register::B,
            _ => unreachable!()
        }
    }
    fn Fetch(program_counter: i32, instructions: &[Instruction]) -> Option<&Instruction> {
        println!("{:?}", program_counter);
        match program_counter {
            p if p < 0 => None,
            p if p as usize >= instructions.len() => None,
            p => Some(&instructions[program_counter as usize])
        }
    }
    fn run(&mut self) -> i32 {
        while let Some(instruction) = Program::Fetch(self.program_counter, &self.instructions) {
            println!("{:?}", instruction);
            match *instruction {
                Instruction::Hlf(Register::A) => {
                    self.reg_a /= 2;
                    self.program_counter += 1
                },
                Instruction::Hlf(Register::B) => {
                    self.reg_b /= 2;
                    self.program_counter += 1
                },
                Instruction::Tpl(Register::A) => {
                    self.reg_a *= 3;
                    self.program_counter += 1},
                Instruction::Tpl(Register::B) => {
                    self.reg_b *= 3;
                    self.program_counter += 1},
                Instruction::Inc(Register::A) => {
                    self.reg_a += 1;
                    self.program_counter += 1},
                Instruction::Inc(Register::B) => {
                    self.reg_b += 1;
                    self.program_counter += 1},
                Instruction::Jmp(offset) => self.program_counter += offset,
                Instruction::Jie(Register::A, offset) => if self.reg_a % 2 == 0 {
                    self.program_counter += offset
                } else {
                    self.program_counter += 1
                },
                Instruction::Jie(Register::B, offset) => if self.reg_b % 2 == 0 {
                    self.program_counter += offset
                } else {
                    self.program_counter += 1
                },
                Instruction::Jio(Register::A, offset) => if self.reg_a == 1 {
                    self.program_counter += offset
                } else {
                    self.program_counter += 1
                },
                Instruction::Jio(Register::B, offset) => if self.reg_b == 1 {
                    self.program_counter += offset
                } else {
                    self.program_counter += 1
                }
            }
        }
        self.reg_b
    }
}
fn main() {
    println!("{:?}", Program::FromFile("input").run());
}

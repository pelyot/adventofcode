import numpy

def conv(p):
    return [ord(c) for c in p]

def rconv(p):
    return ''.join(chr(c) for c in p)

I = ord('i')
O = ord('o')
L = ord('l')

def ok(p):
    diff = [int(x) for x in numpy.ediff1d(p)]
    twogrp = False
    try:
        i = diff.index(0)
        i = diff[i+2:].index(0)
        twogrp = True
    except:
        pass
    three = 'x1x1x' in 'x' + 'x'.join(str(i) for i in diff) + 'x'
    return twogrp and three

def next(p):
    l = len(p)
    i = l - 1
    while i >= 0:
        if p[i] < ord('z'):
            p[i] += 1
            if p[i] not in (I,O,L):
                return
        else:
            if i < l - 2:
                print rconv(p)
            p[i] = ord('a')
            i -= 1

password = conv('cqjxjnds')

#print ok(conv('cqjxmzzz'))
#exit()
cnt = 0
while True:
    next(password)
    if ok(password):
        print rconv(password)
        break
while True:
    next(password)
    if ok(password):
        print rconv(password)
        break
   # cnt += 1
   # if cnt % 10000 == 0:
   #     print cnt







N = 29000000

n = 0
house = 0
m = 0
while n < N:
    house += 1
    factors = set()
    factors.add(1)
    elf = 1
    while elf * elf <= house:
        if house % elf == 0:
            factors.add(elf)
            factors.add(house // elf)
        elf += 1
    factors.add(house)
    n = 0
    for i in factors:
        if house <= i*50:
            n += 11 * i
    if n > m:
        print house, n
        m = n

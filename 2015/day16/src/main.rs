use std::io::BufRead; // trait
use std::str::FromStr;

#[derive(Debug)]
struct Sample {
  name: i32,
  children: i32,
  cats: i32,
  samoyeds: i32,
  pomeranians: i32,
  akitas: i32,
  vizslas: i32,
  goldfish: i32,
  trees: i32,
  cars: i32,
  perfumes: i32
}

impl Sample {
  fn new() -> Sample {
    Sample {name: -1,
        children: -1,
        cats: -1,
        samoyeds: -1,
        pomeranians: -1,
        akitas: -1,
        vizslas: -1,
        goldfish: -1,
        trees: -1,
        cars: -1,
        perfumes: -1
    }
  }

  fn evidence() -> Sample {
    Sample {name: -1,
        children: 3,
        cats: 7,
        samoyeds: 2,
        pomeranians: 3,
        akitas: 0,
        vizslas: 0,
        goldfish: 5,
        trees: 3,
        cars: 2,
        perfumes: 1
    }
  }

  fn read(line: &str) -> Sample {
    let line = line.chars().filter(|&c| c !=':' && c != ',').collect::<String>();
    let mut words = line.split_whitespace();
    let mut s = Sample::new();
    assert!(words.next().expect("Line should start by Sue") == "Sue");
    s.name = FromStr::from_str(words.next().expect("Sue should have a name")).ok().expect("Int not parsable");
    loop {
      match words.next() {
        Some(w) => {
          match w {
            "children" => s.children = FromStr::from_str(words.next().unwrap()).unwrap(),
            "cats" => s.cats = FromStr::from_str(words.next().unwrap()).unwrap(),
            "samoyeds" => s.samoyeds = FromStr::from_str(words.next().unwrap()).unwrap(),
            "pomeranians" => s.pomeranians = FromStr::from_str(words.next().unwrap()).unwrap(),
            "akitas" => s.akitas = FromStr::from_str(words.next().unwrap()).unwrap(),
            "vizslas" => s.vizslas = FromStr::from_str(words.next().unwrap()).unwrap(),
            "goldfish" => s.goldfish = FromStr::from_str(words.next().unwrap()).unwrap(),
            "trees" => s.trees = FromStr::from_str(words.next().unwrap()).unwrap(),
            "cars" => s.cars = FromStr::from_str(words.next().unwrap()).unwrap(),
            "perfumes" => s.perfumes = FromStr::from_str(words.next().unwrap()).unwrap(),
            _ => panic!()
          }
        },
        None => break
      }
    }
    s
  }

  fn compare(&self, evi: &Sample) -> bool {
    if self.children >= 0 && self.children != evi.children {
      return false
    }
    if self.cats >= 0 && self.cats <= evi.cats {
      return false
    }
    if self.samoyeds >= 0 && self.samoyeds != evi.samoyeds {
      return false
    }
    if self.pomeranians >= 0 && self.pomeranians >= evi.pomeranians {
      return false
    }
    if self.akitas >= 0 && self.akitas != evi.akitas {
      return false
    }
    if self.vizslas >= 0 && self.vizslas != evi.vizslas {
      return false
    }
    if self.goldfish >= 0 && self.goldfish >= evi.goldfish {
      return false
    }
    if self.trees >= 0 && self.trees <= evi.trees {
      return false
    }
    if self.cars >= 0 && self.cars != evi.cars {
      return false
    }
    if self.perfumes >= 0 && self.perfumes != evi.perfumes {
      return false
    }
    return true
  }
}

fn main() {
  let fin = std::fs::File::open("input.txt").ok().expect("Cannot open input file");
  let reader = std::io::BufReader::new(fin);
  for line in reader.lines() {
    let line = line.ok().expect("Failed to read line");
    let s = Sample::read(&line);
    let evi = Sample::evidence();
    if s.compare(&evi) {
      println!("{:?}", s);
    }
  }
  println!("done");
}


import sys

cities = dict()
routes = dict()
n = 0
for l in sys.stdin.readlines():
    route, diststr = l.rstrip().split(' = ')
    d = int(diststr)
    f,t = route.split(' to ')
    if f in cities:
        fn = cities[f]
    else:
        cities[f] = n
        fn = n
        n += 1
    if t in cities:
        tn = cities[t]
    else:
        cities[t] = n
        tn = n
        n += 1
    routes[(fn,tn)] = d
    routes[(tn,fn)] = d

import itertools
l = len(cities)
distances = []
for p in itertools.permutations(c for c in range(l)):
    d = 0
    for i in range(l - 1):
        d += routes[p[i],p[i+1]]
    print d
    distances.append(d)
print
print max(distances)


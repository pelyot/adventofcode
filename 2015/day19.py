import string

repl = {}
molecules = set()
with open("day19/input.txt", "r") as f:
  for line in f.readlines():
    if ' => ' in line:
      a, b = line.rstrip().split(' => ')
      repl.setdefault(a, []).append(b.rstrip())
    elif len(line) > 1:
      base = line.rstrip()
for k,va in repl.iteritems():
  print k,va
  for v in va:
    index = string.find(base, k, 0)
    while True:
      index = string.find(base, k, index)
      if index == -1:
        break
      else:
#        print ''
#        print index, index+len(k)
#        print base[0:index]+'['+base[index:index+len(k)]+']'+base[index+len(k):]
#        print base[0:index]+'['+v+']'+base[index+len(k):]
        molecules.add(base[0:index]+v+base[index+len(k):])
        index += 1

print len(molecules)

# My stats:
#hp = 0
#damage = 0
#armor = 0
# Boss's stats:
hp = 109
damage = 8
armor = 2
# Shop:
#Weapons:    Cost  Damage  Armor
#Dagger        8     4       0 2
#Shortsword   10     5       0 2
#Warhammer    25     6       0 4
#Longsword    40     7       0 6
#Greataxe     74     8       0 8

#Armor:      Cost  Damage  Armor
#Leather      13     0       1 13
#Chainmail    31     0       2 16
#Splintmail   53     0       3 18
#Bandedmail   75     0       4 19
#Platemail   102     0       5 21

#Rings:      Cost  Damage  Armor
#Damage +1    25     1       0 25
#Damage +2    50     2       0 25
#Damage +3   100     3       0 33
#Defense +1   20     0       1 20
#Defense +2   40     0       2 20
#Defense +3   80     0       3 26

# guess: 127
weapons = [(8,4), (10,5), (25,6), (40,7), (74,8)]
armors = [(0,0), (13,1), (31,2), (53,3), (75,4), (102,5)]
rings = [(0,0,0), (25,1,0), (50,2,0), (100,3,0), (20,0,1), (40,0,2), (80,0,3)]

import itertools
import math
wins = []
ringsp = list(itertools.combinations(rings,2))
print(ringsp[0])
ringsp.append(((0,0,0), (0,0,0)))

for wc, w in weapons:
    for ac, a in armors:
        for (r1c, r1d, r1a),(r2c, r2d, r2a) in ringsp:
            #r1c, r1d, r1a = r1
            #r2c, r2d, r2a = r2
            parmor = a + r1a + r2a
            dmgb = damage - parmor
            pdamage = w + r1d + r2d
            dmgp = pdamage - armor
            cost = wc + ac + r1c + r2c
            turnsb = math.ceil(hp / max(dmgp, 1))
            turnsp = math.ceil(100 / max(dmgb, 1))
            if turnsb > turnsp:
                wins.append(cost)
                print(cost)
                print(wc, ac, r1c, r2c)

print(max(wins))

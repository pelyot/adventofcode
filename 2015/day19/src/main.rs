use std::io::BufRead;

fn main() {
  let fin = std::fs::File::open("input.txt").unwrap();
  let reader = std::io::BufReader::new(fin);
  //let mut repl = std::collections::HashMap::new();

  for l in reader.lines() {
    let line = l.unwrap();
    if line.is_empty() {
      break;
    }
    for f in line.split(" => ") {
      println!("{:?}", f);
    }
    //let x: i32 = line.split(" => ");
    //let fields = line.split(" => ").collect::<Vec<_>>();
    //repl.insert(fields[0], fields[1]);
  }

  /*for (k,v) in repl {
    println!("{:?}", (k, v));
  }*/
}

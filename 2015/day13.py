import sys

n = 0
a = dict()
aff = dict()
for l in sys.stdin.readlines():
    w = l.split(' ')
    p = w[0]
    o = w[10][:-2]
    if p not in a:
        a[p] = n
        pn = n
        n += 1
    else:
        pn = a[p]
    if o not in a:
        a[o] = n
        on = n
        n += 1
    else:
        on = a[o]
    sign = -1 if w[2] == 'lose' else 1
    aff[(pn,on)] = sign * int(w[3])
import itertools
l = len(a)
happiness = []
for p in itertools.permutations(x for x in range(l)):
    h = 0
    for i in range(l):
        j = (i+1) % l
        h += aff[(p[i], p[j])] + aff[(p[i],p[i-1])]
    happiness.append(h)
    if h == 709:
        print p
        ks = []
        for i in range(l):
            j = (i+1) % l
            k = aff[(p[i], p[j])] + aff[(p[j],p[i])]
            ks.append(k)
            print 'k',p[i],p[j],k
        km = min(ks)
        print h - km

    

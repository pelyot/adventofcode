use std::fmt::{Debug, Display};
use std::fmt;
use std::error::Error;

pub struct AlreadyActive {
    pub spell : &'static str
}

impl Error for AlreadyActive {
    fn description(&self) -> &str {
        "Spell is already active"
    }
}

impl Debug for AlreadyActive {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Already active: {}", self.spell)
    }
}

impl Display for AlreadyActive {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Already active: {}", self.spell)
    }
}


pub struct NotEnoughMana {
    pub actual: i32,
    pub needed: i32
}

impl Error for NotEnoughMana {
    fn description(&self) -> &str {
        "Not enough mana to cast spell"
    }
}

impl Debug for NotEnoughMana {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Not enough mana: player has {} but {} is needed", self.actual, self.needed)
    }
}

impl Display for NotEnoughMana {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Not enough mana: {} but {} is needed", self.actual, self.needed)
    }
}


#[derive(Debug)]
pub enum CastFailed {
    AlreadyActive(AlreadyActive),
    NotEnoughMana(NotEnoughMana)
}

impl Error for CastFailed {
    fn description(&self) -> &str {
        "Failed to cast spell"
    }
}

impl Display for CastFailed {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            CastFailed::AlreadyActive(ref a) => fmt::Debug::fmt(a, f),
            CastFailed::NotEnoughMana(ref n) => fmt::Debug::fmt(n, f)
        }
    }
}

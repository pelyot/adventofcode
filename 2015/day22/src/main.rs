use std::fmt::{Debug};
use std::fmt;
extern crate rand;
use rand::{thread_rng, Rng};
use std::io::{Write,stderr};

mod state;
use state::*;

mod errors;
use errors::*;

mod spells;
use spells::*;

macro_rules! return_some {
    ($option_expr:expr) => ({
        let opt = $option_expr;
        if let Some(item) = opt {
            return Some(item)
        }
    })
}

struct Effect {
    spell: &'static Spell,
    remaining: i32
}
impl Effect {
    fn decrease_duration(&mut self) {
        self.remaining -= 1
    }
    fn is_done(&self) -> bool {
        self.remaining == 0i32
    }
    fn from_spell(spell: &'static Spell) -> Effect {
        match spell.duration {
            Duration::Durable(d) => Effect{spell: spell, remaining: d},
            Duration::Instant => panic!("Cannot add Instant spell as an Effect")
        }
    }
}
impl Debug for Effect {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}", self.spell.name, self.remaining)
    }
}

struct ActiveEffects {
    effects: Vec<Effect>
}
impl Debug for ActiveEffects {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.effects)
    }
}

impl ActiveEffects {
    fn new() -> ActiveEffects {
        ActiveEffects { effects: Vec::new() }
    }
    fn decrease_duration(&mut self) {
        for e in &mut self.effects {
            e.decrease_duration();
        }
    }
    fn remove_finished(&mut self) {
        self.effects.retain(|e| !e.is_done());
    }
    fn is_active(&self, spell: &Spell) -> bool {
        match self.effects.iter().find(|&e| e.spell == spell) {
            Some(_) => true,
            None => false
        }
    }
    fn add(&mut self, spell: &'static Spell) -> Result<(), AlreadyActive> {
        println!("Add effect [{}]", spell.name);
        if self.is_active(spell) {
            return Err(AlreadyActive{spell: spell.name});
        }
        self.effects.push(Effect::from_spell(spell));
        Ok(())
    }
}

#[derive(Debug)]
struct State {
    pub player: Player,
    pub boss: Boss,
    pub spent: i32,
    effects: ActiveEffects
}

impl State {
    fn new() -> State {
        State {player: Player::new(), boss: Boss::new(), effects: ActiveEffects::new(), spent: 0}
    }
    fn print(&self) {
        println!("{:?}", self)
    }
    fn cast_spell(&mut self, spell: &'static Spell) -> Result<(), CastFailed> {
        try!(self.player.take_mana(spell.cost).map_err(|e| CastFailed::NotEnoughMana(e)));
        self.spent += spell.cost;
        match spell.duration {
            Duration::Instant => {
                (spell.effect)(&mut self.player, &mut self.boss);
                Ok(())
            },
            Duration::Durable(_) => self.effects.add(spell).map_err(|e| CastFailed::AlreadyActive(e))
        }
    }
    fn apply_effects(&mut self) {
        for e in  &self.effects.effects {
            (e.spell.effect)(&mut self.player, &mut self.boss);
        }
        self.effects.decrease_duration();
        self.effects.remove_finished();
    }
    fn check_end(&self) -> Option<Condition> {
        if self.player.hp <= 0 {
            return Some(Condition::Defeat)
        }
        if self.boss.hp <= 0 {
            return Some(Condition::Victory)
        }
        None
    }
    fn boss_attack(&mut self) {
        let mut damage = BOSS_ATTACK;
        if self.effects.is_active(&SHIELD) {
            damage -= 7;
        }
        self.player.hp -= damage;
        println!("Boss attack: player.hp = {} ({})", self.player.hp, damage);
    }
    fn pick_spell(&self) -> &'static Spell {
        let mut spell = &MAGIC_MISSILE;
        loop {
            spell = thread_rng().choose(SPELLS).unwrap();
            if ! self.effects.is_active(spell) {
                break
            }
        }
        spell
    }
    fn round(&mut self) -> Option<Condition> {

        self.player.hp -= 1;
        return_some!(self.check_end());
        self.apply_effects();
        return_some!(self.check_end());

        let spell = self.pick_spell();
        if let Err(_) = self.cast_spell(spell) {
            return Some(Condition::Failed)
        }
        return_some!(self.check_end());

        self.apply_effects();
        return_some!(self.check_end());

        self.boss_attack();
        return_some!(self.check_end());

        None
    }
}


#[derive(Debug)]
enum Condition {
    Victory,
    Defeat,
    Ignore,
    Failed
}

fn main() {
    let stderr_ = stderr();
    let mut err = stderr_.lock();
    let mut s = State::new();
    loop {
        match s.round() {
            None => (),
            Some(condition) => {
                if let Condition::Victory = condition {
                    writeln!(err, "{}", s.spent);
                }
                println!("============={:?}=============", condition);
                s = State::new();
            }
        }
    }
}

use std::fmt::{Debug};
use std::fmt;
use std::cmp::{PartialEq};

use state::Player;
use state::Boss;

#[derive(Debug)]
pub enum Duration {
    Instant,
    Durable(i32)
}

pub struct Spell {
    pub name: &'static str,
    pub duration: Duration,
    pub effect: fn(&mut Player, &mut Boss),
    pub cost: i32
}
impl PartialEq for Spell {
    fn eq(&self, other: &Spell) -> bool {
        self.name == other.name
    }
}
impl Debug for Spell {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Spell {{ {}, cost: {}, type: {:?} }}", self.name, self.cost, self.duration)
    }
}

fn magic_missile(player: &mut Player, boss: &mut Boss) {
    boss.hp -= 4;
    println!("Magic Missile: boss.hp = {}", boss.hp);
}

fn drain(player: &mut Player, boss: &mut Boss) {
        boss.hp -= 2;
        player.hp += 2;
        println!("Drain: player.hp = {} / boss.hp = {}", player.hp, boss.hp);
}

fn shield(player: &mut Player, boss: &mut Boss) {
    println!("Shield: noop");
}

fn poison(player: &mut Player, boss: &mut Boss) {
    boss.hp -= 3;
    println!("Poison: boss.hp = {}", boss.hp);
}

fn recharge(player: &mut Player, boss: &mut Boss) {
    player.mana += 101;
    println!("Recharge: player.mana = {}", player.mana);
}
pub static MAGIC_MISSILE : Spell = Spell {
    name: "MagicMissile",
    duration: Duration::Instant,
    effect: magic_missile,
    cost: 53
};
pub static DRAIN : Spell = Spell {
    name: "Drain",
    duration: Duration::Instant,
    effect: drain,
    cost: 73
};
pub static SHIELD : Spell = Spell {
    name: "Shield",
    duration: Duration::Durable(6),
    effect: shield,
    cost: 113
};
pub static POISON : Spell = Spell {
    name: "Poison",
    duration: Duration::Durable(6),
    effect: poison,
    cost: 173
};
pub static RECHARGE : Spell = Spell {
    name: "Recharge",
    duration: Duration::Durable(5),
    effect: recharge,
    cost: 229
};
pub static SPELLS : &'static[&'static Spell] = &[&MAGIC_MISSILE, &DRAIN, &SHIELD, &POISON, &RECHARGE];

use errors::*;

#[derive(Debug)]
pub struct Player {
    pub hp: i32,
    pub mana: i32
}
impl Player {
    pub fn new() -> Player {
        Player {hp: 50, mana: 500}
    }
    pub fn take_mana(&mut self, m: i32) -> Result<(), NotEnoughMana> {
        if self.mana < m {
            return Err(NotEnoughMana{actual: self.mana, needed: m})
        }
        self.mana -= m;
        Ok(())
    }
}

pub const BOSS_ATTACK : i32 = 9;

#[derive(Debug)]
pub struct Boss {
    pub hp: i32
}
impl Boss {
    pub fn new() -> Boss {
        Boss {hp: 51}
    }
}

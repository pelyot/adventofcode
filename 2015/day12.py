import json
import sys

def su(j):
    if isinstance(j, dict):
        for k in j.values():
            if k == "red":
                return 0
        return sum(su(k) for k in j.values())
    elif isinstance(j, list):
        return sum(su(k) for k in j)
    elif isinstance(j, int):
        return j
    else:
        return 0

for l in sys.stdin.readlines():
    j = json.loads(l)
    print su(j) 

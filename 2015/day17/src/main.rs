
fn main() {
  let boxes = vec![11,30,47,31,32,36,3,1,5,3,32,36,15,11,46,26,28,1,19,3];
  let n = boxes.len() as u32;
  let c = (0..2u32.pow(n)).map(|i|
      boxes.iter().enumerate()
          .filter(|&(index,_)| (i >> index) % 2 == 1)
          .map(|(_,elem)| elem)
          .fold((0,0), |(nb,acc), e| (nb + 1, acc + e))
    ).filter(|&(_,acc)| acc == 150)
     .map(|(nb,_)| nb)
     .fold((n, 0), |(min, count), nb| match nb {
        x if x == min => (min, count + 1),
        x if x < min => (x, 1),
        _ => (min, count)
     });
  println!("{}",c.1)
}

import sys
#from __future__ import print_function

class Reindeer(object):
    def __init__(self, name, speed, duration, rest):
        self.name = name
        self.speed = speed
        self.duration = duration
        self.rest = rest
        self.rem = duration
        self.pos = 0
        self.state = 'FLY'
        self.points = 0
    
    def tick(self):
        if self.state == 'FLY':
            self.rem -= 1
            self.pos += self.speed
            if self.rem <= 0:
                self.rem = self.rest
                self.state = 'RST'
        elif self.state == 'RST':
            self.rem -= 1
            if self.rem <= 0:
                self.rem = self.duration
                self.state = 'FLY'

    def __str__(self):
        return '%s %s %s %s' % (self.name, self.speed, self.duration, self.rest)
    def stat(self):
        print self.name, float(self.speed) * float(self.duration) / float(self.duration + self.rest)
        

reindeers = []
for l in sys.stdin.readlines():
    w = l.split()
    reindeers.append(Reindeer(w[0], int(w[3]), int(w[6]), int(w[-2])))

for t in range(2503):
    for r in reindeers:
        r.tick()
    pmax = max(r.pos for r in reindeers)
    for r in [r for r in reindeers if r.pos == pmax]:
        r.points += 1

print max(r.points for r in reindeers)


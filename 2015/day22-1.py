import random

def dbg(*args):
    return
    print(*args)

m = 1000000

class Ignore(Exception):
    pass

class TooExpensive(Exception):
    pass

spells = ['MagicMissile', 'Drain', 'Shield', 'Poison', 'Recharge']

def strategy1():
    while True:
        for i in ['Shield', 'Poison', 'Recharge']:
            yield i

def strategy():
    while True:
        yield random.choice(spells)

costs = {'MagicMissile': 53,
'Drain': 73,
'Shield': 113,
'Poison': 173,
'Recharge': 229}

class Player(object):
    def __init__(self):
        self.hp = 50
        self.mana = 500
        self.spent = 0
        self.spell_picker = strategy()

    def pick_spell(self, effects):
        impossible = [e for e in effects]
        for s in self.spell_picker:
            if costs[s] <= self.mana and not s in impossible:
                return s
            else:
                impossible.append(s)
            if len(impossible) == len(spells):
                break
        raise TooExpensive

    def receive_damage(self, attack, effects):
        if 'Shield' in effects:
            dmg = attack - 7
        else:
            dmg = attack
        self.hp -= dmg
        dbg('Damage {}: p.hp = {}'.format(dmg, self.hp))

    def cast_spell(self, b, effects):
        s = self.pick_spell(effects)
        self.spent += costs[s]
        self.mana -= costs[s]
        if s == 'MagicMissile':
            b.hp -= 4
            dbg('MagicMissile: b.hp = {}'.format(b.hp))
        elif s == 'Drain':
            b.hp -= 2
            self.hp += 2
            dbg('Drain: p.hp = {} /  b.hp = {}'.format(self.hp, b.hp))
        else:
            d = None
            if s == 'Shield':
                d = 6
            elif s == 'Poison':
                d = 6
            elif s == 'Recharge':
                d = 5
            effects[s]=d
#        dbg('Casting {}'.format(s))
        if self.spent >= m:
            raise Ignore

class Boss(object):
    def __init__(self):
        self.hp = 51
        self.damage = 9

def apply_effects(p, b, effects):
    for e in effects:
        #dbg('Applying {}'.format(e))
        effects[e] -= 1
        if e == 'Shield':
            pass
        elif e == 'Poison':
            b.hp -= 3
            dbg('Poison: boss.hp = {}'.format(b.hp))
        elif e == 'Recharge':
            p.mana += 101
            dbg('Recharge: mana = {}'.format(p.mana))
    return dict((e, d) for e, d in effects.items() if d > 0)

class Defeat(Exception):
    pass

class Victory(Exception):
    pass

def check_end(p, b):
    if p.hp <= 0:
        raise Defeat
    if b.hp <= 0:
        raise Victory

def echo(prefix, effects, p, b):
    if 'Shield' in effects:
        armor = 7
    else:
        armor = 0
    #dbg('{}: {}\n spent: {}\n player(hp: {}, mana: {}, armor: {}) boss(hp: {})'.format(prefix, effects, p.spent, p.hp, p.mana, armor, b.hp))
    dbg('spent({}) player(hp:{},mana:{}) boss(hp:{}) effects{}'.format(p.spent, p.hp, p.mana, b.hp, effects))

def round(p, b, effects):
    # player's turn
    effects = apply_effects(p, b, effects);
    #echo('Effects', effects, p, b)
    check_end(p, b)

    # player casts a spells
    p.cast_spell(b, effects)
    #echo('Player\'s turn', effects, p, b)
    check_end(p, b)

    # boss's turn
    effects = apply_effects(p, b, effects);
    #echo('Effects', effects, p, b)
    check_end(p, b)

    # boss attacks
    p.receive_damage(b.damage, effects)
    #echo('Bosses\'s turn', effects, p, b)
    echo('', effects, p, b)
    check_end(p, b)

    dbg('-' * 60)


roundnb = 0
p = Player()
b = Boss()
effects = {}
while True:
    try:
        p = Player()
        b = Boss()
        effects = {}
        while True:
            round(p, b, effects)
            roundnb += 1
    except Defeat:
        dbg('Defeat {} ({})'.format(roundnb, p.spent))
        if roundnb % 100000 == 0:
            print('Defeat {} ({})'.format(roundnb, p.spent))
        dbg('#' * 60)
    except TooExpensive:
        if roundnb % 100000 == 0:
            print('TooExpensive {}'.format(roundnb))
        dbg('#' * 60)
    except Ignore:
        print('Ignore {}'.format(roundnb))
        dbg('#' * 60)
    except Victory:
        print('Victory {} ({})'.format(p.spent, roundnb))
        if p.spent < m:
            print('>>>>>>>> {}'.format(p.spent))
            m = p.spent
        dbg('#' * 60)
        exit()

# Found answer? 558? => Too low!

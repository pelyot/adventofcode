def describe(v):
    lv = len(v)
    res = []
    i = 0
    while i < lv:
        y = x = v[i]
        count = 0
        while y == x:
            count += 1
            i += 1
            if i < lv:
                y = v[i]
            else:
                break
        res.append(count)
        res.append(x)
    #print 'describe',v,' is ',res
    return res

result = [int(i) for i in '1113222113']
for i in range(50):
    result = describe(result)
#    print ''.join(str(t) for t in result)
    
print len(''.join(str(t) for t in result))

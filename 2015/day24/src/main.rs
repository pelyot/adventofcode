use std::io::BufRead;
use std::collections::BTreeSet;
extern crate itertools;
use itertools::Itertools;

fn difference(lhs: &[usize], rhs: &[usize]) -> Vec<usize> {
    let left: BTreeSet<usize> = lhs.iter().cloned().collect();
    let right: BTreeSet<usize> = rhs.iter().cloned().collect();
    left.difference(&right).cloned().collect()
}

fn has_2_bags(bag_size: usize, values: &[usize]) -> bool {
    (1..(values.len()+1)).flat_map(|n| values.iter().combinations_n(n)).find(|bag| bag_size == bag.iter().fold(0, |acc, &x| acc + x)).is_some()
}

fn has_3_bags(bag_size: usize, values: &[usize]) -> bool {
    (1..(values.len()+1)).flat_map(|n| values.iter().cloned().combinations_n(n))
        .filter(|bag| bag_size == bag.iter().fold(0, |acc, &x| acc + x))
        .find(|bag| has_2_bags(bag_size, &difference(&values, bag))).is_some()
}

fn main() {
    let f = std::fs::File::open("input").unwrap();
    let r = std::io::BufReader::new(f);
    let values : Vec<_> = r.lines().map(|l| l.unwrap().parse::<usize>().unwrap()).collect();
    let total = values.iter().fold(0, |acc, x| acc + x);
    let bag_size = total / 4;
    for n in 1..(values.len()+1) {
        println!("{}", n);
        if let Some(quantum_entanglement) = values.iter().cloned().combinations_n(n)
            .filter(|bag| bag_size == bag.iter().fold(0, |acc, &x| acc + x))
            .filter(|bag| has_3_bags(bag_size, &difference(&values, bag)))
            // .inspect(|bag| println!("{:?}", bag))
            .map(|bag| bag.iter().fold(1usize, |acc, x| acc * x))
            .min() {
                println!("=> {:?}", quantum_entanglement);
                break
        }
    }
}

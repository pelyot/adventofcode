struct CircularBuffer {
    data: Vec<usize>,
    curr: usize
}

impl CircularBuffer {
    fn clockwise(&mut self, delta: usize) {
        if self.data.len() == 0 {
            return;
        }
        self.curr += delta;
        self.curr %= self.data.len();
    }
    fn counterclockwise(&mut self, delta: usize) {
        if self.data.len() == 0 {
            return;
        }
        let mut curr_neg = self.curr as i32 - delta as i32;
        while curr_neg <= 0 {
            curr_neg += self.data.len() as i32;
        }
        self.curr = curr_neg as usize;
    }
    fn insert(&mut self, marble: usize) {
        self.curr += 1;
        self.data.insert(self.curr, marble);
    }
    fn pop_current(&mut self) -> usize {
        self.data.remove(self.curr)
    }
}

fn main() {
    const NB_PLAYERS: usize = 405;
    const NB_MARBLES: usize = 70953;
    let mut scores = vec![0usize; NB_PLAYERS];
    let mut buf = CircularBuffer{data: vec![0], curr: 1};
    for i in 1..=NB_MARBLES {
        if i % 23 == 0 {
            scores[i % NB_PLAYERS] += i;
            buf.counterclockwise(7);
            scores[i % NB_PLAYERS] += buf.pop_current();
        } else {
            buf.clockwise(1);
            buf.insert(i);
        }
    }
    println!("Highest scores: {:?}", scores.iter().max().unwrap());
}

fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").expect("failed to open file");
    let reader = std::io::BufReader::new(input);
    let words = reader.lines().map(|l| l.expect("i/o failure").into_bytes()).collect::<Vec<_>>();
    for i in &words {
        for j in &words {
            let diff = i.iter().zip(j.iter()).filter(|(a, b)| a != b).count();
            if diff == 1 {
                println!("{}", i.iter().zip(j.iter()).filter(|(a, b)| a == b).map(|(a, _b)| *a as char).collect::<String>());
                return
            }
        }
    }
}

#[derive(Copy,Clone,Debug)]
struct Task {
    id: usize,
    duration: usize
}

#[derive(Copy,Clone)]
enum Status {
    Pending,Started,Done
}

impl Status {
    fn is_done(&self) -> bool {
        match *self {
            Status::Done => true,
            _ => false
        }
    }
}

fn is_met(tasks: &[Status], deps: &[bool]) -> bool {
    for (&s, &d) in tasks.iter().zip(deps) {
        if d && !s.is_done() {
            return false
        }
    }
    true
}

fn main() {
    const NB_TASKS: usize = 26;
    const NB_WORKERS: usize = 5;
    const BASE_DURATION: usize = 60;
    use std::io::BufRead;
    let input = std::fs::File::open("input").expect("couldn't read file");
    let reader = std::io::BufReader::new(input);
    let mut dependencies = [[false; NB_TASKS]; NB_TASKS];
    for line in reader.lines().map(|l| l.unwrap().into_bytes()) {
        let a = (line[5] - 'A' as u8) as usize;
        let b = (line[36] - 'A' as u8) as usize;
        dependencies[b][a] = true; 
    }
    let mut tasks = [Status::Pending; NB_TASKS];
    let mut done_count = 0;
    let mut workers: [Option<Task>; NB_WORKERS] = [None; NB_WORKERS];
    let mut duration: usize = 0;
    while done_count != NB_TASKS {
        // assign tasks to workers
        for w in &mut workers {
            if w.is_none() {
                // find task
                for id in 0..tasks.len() {
                    match tasks[id] {
                        Status::Pending if is_met(&tasks, &dependencies[id]) => {
                            *w = Some(Task{id: id, duration: BASE_DURATION + id + 1});
                            tasks[id] = Status::Started;
                            break;
                        }
                        _ =>()
                    }
                }
            }
        }
        // advance time
        let dt = workers.iter().filter_map(|w| w.map(|t| t.duration)).min().expect("No running tasks!!!!");
        duration += dt;
        for w in &mut workers {
            if let Some(ref mut t) = *w {
                t.duration -= dt;
                if t.duration == 0 {
                    tasks[t.id] = Status::Done;
                    *w = None;
                    done_count += 1;
                }
            }
        }
    }
    println!("Total duration: {}", duration);
}

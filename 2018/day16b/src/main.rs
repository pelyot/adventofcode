type Value = i64;
type Register = i64;
type Registers = [Register; 4];
type Instruction = (usize, Value, Value, Value);
type Operation = fn(&mut Registers, i64, i64, i64);

mod input;
use input::*;

const INSTRUCTION_MAPPING: [usize; 16] = [7, 6, 9, 2, 8, 0, 10, 13, 11, 5, 3, 12, 4, 14, 1, 15];

const OPERATIONS: [Operation; 16] = [
    addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr,
];

fn main() {
    let mut registers = [0, 0, 0, 0];
    for (opcode, a, b, c) in PROGRAM.iter() {
        OPERATIONS[INSTRUCTION_MAPPING[*opcode]](&mut registers, *a, *b, *c);
    }
    println!("Result (Part Two) {}", registers[0]);
}

fn addr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = registers[a as usize] + registers[b as usize];
}

fn addi(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = registers[a as usize] + b;
}

fn mulr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = registers[a as usize] * registers[b as usize];
}

fn muli(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = registers[a as usize] * b;
}

fn banr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = registers[a as usize] & registers[b as usize];
}

fn bani(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = registers[a as usize] & b;
}

fn borr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = registers[a as usize] | registers[b as usize];
}

fn bori(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = registers[a as usize] | b;
}

fn setr(registers: &mut Registers, a: Register, _: Value, c: Register) {
    registers[c as usize] = registers[a as usize];
}

fn seti(registers: &mut Registers, a: Value, _: Value, c: Register) {
    registers[c as usize] = a;
}

fn gtir(registers: &mut Registers, a: Value, b: Register, c: Register) {
    registers[c as usize] = if a > registers[b as usize] { 1 } else { 0 };
}

fn gtri(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = if registers[a as usize] > b { 1 } else { 0 };
}

fn gtrr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = if registers[a as usize] > registers[b as usize] {
        1
    } else {
        0
    };
}

fn eqir(registers: &mut Registers, a: Value, b: Register, c: Register) {
    registers[c as usize] = if a == registers[b as usize] { 1 } else { 0 };
}

fn eqri(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = if registers[a as usize] == b { 1 } else { 0 };
}

fn eqrr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = if registers[a as usize] == registers[b as usize] {
        1
    } else {
        0
    };
}

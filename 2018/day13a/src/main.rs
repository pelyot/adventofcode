#[derive(Debug,Clone,PartialOrd,Ord,Eq,PartialEq)]
struct Cart {
    row: usize,
    col: usize,
    dir: i8,
    intersection_count: u32,
    enabled: bool
}

impl Cart {
    fn turn(&mut self, way: i8) {
        self.dir += way;
        while self.dir < 0 { self.dir += 4 }
        while self.dir > 3 { self.dir -= 4 }
    }
    fn char_to_dir(c: u8) -> i8 {
        match c as char {
           '^' => 0,
           '>' => 1,
           'v' => 2,
           '<' => 3,
           _ => unreachable!()
        }
    }
    fn dir_to_char(dir: i8) -> char {
        ['^', '>', 'v', '<'][dir as usize]
    }
    fn rotate(&mut self, cell: u8) {
        const LEFT: i8 = -1;
        const STRAIGHT: i8 = 0;
        const RIGHT: i8 = 1;
        const TURN_TABLE: [i8; 2] = [RIGHT, LEFT];
        const INTERSECTION_TURNS: [i8; 3] = [LEFT, STRAIGHT, RIGHT];
        match cell as char {
            '/' => self.turn(TURN_TABLE[self.dir as usize % 2]),
            '\\' => self.turn(TURN_TABLE[(self.dir as usize + 1) % 2]),
            '+' => { self.turn(INTERSECTION_TURNS[self.intersection_count as usize % 3]); self.intersection_count += 1; }
            _ => ()
        }
    }
    fn new(row: usize, col: usize, dir_char: u8) -> Cart {
        Cart {
            row: row,
            col: col,
            dir: Cart::char_to_dir(dir_char),
            intersection_count: 0,
            enabled: true
        }
    }
    fn advance(&mut self) {
        match self.dir {
            0 => self.row -= 1,
            1 => self.col += 1,
            2 => self.row += 1,
            3 => self.col -= 1,
            _ => unreachable!()
        }
    }
}
fn print(circuit: &Vec<Vec<u8>>, carts: &[Cart]) {
    for row in 0..circuit.len() {
        for col in 0..circuit[0].len() {
            if let Some(cart) = carts.iter().find(|c| c.row == row && c.col == col) {
                print!("{}", Cart::dir_to_char(cart.dir))
            } else {
                print!("{}", circuit[row][col] as char);
            }
        }
        println!("");
    }
}
fn main() {
    use std::io::BufRead;
    let input_file = std::fs::File::open("input").expect("");
    // read all input file into addressable array buffer
    let input_buff = std::io::BufReader::new(input_file).lines().map(|l| l.unwrap().as_bytes().to_vec()).collect::<Vec<_>>();
    // Read circuit from input
    let circuit = input_buff.iter().map(|l| l.iter().map(|&c| match c as char { '^'|'v' => '|' as u8, '<'|'>' => '-' as u8, _ => c }).collect::<Vec<_>>()).collect::<Vec<_>>();
    // Get list of carts from input
    let mut carts = Vec::new();
    for r in 0..input_buff.len() {
        for c in 0..input_buff[0].len() {
            let dir_char = input_buff[r][c];
            if ['^','v','>','<'].contains(&(dir_char as char)) {
                carts.push(Cart::new(r, c, dir_char));
            }
        }
    }
    // play simulation
    while carts.iter().filter(|e| e.enabled).count() > 1 {
        carts.sort();
        for i in 0..carts.len() {
            if !carts[i].enabled {
                continue;
            }
            carts[i].advance();
            let row = carts[i].row;
            let col = carts[i].col;
            let collision = carts.iter().enumerate().find(|&(j,c)| c.enabled && c.row == row && c.col == col && i != j).map(|(j,_)| j);
            if let Some(idx) = collision {
                println!("Collision at: {},{}", col, row);
                carts[i].enabled = false;
                carts[idx].enabled = false;
            }
            carts[i].rotate(circuit[row][col]);
        }
    }
    let survivor = carts.iter().find(|e| e.enabled).unwrap();
    println!("Survivor is at {},{}", survivor.col, survivor.row);
}

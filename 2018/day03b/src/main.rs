extern crate regex;
#[macro_use] extern crate lazy_static;

use regex::Regex;
use std::str::FromStr;

#[derive(Debug)]
struct Claim {
    id: usize,
    top: usize,
    left: usize,
    width: usize,
    height: usize
}

#[derive(Debug)]
struct ParseClaimError {}

impl FromStr for Claim {
    type Err = ParseClaimError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            // #123 @ 3,2: 5x4
            static ref RE: Regex = Regex::new(r"#(?P<id>\d+) @ (?P<left>\d+),(?P<top>\d+): (?P<width>\d+)x(?P<height>\d+)").expect("Failed to compile regex");
        }
        let caps = RE.captures(s).expect("failed to parse Claim with regex");
        let claim = Claim{
            id: caps["id"].parse::<usize>().unwrap(),
            top: caps["top"].parse::<usize>().unwrap(),
            left: caps["left"].parse::<usize>().unwrap(),
            width: caps["width"].parse::<usize>().unwrap(),
            height: caps["height"].parse::<usize>().unwrap()
            };
        Ok(claim)
    }
}

    
fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").expect("Failed to open input file");
    let reader = std::io::BufReader::new(input);
    let mut map = vec![0usize; 1000 * 1000];
    let claims = reader.lines().map(|l| l.unwrap().parse::<Claim>().unwrap()).collect::<Vec<_>>();
    for claim in &claims {
        for i in claim.top..claim.top+claim.height {
            for j in claim.left..claim.left+claim.width {
                map[1000*i + j] += 1;
            }
        }      
    }
    'outerloop: for claim in claims {
        for i in claim.top..claim.top+claim.height {
            for j in claim.left..claim.left+claim.width {
                if map[1000*i + j] > 1 {
                    continue 'outerloop;
                }
            }
        }
        println!("Result: {}", claim.id); 
        break 'outerloop;
    }
}

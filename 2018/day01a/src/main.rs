fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").expect("Failed to open input file");
    let reader = std::io::BufReader::new(input);
    let result = reader.lines().map(|l| l.unwrap().parse::<i32>().expect("Failed to parse integer")).sum::<i32>();
    println!("{:?}", result);
}

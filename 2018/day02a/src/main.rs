fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").expect("failed to open file");
    let reader = std::io::BufReader::new(input);
    let mut doubles = 0;
    let mut triples = 0;
    for letters in reader.lines().map(|l| l.expect("i/o failure").into_bytes()) {
        let mut counts = std::collections::HashMap::new();
        for b in letters {
            counts.entry(b).and_modify(|c| *c += 1).or_insert(1);
        }
        if counts.values().any(|e| *e == 2) {
            doubles += 1;
        }
        if counts.values().any(|e| *e == 3) {
            triples += 1;
        }
    } 
    println!("{:?}", doubles * triples);
}

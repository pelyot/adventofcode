fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").expect("Failed to open file");
    let reader = std::io::BufReader::new(input);
    let freq_vec = reader.lines().map(|l| l.unwrap().parse::<i32>().expect("Failed to parse i32")).collect::<Vec<i32>>();
    let mut frequencies = freq_vec.iter().cycle();
    let mut freq = 0;
    let mut found = std::collections::HashSet::new();
    while !found.contains(&freq) {
        found.insert(freq);
        freq += frequencies.next().unwrap();
    }
    println!("{}", freq);
}

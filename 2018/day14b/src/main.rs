const INPUT: &'static[usize] = &[2, 8, 6, 0, 5, 1];

struct State {
    recipes: Vec<usize>,
    elf1: usize,
    elf2: usize
}

impl State {
    fn new() -> State {
        State { recipes: vec![3, 7], elf1: 0, elf2: 1}
    }
    fn step(&mut self) {
        // Add recipes
        let sum = self.recipes[self.elf1] + self.recipes[self.elf2];
        if sum >= 10 {
            self.recipes.push(sum / 10);
        }
        self.recipes.push(sum % 10);
        // Move elfs
        self.elf1 += self.recipes[self.elf1] + 1;
        self.elf1 %= self.recipes.len();
        self.elf2 += self.recipes[self.elf2] + 1;
        self.elf2 %= self.recipes.len();
    }
}

fn main() {
    let mut state = State::new();
    while state.recipes.len() < INPUT.len() {
        state.step();
    }
    let mut len = state.recipes.len();
    loop {
        while len <= state.recipes.len() {
            let slice = &state.recipes[(len - INPUT.len())..len];
            if slice == INPUT {
                println!("{}", len - INPUT.len());
                return;
            }
            len += 1;
        }
        state.step();
    }
}

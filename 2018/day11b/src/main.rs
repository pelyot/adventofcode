const GRID_SERIAL: usize = 7857;
fn power_level(x: usize, y: usize) -> i32 {
    let rack_id = x + 10;
    let p = (rack_id * y + GRID_SERIAL) * rack_id;
    ((p % 1000 - p % 100) / 100) as i32 - 5
}
fn square(grid: &Vec<Vec<i32>>, x: usize, y: usize, size: usize) -> i32 {
    let mut p = 0i32;
    for i in x..x+size {
        for j in y..y+size {
            p += grid[i][j];
        }
    }
    p
}
fn row(grid: &Vec<Vec<i32>>, x: usize, y: usize, size: usize) -> i32 {
    (x..x+size).map(|i| grid[y][i]).sum()
}
fn col(grid: &Vec<Vec<i32>>, x: usize, y: usize, size: usize) -> i32 {
    (y..y+size).map(|j| grid[j][x]).sum()
}
fn max_square(grid: &Vec<Vec<i32>>, size: usize) -> (usize, usize, usize, i32) {
    let nb_squares = 300-size+1;
    let mut memo = vec![vec![i32::min_value();nb_squares];nb_squares];
    memo[0][0] = square(grid, 0, 0, size);
    for x in 1..nb_squares {
        memo[0][x] = memo[0][x-1];
        memo[0][x] += col(&grid, x+size-1, 0, size);
        memo[0][x] -= col(&grid, x-1, 0, size);
    }
    for y in 1..nb_squares {
        for x in 0..nb_squares {
            memo[y][x] = memo[y-1][x] - row(&grid, x, y-1, size) + row(&grid, x, y+size-1, size);
        }
    }
    // find the max:
    let mut max = i32::min_value();
    let mut max_x = 300;
    let mut max_y = 300;
    for y in 0..nb_squares {
        for x in 0..nb_squares {
            let p = memo[y][x];
            if p > max {
                max = p;
                max_x = x;
                max_y = y;
            }
        }
    }
    (max_x, max_y, size, max)
}
fn main() {
    let grid = (0..300).map(|y| (0..300).map(|x| power_level(x, y)).collect::<Vec<i32>>()).collect::<Vec<Vec<i32>>>();
    max_square(&grid, 1);
    let m = (1..=300).map(|size| max_square(&grid, size)).max_by_key(|e| e.3).unwrap();
    println!("{},{},{}", m.0, m.1, m.2);
}

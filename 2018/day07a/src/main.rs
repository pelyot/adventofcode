fn is_met(solved: &[bool], deps: &[bool]) -> bool {
    for (&s, &d) in solved.iter().zip(deps) {
        if d && !s {
            return false
        }
    }
    true
}

fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").expect("couldn't read file");
    let reader = std::io::BufReader::new(input);
    let mut dependencies = [[false; 26]; 26];
    for line in reader.lines().map(|l| l.unwrap().into_bytes()) {
        let a = (line[5] - 'A' as u8) as usize;
        let b = (line[36] - 'A' as u8) as usize;
        dependencies[b][a] = true; 
    }
    let mut solved = [false; 26];
    let mut solved_count = 0;
    while solved_count != 26 {
        for x in 0..=26 {
            if !solved[x] && is_met(&solved, &dependencies[x]) {
                solved[x] = true;
                solved_count += 1;
                print!("{}", (x as u8 + 'A' as u8) as char);
                break;
            }
        }
    }
    println!("");
}

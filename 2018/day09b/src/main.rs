#[derive(Clone,Copy,Debug)]
struct Node {
    prev: usize,
    data: usize,
    next: usize
}

#[derive(Debug)]
struct CircularBuffer {
    data: Vec<Node>,
    curr: usize
}

impl CircularBuffer {
    fn new() -> CircularBuffer {
        let mut buf = CircularBuffer{ data: vec![], curr: 0 };
        buf.data.push(Node{ prev: 0, data: 0, next: 0 });
        buf
    }
    fn clockwise(&mut self, delta: usize) {
        for _ in 0..delta {
            self.curr = self.data[self.curr].next;
        }
    }
    fn counterclockwise(&mut self, delta: usize) {
        for _ in 0..delta {
            self.curr = self.data[self.curr].prev;
        }
    }
    fn insert(&mut self, marble: usize) {
        self.data.push(Node { prev: self.curr, data: marble, next: self.data[self.curr].next });
        let c = self.data[self.curr];
        self.data[c.next].prev = self.data.len() - 1;
        self.data[self.curr].next = self.data.len() - 1;
        self.curr = self.data.len() - 1;
    }
    fn pop_current(&mut self) -> usize {
        let node = self.data[self.curr].clone();
        self.data[node.prev].next = node.next;
        self.data[node.next].prev = node.prev;
        self.data[self.curr].prev = 0;
        self.data[self.curr].next = 0;
        self.curr = node.next;
        node.data
    }
}

fn main() {
    const NB_PLAYERS: usize = 405;
    const NB_MARBLES: usize = 70953 * 100;
    let mut scores = vec![0usize; NB_PLAYERS];
    let mut buf = CircularBuffer::new();
    for i in 1..=NB_MARBLES {
        if i % 23 == 0 {
            scores[i % NB_PLAYERS] += i;
            buf.counterclockwise(7);
            scores[i % NB_PLAYERS] += buf.pop_current();
        } else {
            buf.clockwise(1);
            buf.insert(i);
        }
    }
    println!("Highest scores: {:?}", scores.iter().max().unwrap());
}

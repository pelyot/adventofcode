struct Point(i32,i32,i32,i32);
impl Point {
    fn new(mut l: String) -> Point {
        l.retain(|c| "0123456789- ".chars().find(|&e| e == c).is_some());
        let mut w = l.split_whitespace();
        let px = w.next().unwrap().trim().parse().unwrap();
        let py = w.next().unwrap().trim().parse().unwrap();
        let vx = w.next().unwrap().trim().parse().unwrap();
        let vy = w.next().unwrap().trim().parse().unwrap();
        Point(px, py,vx, vy)
    }
    fn advance(&mut self, step: i32) {
        self.0 += self.2 * step;
        self.1 += self.3 * step;
    }
}
fn render(points: &[Point]) -> Vec<Vec<bool>> {
    let minx = points.iter().map(|p| p.0).min().unwrap();
    let maxx = points.iter().map(|p| p.0).max().unwrap();
    let miny = points.iter().map(|p| p.1).min().unwrap();
    let maxy = points.iter().map(|p| p.1).max().unwrap();
    let size_x = (maxx - minx + 1) as usize;
    let size_y = (maxy - miny + 1) as usize;
    let mut buf = vec![vec![false; size_x]; size_y];
    for p in points {
        buf[(p.1 - miny) as usize][(p.0 - minx) as usize] = true;
    }
    buf
}
fn area(points: &[Point]) -> usize {
    let minx = points.iter().map(|p| p.0).min().unwrap();
    let maxx = points.iter().map(|p| p.0).max().unwrap();
    let miny = points.iter().map(|p| p.1).min().unwrap();
    let maxy = points.iter().map(|p| p.1).max().unwrap();
    let size_x = (maxx - minx + 1) as usize;
    let size_y = (maxy - miny + 1) as usize;
    size_x * size_y
}
fn print(buf: Vec<Vec<bool>>) {
    for line in buf {
        for p in line {
            print!("{}", if p { '#' } else { '.' });
        }
        println!("");
    }
    println!("");
}
fn advance(points: &mut [Point], step: i32) {
    for p in points {
        p.advance(step)
    }
}
fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").unwrap();
    let reader = std::io::BufReader::new(input);
    let mut points = reader.lines().map(|l| Point::new(l.unwrap())).collect::<Vec<_>>();
    // Run these lines with different parameters to find the iteration where the pattern is smallest: for my input it is 10053
    //let min = (0..12000).map(|i| { advance(&mut points, 1); (i.clone(),area(&points))}).min_by_key(|&(_,a)| a).unwrap();
    //println!("{:?}", min); // 10053
    // Use the next lines to calculate the iteration found with the code above
    advance(&mut points, 10053+1);
    let buf = render(&points);
    print(buf);
}

fn meta_sum(numbers: &[usize], idx: &mut usize) -> usize {
    let nb_nodes = numbers[*idx];
    *idx += 1;
    let nb_meta = numbers[*idx];
    *idx += 1;
    let mut sum = 0;
    for _ in 0..nb_nodes {
        sum += meta_sum(numbers, idx);
    }
    for m in &numbers[*idx..*idx+nb_meta] {
        *idx += 1;
        sum += m;
    }
    sum
}

fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").unwrap();
    let reader = std::io::BufReader::new(input);
    let numbers = reader.lines().next().unwrap().unwrap().split_whitespace().map(|s| s.parse::<usize>().unwrap()).collect::<Vec<_>>();
    
    let mut idx = 0;
    println!("{}", meta_sum(&numbers, &mut idx));
}

fn print_poly(polymer: &[u8]) {
    println!("{:?} {}", String::from_utf8(polymer.to_vec()).unwrap(), polymer.len());
}

fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").unwrap();
    let mut reader = std::io::BufReader::new(input);
    let mut buf = String::new();
    reader.read_line(&mut buf).unwrap();
    let mut polymer = buf.trim_end().to_string().into_bytes();
    let diff = 'a' as u8 - 'A' as u8;
    let mut previous_size = 0;
    while previous_size != polymer.len() {
        previous_size = polymer.len();
        let mut i = 0;
        while i < polymer.len() - 1 {
            let a = polymer[i] as u8;
            let b = polymer[i+1] as u8;
            if a + diff == b || b + diff == a {
                polymer.remove(i);
                polymer.remove(i);
            } else {
                i += 1;
            }
        }
    }
    print_poly(&polymer);
}

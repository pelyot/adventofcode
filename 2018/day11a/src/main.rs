const GRID_SERIAL: usize = 7857;
fn power_level(x: usize, y: usize) -> i32 {
    let rack_id = x + 10;
    let p = (rack_id * y + GRID_SERIAL) * rack_id;
    ((p % 1000 - p % 100) / 100) as i32 - 5
}
fn main() {
    let mut grid = (0..300).map(|y| (0..300).map(|x| power_level(x, y)).collect::<Vec<i32>>()).collect::<Vec<Vec<i32>>>();
    let mut square_power = vec![vec![0i32; 298]; 298];
    let mut max = i32::min_value();
    let mut max_x = 300;
    let mut max_y = 300;
    for y in 0..298 {
        for x in 0..298 {
            let p =  grid[y][x]   + grid[y+1][x]   + grid[y+2][x]
                  +  grid[y][x+1] + grid[y+1][x+1] + grid[y+2][x+1]
                  +  grid[y][x+2] + grid[y+1][x+2] + grid[y+2][x+2];
            if p > max {
                max = p;
                max_x = x;
                max_y = y;
            }
        }
    }
    println!("{},{}", max_x, max_y);
}

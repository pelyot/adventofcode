const INPUT: usize = 286051;

struct State {
    recipes: Vec<usize>,
    elf1: usize,
    elf2: usize
}

impl State {
    fn new() -> State {
        let mut s = State { recipes: vec![3, 7], elf1: 0, elf2: 1};
        s.recipes.reserve(INPUT + 10);
        s
    }
    fn step(&mut self) {
        // Add recipes
        let sum = self.recipes[self.elf1] + self.recipes[self.elf2];
        if sum >= 10 {
            self.recipes.push(sum / 10);
        }
        self.recipes.push(sum % 10);
        // Move elfs
        self.elf1 += self.recipes[self.elf1] + 1;
        self.elf1 %= self.recipes.len();
        self.elf2 += self.recipes[self.elf2] + 1;
        self.elf2 %= self.recipes.len();
    }
}

fn main() {
    let mut state = State::new();
    while state.recipes.len() < INPUT + 10 {
        state.step();
    }
    for i in &state.recipes[INPUT..INPUT+10] {
        print!("{}", i.to_string());
    }
}

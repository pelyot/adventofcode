mod grid;
mod position;
mod race;
mod simulation;
mod unit;
use simulation::*;

fn main() {
    let mut elf_attack = 3;
    loop {
        let mut sim = Simulation {
            grid: Grid::from_input("input"),
        };
        let original_elf_count = sim.grid.count_units(Race::Elf);
        sim.set_elfes_attack(elf_attack);
        sim.start();
        println!("{}", elf_attack);
        if sim.grid.count_units(Race::Elf) == original_elf_count {
            break
        }
        elf_attack += 1
    }
}

pub use super::unit::*;

use std::io::BufRead;

use std::cell::RefCell;
use std::rc::Rc;

pub type UnitRef = Rc<RefCell<Unit>>;

#[derive(Debug)]
pub enum Occupant {
    Wall,
    Unit(UnitRef),
}

impl Occupant {
    pub fn new_unit(id: usize, race: Race, row: usize, col: usize) -> Occupant {
        Occupant::Unit(Rc::new(RefCell::new(Unit::new(id, race, row, col))))
    }
}

impl PartialEq for Occupant {
    fn eq(&self, other: &Occupant) -> bool {
        match self {
            Occupant::Wall => match other {
                Occupant::Wall => true,
                Occupant::Unit(_) => false,
            },
            Occupant::Unit(ref self_u) => match other {
                Occupant::Wall => false,
                Occupant::Unit(ref other_u) => self_u.borrow().eq(&other_u.borrow()),
            },
        }
    }
}

#[derive(PartialEq, Debug)]
pub struct Square(Option<Occupant>);

impl Square {
    pub fn unit(&self) -> Option<UnitRef> {
        if let Some(Occupant::Unit(ref unit_ref)) = self.0 {
            Some(unit_ref.clone())
        } else {
            None
        }
    }
}

pub struct Grid(Vec<Vec<Square>>);

impl Grid {
    pub fn at(&self, pos: Position) -> &Square {
        &self.0[pos.row][pos.col]
    }

    pub fn nb_rows(&self) -> usize {
        self.0.len()
    }

    pub fn nb_cols(&self) -> usize {
        self.0[0].len()
    }

    pub fn remove_at(&mut self, pos: Position) {
        self.0[pos.row][pos.col] = Square(None);
    }

    pub fn is_empty(&self, pos: Position) -> bool {
        match self.at(pos) {
            Square(None) => true,
            Square(Some(Occupant::Wall)) => false,
            Square(Some(Occupant::Unit(ref unit_ref))) => !unit_ref.borrow().alive,
        }
    }

    pub fn move_unit(&mut self, source: Position, dest: Position) {
        let tmp = self.at(source).unit().expect("No creature to move!");
        self.remove_at(source);
        self.0[dest.row][dest.col] = Square(Some(Occupant::Unit(tmp)));
    }

    pub fn units(&self) -> impl Iterator<Item = UnitRef> + '_ {
        self.0
            .iter()
            .flat_map(|row| row.iter())
            .filter_map(|square| square.unit().clone())
    }

    pub fn count_units(&self, race: Race) -> usize {
        self.units()
            .filter(|unit_ref| {
                let unit = unit_ref.borrow();
                unit.race == race && unit.alive
            })
            .count()
    }

    pub fn from_input(filename: &str) -> Grid {
        use std::io::BufRead;
        let input = std::fs::File::open(filename).expect(filename);
        let reader = std::io::BufReader::new(input);
        Grid::from_reader(reader)
    }

    pub fn from_str(data: &str) -> Grid {
        use std::io::Cursor;
        let reader = std::io::BufReader::new(Cursor::new(data.as_bytes()));
        Grid::from_reader(reader)
    }

    fn from_reader(reader: impl BufRead) -> Grid {
        let mut id_gen = 0;
        Grid(
            reader
                .lines()
                .enumerate()
                .map(|(row, l)| {
                    l.unwrap()
                        .chars()
                        .enumerate()
                        .map(|(col, c)| match c {
                            '#' => Some(Occupant::Wall),
                            '.' => None,
                            'G' => {
                                id_gen += 1;
                                Some(Occupant::new_unit(id_gen, Race::Goblin, row, col))
                            }
                            'E' => {
                                id_gen += 1;
                                Some(Occupant::new_unit(id_gen, Race::Elf, row, col))
                            }
                            _ => unreachable!(),
                        })
                        .map(|s| Square(s))
                        .collect()
                })
                .collect(),
        )
    }

    pub fn print_single(&self) {
        for r in &self.0 {
            for s in r {
                match s {
                    Square(Some(Occupant::Wall)) => print!("#"),
                    Square(Some(Occupant::Unit(ref cref))) => match cref.borrow().race {
                        Race::Elf => print!("E"),
                        Race::Goblin => print!("G"),
                    },
                    Square(None) => print!("."),
                    _ => (),
                }
            }
            println!("");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn count_units() {
        let g = Grid::from_input("debuginput");
        assert_eq!(g.count_units(Race::Goblin), 8);
        assert_eq!(g.count_units(Race::Elf), 1);
    }

    #[test]
    fn is_empty() {
        let g = Grid::from_str("#.EGE");
        g.at(Position::new(0, 4))
            .unit()
            .expect("Elf expected!")
            .borrow_mut()
            .alive = false;
        assert!(!g.is_empty(Position::new(0, 0)));
        assert!(g.is_empty(Position::new(0, 1)));
        assert!(!g.is_empty(Position::new(0, 2)));
        assert!(!g.is_empty(Position::new(0, 3)));
        assert!(g.is_empty(Position::new(0, 4)));
    }

    #[test]
    fn from_str() {
        let g = Grid::from_str("#.EG#\n.E.G#");
        assert_eq!(*g.at(Position::new(0, 0)), Square(Some(Occupant::Wall)));
        assert_eq!(*g.at(Position::new(0, 4)), Square(Some(Occupant::Wall)));
        assert_eq!(*g.at(Position::new(1, 4)), Square(Some(Occupant::Wall)));
        assert_eq!(*g.at(Position::new(0, 1)), Square(None));
        assert_eq!(*g.at(Position::new(1, 0)), Square(None));
        assert_eq!(*g.at(Position::new(1, 2)), Square(None));
        assert_eq!(
            *g.at(Position::new(0, 2)),
            Square(Some(Occupant::new_unit(1, Race::Elf, 0, 2)))
        );
        assert_eq!(
            *g.at(Position::new(0, 3)),
            Square(Some(Occupant::new_unit(2, Race::Goblin, 0, 3)))
        );
        assert_eq!(
            *g.at(Position::new(1, 1)),
            Square(Some(Occupant::new_unit(3, Race::Elf, 1, 1)))
        );
        assert_eq!(
            *g.at(Position::new(1, 3)),
            Square(Some(Occupant::new_unit(4, Race::Goblin, 1, 3)))
        );
    }

    #[test]
    fn units() {
        let level = "EG\nGE";
        let g = Grid::from_str(&level);
        let mut units = g.units();
        assert_eq!(Race::Elf, units.next().unwrap().borrow().race);
        assert_eq!(Race::Goblin, units.next().unwrap().borrow().race);
        assert_eq!(Race::Goblin, units.next().unwrap().borrow().race);
        assert_eq!(Race::Elf, units.next().unwrap().borrow().race);
        assert_eq!(None, units.next());
    }
}

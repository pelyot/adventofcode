pub use super::position::Position;
pub use super::race::Race;

#[derive(Debug, PartialEq)]
pub struct Unit {
    pub id: usize,
    pub hp: i32,
    pub race: Race,
    pub pos: Position,
    pub alive: bool,
    pub attack: usize

}

impl Unit {
    pub fn new(id: usize, race: Race, row: usize, col: usize) -> Unit {
        Unit {
            id: id,
            hp: 200,
            race: race,
            pos: Position { row: row, col: col },
            alive: true,
            attack: 3
        }
    }
}

type Value = i64;
type Register = i64;
type Registers = [Register; 4];
type Instruction = (Value, Value, Value, Value);
type Operation = fn(&mut Registers, i64, i64, i64);

mod input;
use input::INPUT;

fn main() {
    let result = INPUT
        .iter()
        .map(|(before, instruction, expected)| count_matches(before, expected, instruction))
        .filter(|&count| count >= 3)
        .count();
    println!("Result {}", result);
}

fn addr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = registers[a as usize] + registers[b as usize];
}

fn addi(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = registers[a as usize] + b;
}

fn mulr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = registers[a as usize] * registers[b as usize];
}

fn muli(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = registers[a as usize] * b;
}

fn banr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = registers[a as usize] & registers[b as usize];
}

fn bani(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = registers[a as usize] & b;
}

fn borr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = registers[a as usize] | registers[b as usize];
}

fn bori(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = registers[a as usize] | b;
}

fn setr(registers: &mut Registers, a: Register, _: Value, c: Register) {
    registers[c as usize] = registers[a as usize];
}

fn seti(registers: &mut Registers, a: Value, _: Value, c: Register) {
    registers[c as usize] = a;
}

fn gtir(registers: &mut Registers, a: Value, b: Register, c: Register) {
    registers[c as usize] = if a > registers[b as usize] { 1 } else { 0 };
}

fn gtri(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = if registers[a as usize] > b { 1 } else { 0 };
}

fn gtrr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = if registers[a as usize] > registers[b as usize] {
        1
    } else {
        0
    };
}

fn eqir(registers: &mut Registers, a: Value, b: Register, c: Register) {
    registers[c as usize] = if a == registers[b as usize] { 1 } else { 0 };
}

fn eqri(registers: &mut Registers, a: Register, b: Value, c: Register) {
    registers[c as usize] = if registers[a as usize] == b { 1 } else { 0 };
}

fn eqrr(registers: &mut Registers, a: Register, b: Register, c: Register) {
    registers[c as usize] = if registers[a as usize] == registers[b as usize] {
        1
    } else {
        0
    };
}

const OPERATIONS: [Operation; 16] = [
    addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr,
];

fn apply(registers: &Registers, operation: Operation, params: &Instruction) -> Registers {
    let mut result = registers.clone();
    operation(&mut result, params.1, params.2, params.3);
    result
}

fn count_matches(before: &Registers, expected: &Registers, instruction: &Instruction) -> usize {
    OPERATIONS
        .iter()
        .map(|&op| apply(before, op, instruction))
        .filter(|result| result == expected)
        .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn count_matches_test() {
        let before = &[3, 2, 1, 1];
        let instruction = &[9, 2, 1, 2];
        let expected = &[3, 2, 2, 1];
        assert_eq!(3, count_matches(before, expected, instruction));
    }

    #[test]
    fn basic() {
        let initial = &[3, 2, 1, 1];
        let params = &[9, 2, 1, 2];

        assert_eq!(apply(initial, mulr, params), [3, 2, 2, 1]);
        assert_eq!(apply(initial, addi, params), [3, 2, 2, 1]);
        assert_eq!(apply(initial, seti, params), [3, 2, 2, 1]);
    }
}

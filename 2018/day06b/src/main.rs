#[derive(Debug,Eq,PartialEq,PartialOrd,Clone,Hash)]
struct Point(i32, i32);

impl Point {
    fn distance_to(&self, x: i32, y: i32) -> i32 {
        (x - self.0).abs() + (y - self.1).abs()
    }
}
use std::str::FromStr;

#[derive(Debug)]
struct CustomParseError;

impl FromStr for Point {
    type Err = CustomParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut it = s.trim().split(", ");
        let xs = it.next().unwrap();
        let ys = it.next().unwrap();
        Ok(Point(xs.parse().unwrap(), ys.parse().unwrap()))
    }
}

fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").expect("Failed to open input file");
    let reader = std::io::BufReader::new(input);
    let points = reader.lines().map(|l| l.expect("Failed I/O").parse::<Point>().expect("Failed to parse Point")).collect::<Vec<_>>();
    let xmin = *points.iter().map(|Point(x, _)| x).min().unwrap();
    let xmax = *points.iter().map(|Point(x, _)| x).max().unwrap();
    let ymin = *points.iter().map(|Point(_, y)| y).min().unwrap();
    let ymax = *points.iter().map(|Point(_, y)| y).max().unwrap();
    let result: usize = (ymin..=ymax).map(|y| (xmin..=xmax)
            .filter(|&x| points.iter().map(|p| p.distance_to(x, y)).sum::<i32>() < 10_000)
            .count())
        .sum();

    println!("result: {:?}", result);
}

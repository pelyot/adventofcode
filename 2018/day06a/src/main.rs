#[derive(Debug,Eq,PartialEq,PartialOrd,Clone,Hash)]
struct Point(i32, i32);

impl Point {
    fn distance_to(&self, x: i32, y: i32) -> i32 {
        (x - self.0).abs() + (y - self.1).abs()
    }
}
use std::str::FromStr;

#[derive(Debug)]
struct CustomParseError;

impl FromStr for Point {
    type Err = CustomParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut it = s.trim().split(", ");
        let xs = it.next().unwrap();
        let ys = it.next().unwrap();
        Ok(Point(xs.parse().unwrap(), ys.parse().unwrap()))
    }
}

type Id = usize;

static NOBODY: Id = 0;

fn find_owner(x: i32, y: i32, points: &[Point]) -> usize {
    let id = points.iter().enumerate().min_by_key(|(_, p)| p.distance_to(x, y)).map(|(i,_)| i + 1).unwrap();
    let min_distance = points[id-1].distance_to(x, y);
    let count = points.iter().filter(|e| e.distance_to(x, y) == min_distance).count();
    if count == 1 {
        return id;
    } else {
        return NOBODY;
    }
}

fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").expect("Failed to open input file");
    let reader = std::io::BufReader::new(input);
    let points = reader.lines().map(|l| l.expect("Failed I/O").parse::<Point>().expect("Failed to parse Point")).collect::<Vec<_>>();
    let xmin = *points.iter().map(|Point(x, y)| x).min().unwrap();
    let xmax = *points.iter().map(|Point(x, y)| x).max().unwrap();
    let ymin = *points.iter().map(|Point(x, y)| y).min().unwrap();
    let ymax = *points.iter().map(|Point(x, y)| y).max().unwrap();
    let map = (ymin..=ymax).map(|y| (xmin..=xmax).map(|x| find_owner(x, y, &points)).collect::<Vec<usize>>()).collect::<Vec<_>>();
    // Candidates are not on the outer rim of the map
    let mut candidates = vec![true; points.len()+1];
    for x in xmin..=xmax {
        candidates[map[0][(x-xmin) as usize]] = false;
        candidates[map[(ymax-ymin) as usize][(x-xmin) as usize]] = false;
    }
    for y in ymin..=ymax {
        candidates[map[(y-ymin) as usize][0]] = false;
        candidates[map[(y-ymin) as usize][(xmax-xmin) as usize]] = false;
    }
    let result: usize = (1..candidates.len()).filter(|&id| candidates[id]).map(|id|
        map.iter().map(|line| line.iter().filter(|&&cell| cell == id).count()).sum()
        ).max().unwrap();

    println!("result: {:?}", result);
}

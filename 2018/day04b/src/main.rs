extern crate regex;
#[macro_use] extern crate lazy_static;

use regex::Regex;
use std::str::FromStr;

#[derive(Debug,Ord,Eq,PartialEq,PartialOrd)]
struct Timestamp {
    year: u32,
    month: u8,
    day: u8,
    hour: u8,
    minute: u8
}

#[derive(Debug)]
enum Action {
    Wake,
    Sleep,
    StartShift(u32)
}

#[derive(Debug)]
struct CustomParseError;

impl FromStr for Timestamp {
    type Err = CustomParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            // [1518-11-01 00:00]
            static ref RE: Regex = Regex::new(r"\[(?P<year>\d+)-(?P<month>\d+)-(?P<day>\d+) (?P<hour>\d+):(?P<minute>\d+)\]").expect("Failed to compile regex");
        }
        let caps = RE.captures(s).expect("failed to parse Timestamp with regex");
        let timestamp = Timestamp {
            year: caps["year"].parse::<u32>().unwrap(),
            month: caps["month"].parse::<u8>().unwrap(),
            day: caps["day"].parse::<u8>().unwrap(),
            hour: caps["hour"].parse::<u8>().unwrap(),
            minute: caps["minute"].parse::<u8>().unwrap()
            };
        Ok(timestamp)
    }
}

impl FromStr for Action {
    type Err = CustomParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with('G') {
            return Ok(Action::StartShift(s.split_whitespace().collect::<Vec<_>>()[1].trim_start_matches('#').parse::<u32>().unwrap()))
        } else if s.starts_with('f') {
            return Ok(Action::Sleep)
        } else if s.starts_with('w') {
            return Ok(Action::Wake)
        }
        Err(CustomParseError)
    }
}

#[derive(Debug)]
struct Event {
    timestamp: Timestamp,
    action: Action
}

impl FromStr for Event {
    type Err = CustomParseError;
    
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (timestamp_string, action_string) = s.split_at(19);
        Ok(Event{timestamp: timestamp_string.parse()?, action: action_string.parse()?})
    }
}

 
fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").expect("Failed to open input file");
    let reader = std::io::BufReader::new(input);
    let mut events = reader.lines().map(|l| l.unwrap().parse::<Event>().unwrap()).collect::<Vec<_>>();
    events.sort_by(|e1, e2| e1.timestamp.cmp(&e2.timestamp));
    let mut current_guard : u32 = 0;
    let mut sleep_start : u32 = 0;
    let mut sleep_counters = std::collections::HashMap::new();
    for e in &events {
        match e.action {
            Action::StartShift(guard) => current_guard = guard,
            Action::Sleep => sleep_start = e.timestamp.minute as u32,
            Action::Wake => for minute in sleep_start..e.timestamp.minute as u32 {
                sleep_counters.entry(current_guard).or_insert(vec![0;60])[minute as usize] += 1;
            }
        }
    }
    let (guard, minutes) = sleep_counters.iter().max_by_key(|&(_, minutes)| minutes.iter().max()).unwrap();
    let minute = (0..).zip(minutes).max_by_key(|&(_,m)| m).unwrap().0;
    println!("{:?}", guard * minute);
}

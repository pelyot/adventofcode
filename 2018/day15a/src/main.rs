mod grid;
mod position;
mod race;
mod simulation;
mod unit;
use simulation::*;

fn main() {
    let mut sim = Simulation {
        grid: Grid::from_input("input"),
    };
    sim.start();
}

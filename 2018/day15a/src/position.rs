use std::cmp::Ordering;

pub const DIRECTIONS : [(i32, i32); 4] = [(-1, 0), (0, -1), (0, 1), (1, 0)];

#[derive(PartialEq, Clone, Copy, Debug, Eq)]
pub struct Position {
    pub row: usize,
    pub col: usize
}

impl Position {
    pub fn tuple(&self) -> (usize, usize) {
        (self.row, self.col)
    }

    pub fn new(row: usize, col: usize) -> Position {
        Position { row: row, col: col }
    }
    pub fn moved(&self, delta: (i32, i32)) -> Position {
        Position {
            row: ((self.row as i32) + delta.0) as usize,
            col: ((self.col as i32) + delta.1) as usize
        }
    }
    pub fn neighbors(&self) -> impl Iterator<Item = Position> + '_ {
        let this = self.clone();
        DIRECTIONS.iter().map(move |delta| this.moved(*delta))
    }
    pub fn up(&self) -> Position {
        self.neighbors().collect::<Vec<_>>()[0]
    }
}

impl Ord for Position {
    fn cmp(&self, other: &Position) -> Ordering {
        other.row.cmp(&self.row).then_with(|| other.col.cmp(&self.col))
    }
}

impl PartialOrd for Position {
    fn partial_cmp(&self, other: &Position) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn neighbors() {
        let c = Position { row: 4, col: 4 };
        let neighbors = c.neighbors().collect::<Vec<_>>();
        assert_eq!(neighbors[0].tuple(), (3, 4));
        assert_eq!(neighbors[1].tuple(), (4, 3));
        assert_eq!(neighbors[2].tuple(), (4, 5));
        assert_eq!(neighbors[3].tuple(), (5, 4));
    }
}

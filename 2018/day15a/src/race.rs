
#[derive(PartialEq, Clone, Copy, Debug)]
pub enum Race {
    Goblin,
    Elf
}

impl Race {
    pub fn enemy(&self) -> Race {
        match self {
            Race::Goblin => Race::Elf,
            Race::Elf => Race::Goblin
        }
    }
    pub fn is_enemy(&self, other: Race) -> bool {
        *self != other
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_enemy() {
        assert!(Race::Elf.is_enemy(Race::Goblin));
        assert!(Race::Goblin.is_enemy(Race::Elf));
        assert!(!Race::Goblin.is_enemy(Race::Goblin));
        assert!(!Race::Elf.is_enemy(Race::Elf));
    }
}

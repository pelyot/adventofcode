pub use super::grid::*;
use super::position::DIRECTIONS;
use std::rc::Rc;

use std::collections::BinaryHeap;

pub struct Simulation {
    pub grid: Grid,
}

impl Simulation {
    pub fn enemy_with_lowest_hp(&self, unit: &Unit) -> Option<UnitRef> {
        let mut result_enemy = None;
        let mut lowest_hp = i32::max_value();
        for pos in unit.pos.neighbors() {
            if let Some(ref enemy_ref) = self.grid.at(pos).unit() {
                let enemy = enemy_ref.borrow();
                if enemy.alive && enemy.race.is_enemy(unit.race) && enemy.hp < lowest_hp {
                    result_enemy = Some(Rc::clone(enemy_ref));
                    lowest_hp = enemy.hp
                }
            }
        }
        result_enemy
    }

    pub fn attack(&mut self, attacker: &Unit, enemy: &mut Unit) {
        enemy.hp -= 3;
        if enemy.hp <= 0 {
            enemy.alive = false;
            self.grid.remove_at(enemy.pos);
        }
    }

    pub fn next_position(&self, unit: &Unit) -> Option<Position> {
        let mut visited = vec![vec![None; self.grid.nb_cols()]; self.grid.nb_rows()];
        let mut open = BinaryHeap::new(); // contains (cost, coords, first)
        visited[unit.pos.row][unit.pos.col] = Some(0);
        for (i, new) in unit.pos.neighbors().enumerate() {
            if self.grid.is_empty(new) {
                open.push((0i32, new, i));
                visited[new.row][new.col] = Some(i);
            }
        }

        while !open.is_empty() {
            let (cost, current, first) = open.pop().unwrap();
            for new in current.neighbors() {
                if visited[new.row][new.col].is_some() {
                    continue;
                }
                // if cell is empty => expand
                if self.grid.is_empty(new) {
                    open.push((cost - 1, new, first));
                    visited[new.row][new.col] = Some(first);
                } else if let Some(new_unit_ref) = self.grid.at(new).unit() {
                    // if cell is enemy => return
                    let new_unit = new_unit_ref.borrow();
                    if new_unit.alive && unit.race.is_enemy(new_unit.race) {
                        return Some(unit.pos.moved(DIRECTIONS[first]));
                    }
                }
            }
        }
        None
    }

    fn unit_turn(&mut self, unit: &mut Unit) {
        // attack if possible
        if let Some(enemy_ref) = self.enemy_with_lowest_hp(unit) {
            self.attack(unit, &mut enemy_ref.borrow_mut());
            return;
        }
        // move to the closest cell in range. If none, turn ends
        {
            if let Some(next_pos) = self.next_position(unit) {
                self.grid.move_unit(unit.pos, next_pos);
                unit.pos = next_pos;
            }
        }
        // try again to attack
        {
            if let Some(enemy_ref) = self.enemy_with_lowest_hp(unit) {
                self.attack(unit, &mut enemy_ref.borrow_mut());
            }
        }
    }

    fn turn(&mut self) -> bool {
        let units = self.grid.units().collect::<Vec<_>>();
        for unit_ref in &units {
            {
                let unit = unit_ref.borrow();
                if self.grid.count_units(unit.race.enemy()) == 0 {
                    println!("No more {:?}, {:?} have won!", unit.race.enemy(), unit.race);
                    return false;
                }
            }
            let mut unit = unit_ref.borrow_mut();
            if !unit.alive {
                continue;
            }
            self.unit_turn(&mut unit);
        }
        return true;
    }

    pub fn start(&mut self) -> usize {
        let mut cont = true;
        let mut turns = 0;
        loop {
            if !self.turn() {
                break;
            }
            turns += 1;
        }
        let remaining_hp = self
            .grid
            .units()
            .map(|u| u.borrow().hp)
            .filter(|hp| *hp > 0)
            .sum::<i32>();
        println!(
            "rem hp {} turns {}: {}",
            remaining_hp,
            turns,
            remaining_hp as usize * turns
        );
        remaining_hp as usize * turns
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn enemy_with_lowest_hp() {
        let level = "\
#####
#GGG#
#GEG#
#GGG#
#####";
        let s = Simulation {
            grid: Grid::from_str(&level),
        };
        let elf = s
            .grid
            .at(Position::new(2, 2))
            .unit()
            .expect("Elf expected!");
        {
            let enemy = s
                .enemy_with_lowest_hp(&elf.borrow())
                .expect("enemy expected!");
            assert_eq!(enemy.borrow().pos, Position::new(1, 2));
        }
        {
            s.grid
                .at(Position::new(3, 2))
                .unit()
                .expect("Goblin expected!")
                .borrow_mut()
                .hp -= 3;
            let enemy = s
                .enemy_with_lowest_hp(&elf.borrow())
                .expect("enemy expected!");
            assert_eq!(enemy.borrow().pos, Position::new(3, 2));
        }
        {
            s.grid
                .at(Position::new(2, 3))
                .unit()
                .expect("Goblin expected!")
                .borrow_mut()
                .hp -= 3;
            let enemy = s
                .enemy_with_lowest_hp(&elf.borrow())
                .expect("enemy expected!");
            assert_eq!(enemy.borrow().pos, Position::new(2, 3));
        }
        {
            s.grid
                .at(Position::new(2, 1))
                .unit()
                .expect("Goblin expected!")
                .borrow_mut()
                .hp -= 3;
            let enemy = s
                .enemy_with_lowest_hp(&elf.borrow())
                .expect("enemy expected!");
            assert_eq!(enemy.borrow().pos, Position::new(2, 1));
        }
    }

    #[test]
    fn attack() {
        let level = "EG";
        let mut s = Simulation {
            grid: Grid::from_str(&level),
        };
        let elf = s
            .grid
            .at(Position::new(0, 0))
            .unit()
            .expect("Elf expected!");
        let goblin = s
            .grid
            .at(Position::new(0, 1))
            .unit()
            .expect("Goblin expected!");
        s.attack(&elf.borrow(), &mut goblin.borrow_mut());
        assert_eq!(197, goblin.borrow().hp);

        goblin.borrow_mut().hp = 3;
        s.attack(&elf.borrow(), &mut goblin.borrow_mut());
        assert_eq!(0, goblin.borrow().hp);
        assert!(!goblin.borrow().alive);
        assert!(s.grid.at(Position::new(0, 1)).unit().is_none());

        goblin.borrow_mut().hp = 1;
        goblin.borrow_mut().alive = true;
        s.attack(&elf.borrow(), &mut goblin.borrow_mut());
        assert_eq!(-2, goblin.borrow().hp);
        assert!(!goblin.borrow().alive);
    }

    fn next_pos_test_helper(s: &Simulation, pos: (usize, usize), expected: Option<Position>) {
        let unit_ref = s
            .grid
            .at(Position::new(pos.0, pos.1))
            .unit()
            .expect("Unit!");
        assert_eq!(expected, s.next_position(&unit_ref.borrow()));
    }

    #[test]
    fn next_position_1() {
        let level = "\
#########
#G..G..G#
#.......#
#.......#
#G..E..G#
#.......#
#.......#
#G..G..G#
#########";
        let mut s = Simulation {
            grid: Grid::from_str(&level),
        };
        next_pos_test_helper(&s, (1, 1), Some(Position::new(1, 2)));
        next_pos_test_helper(&s, (1, 4), Some(Position::new(2, 4)));
        next_pos_test_helper(&s, (1, 7), Some(Position::new(1, 6)));
        next_pos_test_helper(&s, (4, 1), Some(Position::new(4, 2)));
        next_pos_test_helper(&s, (4, 4), Some(Position::new(3, 4)));
        next_pos_test_helper(&s, (4, 7), Some(Position::new(4, 6)));
        next_pos_test_helper(&s, (7, 1), Some(Position::new(6, 1)));
        next_pos_test_helper(&s, (7, 4), Some(Position::new(6, 4)));
        next_pos_test_helper(&s, (7, 7), Some(Position::new(6, 7)));
    }

    #[test]
    fn full_run() {
        {
            let level = "\
#######
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#
#######";
            let mut s = Simulation {
                grid: Grid::from_str(&level),
            };
            assert_eq!(27730, s.start());
        }
        {
            let level = "\
#######
#G..#E#
#E#E.E#
#G.##.#
#...#E#
#...E.#
#######";
            let mut s = Simulation {
                grid: Grid::from_str(&level),
            };
            assert_eq!(36334, s.start());
        }
        {
            let level = "\
#######
#E..EG#
#.#G.E#
#E.##E#
#G..#.#
#..E#.#
#######";
            let mut s = Simulation {
                grid: Grid::from_str(&level),
            };
            assert_eq!(39514, s.start());
        }
        {
            let level = "\
#######
#E.G#.#
#.#G..#
#G.#.G#
#G..#.#
#...E.#
#######";
            let mut s = Simulation {
                grid: Grid::from_str(&level),
            };
            assert_eq!(27755, s.start());
        }
        {
            let level = "\
#######
#.E...#
#.#..G#
#.###.#
#E#G#G#
#...#G#
#######";
            let mut s = Simulation {
                grid: Grid::from_str(&level),
            };
            assert_eq!(28944, s.start());
        }
        {
            let level = "\
#########
#G......#
#.E.#...#
#..##..G#
#...##..#
#...#...#
#.G...G.#
#.....G.#
#########";
            let mut s = Simulation {
                grid: Grid::from_str(&level),
            };
            assert_eq!(18740, s.start());
        }
    }

    #[test]
    #[ignore]
    fn next_position_2() {
        let level = "\
#######
#.G...#
#...EG#
#.#.#G#
#..G#E#
#.....#
#######";
        let mut s = Simulation {
            grid: Grid::from_str(&level),
        };
        s.grid.print_single();
        for i in 1..=48 {
            println!("Round #{}", i);
            s.turn();
            s.grid.print_single();
        }
        assert!(false);
    }

}

fn meta_sum(numbers: &[usize], idx: &mut usize) -> usize {
    let nb_nodes = numbers[*idx];
    *idx += 1;
    let nb_meta = numbers[*idx];
    *idx += 1;
    if nb_nodes == 0 {
        let mut sum = 0;
        for m in &numbers[*idx..*idx+nb_meta] {
            *idx += 1;
            sum += m;
        }
        sum
    } else {
        let mut nodes = vec![0usize; nb_nodes];
        for i in 0..nodes.len() {
            nodes[i] = meta_sum(numbers, idx);
        }
        let mut sum = 0;
        for m in &numbers[*idx..*idx+nb_meta] {
            *idx += 1;
            sum += match *m {
                0 => 0,
                x if x <= nb_nodes => nodes[x-1],
                _ => 0
            }; 
        }
        sum
    }
}

fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").unwrap();
    let reader = std::io::BufReader::new(input);
    let numbers = reader.lines().next().unwrap().unwrap().split_whitespace().map(|s| s.parse::<usize>().unwrap()).collect::<Vec<_>>();
    
    let mut idx = 0;
    println!("{}", meta_sum(&numbers, &mut idx));
}

use std::collections::HashMap;
use std::num::ParseIntError;
use std::str::FromStr;

type Ingredient = String;

#[derive(Debug)]
struct Reactant {
    ingredient: Ingredient,
    quantity: usize,
}

#[derive(Debug)]
struct Reaction {
    reactants: Vec<Reactant>,
    product: Reactant,
}

fn calculate(reactions: &HashMap<Ingredient, Reaction>) -> usize {
    let mut needs = HashMap::new();
    needs.insert("FUEL".to_string(), 1usize);
    for ingredient in get_products_in_order(reactions) {
        let quantity_needed = *needs.get(&ingredient).expect("Need not found!");
        let reaction = reactions.get(&ingredient).expect("Missing reaction");
        let reactions_needed =
            (quantity_needed as f64 / reaction.product.quantity as f64).ceil() as usize;
        for reactant in &reaction.reactants {
            let reactant_needed = needs.entry(reactant.ingredient.clone()).or_insert(0);
            *reactant_needed += reactant.quantity * reactions_needed;
        }
    }
    *needs.get("ORE").expect("No solution!!?")
}

fn main() {
    let input = "\
8 SPJN, 2 LJRB, 1 QMDTJ => 1 TFPRF
111 ORE => 5 GCFP
5 NGCKP => 6 QXQZ
21 RGRLZ => 7 DKVN
2 DCKF => 9 FCMVJ
7 SGHSV, 4 LZPCS => 9 DQRCZ
4 QNRH => 8 WGKHJ
135 ORE => 6 BPLFB
4 SPJN, 1 DCKF, 9 KJVZ, 1 DKVN, 4 ZKVPL, 11 TFPRF, 1 CWPVT => 8 BVMK
8 TGPV, 4 MQPLD => 2 SPFZ
11 QMDTJ, 15 LVPK, 5 LZPCS => 3 KJVZ
2 RNXF, 3 MKMQ => 6 LJRB
11 RKCXJ, 4 BJHW, 2 DKDST => 3 QNRH
3 NZHP, 1 QMDTJ => 9 BCMKN
10 DQRCZ, 1 GBJF => 7 RGRLZ
2 WLKC, 1 GBJF, 7 SPJN => 5 GBWQT
4 TGPV, 1 LTSB => 2 LZPCS
6 LJRB => 4 LQHB
3 LZPCS, 3 MDTZL, 12 DLHS => 2 CBTK
1 TGPV, 1 CQPR => 9 XQZFV
26 FSQBL => 8 HQPG
9 LQHB => 1 GBJF
7 NGCKP => 5 WLKC
9 DKDST, 1 XQZFV => 9 TPZBM
144 ORE => 9 RNXF
1 LJRB => 6 CQPR
9 MKMQ, 12 RNXF => 9 JWPLZ
5 LZPCS, 28 QMDTJ, 1 QNRH => 5 LVPK
5 TGPV, 1 HQPG => 6 FCBLK
8 LVPK, 9 DQRCZ, 1 MDTZL => 6 DCKF
1 RKCXJ, 2 LZPCS, 13 LJNJ => 1 QWFG
4 DKDST, 1 XQZFV, 10 NSXFK => 4 JRDXQ
7 QWFG, 1 BVMK, 4 BJHW, 21 QNSWJ, 3 FBTW, 3 FCBLK, 59 SPFZ, 4 GBWQT => 1 FUEL
28 LZPCS, 17 NGCKP, 1 MQPLD => 5 MDTZL
1 FCBLK, 5 WGKHJ => 7 ZKVPL
7 LJNJ => 9 BLDJP
11 FSQBL, 2 BCMKN, 1 CBTK => 9 CWPVT
1 BJHW => 1 MQPLD
11 SGHSV, 3 LJNJ => 1 NGCKP
2 FSQBL, 7 FCBLK, 1 CQPR => 4 RKCXJ
1 JRDXQ => 4 SGHSV
107 ORE => 6 MKMQ
1 DQRCZ, 3 QMDTJ, 9 XQZFV => 4 FZVH
6 NSXFK, 1 MKMQ => 6 DLHS
4 CQPR, 1 RNXF, 1 HQPG => 5 DKDST
9 RNXF => 8 LTZTR
1 LTSB, 8 BLDJP => 4 SPJN
1 FCBLK => 4 LJNJ
1 NGCKP => 3 NZHP
11 LZPCS, 22 DQRCZ, 1 QWFG, 1 QXQZ, 6 DKVN, 16 FZVH, 3 MQPLD, 23 HQPG => 3 QNSWJ
26 DLHS, 1 NSXFK => 9 BJHW
3 FCBLK, 10 HQPG => 3 LTSB
10 LTZTR, 13 JWPLZ, 16 FSQBL => 4 TGPV
11 LTSB, 1 XQZFV, 3 DQRCZ => 4 CZCJ
1 HQPG, 12 XQZFV, 17 TPZBM => 6 QMDTJ
2 LTZTR => 7 FSQBL
1 GCFP, 5 BPLFB => 1 NSXFK
3 KJVZ, 1 QXQZ, 6 DKDST, 1 FCMVJ, 2 CZCJ, 1 QNRH, 7 WLKC => 4 FBTW";
    let reactions = parse_reactions(&input);
    println!("{:?}", calculate(&reactions));
}

fn parse_reactions(input: &str) -> HashMap<Ingredient, Reaction> {
    input
        .lines()
        .map(|l| l.parse::<Reaction>().expect("Bad reaction"))
        .fold(HashMap::new(), |mut acc, reaction| {
            acc.insert(reaction.product.ingredient.clone(), reaction);
            acc
        })
}

fn calculate_generations_rec(
    reactions: &HashMap<Ingredient, Reaction>,
    generations: &mut HashMap<Ingredient, usize>,
    ingredient: &Ingredient,
) -> usize {
    if let Some(gen) = generations.get(ingredient) {
        *gen
    } else {
        if ingredient == "ORE" {
            0
        } else {
            let reaction = reactions.get(ingredient).expect("Missing reaction");
            let parent_gen = reaction
                .reactants
                .iter()
                .map(|reactant| {
                    calculate_generations_rec(reactions, generations, &reactant.ingredient)
                })
                .max()
                .unwrap();
            let gen = parent_gen + 1;
            generations.insert(ingredient.to_string(), gen);
            gen
        }
    }
}

fn get_products_in_order(reactions: &HashMap<Ingredient, Reaction>) -> Vec<Ingredient> {
    let mut generations = HashMap::new();
    calculate_generations_rec(reactions, &mut generations, &"FUEL".to_string());
    let mut products_in_order = generations.keys().cloned().collect::<Vec<_>>();
    products_in_order.sort_by_key(|p| -(*generations.get(p).unwrap() as i64));
    products_in_order
}

impl FromStr for Reaction {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(" => ");
        let reqs_s = split.next().unwrap();
        let reactants = reqs_s
            .split(", ")
            .map(|s| s.parse::<Reactant>().expect("Bad requirement"))
            .collect::<Vec<_>>();
        let product = split.next().unwrap().parse::<Reactant>()?;
        Ok(Reaction { reactants, product })
    }
}

impl FromStr for Reactant {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(' ');
        let quantity = split.next().unwrap().parse::<usize>()?;
        let ingredient = split.next().unwrap().to_string();
        Ok(Reactant {
            ingredient,
            quantity,
        })
    }
}

#[test]
fn test_1() {
    let input = "\
10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL";
    let reactions = parse_reactions(&input);
    assert_eq!(31, calculate(&reactions));
}

#[test]
fn test_2() {
    let input = "\
9 ORE => 2 A
8 ORE => 3 B
7 ORE => 5 C
3 A, 4 B => 1 AB
5 B, 7 C => 1 BC
4 C, 1 A => 1 CA
2 AB, 3 BC, 4 CA => 1 FUEL";
    let reactions = parse_reactions(&input);
    assert_eq!(165, calculate(&reactions));
}

#[test]
fn test_3() {
    let input = "\
157 ORE => 5 NZVS
165 ORE => 6 DCFZ
44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL
12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ
179 ORE => 7 PSHF
177 ORE => 5 HKGWZ
7 DCFZ, 7 PSHF => 2 XJWVT
165 ORE => 2 GPVTF
3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT";
    let reactions = parse_reactions(&input);
    assert_eq!(13312, calculate(&reactions));
}

#[test]
fn test_4() {
    let input = "\
2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG
17 NVRVD, 3 JNWZP => 8 VPVL
53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL
22 VJHF, 37 MNCFX => 5 FWMGM
139 ORE => 4 NVRVD
144 ORE => 7 JNWZP
5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC
5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV
145 ORE => 6 MNCFX
1 NVRVD => 8 CXFTF
1 VJHF, 6 MNCFX => 4 RFSQX
176 ORE => 6 VJHF";
    let reactions = parse_reactions(&input);
    assert_eq!(180697, calculate(&reactions));
}

#[test]
fn test_5() {
    let input = "\
171 ORE => 8 CNZTR
7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL
114 ORE => 4 BHXH
14 VRPVC => 6 BMBT
6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL
6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT
15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW
13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW
5 BMBT => 4 WPTQ
189 ORE => 9 KTJDG
1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP
12 VRPVC, 27 CNZTR => 2 XDBXC
15 KTJDG, 12 BHXH => 5 XCVML
3 BHXH, 2 VRPVC => 7 MZWV
121 ORE => 7 VRPVC
7 XCVML => 6 RJRHP
5 BHXH, 4 VRPVC => 5 LTCX";
    let reactions = parse_reactions(&input);
    assert_eq!(2210736, calculate(&reactions));
}

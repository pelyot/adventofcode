fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").unwrap();
    let reader = std::io::BufReader::new(input);
    let res = reader
        .lines()
        .map(|s| s.unwrap().parse::<u64>().unwrap() / 3 - 2)
        .sum::<u64>();
    println!("{}", res);
}

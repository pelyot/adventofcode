use std::collections::HashSet;
use std::collections::HashMap;
extern crate num_integer;


fn part2(input: &str) -> i32 {
    let mut map : HashSet<(i32, i32)> = HashSet::new();
    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            if c == '#' {
                map.insert((x as i32, y as i32));
            }
        }
    }
    let max_x = input.lines().next().unwrap().chars().count() as i32;
    let max_y = input.lines().count() as i32;

    let station = part1(input);

    let mut destroyed_count = 0;
    let mut res200 = (-1, -1);
    let mut destroy = |a: (i32, i32), dcount: &mut i32| {
        let mut line_vec = (a.0 - station.0, a.1 - station.1);
        let scale = num_integer::gcd(line_vec.0, line_vec.1);
        line_vec = (line_vec.0 / scale, line_vec.1 / scale);
        println!("Targetting {:?} with vec {:?}", a, line_vec);
        let mut pos = station;
        pos.0 += line_vec.0;
        pos.1 += line_vec.1;
        while pos != a {
            let destroyed = map.remove(&pos);
            if destroyed {
                *dcount += 1;
                println!("Destroyed #{} at {:?} from {:?}", dcount, pos, a);
                if *dcount == 200 {
                    println!("Result (part 2): {:?}", pos);
                    res200 = pos;
                }
                return
            }
            pos.0 += line_vec.0;
            pos.1 += line_vec.1;
        }
    };
    let mut loop_count = 0;
    while destroyed_count < 200 {
        loop_count += 1;
        println!("loop #{} {}", loop_count, destroyed_count,);
        for x in station.0..max_x {
            destroy((x, 0), &mut destroyed_count);
        }
        println!("-");
        for y in 0..max_y {
            destroy((max_x - 1, y), &mut destroyed_count);
        }
        println!("-");
        for x in (0..max_x).rev() {
            destroy((x, max_y - 1), &mut destroyed_count);
        }
        println!("-");
        for y in (0..max_y).rev() {
            destroy((0, y), &mut destroyed_count);
        }
        println!("-");
        for x in 0..station.0 {
            destroy((x, 0), &mut destroyed_count);
        }
    }

    let res = 100 * res200.0 + res200.1;
    println!("Result (part 2): {:?}", res);
    res    
}

fn part1(input: &str) -> (i32, i32) {
    let mut map : HashSet<(i32, i32)> = HashSet::new();
    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            if c == '#' {
                map.insert((x as i32, y as i32));
            }
        }
    }

    let mut visible_counts : HashMap<(i32, i32), usize> = HashMap::new();
    for a1 in &map {
        let mut count = 0;
        for a2 in &map {
            if a1 == a2 {
                continue
            }
            // is visible?
            let mut visible = true;
            let mut line_vec = (a2.0 - a1.0, a2.1 - a1.1);
            let scale = num_integer::gcd(line_vec.0, line_vec.1);
            line_vec = (line_vec.0 / scale, line_vec.1 / scale);
            let mut pos = *a1;
            pos.0 += line_vec.0;
            pos.1 += line_vec.1;
            while pos != *a2 {
                if map.contains(&pos) {
                    visible = false;
                    break;
                }
                pos.0 += line_vec.0;
                pos.1 += line_vec.1;
            }

            if visible {
                count += 1;
            }
        }
        visible_counts.insert(*a1, count);
    }

    let res = visible_counts.iter().max_by_key(|e| e.1).unwrap();
    println!("Result: {:?}", res);
    *res.0
}

#[test]
fn test_part1() {
    let input = "\
......#.#.
#..#.#....
..#######.
.#.#.###..
.#..#.....
..#....#.#
#..#....#.
.##.#..###
##...#..#.
.#....####";
    assert_eq!((5, 8), part1(&input));
}

#[test]
fn test_part2() {
    let input = "\
.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##";
    part2(&input);
}

fn main() {
    let input = "\
#..#.#.#.######..#.#...##
##.#..#.#..##.#..######.#
.#.##.#..##..#.#.####.#..
.#..##.#.#..#.#...#...#.#
#...###.##.##..##...#..#.
##..#.#.#.###...#.##..#.#
###.###.#.##.##....#####.
.#####.#.#...#..#####..#.
.#.##...#.#...#####.##...
######.#..##.#..#.#.#....
###.##.#######....##.#..#
.####.##..#.##.#.#.##...#
##...##.######..##..#.###
...###...#..#...#.###..#.
.#####...##..#..#####.###
.#####..#.#######.###.##.
#...###.####.##.##.#.##.#
.#.#.#.#.#.##.#..#.#..###
##.#.####.###....###..##.
#..##.#....#..#..#.#..#.#
##..#..#...#..##..####..#
....#.....##..#.##.#...##
.##..#.#..##..##.#..##..#
.##..#####....#####.#.#.#
#..#..#..##...#..#.#.#.##";
    println!("Part2: {:?}", part2(&input));
}
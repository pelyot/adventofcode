use std::collections::HashSet;

#[derive(Debug, PartialEq, Clone)]
struct Moon {
    pos: [i64; 3],
    velocity: [i64; 3],
}

impl Moon {
    fn new(x: i64, y: i64, z: i64) -> Moon {
        Moon {
            pos: [x, y, z],
            velocity: [0; 3],
        }
    }

    fn potential_energy(&self) -> i64 {
        self.pos.iter().map(|p| p.abs()).sum()
    }

    fn kinetic_energy(&self) -> i64 {
        self.velocity.iter().map(|v| v.abs()).sum()
    }

    fn energy(&self) -> i64 {
        self.kinetic_energy() * self.potential_energy()
    }
}

#[derive(Debug)]
struct System {
    moons: [Moon; 4],
}

impl System {
    fn new(moons: [Moon; 4]) -> System {
        System { moons }
    }

    fn apply_gravity(&mut self) {
        for axis in 0..3 {
            for m in 0..self.moons.len() {
                let attractor: i64 = self.moons[m].pos[axis];
                for moon in &mut self.moons {
                    moon.velocity[axis] += (attractor - moon.pos[axis]).signum();
                }
            }
        }
    }

    fn apply_velocity(&mut self) {
        for moon in &mut self.moons {
            moon.pos
                .iter_mut()
                .zip(moon.velocity.iter())
                .for_each(|(pos, velocity)| *pos += velocity);
        }
    }

    fn step(&mut self) {
        self.apply_gravity();
        self.apply_velocity();
    }

    fn energy(&self) -> i64 {
        self.moons.iter().map(Moon::energy).sum()
    }

    fn find_moon_period(mut self, m: usize) -> usize {
        let mut step = 0;
        let orig = self.moons[m].clone();
        loop {
            self.step();
            step += 1;
            if orig.pos == self.moons[0].pos {
                break;
            }
        }
        step
    }
}


#[derive(PartialEq, Eq, Debug, Clone, Hash)]
struct System1D {
    pos: [i64; 4],
    vel: [i64; 4],
}

impl System1D {
    fn new(positions: [i64; 4]) -> System1D {
        System1D {
            pos: positions,
            vel: [0; 4],
        }
    }

    fn apply_gravity(&mut self) {
        for p in 0..self.pos.len() {
            let attractor: i64 = self.pos[p];
            for k in 0..self.pos.len() {
                self.vel[k] += (attractor - self.pos[k]).signum();
            }
        }
    }

    fn apply_velocity(&mut self) {
        for k in 0..self.pos.len() {
            self.pos[k] += self.vel[k];
        }
    }

    fn step(&mut self) {
        self.apply_gravity();
        self.apply_velocity();
    }

    fn find_period(mut self) -> usize {
        let mut step = 0;
        let mut found = HashSet::new();
        loop {
            found.insert(self.clone());
            self.step();
            step += 1;
            if found.contains(&self) {
                break;
            }
        }
        step
    }
}

use num::Integer;

fn main() {
    let moons_by_axis = [
        [1, 17, -1, 12],
        [3, -10, -15, -4],
        [-11, -8, 2, -4]
    ];

    let periods = moons_by_axis.into_iter()
        .map(|m| System1D::new(*m).find_period())
        .collect::<Vec<_>>();
        
    let period = periods[0].lcm(&periods[1]).lcm(&periods[2]);

    println!("Period: {}", period);
}

use num_bigint::{BigInt, ToBigInt};

#[derive(Debug)]
enum AccessMode {
    Position,
    Immediate,
    Relative
}

#[derive(Debug)]
struct Instruction {
    address: BigInt,
    opcode: BigInt,
    access_modes : [AccessMode; 3]
}

use std::collections::HashMap;

#[derive(Debug)]
struct Process {
    memory: HashMap<BigInt, BigInt>,
    ip: BigInt,
    relative_base_offset: BigInt
}

impl Process {
    fn from_program(program: &[i64]) -> Process {
        let mut mem = program.iter()
            .enumerate()
            .map(|(k, v)| (BigInt::from(k), BigInt::from(*v)))
            .collect::<HashMap<BigInt, BigInt>>();
        Process {
            memory: mem,
            ip: BigInt::from(0),
            relative_base_offset: BigInt::from(0),
        }
    }
    
    fn set(&mut self, address: &BigInt, val: BigInt) {
        if *address < BigInt::from(0) {
            panic!("Negative address!");
        } else {
            self.memory.entry(address.clone()).or_insert(val);
        }
    }
    
    fn at(&self, address: &BigInt) -> BigInt {
        if *address < BigInt::from(0) {
            panic!("address < 0 !!!");
        }
        self.memory.get(address).unwrap_or(&BigInt::from(0)).clone()
    }
}
impl AccessMode {
    fn from_int(i: BigInt) -> AccessMode {
        if i == BigInt::from(0) {
            AccessMode::Position
        } else if i == BigInt::from(1) {
            AccessMode::Immediate
        } else if i == BigInt::from(2) {
            AccessMode::Relative
        } else {
            unreachable!()
        }
    }
}

impl Instruction {
    fn decode(address: &BigInt, process: &Process) -> Instruction {
        let Big10 : BigInt = BigInt::from(10);
        let Big100: BigInt = BigInt::from(100);
        let Big1000: BigInt = BigInt::from(1000);
        let Big10000: BigInt = BigInt::from(10_000);
        
        let i = process.at(address);
        Instruction {
            address: address.clone(),
            opcode: i.clone() % Big100.clone(),
            access_modes: [AccessMode::from_int(i.clone() / Big100.clone() % Big10.clone()),
                AccessMode::from_int(i.clone() / Big1000 % Big10.clone()),
                AccessMode::from_int(i.clone() / Big10000 % Big10.clone())]
        }
    }
    
    fn op(&self, index: usize, process: &Process) -> BigInt {
        let offset = self.address.clone() + index;
        let op = process.at(&offset);
        if index > self.access_modes.len() {
            println!("INDEX {}", index);
            panic!();
        }
        match self.access_modes[index] {
            AccessMode::Position => process.at(&op),
            AccessMode::Immediate => op,
            AccessMode::Relative => {
                let offset = op + process.relative_base_offset.clone();
                process.at(&offset)
            }
        }
    }
}

fn run_process_partial(process: &mut Process, input: &mut Vec<BigInt>, output: &mut Vec<BigInt>) -> bool {
    let mut finished = false;
    loop {
        let i = Instruction::decode(&process.ip, &process);
        if i.opcode == BigInt::from(1) {
            process.set(&i.op(3, &process), i.op(1, &process) + i.op(2, &process));
            process.ip += 4;
        } else if i.opcode == BigInt::from(2) {
            process.set(&i.op(3, &process), i.op(1, &process) * i.op(2, &process));
            process.ip += 4;
        } else if i.opcode == BigInt::from(3) {
            if !input.is_empty() {
                process.set(&i.op(1, &process), input.remove(0));
                process.ip += 2;
            } else {
                break;
            }
        } else if i.opcode == BigInt::from(4) {
            output.push(i.op(1, &process));
            process.ip += 2;
        } else if i.opcode == BigInt::from(5) {
            if i.op(1, &process) != BigInt::from(0) {
                process.ip = i.op(2, &process);
            } else {
                process.ip += 3;
            }
        } else if i.opcode == BigInt::from(6) {
            if i.op(1, &process) == BigInt::from(0) {
                process.ip = i.op(2, &process);
            } else {
                process.ip += 3;
            }
        } else if i.opcode == BigInt::from(7) {
            let result = if i.op(1, &process) < i.op(2, &process) {
                BigInt::from(1)
            } else {
                BigInt::from(0)
            };
            process.set(&i.op(3, &process), result);
            process.ip += 4;
        } else if i.opcode == BigInt::from(8) {
            let result = if i.op(1, &process) == i.op(2, &process) {
                BigInt::from(1)
            } else {
                BigInt::from(0)
            };
            process.set(&i.op(3, &process), result);
            process.ip += 4;
        } else if i.opcode == BigInt::from(9) {
                process.relative_base_offset += i.op(1, &process);
                process.ip += 2;
        } else if i.opcode == BigInt::from(99) {
            finished = true;
            break;
        } else {
            unreachable!("unknown operand")
        }
    }    
    finished
}

fn main() {
    let test_program = &[109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99];
    let mut test_process = Process::from_program(test_program);
    let mut test_input = Vec::new();
    let mut test_output = Vec::new();
    let roro = run_process_partial(&mut test_process, &mut test_input, &mut test_output);
    println!("Hello, world!");
}

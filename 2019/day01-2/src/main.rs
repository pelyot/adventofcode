fn fuel(mass: i64) -> i64 {
    let f = mass / 3 - 2;
    if f > 0 {
        f + fuel(f)
    } else {
        0
    }
}

fn main() {
    use std::io::BufRead;
    let input = std::fs::File::open("input").unwrap();
    let reader = std::io::BufReader::new(input);
    let res = reader
        .lines()
        .filter_map(|l| l.ok())
        .filter_map(|l| l.parse::<i64>().ok())
        .map(fuel)
        .sum::<i64>();
    println!("{}", res);
}

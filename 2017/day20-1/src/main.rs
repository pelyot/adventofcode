extern crate regex;
use regex::Regex;
use std::str::FromStr;

fn main() {
    let input = include_str!("../input.txt");
    let re = Regex::new(r"<(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)>").unwrap();
    let res = input
        .trim_right()
        .lines()
        .map(|line| {
            let mut it = re.captures_iter(line)
                .map(|cap| cap.iter().skip(1).map(|c| i64::from_str(c.unwrap().as_str()).unwrap().abs()).max().unwrap());
            let pos = it.next().unwrap();
            let vel = it.next().unwrap();
            let acc = it.next().unwrap();
            (acc, vel, pos)
        })
        .enumerate()
        .min_by_key(|&(_, c)| c)
        .unwrap();
    println!("Result: {:?}", res.0);
}

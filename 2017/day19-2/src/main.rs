#[derive(Debug)]
enum Dir { North, South, East, West }

fn next(x: &mut i64, y: &mut i64, dir: &Dir) {
    match *dir {
        Dir::North => *y -= 1,
        Dir::South => *y += 1,
        Dir::East  => *x -= 1,
        Dir::West  => *x += 1
    }
}

fn main() {
    let input = include_str!("../input.txt");
    let grid = input.lines().map(|line| line.chars().collect::<Vec<char>>()).collect::<Vec<Vec<char>>>();
    let mut y: i64 = 0;
    let mut x: i64 = grid[0].iter().position(|e| *e == '|').unwrap() as i64;
    let mut dir = Dir::South;
    let mut count = 0;

    let mut letters = String::new();
    loop {
        count += 1;
        next(&mut x, &mut y, &dir);
        match grid[y as usize][x as usize] {
            '+' => {
                match dir {
                    Dir::North | Dir::South => {
                        if x > 0 {
                            if grid[y as usize][(x-1) as usize] != ' ' {
                                dir = Dir::East;
                            }
                        }
                        if x < (grid[0].len() - 1) as i64 {
                            if grid[y as usize][(x+1) as usize] != ' ' {
                                dir = Dir::West;
                            }
                        }
                    },
                    Dir::East | Dir::West => {
                        if y > 0 {
                            if grid[(y-1) as usize][x as usize] != ' ' {
                                dir = Dir::North;
                            }
                        }
                        if y < (grid.len() - 1) as i64 {
                            if grid[(y+1) as usize][x as usize] != ' ' {
                                dir = Dir::South;
                            }
                        }
                    }
                }
            },
            '|' | '-' => (),
            ' ' => break,
            e => letters.push(e),
        }
    }
    println!("{}", letters);
    println!("{}", count);
}

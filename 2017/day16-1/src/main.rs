use std::str::FromStr;

fn solve(input: &str, orig: &str) -> String {
    let mut programs = String::from(orig).into_bytes();
    let len = programs.len();
    let mut start = 0;
    for i in input.trim_right().split(',').map(|i| i.as_bytes()) {
        println!("i {}", std::str::from_utf8(i).unwrap());
        match i[0] as char {
            's' => { let spin = usize::from_str(std::str::from_utf8(&i[1..]).unwrap()).unwrap();start += len - spin; },
            'x' => {
                let mut it = i[1..].split(|&e| e == '/' as u8);
                let a = usize::from_str(&std::str::from_utf8(it.next().unwrap()).unwrap()).unwrap();
                let b = usize::from_str(&std::str::from_utf8(it.next().unwrap()).unwrap()).unwrap();
                programs.swap((start + a) % len, (start + b) % len);
            },
            'p' => {
                let a = programs.iter().position(|&e| e == i[1]).unwrap();
                let b = programs.iter().position(|&e| e == i[3]).unwrap();
                programs.swap(a, b);
            },
            _ => panic!("bad input")
        }

    }
    programs.iter().cycle().skip(start).take(len).map(|&e| e as char).collect()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("Result: {}", solve(input, &"abcdefghijklmnop"));
}



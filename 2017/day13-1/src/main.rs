use std::str::FromStr;

fn solve(input: &str) -> usize {
    input.lines().flat_map(|l| l.split(": ").map(|f| usize::from_str(f).unwrap()))
        .collect::<Vec<usize>>().chunks(2)
        .filter(|s| s[0] % (2 * s[1] - 2) == 0)
        .map(|a| a[0] * a[1])
        .sum()
}

fn main() {
    println!("Result {}", solve(INPUT));
}

#[cfg(test)] mod tests {
    use super::*;

    #[test] fn sample() {
        assert_eq!(24, solve(&"0: 3\n1: 2\n4: 4\n6: 4"));
    }
}

static INPUT: &'static str = "0: 3
1: 2
2: 4
4: 6
6: 5
8: 6
10: 6
12: 4
14: 8
16: 8
18: 9
20: 8
22: 6
24: 14
26: 12
28: 10
30: 12
32: 8
34: 10
36: 8
38: 8
40: 12
42: 12
44: 12
46: 12
48: 14
52: 14
54: 12
56: 12
58: 12
60: 12
62: 14
64: 14
66: 14
68: 14
70: 14
72: 14
80: 18
82: 14
84: 20
86: 14
90: 17
96: 20
98: 24";

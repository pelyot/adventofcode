fn solve(a: u64, b: u64) -> usize {
    let iter_a = std::iter::repeat(()).scan(a, |a, _| { *a = (*a * 16807) % 2147483647; Some(*a) }).filter(|&a| a % 4 == 0);
    let iter_b = std::iter::repeat(()).scan(b, |b, _| { *b = (*b * 48271) % 2147483647; Some(*b) }).filter(|&b| b % 8 == 0);
    iter_a.zip(iter_b)
        .take(5_000_000)
        .filter(|&(a, b)| a & 0b1111_1111_1111_1111 == b & 0b1111_1111_1111_1111)
        .count()
}

fn main() {
    println!("Result {}", solve(516, 190));
}

#[cfg(test)] mod tests {
    use super::*;

    #[test] fn sample() {
        assert_eq!(309, solve(65, 8921));
    }
}

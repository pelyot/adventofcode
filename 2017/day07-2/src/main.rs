#[macro_use] extern crate lazy_static;
extern crate regex;

use regex::Regex;
use std::string::ToString;
use std::str::FromStr;
use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::collections::HashMap;
use std::collections::HashSet;
use std::iter::FromIterator;

#[derive(PartialEq,Debug)]
struct Program {
    name: String,
    weight: u64,
    children: Vec<String>
}

fn parse_line(input: &str) -> Program {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(?P<name>[a-z]+) \((?P<weight>\d+)\)(?: -> (?P<children>(?:[a-z]+, )*(?:[a-z]+)))?").unwrap();
    }
    let captures = RE.captures(input).expect("no match");
    let name = captures.name("name").unwrap().as_str().to_string();
    let weight = u64::from_str(captures.name("weight").unwrap().as_str()).unwrap();
    let children : Vec<String>= if let Some(children_match) = captures.name("children") {
            children_match.as_str().split(&", ").map(|s| s.to_string()).collect()
        } else {
            Vec::new()
        };
    Program { name: name, weight: weight, children: children }
}

fn load(filename: &str) -> HashMap<String, Program> {
    let file = File::open(filename).expect("Could not open file");
    let reader = BufReader::new(file);
    let mut programs = HashMap::new();
    for l in reader.lines().map(|l| l.unwrap()) {
        let p = parse_line(&l);
        programs.insert(p.name.clone(), p);
    }
    programs
}

fn solve_1(stack: &HashMap<String, Program>) -> String {
    let mut sons : HashSet<String> = HashSet::new();
    for (_, program) in stack {
        for c in &program.children {
            sons.insert(c.clone());
        }
    }
    let all_progs = HashSet::from_iter(stack.keys().cloned());
    all_progs.difference(&sons).next().unwrap().to_string()
}

fn weight(stack: &HashMap<String, Program>, prog_name: &String) -> u64 {
    let prog = stack.get(prog_name).unwrap();
    if prog.children.is_empty() {
        prog.weight
    } else if prog.children.len() == 1 {
        prog.weight + stack.get(&prog.children[0]).unwrap().weight
    } else {
        let mut weights : HashMap<String, u64> = HashMap::new();        
        let mut sum = 0;
        for c in &prog.children {
            let w = weight(stack, c);
            sum += w;
            weights.insert(c.clone(), w);
        }
        let avg_weight = sum as f64 / weights.len() as f64;
        for (name, w) in &weights {
            if *w as f64 > avg_weight {
                let expected_weight = weights.values().filter(|&w2| w2 != w).next().unwrap();
                let weight_difference = w - expected_weight;
                println!("Imbalance! {} needs to be {}", name, stack.get(name).unwrap().weight - weight_difference);
            }
        }
        prog.weight + sum
    }
}

fn main() {
   let stack = load(&"input.txt"); 
   let root = solve_1(&stack);
   weight(&stack, &root);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_line_tst() {
        let expected = Program{name: String::from("kmpjivb"), weight: 175, children: vec![String::from("hrfldlv"), String::from("empkyy"), String::from("thafg")]};
        assert_eq!(expected, parse_line(&"kmpjivb (175) -> hrfldlv, empkyy, thafg"));
    }

    #[test]
    fn parse_line_tst_2() {
        let expected = Program{name: String::from("kmpjivb"), weight: 175, children: Vec::new()};
        assert_eq!(expected, parse_line(&"kmpjivb (175)"));
    }

    #[test]
    fn sample_1() {
        assert_eq!(String::from("tknk"), solve_1(&load(&"sample1.txt")))
    }
}

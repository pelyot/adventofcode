fn main() {
    println!("Result {}", solve(INPUT));
}

type Register = usize;

fn decode_reg(s: &str) -> Register {
    s.chars().next().unwrap() as usize - 'a' as usize
}

#[derive(Debug)]
enum Value {
    Register(usize),
    Number(i64)
}

impl Value {
    fn from(s: &str) -> Value {
        use std::str::FromStr;
        if let Some(number) = i64::from_str(s).ok() {
            Value::Number(number)
        } else {
            Value::Register(decode_reg(s))
        }
    }

    fn extract(&self, registers: &Vec<i64>) -> i64 {
        match *self {
            Value::Register(reg) => registers[reg],
            Value::Number(nb) => nb
        }
    }
}

#[derive(Debug)]
enum Instruction {
    Snd(Value),
    Set(Register, Value),
    Add(Register, Value),
    Mul(Register, Value),
    Mod(Register, Value),
    Rcv(Register),
    Jgz(Value, Value)
}

fn decode(input: &str) -> Vec<Instruction> {
    input.lines()
        .map(|line| {
            let l = line.split_whitespace().collect::<Vec<&str>>();
            match l[0] {
                "snd" => Instruction::Snd(Value::from(&l[1])),
                "set" => Instruction::Set(decode_reg(&l[1]), Value::from(&l[2])),
                "add" => Instruction::Add(decode_reg(&l[1]), Value::from(&l[2])),
                "mul" => Instruction::Mul(decode_reg(&l[1]), Value::from(&l[2])),
                "mod" => Instruction::Mod(decode_reg(&l[1]), Value::from(&l[2])),
                "rcv" => Instruction::Rcv(decode_reg(&l[1])),
                "jgz" => Instruction::Jgz(Value::from(&l[1]), Value::from(&l[2])),
                _ => panic!("bad instruction")
            }
        })
    .collect()
}

fn solve(input: &str) -> i64 {
    let instructions = decode(input);
    let mut ip: i64 = 0;
    let mut registers = vec![0i64; 26];
    let mut sound = 0;
    while ip < instructions.len() as i64 && ip >= 0 {
        match &instructions[ip as usize] {
            &Instruction::Snd(ref value)      => { sound = value.extract(&registers); ip += 1; },
            &Instruction::Set(reg, ref value) => { registers[reg] = value.extract(&registers); ip += 1; },
            &Instruction::Add(reg, ref value) => { registers[reg] += value.extract(&registers); ip += 1; },
            &Instruction::Mul(reg, ref value) => { registers[reg] *= value.extract(&registers); ip += 1; },
            &Instruction::Mod(reg, ref value) => { registers[reg] %= value.extract(&registers); ip += 1; },
            &Instruction::Rcv(reg)            => { if registers[reg] != 0 { break }; ip += 1; },
            &Instruction::Jgz(ref cond, ref value) => if cond.extract(&registers) != 0 { ip += value.extract(&registers) } else { ip += 1 },
        }
    }
    sound
}

#[cfg(test)] mod tests {
    use super::*;
    #[test] fn sample() {
        const TEST: &'static str = "set a 1
add a 2
mul a a
mod a 5
snd a
set a 0
rcv a
jgz a -1
set a 1
jgz a -2";
        assert_eq!(4, solve(TEST));
    }
}

static INPUT: &'static str = "set i 31
set a 1
mul p 17
jgz p p
mul a 2
add i -1
jgz i -2
add a -1
set i 127
set p 618
mul p 8505
mod p a
mul p 129749
add p 12345
mod p a
set b p
mod b 10000
snd b
add i -1
jgz i -9
jgz a 3
rcv b
jgz b -1
set f 0
set i 126
rcv a
rcv b
set p a
mul p -1
add p b
jgz p 4
snd a
set a b
jgz 1 3
snd b
set f 1
add i -1
jgz i -11
snd a
jgz f -16
jgz a -19";


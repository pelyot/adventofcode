use std::str::FromStr;
use std::fmt::Debug;

enum Instruction {
    Spin(usize),
    Exchange(usize, usize),
    Partner(u8, u8)
}

fn parse_int<T>(input: &[u8]) -> T 
where T: FromStr, T::Err: Debug {
    use std::str::from_utf8;
    T::from_str(&from_utf8(input).expect("Bad char")).expect("bad number")
}

fn parse(input: &str) -> Vec<Instruction> {
    input.trim_right().split(',').map(|i| i.as_bytes())
        .map(|i| match i[0] as char {
            's' => Instruction::Spin(parse_int(&i[1..])),
            'x' => {
                let mut it = i[1..].split(|&e| e == '/' as u8);
                Instruction::Exchange(parse_int(it.next().unwrap()), parse_int(it.next().unwrap()))
            },
            'p' => Instruction::Partner(i[1], i[3]),
            _ => panic!("bad input")
        }
        )
        .collect()
}

fn exec(programs:&mut [u8], begin: &mut usize, instructions: &[Instruction]) {
    let len = programs.len();
    for i in instructions {
        match *i {
            Instruction::Spin(spin) => *begin += len - spin,
            Instruction::Exchange(a, b) => programs.swap((*begin + a) % len, (*begin + b) % len),
            Instruction::Partner(a, b) => {
                let pos_a = programs.iter().position(|&e| e ==  a).unwrap();
                let pos_b = programs.iter().position(|&e| e ==  b).unwrap();
                programs.swap(pos_a, pos_b)
            }
        }
    }
}

fn format_result(programs: &[u8], begin: usize) -> String {
    programs.iter().cycle().skip(begin).take(programs.len()).map(|u| *u as char).collect()
}

fn compare(orig: &[u8], programs: &[u8], begin: usize) -> bool {
    ! orig.iter().zip(programs.iter().cycle().skip(begin).take(programs.len())).any(|(e1, e2)| e1 != e2)
}

fn solve(input: &str, orig: &str) -> String {
    let init = orig.as_bytes();
    let instructions = parse(input);

    let mut programs = init.to_vec();
    let mut begin = 0;
    
    // Find number of iterations before it loops back to initial input
    exec(&mut programs, &mut begin, &instructions);
    let mut j = 1;
    while !compare(init, &programs, begin) {
        exec(&mut programs, &mut begin, &instructions);
        j += 1;
    }
    
    // iterate only modulo the loop size
    programs = init.to_vec();
    begin = 0;
    for _ in 0..(1_000_000_000 % j) {
        exec(&mut programs, &mut begin, &instructions);
        j += 1;
    }
    format_result(&programs, begin)
}


fn main() {
    let input = include_str!("../input.txt");
    println!("Result: {}", solve(input, &"abcdefghijklmnop"));
}

#[cfg(test)] mod tests {
    use super::*;

    #[test] fn test_compare() {
        assert_eq!(true, compare(&"abcd".as_bytes(), &"abcd".as_bytes(), 0));
        assert_eq!(true, compare(&"abcd".as_bytes(), &"bcda".as_bytes(), 3));
    }
}

use std::fs::File;
use std::io::BufReader;
use std::io::BufRead;
use std::str::FromStr;
use std::collections::HashSet;

fn solve(visited: &mut HashSet<usize>, pipes: &Vec<Vec<usize>>, node: usize) {
    visited.insert(node);
    for n in &pipes[node] {
        if !visited.contains(n) {
            solve(visited, pipes, *n);
        }
    }
}

fn main() {
    let file = File::open("input.txt").unwrap();
    let reader = BufReader::new(file);
    let pipes : Vec<Vec<_>> = reader.lines().map(|l| 
                                                 l.unwrap()
                                                 .split_whitespace()
                                                 .skip(2)
                                                 .map(|w| usize::from_str(w.trim_right_matches(',')).unwrap()).collect::<Vec<_>>())
        .collect();
    let mut s : HashSet<usize> = HashSet::new();
    solve(&mut s, &pipes, 0);
    println!("Result {}", s.len());
}


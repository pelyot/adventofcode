fn solve(a: u64, b: u64) -> usize {
    (0..40_000_000).scan((a, b), |&mut (ref mut a, ref mut b), _| {
                         *a = (*a * 16807) % 2147483647;
                         *b = (*b * 48271) % 2147483647;
                         Some((*a, *b))
        })
        .filter(|&(a, b)| a & 0b1111_1111_1111_1111 == b & 0b1111_1111_1111_1111)
        .count()
}

fn main() {
    println!("Result {}", solve(516, 190));
}

#[cfg(test)] mod tests {
    use super::*;

    #[test] fn sample() {
        assert_eq!(588, solve(65, 8921));
    }
}

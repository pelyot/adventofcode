fn parse(input: &str) -> Vec<usize> {
    use std::str::FromStr;    
    input.split_whitespace().map(|w| usize::from_str(w).unwrap()).collect()
}

fn solve(input: &str) -> usize {
    use std::collections::HashMap;
    let mut configurations : HashMap<Vec<usize>, usize> = HashMap::new();
    let mut banks = parse(input);
    let mut rebalance_count = 0;
    while !configurations.contains_key(&banks) {
        configurations.insert(banks.clone(), rebalance_count);
        rebalance(&mut banks);
        rebalance_count += 1;
    }
    rebalance_count - configurations.get(&banks).unwrap()
}

fn rebalance(banks: &mut Vec<usize>) {
    let mut bank = 0;
    let mut blocks = banks[bank];
    for (i,&b) in banks.iter().enumerate() {
        if b > blocks {
            blocks = b;
            bank = i;
        }
    }
    banks[bank] = 0;
    bank += 1;
    let len = banks.len();
    while blocks > 0 {
        banks[bank % len] += 1;
        blocks -= 1;
        bank += 1;
    }
}

fn main() {
    println!("Result {}", solve(INPUT));
}

static INPUT: &'static str = "2 8 8 5 4 2 3 1 5 5 1 2 15 13 5 14";

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn sample() {
        assert_eq!(4, solve(&"0 2 7 0"));
    }
}

fn main() {
    println!("Result {}", solve_vecdeque(304));
}

fn solve_vec(input: usize) -> usize {
    let mut buffer = vec![0, 1];
    let mut current = 1;
    for k in 2..5_000_001 {
        current = (current + input + 1) % buffer.len();
        buffer.insert(current, k);
        if k % 5000 == 0 { println!("k = {} ({} / 1000)", k, k / 5000) }
    }
    buffer[(buffer.iter().position(|f| *f == 0).unwrap() + 1) % buffer.len()]
}

use std::collections::VecDeque;

fn solve_vecdeque(input: usize) -> usize {
    let mut buffer = VecDeque::new();
    buffer.push_back(0);
    buffer.push_back(1);
    let mut current = 1;
    for k in 2..5_000_001 {
        current = (current + input + 1) % buffer.len();
        buffer.insert(current, k);
        if k % 5000 == 0 { println!("k = {} ({} / 1000)", k, k / 5000) }
    }
    buffer[(buffer.iter().position(|f| *f == 0).unwrap() + 1) % buffer.len()]
}

use std::collections::LinkedList;

fn solve_list(input: usize) -> usize {
    let mut buffer : LinkedList<usize> = LinkedList::new();
    buffer.push_back(0);
    buffer.push_back(1);
	let mut len = 2;
    let mut current = 1;
    for k in 2..5_000_001 {
        current = (current + input + 1) % len;
		let mut end = buffer.split_off(current % len);
		buffer.push_back(k);
		buffer.append(&mut end);
		len += 1;
        if k % 5000 == 0 { println!("k = {} ({} / 1000)", k, k / 5000) }
    }
	let pos_zero = buffer.iter().position(|f| *f == 0).unwrap();
    let remain = buffer.split_off(pos_zero + 1);
	if remain.is_empty() {
	    *buffer.front().unwrap()
    } else {
        *remain.front().unwrap()
    }
}


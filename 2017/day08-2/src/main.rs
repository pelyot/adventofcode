use std::collections::HashMap;

type Register = String;

#[derive(Debug, PartialEq)]
enum OpType { Inc, Dec }

#[derive(Debug, PartialEq)]
struct Operation {
    register: Register,
    optype: OpType,
    amount: i64
}

#[derive(Debug, PartialEq)]
enum CmpOp { Equal, NotEqual, Greater, Lower, GreaterEq, LowerEq }

impl CmpOp {
    fn from_tok(s: &str) -> CmpOp {
        if s == "==" {
            CmpOp::Equal
        } else if s == "!=" {
            CmpOp::NotEqual
        } else if s == ">=" {
            CmpOp::GreaterEq
        } else if s == "<=" {
            CmpOp::LowerEq
        } else if s == ">" {
            CmpOp::Greater
        } else if s == "<" {
            CmpOp::Lower
        } else {
            unreachable!();
        }
    }
}

#[derive(Debug, PartialEq)]
struct Condition {
    register: Register,
    operator: CmpOp,
    amount: i64
}

#[derive(Debug, PartialEq)]
struct Instruction {
    operation: Operation,
    condition: Condition
}

fn parse_line(input: &str) -> Instruction {
    use std::str::FromStr;
    let words : Vec<&str> = input.split_whitespace().collect();
    Instruction {
        operation: Operation{
            register: words[0].to_string(),
            optype: if words[1] == "inc" { OpType::Inc } else { OpType::Dec },
            amount: i64::from_str(words[2]).unwrap()
        },
        condition: Condition{
            register: words[4].to_string(),
            operator: CmpOp::from_tok(words[5]),
            amount: i64::from_str(words[6]).unwrap()
        }
    }
}

fn load_file(filename: &str) -> Vec<Instruction> {
    use std::io::BufRead;
    let file = std::fs::File::open(filename).unwrap();
    let reader = std::io::BufReader::new(file);
    reader.lines().map(|l| parse_line(&l.unwrap())).collect()
}

type State = HashMap<String, i64>;

impl Condition {
    fn evaluate(&self, state: &State) -> bool {
        match self.operator {
            CmpOp::Equal     => *state.get(&self.register).unwrap() == self.amount,
            CmpOp::NotEqual  => *state.get(&self.register).unwrap() != self.amount,
            CmpOp::Greater   => *state.get(&self.register).unwrap() > self.amount,
            CmpOp::Lower     => *state.get(&self.register).unwrap() < self.amount,
            CmpOp::GreaterEq => *state.get(&self.register).unwrap() >= self.amount,
            CmpOp::LowerEq   => *state.get(&self.register).unwrap() <= self.amount
        }
    }
}

impl Operation {
    fn apply(&self, state: &mut State) {
        match self.optype {
            OpType::Inc => *state.get_mut(&self.register).unwrap() += self.amount,
            OpType::Dec => *state.get_mut(&self.register).unwrap() -= self.amount
        }
    }
}

fn run(instructions: Vec<Instruction>) -> i64 {
    let mut state = HashMap::new();
    for i in &instructions {
        state.insert(i.condition.register.clone(), 0);
        state.insert(i.operation.register.clone(), 0);
    }
    let mut regmax = 0;
    for i in &instructions {
        if i.condition.evaluate(&state) {
            i.operation.apply(&mut state);
            regmax = std::cmp::max(regmax, *state.get(&i.operation.register).unwrap());
        }
    }
    regmax
}

fn main() {
    println!("Result: {}", run(load_file(&"input.txt")));
}

#[cfg(test)]
mod tests {
    use super::*;

    fn load(input: &str) -> Vec<Instruction> {
        input.lines().map(|l| parse_line(&l)).collect()
    }

    #[test]
    fn test_sample() {
        let input = &"\
b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10";
        assert_eq!(10, run(load(input)));
    }

    #[test]
    fn parse_line_test()  {
        assert_eq!(Instruction{
            operation: Operation {
                register: String::from("b"),
                optype: OpType::Inc,
                amount: 5},
            condition: Condition {
                register: String::from("a"),
                operator: CmpOp::Greater,
                amount: 1}
            }, parse_line(&"b inc 5 if a > 1"));
    }
}

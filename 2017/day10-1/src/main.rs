fn index(i: usize, len: usize) -> usize {
    (i % len) as usize
}

fn solve(input: Vec<usize>, list_len: usize) -> usize {
    let mut list: Vec<u8> = (0..list_len).map(|e| e as u8).collect();
    let mut current: usize = 0;
    let mut skip_size: usize= 0;
    for length in input {
        if length > list_len {
            continue;
        }
        for i in 0..length / 2 {
            list.swap(index(current + i, list_len), index(current + length - i - 1, list_len));
        }
        current += length + skip_size;
        skip_size += 1;
    }
    list[0] as usize * list[1] as usize
}

fn main() {
    let input: Vec<usize> = vec![227,169,3,166,246,201,0,47,1,255,2,254,96,3,97,144];
    println!("Result: {}", solve(input, 256));
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn sample() {
        assert_eq!(12, solve(vec![3, 4, 1, 5], 5));
    }
}

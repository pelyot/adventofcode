
const INPUT: i64 = 312051;

fn up   (_: &mut i64, y: &mut i64) { *y += 1; }
fn down (_: &mut i64, y: &mut i64) { *y -= 1; }
fn left (x: &mut i64, _: &mut i64) { *x -= 1; }
fn right(x: &mut i64, _: &mut i64) { *x += 1; }

fn sum_around(grid: &Vec<Vec<i64>>, x: i64, y: i64) -> i64 {
    let mut s = 0;
    for i in x-1..(x+2) {
        for j in y-1..(y+2) {
            s += grid[i as usize][j as usize];
        }
    }
    s
}

fn next<M>(grid: &mut Vec<Vec<i64>>, x: &mut i64, y: &mut i64, movement: M)
    where M: Fn(&mut i64, &mut i64) {
    let s = sum_around(grid, *x, *y);
    grid[*x as usize][*y as usize] = s;
    movement(x, y);
    if s > INPUT {
        println!("Result: {} ! ONLY THE FIRST PRINT OF RESULT IS VALID !", s);
    }
}

fn print_grid(grid: &Vec<Vec<i64>>) {
    for i in 0..grid.len() {
        for j in 0..grid.len() {
            print!("{} ", grid[j][grid.len() - 1 - i]);
        }
        println!("")
    }
    println!("");
}


fn populate_grid(n: usize) -> i64 {
    let size = 2*(n+1)+1;
    let mut grid = vec![vec![0i64; size]; size];
    grid[n+1][n+1] = 1;
    let mut x = n as i64+2;
    let mut y = n as i64+1;
    let mut k = 1;
    while k <= n {
        for _ in 0..(2*k-1) { next(&mut grid, &mut x, &mut y, up); }
        for _ in 0..(2*k)   { next(&mut grid, &mut x, &mut y, left); }
        for _ in 0..(2*k)   { next(&mut grid, &mut x, &mut y, down); }
        for _ in 0..(2*k+1) { next(&mut grid, &mut x, &mut y, right); }
        k += 1;
    }
    print_grid(&grid);
    let max_grid = grid[2*n+1][1];
    max_grid
}

fn main() {
    let mut n = 1;
    while populate_grid(n) < INPUT {
        n += 1;
    }
}

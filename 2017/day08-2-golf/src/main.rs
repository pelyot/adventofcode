use std::collections::HashMap;
use std::io::Read;
use std::fs::File;
use std::str::FromStr;
use std::cmp::max;
use std::ops::*;

fn main() {
    let mut input = String::new();
    File::open(&"input.txt").expect("Failed to open file").read_to_string(&mut input).expect("Failed to read file");
    let mut state : HashMap<&str, i64> = HashMap::new();
    let mut maxreg = 0;
    for l in input.lines() {
        let i : Vec<&str> = l.split_whitespace().collect();
        let cmpop = match i[5] {
            "==" => PartialEq::eq,
            "!=" => PartialEq::ne,
            ">=" => PartialOrd::ge,
            "<=" => PartialOrd::le,
            ">"  => PartialOrd::gt,
            _    => PartialOrd::lt,
        };
        if cmpop(state.entry(i[4]).or_insert(0), &i64::from_str(i[6]).unwrap()) {
            let op = match i[1] {
                "inc" => AddAssign::add_assign,
                _     => SubAssign::sub_assign
            };
            op(state.entry(i[0]).or_insert(0), i64::from_str(i[2]).unwrap());
            maxreg = max(*state.get(i[0]).unwrap(), maxreg);
        }
    }
    println!("day8-1 result: {:?}", state.values().max().unwrap());
    println!("day8-2 result: {:?}", maxreg);
}

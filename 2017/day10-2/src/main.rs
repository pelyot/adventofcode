fn extend_input(input:  Vec<u8>) -> Vec<usize> {
    let mut extended = input.clone();
    extended.extend_from_slice(&[17, 31, 73, 47, 23]);
    extended.iter().map(|&e| e as usize).collect()
}

fn solve(input: &str) -> String {
    let lengths = extend_input(input.as_bytes().to_vec());
    let mut list: Vec<u8> = (0..256u16).map(|e| e as u8).collect();
    let mut current: usize = 0;
    let mut skip_size: usize= 0;
    for _ in 0..64 {
        for &length in &lengths {
            for i in 0..length / 2 {
                list.swap((current + i) % 256, (current + length - i - 1) % 256);
            }
            current += length + skip_size;
            skip_size += 1;
        }
    }
    list.chunks(16)
        .map(|c| c.iter().fold(0, std::ops::BitXor::bitxor))
        .fold(String::new(), |s, e| s + &format!("{:02x}", e))
}

fn main() {
    println!("Result: {}", solve(&"227,169,3,166,246,201,0,47,1,255,2,254,96,3,97,144"));
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn sample() {
        assert_eq!(String::from("a2582a3a0e66e6e86e3812dcb672a272"), solve(&""));
        assert_eq!(String::from("33efeb34ea91902bb2f59c9920caa6cd"), solve(&"AoC 2017"));
        assert_eq!(String::from("3efbe78a8d82f29979031a4aa0b16a9d"), solve(&"1,2,3"));
        assert_eq!(String::from("63960835bcdc130f0b66d7ff4f6a5a8e"), solve(&"1,2,4"));
    }
}

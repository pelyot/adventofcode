fn extend_input(input:  &str, rownum: usize) -> Vec<usize> {
    let mut extended = format!("{}-{}", input, rownum).as_bytes().to_vec();
    extended.extend_from_slice(&[17, 31, 73, 47, 23]);
    extended.iter().map(|&e| e as usize).collect()
}

fn create_row(input: &str, rownum: usize) -> Vec<(bool, Option<usize>)> {
    let lengths = extend_input(input, rownum);
    let mut list: Vec<u8> = (0..256u16).map(|e| e as u8).collect();
    let mut current: usize = 0;
    let mut skip_size: usize= 0;
    for _ in 0..64 {
        for &length in &lengths {
            for i in 0..length / 2 {
                list.swap((current + i) % 256, (current + length - i - 1) % 256);
            }
            current += length + skip_size;
            skip_size += 1;
        }
    }
    list.chunks(16)
        .map(|c| c.iter().fold(0, std::ops::BitXor::bitxor))
        .fold(String::new(), |s, u| s + &format!("{:08b}", u))
        .as_bytes()
        .iter()
        .map(|&u| (u == '1' as u8, None))
        .collect()
}

type Grid = Vec<Vec<(bool, Option<usize>)>>;

fn pad_grid(grid: &mut Grid) {
    for r in grid.iter_mut() {
        r.insert(0, (false, None));
        r.push((false, None));
    }
    let len = grid.len();
    grid.insert(0, vec![(false, None); len + 2]);
    grid.push(vec![(false, None); len + 2]);
}

fn solve(input: &str) -> usize {
    let mut grid = (0..128).map(|r| create_row(input, r)).collect::<Vec<_>>();
    pad_grid(&mut grid);
    regions(&mut grid)
}

fn explore(x: usize, y: usize, grid: &mut Grid) {
    for &(i, j) in &[(x-1, y), (x+1, y), (x, y-1), (x, y+1)] {
        if grid[i][j].0 && grid[i][j].1.is_none() {
            grid[i][j].1 = grid[x][y].1;
            explore(i, j, grid);
        }
    }
}

fn regions(grid: &mut Grid) -> usize {
    let mut region = 0;
    for x in 1..grid.len() - 1 {
        for y in 1..grid.len() - 1 {
            if grid[x][y].0 && grid[x][y].1.is_none() {
                region += 1;
                grid[x][y].1 = Some(region);
                explore(x, y, grid);
            }
        }
    }
    region
}

fn main() {
    println!("Result: {}", solve(&"jxqlasbh"));
}

#[cfg(test)] mod tests {
    use super::*;

    #[test] fn sample() {
        assert_eq!(1242, solve(&"flqrgnkx"));
    }
}

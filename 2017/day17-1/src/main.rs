fn main() {
    println!("Result {}", solve(304));
}

fn solve(input: usize) -> usize {
    let mut buffer = vec![0, 1];
    let mut current = 1;
    for k in 2..2018 {
        current = (current + input + 1) % buffer.len();
        buffer.insert(current, k);
    }
    println!("current {}, buff {:?}", current, buffer);
    buffer[(current + 1) % buffer.len()]
}

#[cfg(test)] mod tests {
    use super::*;
    #[test] fn sample() {
        assert_eq!(638, solve(3));
    }
}

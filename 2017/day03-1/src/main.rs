fn find_n(input: i64) -> (i64,i64) {
    let mut c = 1;
    let mut n = 0;
    while c + 8 * n < input {
        c += 8 * n;
        n += 1;
    }
    (n, c)
}

fn solve(input: i64) -> i64 {
    if input == 1 { return  0; }
    let (n, c) = find_n(input);
    let offset = (input - c) % (2*n);
    n + (offset - n).abs()
}

const INPUT: i64 = 312051;

fn main() {
    println!("Result: {}", solve(INPUT));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn find_n_tst() {
        assert_eq!((0, 1), find_n(1));
        assert_eq!((1, 1), find_n(2));
        assert_eq!((1, 1), find_n(9));
        assert_eq!((2, 9), find_n(10));
        assert_eq!((2, 9), find_n(25));
        assert_eq!((2, 9), find_n(25));
        assert_eq!((3, 25), find_n(26));
        assert_eq!((3, 25), find_n(49));
        assert_eq!((4, 49), find_n(50));
    }

    #[test]
    fn solve_tst() {
        assert_eq!(0, solve(1));
        assert_eq!(3, solve(12));
        assert_eq!(2, solve(23));
        assert_eq!(31, solve(1024));
    }
}

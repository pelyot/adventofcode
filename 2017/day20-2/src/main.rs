extern crate regex;
use regex::Regex;
use std::str::FromStr;
use std::iter::FromIterator;

#[derive(Debug, Hash, PartialEq, Clone, Copy, Eq)]
struct Point {
    x: i64,
    y: i64,
    z: i64
}

impl std::ops::AddAssign for Point {
    fn add_assign(&mut self, other: Point) {
        self.x += other.x;
        self.y += other.y;
        self.z += other.z;
    }
}

impl FromIterator<i64> for Point {
    fn from_iter<I: IntoIterator<Item=i64>>(iter: I) -> Self  {
        let mut it = IntoIterator::into_iter(iter);
        Point {
            x: it.next().unwrap(),
            y: it.next().unwrap(),
            z: it.next().unwrap()
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
struct Particule {
    pos: Point,
    vel: Point,
    acc: Point
}

impl FromIterator<Point> for Particule {
    fn from_iter<I: IntoIterator<Item=Point>>(iter: I) -> Self {
        let mut it = IntoIterator::into_iter(iter);
        Particule {
            pos: it.next().unwrap(),
            vel: it.next().unwrap(),
            acc: it.next().unwrap()
        }
    }
}

impl Particule {
    fn tick(&mut self) {
        self.vel += self.acc;
        self.pos += self.vel;
    }
}

use std::collections::HashMap;

fn detect_collisions(particules: &mut Vec<Particule>) {
    let mut counts = HashMap::new();
    for p in particules.iter() {
        counts.entry(p.pos.clone()).or_insert(Vec::new()).push(p.clone());
    }
    for v in counts.values() {
        if v.len() > 1 {
            for p in v {
                particules.retain(|p1| *p1 != *p);
            }
        }
    }
}

fn main() {
    let input = include_str!("../input.txt");
    let re = Regex::new(r"<(-?[0-9]+),(-?[0-9]+),(-?[0-9]+)>").unwrap();
    let mut particules = input
        .trim_right()
        .lines()
        .map(|line| {
            let mut it = re.captures_iter(line)
                .map(|cap| cap.iter().skip(1).map(|c| i64::from_str(c.unwrap().as_str()).unwrap()).collect::<Point>());
            Particule {
                pos: it.next().unwrap(),
                vel: it.next().unwrap(),
                acc: it.next().unwrap()
            }
        })
        .collect::<Vec<_>>();
    loop {
        detect_collisions(&mut particules);
        for p in &mut particules {
            p.tick()
        }
        println!("Particules left: {}", particules.len());
    }
}

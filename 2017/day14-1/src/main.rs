fn extend_input(input:  &str, rownum: usize) -> Vec<usize> {
    let mut extended = format!("{}-{}", input, rownum).as_bytes().to_vec();
    extended.extend_from_slice(&[17, 31, 73, 47, 23]);
    extended.iter().map(|&e| e as usize).collect()
}

fn row(input: &str, rownum: usize) -> u32 {
    let lengths = extend_input(input, rownum);
    let mut list: Vec<u8> = (0..256u16).map(|e| e as u8).collect();
    let mut current: usize = 0;
    let mut skip_size: usize= 0;
    for _ in 0..64 {
        for &length in &lengths {
            for i in 0..length / 2 {
                list.swap((current + i) % 256, (current + length - i - 1) % 256);
            }
            current += length + skip_size;
            skip_size += 1;
        }
    }
    list.chunks(16)
        .map(|c| c.iter().fold(0, std::ops::BitXor::bitxor).count_ones())
        .sum()
}

fn solve(input: &str) -> u32 {
    (0..128).map(|r| row(input, r)).sum()
}

fn main() {
    println!("Result: {}", solve(&"jxqlasbh"));
}

#[cfg(test)] mod tests {
    use super::*;

    #[test] fn sample() {
        assert_eq!(8108, solve(&"flqrgnkx"));
    }
}

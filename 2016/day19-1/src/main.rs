fn main() {
    let input = 3004953;
    let mut buffer = vec![true; input];
    let mut idx = 0;
    let mut ring_size = input;
    loop {
        if ring_size == 1 { break; }
        let opposite = buffer.iter().enumerate().cycle().skip(idx).filter(|&(_,&e)| e).map(|(i, _)| i).nth(ring_size / 2).unwrap();
        buffer[opposite] = false;
        ring_size -= 1;
        idx += 1;
        idx %= input;
        while !buffer[idx] {
            idx += 1;
            idx %= input;
        }
    }
    println!(">>> {}", idx + 1);
}

extern crate crypto;
use std::io::Write;

use crypto::md5::Md5;
use crypto::digest::Digest;

static INPUT : &'static [u8] = b"uqwqemis";

fn hash_to_string(hash: &[u8; 16]) -> String {
    use std::io::Cursor;
    let mut buf = Cursor::new(Vec::with_capacity(32));
    for byte in hash {
        write!(buf, "{:0>2x}", byte).expect("Not supposed to fail");
    }
    unsafe {
        String::from_utf8_unchecked(buf.into_inner())
    }
}

fn main() {
    let mut pwd = ['*' as u8; 8];
    let mut iteration = 0;
    let mut buf = Vec::with_capacity(8);
    let mut hasher = Md5::new();
    let mut count = 0;
    while count < 8 {
        hasher.input(INPUT);
        buf.clear();
        write!(buf, "{}", iteration).expect("Failed to write");
        hasher.input(&buf);
        let mut hash = [0u8; 16];
        hasher.result(&mut hash);
        let first_five = hash[0] | hash[1] | (hash[2] >> 4);
        if first_five == 0 {
            let ch6 = hash[2] & 0x0F;
            if ch6 < 8 {
                let ch7 = format!("{:x}", hash[3] >> 4).chars().next().unwrap();
                if '*' as u8 == pwd[ch6 as usize] {
                    pwd[ch6 as usize] = ch7 as u8;
                    println!("Iteration {} yielded {} ({} at {:x} -> {})", iteration, hash_to_string(&hash), ch7, ch6, String::from_utf8_lossy(&pwd));
                    count += 1;
                }
            }
        }
        iteration += 1;
        hasher.reset();
    }
    println!("{}", String::from_utf8_lossy(&pwd));
}

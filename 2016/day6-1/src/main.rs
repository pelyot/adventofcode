use std::io::BufRead;

fn as_index(c: char) -> usize {
    c as usize - 'a' as usize
}

fn as_char(idx: usize) -> char {
    (idx as u8 + b'a') as char
}

fn main() {
    let fin = std::fs::File::open("input.txt").expect("Failed to read input file");
    let reader = std::io::BufReader::new(fin);
    let mut counters = [[0u32; 26];8];
    for l in reader.lines() {
        for (i, c) in l.unwrap().trim().chars().enumerate() {
            counters[i][as_index(c)] += 1
        }
    }
    for counter in counters.iter() {
        let (max_idx, _) = counter.iter().enumerate().max_by_key(|&(_, e)| e).unwrap();
        print!("{}", as_char(max_idx));
    } 
    println!("");
}

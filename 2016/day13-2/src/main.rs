use std::fmt;
use std::mem;

type Numt = i64;

const INPUT : Numt = 1362;
//const INPUT : Numt = 10;
const TARGET_X : Numt = 31;
//const TARGET_X : Numt = 7;
const TARGET_Y : Numt = 39;
//const TARGET_Y : Numt = 4;

fn is_odd_parity(n: Numt) -> bool {
    let mut bit_shift = mem::size_of::<Numt>() * 8 / 2; // We start with a shift of half the length of the type
    let mut x = n;
    while bit_shift > 0 {
        x ^= x >> bit_shift;
        bit_shift /= 2;
    }
    x & 1 == 1
}

#[derive(Debug, Clone)]
enum Cell {
    Wall,
    Empty
}

struct Area {
    map: Vec<Vec<Cell>>
}

impl Area {
    fn new_map(x: usize, y: usize) -> Vec<Vec<Cell>> {
        (0..x as Numt).map(|r|
            (0..y as Numt).map(|s| if is_wall(r, s) { Cell::Wall } else { Cell::Empty }).collect()
        ).collect()
    }

    fn new(x: usize, y: usize) -> Area {
        Area { map: Area::new_map(x, y) }
    }

    fn grow(&mut self) {
        let new_x = self.map.len() * 2;
        let new_y = self.map[0].len() * 2;
        println!("Grow!");
        self.map = Area::new_map(new_x, new_y);
    }

    fn get(&mut self, x: Numt, y: Numt) -> Cell {
        if x < 0 || y < 0 {
            Cell::Wall
        } else if x >= self.map.len() as i64 || y >= self.map[0].len() as i64 {
            self.grow();
            self.get(x, y)
        } else {
            self.map[x as usize][y as usize].clone()
        }
    }
}

impl fmt::Display for Cell {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match *self {
            Cell::Wall => '#',
            Cell::Empty => '.'
        })
    }
}

impl fmt::Display for Area {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let max_x = self.map.len();
        let max_y = self.map[0].len();
        for y in 0..max_y {
            for x in 0..max_x {
                try!(write!(f, "{}", self.map[x][y]));
            }
            try!(writeln!(f, ""));
        }
        Ok(())
    }
}

fn is_wall(x: Numt, y: Numt) -> bool {
    let num = x*x + 3*x + 2*x*y + y + y*y + INPUT;
    is_odd_parity(num)
}

use std::collections::{HashSet, VecDeque};

fn solve(area: & mut Area) -> usize {
    let mut visited : HashSet<(Numt, Numt)> = HashSet::new();
    visited.insert((1, 1));
    let mut open : VecDeque<(Numt, Numt, usize)> = VecDeque::new();
    open.push_back((1, 1, 0));
    loop {
        let (x, y, n) = open.pop_front().unwrap();
        if n == 50{
            return visited.len();
        } else {
            for &(r, s) in &[(x-1, y), (x+1, y), (x, y-1), (x, y+1)] {
                match area.get(r, s) {
                    Cell::Wall => (),
                    Cell::Empty => if !visited.contains(&(r, s)) {
                        visited.insert((r, s));
                        open.push_back((r, s, n + 1));
                    }
                }
            }
        }
    }
    panic!("no solution");
}

fn main() {
    println!("{}", Area::new(10, 7));
    let mut a = Area::new(2,2);
    println!("{}", solve(&mut a));
}

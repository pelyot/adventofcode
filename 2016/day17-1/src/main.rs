extern crate crypto;
use crypto::md5;
use crypto::digest::Digest;

const UP: usize = 0;
const DOWN: usize = 1;
const LEFT: usize = 2;
const RIGHT: usize = 3;

fn is_open(c: char) -> bool {
    ['b', 'c', 'd', 'e', 'f'].contains(&c)
}

fn is_open_dir(hash: &str, dir: usize) -> bool {
    let o = is_open(hash.chars().nth(dir).unwrap());
    o
}

use std::collections::VecDeque;

fn solve(h: &mut md5::Md5, init_passcode: &mut String) -> String {
    let mut open = VecDeque::new();
    open.push_back((0, 3, init_passcode.clone()));
    let mut current_longest = String::new();
    loop {
        if let Some((x, y, passcode)) = open.pop_back() {
            if x == 3 && y == 0 {
                if current_longest.len() < passcode.len() {
                    current_longest = passcode.clone()
                }
                continue
            }
            h.reset();
            h.input_str(&passcode);
            let doors = h.result_str();
            if y < 3 && is_open_dir(&doors, UP) {
                let mut new_passcode = passcode.clone();
                new_passcode.push('U');
                open.push_back((x, y+1, new_passcode));
            }
            if y > 0 && is_open_dir(&doors, DOWN) {
                let mut new_passcode = passcode.clone();
                new_passcode.push('D');
                open.push_back((x, y-1, new_passcode));
            }
            if x > 0 && is_open_dir(&doors, LEFT) {
                let mut new_passcode = passcode.clone();
                new_passcode.push('L');
                open.push_back((x-1, y, new_passcode));
            }
            if x < 3 && is_open_dir(&doors, RIGHT) {
                let mut new_passcode = passcode.clone();
                new_passcode.push('R');
                open.push_back((x+1, y, new_passcode));
            }
        } else {
            break
        }
    }
    return current_longest; 
}

fn main() {
    let mut hasher = md5::Md5::new();
    let input = "njfxhljp";
    let mut passcode = input.to_string();
    let solved = solve(&mut hasher, &mut passcode);
    println!("{}", solved.len() - input.len());
}

use std::io::BufRead;

fn main() {
    let fin = std::fs::File::open("input.txt").unwrap();
    let reader = std::io::BufReader::new(fin);
    
    let mut num = 5;
    for line in reader.lines() {
        for mv in line.unwrap().chars() {
            num = match mv {
                'U' => if num < 4 { num } else { num - 3},
                'D' => if num > 6 { num } else { num + 3},
                'L' => if num % 3 == 1 { num } else { num - 1},
                'R' => if num % 3 == 0 { num } else { num + 1},
                _   => num
            }
        } 
        println!("{:?}", num);
    }
}

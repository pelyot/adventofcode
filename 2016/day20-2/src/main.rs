#[macro_use] extern crate lazy_static;
extern crate regex;
use std::io::BufRead;
use regex::Regex;

fn read_input(filename: &str) -> Vec<(u32, u32)> {
    let fin  = std::fs::File::open(filename).unwrap();
    let reader = std::io::BufReader::new(fin);
    reader.lines().map(|r| {
        let l = r.unwrap();
        lazy_static! { static ref RE_IN: Regex = Regex::new(r"^(\d+)-(\d+)$").unwrap(); }
        let capture = RE_IN.captures(&l).unwrap();
        let left = capture[1].parse().unwrap();
        let right = capture[2].parse().unwrap();
        (left, right)
    }).collect()
}

fn main() {
    let mut ranges = read_input("input.txt");
    ranges.sort();
    let mut open = vec![(0, std::u32::MAX)];
    for r in ranges {
        assert!(r.0 <= r.1);
        let mut max_iter = open.len();
        let mut i = 0usize;
        while i < max_iter {
            if r.1 < open[i].0 {
                break; // next range to process
            } else if r.0 <= open[i].0 && r.1 < open[i].1 {
                // reduce open
                open[i].0 = r.1 + 1;
                break;
            } else if r.0 <= open[i].0 && r.1 >= open[i].1 {
                // remove open & recurse
                open.remove(i);
                max_iter -= 1;
                // DO NOT DO: i += 1; (since we remove 1 item, i already points to the next one)
            } else if r.0 > open[i].0 && r.1 < open[i].1 {
                let to_insert = (open[i].0.clone(), r.0 -1);
                open.insert(i, to_insert);
                i += 1;
                open[i].0 = r.1 + 1;
                break;
            } else if r.0 <= open[i].1 && r.1 >= open[i].1 {
                open[i].1 = r.0 - 1;
                i += 1;
            } else {
                i += 1;
            }
        }
    }
    assert!(open.len() > 0);
    let mut count: u64 = 0;
    for r in open {
        count += r.1 as u64 - r.0 as u64 + 1;
    }
    println!("count {}", count);
}

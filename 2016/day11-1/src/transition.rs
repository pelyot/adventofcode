use std::fmt;

use element::*;

#[derive(Clone, Copy)]
pub struct Transition<'a> {
    pub elevator: (&'a Element, Option<&'a Element>),
    pub next_level: u8
}

impl<'a> Transition<'a> {
    pub fn is_valid(&self) -> bool {
        match self.elevator.1 {
            Some(e) => are_compatible(self.elevator.0, e),
            None => true
        }
    }
}

impl<'a> fmt::Debug for Transition<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.elevator.1.is_some() {
            write!(f, "[{} {}]->{}", self.elevator.0, self.elevator.1.unwrap(), self.next_level)
        } else {
            write!(f, "[{}]->{}", self.elevator.0, self.next_level)
        }
    }
}
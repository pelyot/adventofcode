use std::fmt;

#[derive(Clone, Copy, Eq, PartialEq, Hash)]
pub enum ElementType {
    Microchip,
    Generator
}

#[derive(Clone, Copy, Eq, PartialEq, Hash)]
pub struct Element {
    pub number: u8,
    pub level: u8,
    pub etype: ElementType
}

impl Element {

    pub fn microchip(number: u8, level: u8) -> Element {
        Element { number: number, level: level, etype: ElementType::Microchip }
    }

    pub fn generator(number: u8, level: u8) -> Element {
        Element { number: number, level: level, etype: ElementType::Generator }
    }
}


pub fn is_valid<'a, I>(elements: I) -> bool
    where I: Iterator<Item=&'a Element> {
    // There are no generators || all chips have their generator
    let (chips, generators) : (Vec<&'a Element>, Vec<&'a Element>) = elements.partition(|e| e.etype == ElementType::Microchip);
    if generators.is_empty() {
        return true;
    } else {
        for c in chips {
            if !generators.contains(&&Element { etype: ElementType::Generator, number: c.number, level: c.level }) {
                return false;
            }
        }
        return true;
    }
}

pub fn are_compatible(e1: &Element, e2: &Element) -> bool {
    e1.etype == e2.etype || e1.number == e2.number
}

impl fmt::Debug for ElementType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            &ElementType::Microchip => 'M',
            &ElementType::Generator => 'G'
        })
    }
}

impl fmt::Display for ElementType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(self, f)
    }
}

impl fmt::Display for Element {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}{}", self.etype, self.number)
    }
}
impl fmt::Debug for Element {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(self, f)
    }
}
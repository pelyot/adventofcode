#[macro_use] extern crate itertools;

mod element;
mod state;
mod transition;

use element::*;
use state::*;
use std::collections::HashSet;
use std::collections::VecDeque;

fn input_state() -> State {
    let mut s = State::new();
    s.elements.push(Element::microchip(0, 0));
    s.elements.push(Element::generator(0, 0));

    s.elements.push(Element::microchip(5, 0));
    s.elements.push(Element::generator(5, 0));
    s.elements.push(Element::microchip(6, 0));
    s.elements.push(Element::generator(6, 0));

    s.elements.push(Element::generator(1, 1));
    s.elements.push(Element::generator(2, 1));
    s.elements.push(Element::generator(3, 1));
    s.elements.push(Element::generator(4, 1));

    s.elements.push(Element::microchip(1, 2));
    s.elements.push(Element::microchip(2, 2));
    s.elements.push(Element::microchip(3, 2));
    s.elements.push(Element::microchip(4, 2));

    s
}

fn solve(initial_state: State) -> Vec<State> {
    let mut closed : HashSet<State> = HashSet::new();
    let mut open : VecDeque<(State, Vec<State>)> = VecDeque::from(vec![(initial_state.clone(), vec![initial_state.clone()])]);

    while let Some((state, solution)) = open.pop_front() {
        if state.is_final() {
            return solution
        } else {
            let succ = state.all_transitions().into_iter()
                        .filter(|t| t.is_valid())
                        .map(|t| state.apply(&t))
                        .filter(|s| s.is_valid());
            for s in succ {
                if closed.contains(&s) { continue }
                closed.insert(s.clone());
                let mut newsolution = solution.clone();
                newsolution.push(s.clone());
                open.push_back((s.clone(), newsolution));
            }
        }
    }
    unreachable!();
}

fn main() {
    //println!("{}", invalid_state());
    let solution = solve(input_state());
    for s in &solution {
        println!("{}", s);
    }
    println!("{}", solution.len());
}

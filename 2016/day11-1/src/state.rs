use std::fmt;
use itertools::join;

use element::*;
use transition::*;

#[derive(Clone, Eq, PartialEq, Hash)]
pub struct State {
    pub elements: Vec<Element>,
    pub elevator_level: u8
}

fn combinations_of_2<'a>(elements: &Vec<&'a Element>) -> Vec<(&'a Element, &'a Element)> {
    iproduct!(elements.iter(), elements.iter())
        .filter(|&(e1,e2)| if e1.etype == e2.etype { e1.number < e2.number } else { e2.etype != ElementType::Microchip })
        .map(|(e1, e2)| (*e1, *e2))
        .collect()
}

impl State {

    pub fn new() -> State {
        State { elements: Vec::new(), elevator_level: 0u8 }
    }

    pub fn print(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for level in 0..4 {
            try!(self.printlvl(level, f));
        }
        write!(f, "({})", self.is_valid())
    }

    fn printlvl(&self, level: u8, f: &mut fmt::Formatter) -> fmt::Result {
        let elevator = if self.elevator_level == level { 'E' } else { '.' };
        writeln!(f, "{} {} {}", level, elevator, join(self.elements.iter().filter(|e| e.level == level), " "))
    }

    pub fn all_transitions<'a>(&'a self) -> Vec<Transition<'a>> {
        let mut transitions = Vec::new();
        let go_down = self.elevator_level > 0 && self.elements.iter().find(|e| e.level == self.elevator_level - 1).is_some(); // Never go down if that level is already empty

        let level_elements = self.elements.iter().filter(|e| e.level == self.elevator_level);
        for e in level_elements {
            if self.elevator_level < 3 {
                transitions.push(Transition{ elevator: (e, None), next_level: self.elevator_level + 1});
            }
            if go_down {        
                transitions.push(Transition{ elevator: (e, None), next_level: self.elevator_level - 1});
            }
        }

        let level_elements2 : Vec<&Element> = self.elements.iter()
                                                .filter(|e| e.level == self.elevator_level)
                                                .collect();
        for (e1, e2) in combinations_of_2(&level_elements2) {
            if self.elevator_level < 3 {
                transitions.push(Transition{ elevator: (e1, Some(e2)), next_level: self.elevator_level + 1});
            }
            if go_down { // Never go down if that level is already empty
                transitions.push(Transition{ elevator: (e1, Some(e2)), next_level: self.elevator_level - 1});
            }
        }

        transitions
    }

    pub fn apply(&self, t: &Transition) -> State {
        let mut newstate = self.clone();
        newstate.elements.iter_mut()
            .find(|&& mut e| e == *t.elevator.0)
            .unwrap().level = t.next_level;
        if let Some(&te) = t.elevator.1 {
            newstate.elements.iter_mut()
                .find(|&& mut e| e == te)
                .unwrap().level = t.next_level;
        }
        newstate.elevator_level = t.next_level;
        newstate
    }

    pub fn is_valid(&self) -> bool {
        for level in 0..4 {
            if ! is_valid(self.elements.iter().filter(|e| e.level == level)) {
                return false;
            }
        }
        true
    }

    pub fn is_final(&self) -> bool {
        self.elements.iter().find(|e| e.level != 3).is_none()
    }
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(self.print(f));
        Ok(())
    }
}

#[macro_use] extern crate lazy_static;
extern crate regex;
use std::io::BufRead;
use regex::Regex;

#[derive(Debug, Clone, Copy)]
struct Bot {
    high: Option<usize>,
    low: Option<usize>,
    give_high_to: Option<usize>,
    give_low_to: Option<usize>
}
impl Bot {
    fn new() -> Bot {
        Bot{high: None, low: None, give_high_to: None, give_low_to: None}
    }
    fn receive_chip(&mut self, chip: usize) -> Result<(),()> {
        match (self.high, self.low) {
            (Some(_), Some(_)) => Err(()),
            (Some(h), None) => { self.set(h, chip); Ok(()) },
            (None, Some(l)) => { self.set(l, chip); Ok(()) },
            (None, None) => { self.low = Some(chip); Ok(()) }
        }
    }
    fn set(&mut self, chip1: usize, chip2: usize) {
        if chip1 < chip2 {
            self.low = Some(chip1);
            self.high = Some(chip2);
        } else {
            self.low = Some(chip2);
            self.high = Some(chip1);
        }
    }
    fn try_give(bots: &mut[Bot], giver_idx: usize) -> bool {
        if bots[giver_idx].high.is_some() && bots[giver_idx].low.is_some() {
            println!("OK: {} {:?}", giver_idx, bots[giver_idx]);
            let high = bots[giver_idx].high.unwrap();
            let low = bots[giver_idx].low.unwrap();
            println!("Bot {} is comparing {}-{} chips", giver_idx, high, low);
            if let Some(high_to) = bots[giver_idx].give_high_to {
                bots[high_to].receive_chip(high).unwrap();
                println!("- : {} {:?}", high_to, bots[giver_idx]);
                bots[giver_idx].high.take();
            }
            if let Some(low_to) = bots[giver_idx].give_low_to {
                bots[low_to].receive_chip(low).unwrap();
                println!("- : {} {:?}", low_to, bots[giver_idx]);
                bots[giver_idx].low.take();
            }
            true
        } else {
            println!("KO: {} {:?}", giver_idx, bots[giver_idx]);
            false
        }
    }
}

const NB_BOTS: usize = 210;
const NB_OUTPUTS: usize = 30;

fn main() {
    let fin = std::fs::File::open("input.txt").expect("Failed to open input file");
    let reader = std::io::BufReader::new(fin);
    let mut bots = Vec::with_capacity(NB_BOTS + NB_OUTPUTS);
    bots.resize(NB_BOTS + NB_OUTPUTS, Bot::new());
    let mut orders = Vec::new();
    for line in reader.lines().map(|line| line.unwrap()) {
        lazy_static! { static ref RE_VALUE: Regex = Regex::new(r"^value (\d+) goes to bot (\d+)$").unwrap(); }
        lazy_static! { static ref RE_GIVE: Regex = Regex::new(r"^bot (\d+) gives low to (output|bot) (\d+) and high to (output|bot) (\d+)$").unwrap(); }
        if let Some(capture) = RE_VALUE.captures(&line) {
            let value = capture[1].parse::<usize>().unwrap();
            let id = capture[2].parse::<usize>().unwrap();
            bots[id].receive_chip(value).unwrap();
            println!("{} receives {} | {:?}", id, value, bots[id]);
        } else if let Some(capture) = RE_GIVE.captures(&line) {
            let giver = capture[1].parse::<usize>().unwrap();
            let low_type = &capture[2];
            let low = capture[3].parse::<usize>().unwrap();
            let high_type = &capture[4];
            let high = capture[5].parse::<usize>().unwrap();
            if low_type == "output" {
                bots[giver].give_low_to = Some(low + NB_BOTS);
            } else {
                bots[giver].give_low_to = Some(low);
            }
            if high_type == "output" {
                bots[giver].give_high_to = Some(high + NB_BOTS);
            } else {
                bots[giver].give_high_to = Some(high);
            }
            orders.push(giver);
            println!("{} {:?}", giver, bots[giver]);
        }
    }
    let mut i =  0;
    loop {
        println!("\nLoop {}", i);
        i += 1;
        let mut changed = false;
        for &o in &orders {
            changed = changed || Bot::try_give(&mut bots, o);
        }
        if changed == false {
            break;
        }
    }
    println!("OUT 0: {:?}", bots[NB_BOTS]);
    println!("OUT 1: {:?}", bots[NB_BOTS + 1]);
    println!("OUT 2: {:?}", bots[NB_BOTS + 2]);
}

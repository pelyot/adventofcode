#[macro_use] extern crate lazy_static;
extern crate regex;
use std::io::{Read, Write};

fn parse_usize(s: &[u8]) -> usize {
    unsafe {
        std::str::from_utf8_unchecked(s).parse().unwrap()
    }
}

fn decompress(input: &[u8]) -> Vec<u8> {
    let mut out = std::io::Cursor::new(Vec::with_capacity(input.len() * 2));
    let mut next_pos = 0;
    lazy_static! {
        static ref RE: regex::bytes::Regex = regex::bytes::Regex::new(r"\((\d+)x(\d+)\)").unwrap();
    }
    let len = if *input.last().unwrap() == b'\n' { input.len() - 1 } else { input.len() };
    for cap in RE.captures_iter(&input[0..len]) {
        let (marker_begin, marker_end) = cap.pos(0).unwrap();
        let size = parse_usize(&cap[1]);
        let reps = parse_usize(&cap[2]);
        if marker_begin < next_pos {
            continue
        }
        out.write(&input[next_pos..marker_begin]).expect("Write failed");
        next_pos = marker_end + size;
        for _ in 0..reps {
            out.write(&input[marker_end..next_pos]).expect("Write failed");
        }
    }
    out.write(&input[next_pos..len]).expect("Write failed");
    println!("{:b} {}", out.get_ref(), out.get_ref().len());
    outbuf
}

fn main() {
    let mut fin = std::fs::File::open("input.txt").expect("Failed to open input file");
    let mut input = Vec::new();
    fin.read_to_end(&mut input).expect("Failed to read input");
    decompress(&input);
}

use std::io::BufRead;
use std::collections::HashMap;

fn main() {
    let fin = std::fs::File::open("input.txt").unwrap();
    let reader = std::io::BufReader::new(fin);
    
    let mut transitions = HashMap::new();
    /*
        1
      2 3 4
    5 6 7 8 9
      A B C
        D
    */
    // (up, down, left, right)
    transitions.insert('1', ('1','3','1','1'));
    transitions.insert('2', ('2','6','2','3'));
    transitions.insert('3', ('1','7','2','4'));
    transitions.insert('4', ('4','8','3','4'));
    transitions.insert('5', ('5','5','5','6'));
    transitions.insert('6', ('2','A','5','7'));
    transitions.insert('7', ('3','B','6','8'));
    transitions.insert('8', ('4','C','7','9'));
    transitions.insert('9', ('9','9','8','9'));
    transitions.insert('A', ('6','A','A','B'));
    transitions.insert('B', ('7','D','A','C'));
    transitions.insert('C', ('8','C','B','C'));
    transitions.insert('D', ('B','D','D','D'));

    let mut num = '5';
    for line in reader.lines() {
        for mv in line.unwrap().chars() {
            num = match mv {
                'U' => transitions[&num].0,
                'D' => transitions[&num].1,
                'L' => transitions[&num].2,
                'R' => transitions[&num].3,
                _   => num
            }
        } 
        println!("{:?}", num);
    }
}

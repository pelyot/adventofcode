use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let fin = File::open("input.txt").unwrap();
    let reader = BufReader::new(fin);
    let mut count = 0;
    for line in reader.lines() {
        let l = line.unwrap();
        let mut it = l.trim().split_whitespace().map(|e| e.parse::<i32>().unwrap());
        let a = it.next().unwrap();
        let b = it.next().unwrap();
        let c = it.next().unwrap();
        count += if a >= b && a >= c && a < b + c { 1 }
            else if b >= c && b >= a && b < c + a { 1 }
            else if c >= b && c >= a && c < b + a { 1 }
            else { 0 };
    }
    println!("{}", count);
}

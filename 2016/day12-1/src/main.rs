#[macro_use] extern crate lazy_static;
extern crate regex;
use regex::Regex;

#[derive(Debug)]
enum Register { A, B, C, D }

impl Register {

    fn parse(c: char) -> Register {
        match c {
            'a' => Register::A,
            'b' => Register::B,
            'c' => Register::C,
            'd' => Register::D,
            _   => panic!("Unparsable register")
        }
    }

}

#[derive(Debug)]
enum Instruction {
    CpyReg(Register, Register),
    CpyVal(i64, Register),
    Inc(Register),
    Dec(Register),
    JnzReg(Register, i64),
    JnzVal(i64, i64)
}

#[derive(Debug)]
struct Computer {
    reg_a: i64,
    reg_b: i64,
    reg_c: i64,
    reg_d: i64,
    pc: i64
}

impl Computer {

    fn new() -> Computer {
        Computer{ reg_a: 0, reg_b: 0, reg_c: 1, reg_d: 0, pc: 0}
    }

    fn execute(&mut self, i: &Instruction)  {
        match *i {
            Instruction::CpyReg(ref src, ref dst) => { *self.register_mut(dst) = self.register(src); self.pc += 1; },
            Instruction::CpyVal(ref val, ref dst) => { *self.register_mut(dst) = *val; self.pc += 1; },
            Instruction::Dec(ref target)          => { *self.register_mut(target) -= 1; self.pc += 1; },
            Instruction::Inc(ref target)          => { *self.register_mut(target) += 1; self.pc += 1; },
            Instruction::JnzReg(ref reg, offset)     => if self.register(reg) != 0 { self.pc += offset; } else { self.pc  += 1 },
            Instruction::JnzVal(val, offset)     => if val != 0 { self.pc += offset; } else { self.pc  += 1 },
        }
    }

    fn register_mut(&mut self, r: &Register) -> &mut i64 {
        match *r {
            Register::A => &mut self.reg_a,
            Register::B => &mut self.reg_b,
            Register::C => &mut self.reg_c,
            Register::D => &mut self.reg_d
        }
    }

    fn register(&self, r: &Register) -> i64 {
        match *r {
            Register::A => self.reg_a,
            Register::B => self.reg_b,
            Register::C => self.reg_c,
            Register::D => self.reg_d
        }
    }

    fn run(&mut self, program: &Vec<Instruction>) -> i64 {
        self.pc = 0;
        let end = program.len() as i64;
        while self.pc < end {
            let pc = self.pc;
            self.execute(&program[pc as usize]);
        }
        self.reg_a
    }
}

impl Instruction {

    fn parse(line: &str) -> Instruction {
        lazy_static! { static ref RE_CPY_REG: Regex = Regex::new(r"^cpy ([abcd]) ([abcd])\n?$").unwrap(); }
        lazy_static! { static ref RE_CPY_VAL: Regex = Regex::new(r"^cpy (\d+) ([abcd])\n?$").unwrap(); }
        lazy_static! { static ref RE_INC: Regex = Regex::new(r"^inc ([abcd])\n?$").unwrap(); }
        lazy_static! { static ref RE_DEC: Regex = Regex::new(r"^dec ([abcd])\n?$").unwrap(); }
        lazy_static! { static ref RE_JNZ_REG: Regex = Regex::new(r"^jnz ([abcd]) (-?\d+)\n?$").unwrap(); }
        lazy_static! { static ref RE_JNZ_VAL: Regex = Regex::new(r"^jnz (-?\d+) (-?\d+)\n?$").unwrap(); }

        if let Some(capture) = RE_CPY_REG.captures(&line) {
            let src = Register::parse(capture[1].chars().nth(0).unwrap());
            let dst = Register::parse(capture[2].chars().nth(0).unwrap());
            Instruction::CpyReg(src, dst)
        } else if let Some(capture) = RE_CPY_VAL.captures(&line) {
            let val = capture[1].parse::<i64>().unwrap();
            let dst = Register::parse(capture[2].chars().nth(0).unwrap());
            Instruction::CpyVal(val, dst)
        } else if let Some(capture) = RE_INC.captures(&line) {
            let target = Register::parse(capture[1].chars().nth(0).unwrap());
            Instruction::Inc(target)
        } else if let Some(capture) = RE_DEC.captures(&line) {
            let target = Register::parse(capture[1].chars().nth(0).unwrap());
            Instruction::Dec(target)
        } else if let Some(capture) = RE_JNZ_REG.captures(&line) {
            let cond = Register::parse(capture[1].chars().nth(0).unwrap());
            let offset = capture[2].parse::<i64>().unwrap();
            Instruction::JnzReg(cond, offset)
        } else if let Some(capture) = RE_JNZ_VAL.captures(&line) {
            let cond = capture[1].parse::<i64>().unwrap();
            let offset = capture[2].parse::<i64>().unwrap();
            Instruction::JnzVal(cond, offset)
        } else {
            panic!("Failed to parse Instruction");
        }
    }
}

fn load_program(text: &str) -> Vec<Instruction> {
    let mut program = Vec::new();
    program.extend(text.split('\n').map(|s| Instruction::parse(s)));
    program
}

fn debug_program() -> Vec<Instruction> {
    let prog = "cpy 41 a
inc a
inc a
dec a
jnz a 2
dec a";
    load_program(prog)
}

fn input_program() -> Vec<Instruction> {
    load_program(&"cpy 1 a
cpy 1 b
cpy 26 d
jnz c 2
jnz 1 5
cpy 7 c
inc d
dec c
jnz c -2
cpy a c
inc a
dec b
jnz b -2
cpy c b
dec d
jnz d -6
cpy 17 c
cpy 18 d
inc a
dec d
jnz d -2
dec c
jnz c -5")
}

fn main() {
    let mut vm = Computer::new();
    let res = vm.run(&input_program());
    println!("{}", res);
}


use std::iter::FromIterator;

fn grow(data: &mut Vec<bool>) {
    let final_size = data.len() * 2 + 1;
    data.reserve(final_size);
    let len_a = data.len();
    data.push(false);
    for i in 0..len_a {
        let bit = !data[len_a - 1 - i];
        data.push(bit);
    }
}

fn grow_until(data: &mut Vec<bool>, size: usize) {
    while data.len() < size {
        grow(data);
    }
    data.truncate(size);
}

fn as_string(data: &[bool]) -> String {
    data.iter().map(|&b| if b  { '1' } else { '0' }).collect::<String>()
}

fn checksum_reduce(data: &[bool]) -> Vec<bool> {
    data.chunks(2).map(|c| ! c[0] == c[1]).collect()
}

fn checksum(data: &[bool]) -> Vec<bool> {
    let mut current : Vec<bool> = Vec::from_iter(data.iter().cloned());
    while current.len() % 2 == 0 {
        current = current.as_slice().chunks(2).map(|c| c[0] == c[1]).collect()
    }
    current
}

fn from_string(s: &str) -> Vec<bool> {
    s.chars().map(|c| if c == '1' { true } else { false }).collect()
}

fn print(data: &[bool]) {
    println!("{}", as_string(&data));
}

fn main() {
    let mut data : Vec<bool> = from_string("01111001100111011");
    grow_until(&mut data, 35651584);
    print(&checksum(&data));
}

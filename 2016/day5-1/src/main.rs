extern crate crypto;
use std::io::Write;
use crypto::md5::Md5;
use crypto::digest::Digest;

static INPUT : &'static [u8] = b"uqwqemis";

fn main() {
    let mut pwd = String::new();
    let mut iteration = 0;
    let mut buf = Vec::with_capacity(8);
    let mut hasher = Md5::new();
    while pwd.len() < 8 {
        hasher.input(INPUT);
        buf.clear();
        write!(buf, "{}", iteration).expect("Failed to write");
        hasher.input(&buf);
        let mut output = [0u8; 16];
        hasher.result(&mut output);
        let first_five = output[0] | output[1] | (output[2] >> 4);
        if first_five == 0 {
            let ch = format!("{:x}", output[2] & 0x0F).chars().next().unwrap();
            println!("Iteration {} yielded {:?} ({})", iteration, output, ch);    
            pwd.push(ch);
        }
        if iteration % (1 << 18) == 0 {
            println!("Iteration {}", iteration);
        }
        iteration += 1;
        hasher.reset();
    }
    println!("{:?}", pwd);
}

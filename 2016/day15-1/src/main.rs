const WHEELS : &'static [i64] = &[7, 13, 3, 5, 17, 19];

fn main() {
    let mut times = &mut [-1, -2, -2, -1, -5, -13];
    let mut t = times[0];
    let mut loops = 0;
    while !times.iter().all(|&p| p == t) {
        if loops % 100000 == 0 {
            println!("{}", loops);
        }
        if loops % 1000 == 0 {
            print!(".");
        }
        loops += 1;
        t += WHEELS[0];
        for i in 0..WHEELS.len() {
            while times[i] < t {
                times[i] += WHEELS[i];
            }
        }
    }
    println!("");
    println!("{}", t);
}

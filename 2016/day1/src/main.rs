use std::io::Read;
use std::collections::HashSet;

#[derive(Debug)]
enum Dir {
    North, South, East, West
}

impl Dir {
    fn left(&self) -> Dir {
        match *self {
            Dir::North => Dir::West,
            Dir::South => Dir::East,
            Dir::East => Dir::North,
            Dir::West => Dir::South
        }
    }    
    fn right(&self) -> Dir {
        match *self {
            Dir::North => Dir::East,
            Dir::South => Dir::West,
            Dir::East => Dir::South,
            Dir::West => Dir::North
        }
    }
    fn turn(&self, dir: &str) -> Dir {
        match dir {
            "L" => self.left(),
            "R" => self.right(),
            &_ => panic!("unreachable")
        } 
    }
}

#[derive(Debug)]
struct State {
    x: i32,
    y: i32,
    dir: Dir,
    visited: HashSet<(i32, i32)>
}

impl State {
    fn advance(&mut self, turn: &str, distance: i32) -> Option<(i32, i32)> {
        self.dir = self.dir.turn(turn);
        self.move_by(distance)
    }
    fn move_by(&mut self, mut distance: i32) -> Option<(i32, i32)> {
        loop {
            match self.dir {
                Dir::North => self.y += 1,
                Dir::South => self.y -= 1,
                Dir::East => self.x += 1,
                Dir::West => self.x -= 1
            };
            if self.visited.contains(&(self.x, self.y)) {
                return Some((self.x, self.y))
            }
            self.visited.insert((self.x, self.y));
            distance -= 1;
            if distance == 0 {
                break;
            }
        }
        None
    }
    fn new() -> State {
        let mut state = State{x:0, y:0, dir: Dir::North, visited: HashSet::new()};
        state.visited.insert((state.x, state.y));
        state
    }
}

fn main() {
    let fin = std::fs::File::open("input.txt").unwrap();
    let mut reader = std::io::BufReader::new(fin);
    let mut inbuf = String::new();
    reader.read_to_string(&mut inbuf).expect("Failed to read input");
    let mut state = State::new();
    let instructions: Vec<_> = inbuf.trim_right().split(", ").map(|i| {
        let (turn, diststr) = i.split_at(1);
        let distance = diststr.parse::<i32>().unwrap();
        (turn, distance)
    }).collect();
    for i in instructions {
        let (turn, distance) = i;
        if let Some((_, _)) = state.advance(turn, distance) {
            break;        
        }
    }
    println!("{:?}", state);
    println!("{}", state.x.abs() + state.y.abs());
}

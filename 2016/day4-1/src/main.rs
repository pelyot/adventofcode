#[macro_use] extern crate lazy_static;
extern crate regex;
use regex::Regex;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn char_to_index(c: char) -> usize {
    (c as u8 - 'a' as u8) as usize
}

fn index_to_char(idx: usize) -> char {
    (idx as u8 + 'a' as u8) as char
}

fn calculate_checksum(letters: &str) -> String {
    let mut hist = [0; 26];
    for c in letters.chars() {
        hist[char_to_index(c)] += 1;
    }
    let mut hist_vals = hist.iter().enumerate()
        .map(|(i, e)| (index_to_char(i), e))
        .rev().collect::<Vec<_>>();
    hist_vals.sort_by_key(|&(_, count)| count);
    hist_vals.iter().rev()
        .take(5)
        .map(|&(ch, _)| ch)
        .collect()
}

fn rotate(c: char, s: usize) -> char {
    match c {
        '-' => ' ',
        ch => index_to_char((char_to_index(ch) + s) % 26)
    }
}

fn main() {
    let fin = File::open("input.txt").expect("Cannot open input file");
    let reader = BufReader::new(fin);
    let mut count = 0;
    let mut north_pole_sector_id = 0;
    for line in reader.lines().map(|e| e.unwrap()) {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^((?:[a-z]+-)+)(\d+)\[([a-z]+)\]$").unwrap();
        }
        let captures = RE.captures(&line).unwrap();
        let letters = &captures[1];
        let only_letters = &letters.chars().filter(|&e| e != '-').collect::<String>();
        let sectorid = captures[2].parse::<usize>().unwrap();
        let checksum = &captures[3];
        let calculated_checksum = calculate_checksum(&only_letters);
        if checksum == calculated_checksum {
            count += sectorid;
            let decoded = &letters.chars().map(|ch| rotate(ch, sectorid)).collect::<String>();
            println!("Winner ==> {} {} {} {} {:?}", &letters, &sectorid, &checksum, calculated_checksum, &decoded);
            if decoded == "northpole object storage " {
                north_pole_sector_id = sectorid;
            }
        }
    }
    println!("North Pole items are in sector {}", north_pole_sector_id); 
    println!("{}", count);
}

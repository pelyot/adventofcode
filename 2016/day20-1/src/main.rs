#[macro_use] extern crate lazy_static;
extern crate regex;
use std::io::BufRead;
use regex::Regex;

fn read_input(filename: &str) -> Vec<(u32, u32)> {
    let fin  = std::fs::File::open(filename).unwrap();
    let reader = std::io::BufReader::new(fin);
    reader.lines().map(|r| {
        let l = r.unwrap();
        lazy_static! { static ref RE_IN: Regex = Regex::new(r"^(\d+)-(\d+)$").unwrap(); }
        let capture = RE_IN.captures(&l).unwrap();
        let left = capture[1].parse().unwrap();
        let right = capture[2].parse().unwrap();
        (left, right)
    }).collect()
}

fn main() {
    let mut ranges = read_input("input.txt");
    ranges.sort();
    let mut lowest = 0u32;
    for r in ranges {
        if r.0 > lowest {
            break;
        } else {
            lowest = r.1 + 1;
            println!("new lowest: {}", lowest);
        }
    }
    println!("{}", lowest);
}

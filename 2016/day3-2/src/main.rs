use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let fin = File::open("input.txt").unwrap();
    let reader = BufReader::new(fin);
    let mut lines = reader.lines().map(|e| e.unwrap().trim().split_whitespace().map(|e| e.parse::<i32>().unwrap()).collect::<Vec<_>>());
    
    let mut count = 0;
    while let Some(la) = lines.next() {
        let lb = lines.next().unwrap();
        let lc = lines.next().unwrap();
        for i in 0..3 {
            let a = la[i];
            let b = lb[i];
            let c = lc[i];
            count += if a >= b && a >= c && a < b + c { 1 }
                else if b >= c && b >= a && b < c + a { 1 }
                else if c >= b && c >= a && c < b + a { 1 }
                else { 0 };
        }
    }
    println!("{:?}", count); 
}

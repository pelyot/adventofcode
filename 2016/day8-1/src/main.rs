extern crate regex;
use std::io::BufRead;

const DISPLAY_ROWS: usize = 6;
const DISPLAY_COLUMNS: usize = 50;

struct Display {
    buf: [[bool; DISPLAY_COLUMNS]; DISPLAY_ROWS]
}
impl Display {
    fn new() -> Display {
        Display{buf: [[false; DISPLAY_COLUMNS]; DISPLAY_ROWS]}
    }
    fn rotate_column(&mut self, col: usize, by: usize) {
        let mut column = [false; DISPLAY_ROWS];
        for y in 0..DISPLAY_ROWS {
            column[y] = self.buf[y][col];
        }
        for (y, &cell) in column.iter().rev().cycle().skip(by).take(DISPLAY_ROWS).enumerate() {
            self.buf[DISPLAY_ROWS - 1 - y][col] = cell;
        }
    }
    fn rotate_row(&mut self, row: usize, by: usize) {
        let mut row_tmp = [false; DISPLAY_COLUMNS];
        for x in 0..DISPLAY_COLUMNS {
            row_tmp[x] = self.buf[row][x];
        }
        for (x, &cell) in row_tmp.iter().rev().cycle().skip(by).take(DISPLAY_COLUMNS).enumerate() {
            self.buf[row][DISPLAY_COLUMNS - 1 - x] = cell;
        }
    }
    fn fill_rect(&mut self, width: usize, height: usize) {
        for y in 0..height {
            for x in 0..width {
                self.buf[y][x] = true;
            }
        }
    } 
    fn count(&self) -> usize {
        self.buf.iter().map(|row| {
            let s: usize = row.iter().map(|c| if *c { 1 } else { 0 }).sum();
            s
        }).sum()
    }
}
impl std::fmt::Display for Display {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for col in self.buf.iter() {
            for cell in col.iter() {
                write!(f, "{}", if *cell { '#' } else { '.' });
            }
            writeln!(f, "");
        }
        Ok(())
    }
}

fn main() {
    let fin = std::fs::File::open("input.txt").expect("Failed to open input file");
    let reader = std::io::BufReader::new(fin);
    let re_rect = regex::Regex::new(r"^rect (\d+)x(\d+)$").unwrap();
    let re_rotc = regex::Regex::new(r"^rotate column x=(\d+) by (\d+)$").unwrap();
    let re_rotr = regex::Regex::new(r"^rotate row y=(\d+) by (\d+)$").unwrap();
    let mut display = Display::new();
    for line in reader.lines().map(|l| l.unwrap()) {
        if let Some(caps) = re_rect.captures(&line) {
            let a = caps[1].parse::<usize>().unwrap();
            let b = caps[2].parse::<usize>().unwrap();
            display.fill_rect(a, b);
            println!("Rect {} {}", a, b);
        } else if let Some(caps) = re_rotc.captures(&line) {
            let x = caps[1].parse::<usize>().unwrap();
            let by = caps[2].parse::<usize>().unwrap();
            display.rotate_column(x, by);
            println!("rot col {} by {}", x, by);
        } else if let Some(caps) = re_rotr.captures(&line)  {
            let y = caps[1].parse::<usize>().unwrap();
            let by = caps[2].parse::<usize>().unwrap();
            display.rotate_row(y, by);
            println!("rot row {} by {}", y, by);
        }
        println!("{}", display);
    }
    println!("Count: {}", display.count());
}

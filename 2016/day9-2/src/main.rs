#[macro_use] extern crate lazy_static;
extern crate regex;
use std::io::Read;
use std::time::{Duration, Instant};

fn parse_usize(s: &[u8]) -> usize {
    unsafe {
        std::str::from_utf8_unchecked(s).parse().unwrap()
    }
}

fn decompress(input: &[u8]) -> usize {
    let mut count: usize = 0;
    let mut next_pos = 0;
    lazy_static! {
        static ref RE: regex::bytes::Regex = regex::bytes::Regex::new(r"\((\d+)x(\d+)\)").unwrap();
    }
    let len = if *input.last().unwrap() == b'\n' { input.len() - 1 } else { input.len() };
    for cap in RE.captures_iter(&input[0..len]) {
        let (marker_begin, marker_end) = cap.pos(0).unwrap();
        let size = parse_usize(&cap[1]);
        let reps = parse_usize(&cap[2]);
        if marker_begin < next_pos {
            continue
        }
        count += marker_begin - next_pos;
        next_pos = marker_end + size;
        count += reps * decompress(&input[marker_end..next_pos]);
    }
    count += len - next_pos;
    count
}

fn main() {
    let mut fin = std::fs::File::open("input.txt").expect("Failed to open input file");
    let mut input = Vec::new();
    fin.read_to_end(&mut input).expect("Failed to read input");
    let now = Instant::now();
    println!("{}", decompress(&input));
    println!("{:?}", now.elapsed());
}

fn input_to_vec(s: &str) -> Vec<bool> {
    s.chars().map(|c| c == '^').collect()
}

fn print(v: &[bool]) {
    let s = v[1..v.len() - 1].iter().map(|&b| if b  { '^' } else { '.' }).collect::<String>();
    println!("{}", s);
}

fn main() {
    let input = input_to_vec(".^..^....^....^^.^^.^.^^.^.....^.^..^...^^^^^^.^^^^.^.^^^^^^^.^^^^^..^.^^^.^^..^.^^.^....^.^...^^.^.");
    const NB_ROWS: usize = 400_000;
    let mut prev = vec![false; input.len() + 2];
    prev[1..input.len() + 1].copy_from_slice(&input);
    let mut count : usize = prev.iter().filter(|&&c| !c).count() - 2; // -2 accounts for the padding on the sides
    let mut current = vec![false; input.len() + 2];

    for _ in 1..NB_ROWS {
        current[1..input.len() + 1].copy_from_slice(prev.as_slice().windows(3).map(|w| w[0] != w[2]).collect::<Vec<bool>>().as_slice());
        count += current.iter().filter(|&&c| !c).count() - 2; // -2 to account for the padding on the sides
        std::mem::swap(&mut prev, &mut current);
    }
    println!("{}", count);
}

use std::io::BufRead;

fn is_aba(s: &[u8]) -> bool {
    s[0] == s[2] && s[0] != s[1]
}

struct State {
    hypernet: bool,
    supernet_abas: Vec<(u8, u8)>,
    hypernet_babs: Vec<(u8, u8)>
}

impl State {
    fn new() -> State {
        State{ hypernet: false, supernet_abas: Vec::new(), hypernet_babs: Vec::new()}
    }
    fn ssl_support(&self) -> bool {
        for &(a, b) in &self.supernet_abas {
            if self.hypernet_babs.iter().find(|&&e| e == (b, a)).is_some() {
                return true
            }
        }
        false
    } 
}

fn main() {
    let fin = std::fs::File::open("input.txt").expect("Failed to open input");
    let reader = std::io::BufReader::new(fin);
    let count = reader.lines().filter(|l| {
        let mut st = State::new();
        for w in l.as_ref().unwrap().as_bytes().windows(3) {
            match w[0] {
                b'[' => st.hypernet = true,
                b']' => st.hypernet = false,
                 _  => if is_aba(w) {
                            if st.hypernet {
                                st.hypernet_babs.push((w[0], w[1]));
                            } else {
                                st.supernet_abas.push((w[0], w[1]));
                            }
                        }
            }
        }
        st.ssl_support()
    }).count();
    println!("{}", count);
}

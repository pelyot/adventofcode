use std::{
    collections::HashSet,
    io::{stdin, BufRead},
};

fn main() {
    let input = stdin().lock().lines().flatten().next().unwrap();
    println!("{}", silver(&input));
    println!("{}", gold(&input));
}

fn silver(input: &str) -> usize {
    input
        .as_bytes()
        .windows(4)
        .position(|w| {
            w[3] != w[2]
                && w[3] != w[1]
                && w[3] != w[0]
                && w[2] != w[1]
                && w[2] != w[0]
                && w[1] != w[0]
        })
        .unwrap()
        + 4
}

fn gold(input: &str) -> usize {
    input
        .as_bytes()
        .windows(14)
        .position(|w| HashSet::<u8>::from_iter(w.iter().copied()).len() == 14)
        .unwrap()
        + 14
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_silver() {
        assert_eq!(silver(&"bvwbjplbgvbhsrlpgdmjqwftvncz"), 5);
        assert_eq!(silver(&"nppdvjthqldpwncqszvftbrmjlhg"), 6);
        assert_eq!(silver(&"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"), 10);
        assert_eq!(silver(&"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), 11);
    }

    #[test]
    fn test_gold() {
        assert_eq!(gold(&"mjqjpqmgbljsphdztnvjfqwrcgsmlb"), 19);
        assert_eq!(gold(&"bvwbjplbgvbhsrlpgdmjqwftvncz"), 23);
        assert_eq!(gold(&"nppdvjthqldpwncqszvftbrmjlhg"), 23);
        assert_eq!(gold(&"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"), 29);
        assert_eq!(gold(&"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), 26);
    }
}

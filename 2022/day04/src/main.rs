use std::fmt::Display;
use std::num::ParseIntError;
use std::str::FromStr;

const INPUT: &'static str = include_str!("input.txt");

fn main() {
    println!("{}", silver(INPUT));
    println!("{}", gold(INPUT));
}

fn silver(input: &str) -> usize {
    parse_input(input)
        .map(|(l, r)| (l.includes(&r) || r.includes(&l)) as usize)
        .sum()
}

fn gold(input: &str) -> usize {
    parse_input(input)
        .map(|(l, r)| l.overlaps(&r) as usize)
        .sum()
}

struct Section {
    start: usize,
    end: usize,
}

impl Section {
    fn includes(&self, other: &Section) -> bool {
        self.start <= other.start && self.end >= other.end
    }
    fn contains(&self, e: usize) -> bool {
        self.start <= e && self.end >= e
    }
    fn overlaps(&self, other: &Section) -> bool {
        self.contains(other.start) || self.contains(other.end) || other.includes(&self)
    }
}

impl FromStr for Section {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (l, r) = s.split_once('-').ok_or(ParseError)?;
        let start = l.parse::<usize>()?;
        let end = r.parse::<usize>()?;
        Ok(Section { start, end })
    }
}

#[derive(Debug)]
struct ParseError;

impl Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("parse error")?;
        Ok(())
    }
}

impl From<ParseIntError> for ParseError {
    fn from(_: ParseIntError) -> Self {
        ParseError
    }
}

fn parse_input<'a>(input: &'a str) -> impl Iterator<Item = (Section, Section)> + 'a {
    input.lines().map(|line| {
        let (l, r) = line.split_once(',').unwrap();
        let l = l.parse::<Section>().unwrap();
        let r = r.parse::<Section>().unwrap();
        (l, r)
    })
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn run_silver() {
        assert_eq!(silver(INPUT), 509);
    }

    #[test]
    fn run_gold() {
        assert_eq!(gold(INPUT), 870);
    }

    #[test]
    fn test_gold() {
        assert_eq!(gold(TEST_INPUT), 4);
    }

    const TEST_INPUT: &'static str = &"2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";
}

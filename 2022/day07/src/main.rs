use inpt::{inpt, Inpt};
use std::{
    cell::RefCell,
    collections::HashMap,
    io::{stdin, BufRead},
    ops::{AddAssign, Deref},
    rc::Rc,
};

#[derive(Debug, PartialEq)]
struct Dir {
    size: usize,
    dirs: HashMap<String, Rc<RefCell<Dir>>>,
    files_size: usize,
    parent: Option<Rc<RefCell<Dir>>>,
}

impl Dir {
    fn new(parent: Option<Rc<RefCell<Dir>>>) -> Dir {
        Self {
            size: 0,
            dirs: HashMap::new(),
            files_size: 0,
            parent,
        }
    }
}

#[derive(Inpt, Debug, PartialEq)]
enum InputLine {
    #[inpt(regex = r"\$ ls")]
    Ls,
    #[inpt(regex = r"\$ cd ..")]
    Up,
    #[inpt(regex = r"\$ cd (.+)")]
    Cd(String),
    #[inpt(regex = r"(\d+) .+")]
    File(usize),
    #[inpt(regex = r"dir (.+)")]
    Dir(String),
}

fn main() {
    let root = Rc::new(RefCell::new(Dir::new(None)));
    let mut current_dir = root.clone();
    for line in stdin()
        .lock()
        .lines()
        .flatten()
        .skip(1)
        .map(|l| inpt::<InputLine>(&l).unwrap())
    {
        match line {
            InputLine::Up => {
                let parent = current_dir.borrow().parent.as_ref().unwrap().clone();
                current_dir = parent;
            }
            InputLine::Cd(name) => {
                let child = current_dir.borrow().dirs[&name].clone();
                current_dir = child;
            }
            InputLine::Ls => (),
            InputLine::Dir(name) => {
                current_dir
                    .deref()
                    .borrow_mut()
                    .dirs
                    .entry(name)
                    .or_insert(Rc::new(RefCell::new(Dir::new(Some(current_dir.clone())))));
            }
            InputLine::File(size) => {
                current_dir.deref().borrow_mut().files_size.add_assign(size);
            }
        };
    }
    println!("{}", silver(&mut root.deref().borrow_mut()));
    println!("{}", gold(&root.deref().borrow_mut()));
}

fn silver(root: &mut Dir) -> usize {
    let mut acc = 0;
    silver_rec(root, &mut acc);
    acc
}

fn silver_rec(dir: &mut Dir, acc: &mut usize) -> usize {
    let dirs_size: usize = dir
        .dirs
        .iter()
        .map(|(_, dir)| silver_rec(&mut dir.deref().borrow_mut(), acc))
        .sum();
    let size = dir.files_size + dirs_size;
    dir.size = size;
    if size <= 100_000 {
        *acc += size;
    }
    size
}

fn gold(root: &Dir) -> usize {
    let available_space = 70_000_000 - root.size;
    let missing_space = 30_000_000 - available_space;
    let mut smallest = root.size;
    gold_rec(root, missing_space, &mut smallest)
}

fn gold_rec(dir: &Dir, needed: usize, smallest_ok: &mut usize) -> usize {
    for (_, d) in &dir.dirs {
        gold_rec(&d.deref().borrow(), needed, smallest_ok);
    }
    if dir.size >= needed && dir.size < *smallest_ok {
        *smallest_ok = dir.size;
    }
    *smallest_ok
}

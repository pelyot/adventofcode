use inpt::{inpt, Inpt};
use std::collections::HashSet;
use std::io::{stdin, BufRead};

#[derive(Debug, PartialEq, Clone, Copy)]
enum Direction {
    NoMove,
    Up,
    Down,
    Left,
    Right,
    UpLeft,
    UpRight,
    DownLeft,
    DownRight,
}

fn tail_move(head_pos: Direction, head_move: Direction) -> Direction {
    match head_pos {
        Direction::NoMove => Direction::NoMove,
        Direction::Left | Direction::Right | Direction::Down | Direction::Up => {
            if head_pos == head_move {
                head_move
            } else {
                Direction::NoMove
            }
        }
        Direction::UpLeft => match head_move {
            Direction::Down | Direction::Right => Direction::NoMove,
            Direction::Up | Direction::Left => Direction::UpLeft,
            _ => panic!("bad move"),
        },
        Direction::UpRight => match head_move {
            Direction::Down | Direction::Left => Direction::NoMove,
            Direction::Up | Direction::Right => Direction::UpRight,
            _ => panic!("bad move"),
        },
        Direction::DownLeft => match head_move {
            Direction::Up | Direction::Right => Direction::NoMove,
            Direction::Down | Direction::Left => Direction::DownLeft,
            _ => panic!("bad move"),
        },
        Direction::DownRight => match head_move {
            Direction::Up | Direction::Left => Direction::NoMove,
            Direction::Down | Direction::Right => Direction::DownRight,
            _ => panic!("bad move"),
        },
    }
}

fn move_point(p: &mut (i32, i32), dir: Direction) {
    match dir {
        Direction::Up => p.1 += 1,
        Direction::Down => p.1 -= 1,
        Direction::Left => p.0 -= 1,
        Direction::Right => p.0 += 1,
        Direction::UpLeft => {
            p.0 -= 1;
            p.1 += 1;
        }
        Direction::UpRight => {
            p.0 += 1;
            p.1 += 1;
        }
        Direction::DownRight => {
            p.0 += 1;
            p.1 -= 1;
        }
        Direction::DownLeft => {
            p.0 -= 1;
            p.1 -= 1;
        }
        Direction::NoMove => (),
    }
}

fn relative_position(head: &(i32, i32), tail: &(i32, i32)) -> Direction {
    match (head.0 - tail.0, head.1 - tail.1) {
        (x, y) if x < 0 && y == 0 => Direction::Left,
        (x, y) if x > 0 && y == 0 => Direction::Right,
        (x, y) if x == 0 && y > 0 => Direction::Up,
        (x, y) if x == 0 && y < 0 => Direction::Down,
        (x, y) if x < 0 && y < 0 => Direction::DownLeft,
        (x, y) if x < 0 && y > 0 => Direction::UpLeft,
        (x, y) if x > 0 && y < 0 => Direction::DownRight,
        (x, y) if x > 0 && y > 0 => Direction::UpRight,
        (x, y) if x == 0 && y == 0 => Direction::NoMove,
        _ => panic!("bad match"),
    }
}

#[derive(Inpt, Debug)]
#[inpt(regex = r"(.) (\d+)")]
struct Move {
    direction: char,
    distance: usize,
}

fn silver(input: &[Move]) -> usize {
    let mut visited = HashSet::<(i32, i32)>::new();
    let mut head: (i32, i32) = (0, 0);
    let mut tail: (i32, i32) = (0, 0);
    visited.insert(tail);
    for m in input {
        let head_move = match m.direction {
            'U' => Direction::Up,
            'D' => Direction::Down,
            'L' => Direction::Left,
            'R' => Direction::Right,
            _ => panic!("bad input"),
        };
        for _ in 0..m.distance {
            let head_pos = relative_position(&head, &tail);
            move_point(&mut tail, tail_move(head_pos, head_move));
            move_point(&mut head, head_move);
            visited.insert(tail);
        }
    }
    visited.len()
}
fn main() {
    let input: Vec<Move> = stdin()
        .lock()
        .lines()
        .flatten()
        .map(|l| inpt::<Move>(&l).unwrap())
        .collect();
    println!("{}", silver(&input));
}

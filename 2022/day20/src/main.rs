use std::io::{stdin, BufRead};

#[derive(Debug)]
struct Elem {
    number: i64,
    prev: usize,
    next: usize,
}

impl Elem {
    fn new(number: i64) -> Self {
        Self {
            number,
            prev: 0,
            next: 0,
        }
    }
}

struct Cursor<'a> {
    file: &'a mut [Elem],
    i: usize,
}

impl<'a> Cursor<'a> {
    fn new(file: &'a mut [Elem], i: usize) -> Self {
        Cursor { file, i }
    }
    fn advance(&self, mut i: usize, shift: i64) -> usize {
        assert!(shift > 0);
        for _ in 0..shift {
            i = self.file[i].next;
        }
        i
    }
    fn move_by(&mut self, shift: i64) {
        let shift = shift % (self.file.len() as i64 - 1);
        let shift = if shift < 0 {
            self.file.len() as i64 - 1 + shift
        } else {
            shift
        };
        if shift == 0 {
            return;
        }

        // unlink element at index self.i
        let old_p = self.file[self.i].prev;
        let old_n = self.file[self.i].next;
        self.file[old_p].next = old_n;
        self.file[old_n].prev = old_p;

        let new_i = self.advance(self.i, shift);

        // insert elem at self.i just after elem at new_i
        let new_next = self.file[new_i].next;
        self.file[new_i].next = self.i;
        self.file[self.i].prev = new_i;
        self.file[self.i].next = new_next;
        self.file[new_next].prev = self.i;
    }
}

fn setup_links(file: &mut [Elem]) {
    let len = file.len();
    for i in 1..file.len() {
        file[i].prev = i - 1;
    }
    for i in 0..file.len() - 1 {
        file[i].next = i + 1;
    }
    file[0].prev = file.len() - 1;
    file[len - 1].next = 0;
}

fn mix(file: &mut [Elem]) {
    for curr in 0..file.len() {
        let shift = file[curr].number;
        let mut c = Cursor::new(file, curr);
        c.move_by(shift);
    }
}

fn extract(file: &[Elem]) -> i64 {
    let mut cur = file.iter().position(|e| e.number == 0).unwrap();
    let mut res = 0;
    for i in 1..=3000 {
        cur = file[cur].next;
        if i % 1000 == 0 {
            res += file[cur].number;
        }
    }
    res
}

fn main() {
    let mut file = stdin()
        .lock()
        .lines()
        .flatten()
        .map(|l| Elem::new(l.parse::<i64>().unwrap()))
        .collect::<Vec<Elem>>();

    // silver
    setup_links(&mut file);
    mix(&mut file);
    println!("silver: {}", extract(&mut file));

    // gold
    setup_links(&mut file);
    for e in file.iter_mut() {
        e.number *= 811589153;
    }
    for _ in 0..10 {
        mix(&mut file);
    }
    println!("gold: {}", extract(&mut file));
}

#[allow(unused)]
fn print(file: &[Elem], start_at: usize) {
    let mut curr = start_at;
    for _ in 0..file.len() {
        print!("{} ", file[curr].number);
        curr = file[curr].next;
    }
    println!();
}

#[allow(unused)]
fn print_dbg(file: &[Elem]) {
    for i in 0..file.len() {
        print!("{:3}", i);
    }
    println!();
    for e in file {
        print!("{:3}", e.number);
    }
    println!();
    for e in file {
        print!("{:3}", file[e.prev].number);
    }
    println!();
    for e in file {
        print!("{:3}", file[e.next].number);
    }
    println!();
}

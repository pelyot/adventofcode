use std::io::{stdin, BufRead};

struct Grid {
    data: Vec<Vec<u8>>,
    w: usize,
    h: usize,
}

fn silver(g: &Grid) -> usize {
    let mut vis = vec![vec![[false; 4]; g.w]; g.h];
    // left <-> right
    for r in 0..g.h {
        let mut m = 0;
        for c in 0..g.w {
            if g.data[r][c] > m {
                vis[r][c][0] = true;
                m = g.data[r][c];
            }
        }
        let mut m = 0;
        for c in (0..g.w).rev() {
            if g.data[r][c] > m {
                vis[r][c][1] = true;
                m = g.data[r][c];
            }
        }
    }
    // up <-> down
    for c in 0..g.w {
        let mut m = 0;
        for r in 0..g.h {
            if g.data[r][c] > m {
                vis[r][c][2] = true;
                m = g.data[r][c];
            }
        }
        let mut m = 0;
        for r in (0..g.h).rev() {
            if g.data[r][c] > m {
                vis[r][c][3] = true;
                m = g.data[r][c];
            }
        }
    }
    vis.iter()
        .map(|row| row.iter().filter(|&&v| v != [false; 4]).count())
        .sum()
}

fn gold(g: &Grid) -> usize {
    let mut score_max = 0;
    for row in 1..g.h - 1 {
        for col in 1..g.w - 1 {
            let mut score = 1;
            let tree = g.data[row][col];

            // up
            let mut view = 0;
            for r in (0..row).rev() {
                view += 1;
                if g.data[r][col] >= tree {
                    break;
                }
            }
            score *= view;

            // left
            view = 0;
            for c in (0..col).rev() {
                view += 1;
                if g.data[row][c] >= tree {
                    break;
                }
            }
            score *= view;

            // down
            view = 0;
            for r in row + 1..g.h {
                view += 1;
                if g.data[r][col] >= tree {
                    break;
                }
            }
            score *= view;

            // right view
            view = 0;
            for c in col + 1..g.w {
                view += 1;
                if g.data[row][c] >= tree {
                    break;
                }
            }
            score *= view;

            score_max = score.max(score_max);
        }
    }
    score_max
}

fn main() {
    let data: Vec<Vec<u8>> = stdin()
        .lock()
        .lines()
        .flatten()
        .map(|l| l.as_bytes().to_vec())
        .collect();
    let g = Grid {
        w: data[0].len(),
        h: data.len(),
        data,
    };
    println!("{}", silver(&g));
    println!("{}", gold(&g));
}

use inpt::{inpt, Inpt};
use std::io::{stdin, BufRead};

#[derive(Inpt, Debug, PartialEq)]
enum Instruction {
    #[inpt(regex = "noop")]
    NoOp,
    #[inpt(regex = r"addx (-?\d+)")]
    Addx(i64),
}

fn main() {
    let mut decoded = Vec::new();
    for i in stdin()
        .lock()
        .lines()
        .flatten()
        .map(|l| inpt::<Instruction>(&l).unwrap())
    {
        decoded.push(Instruction::NoOp);
        match i {
            Instruction::Addx(_) => decoded.push(i),
            _ => (),
        }
    }
    let mut x = 1;
    let mut res = 0;
    let mut screen = Vec::new();
    let mut line = String::new();
    for (i, inst) in decoded.iter().enumerate() {
        let px  = (i%40) as i64;
        match i+1 {
            20 | 60 | 100 | 140 | 180 | 220 => res += (i+1) as i64 * x,
            _ => (),
        }
        let c = if px == x || px == x-1 || px == x+1 {
            '#'
        } else {
            '.'
        };
        line.push(c);
        match inst {
            Instruction::NoOp => (),
            Instruction::Addx(dx) => x += dx,
        }
        if px == 39 {
            screen.push(line);
            line = String::new();
        }
    }

    println!("Silver: {res}");
    println!("Gold:");
    for l in screen {
        println!("{l}");
    }
}

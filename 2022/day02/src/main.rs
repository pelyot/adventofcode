const WIN_POINTS: [[usize; 3]; 3] = [[3, 0, 6], [6, 3, 0], [0, 6, 3]];
const MOVE_FOR_OUTCOME_AGAINST: [[usize; 3]; 3] = [[2, 0, 1], [0, 1, 2], [1, 2, 0]];

const INPUT: &'static str = &include_str!("input");

fn main() {
    println!("{}", solve_silver(INPUT));
    println!("{}", solve_gold(INPUT));
}

fn parse_line(l: &str) -> (usize, usize) {
    let left = (l.as_bytes()[0] - b'A') as usize;
    let right = (l.as_bytes()[2] - b'X') as usize;
    (left, right)
}

fn solve_gold(input: &str) -> usize {
    input
        .lines()
        .map(parse_line)
        .map(|(left, outcome)| {
            let right = MOVE_FOR_OUTCOME_AGAINST[outcome][left];
            right + 1 + WIN_POINTS[right][left]
        })
        .sum()
}

fn solve_silver(input: &str) -> usize {
    input
        .lines()
        .map(parse_line)
        .map(|(left, right)| right + 1 + WIN_POINTS[right][left])
        .sum()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn silver() {
        assert_eq!(solve_silver(INPUT), 14163);
    }

    #[test]
    fn gold() {
        assert_eq!(solve_gold(INPUT), 12091);
    }

    #[test]
    fn test_gold() {
        let input = &"A Y
B X
C Z";
        assert_eq!(solve_gold(input), 12);
    }
}

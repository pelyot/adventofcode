use std::io::{stdin, Read};

struct Map {
    grid: Vec<Vec<char>>,
}

impl Map {
    fn new(map_str: &str) -> Self {
        Map {
            grid: map_str
                .lines()
                .map(|l| l.chars().collect::<Vec<char>>())
                .collect::<Vec<Vec<char>>>(),
        }
    }
    fn wrap_col_top(&self, col: i64) -> i64 {
        for row in 0..self.grid.len() {
            if self.at(row as i64, col) != ' ' {
                println!("Wrapped top col: {col} at row: {}", row);
                return row as i64;
            }
        }
        panic!("no wrap col top");
    }
    fn wrap_col_bottom(&self, col: i64) -> i64 {
        for row in 1..=self.grid.len() {
            let row = (self.grid.len() - row) as i64;
            if self.at(row, col) != ' ' {
                println!("Wrapped bottom col: {col} at row: {}", row);
                return row;
            }
        }
        panic!("no wrap col bottom");
    }
    fn at(&self, r: i64, c: i64) -> char {
        if r < 0 || c < 0 {
            ' '
        } else {
            let r = r as usize;
            let c = c as usize;
            if r >= self.grid.len() {
                ' '
            } else {
                let row = &self.grid[r];
                if c >= row.len() {
                    ' '
                } else {
                    match row[c] {
                        '.' | '>' | '<' | 'v' | '^' => '.',
                        c => c,
                    }
                }
            }
        }
    }
}

#[derive(Debug)]
struct Instruction {
    steps: usize,
    turn: Option<char>,
}

impl Instruction {
    fn new(i_str: &str) -> Self {
        let mut steps = 0;
        let mut turn = None;
        for c in i_str.trim_end().chars() {
            match c {
                'R' | 'L' => turn = Some(c),
                _ => {
                    steps *= 10;
                    steps += (c as u8 - b'0') as usize;
                }
            }
        }
        Instruction { steps, turn }
    }
}

const DIRECTIONS: [(i64, i64); 4] = [(0, 1), (1, 0), (0, -1), (-1, 0)];
const DIRCHARS: [char; 4] = ['>', 'v', '<', '^'];
const DIRNAMES: [&'static str; 4] = [&"right", &"down", &"left", &"up"];

struct Human {
    pos: (i64, i64),
    dir: usize,
}

impl Human {
    fn new(map: &Map) -> Self {
        let c = map.grid[0].iter().position(|cell| *cell == '.').unwrap() as i64;
        Self {
            pos: (0, c),
            dir: 0,
        }
    }
    fn rotate(&mut self, r: char) {
        match r {
            'R' => {
                self.dir += 1;
                self.dir %= 4;
            }
            'L' => {
                self.dir += 4;
                self.dir -= 1;
                self.dir %= 4;
            }
            _ => panic!("bad dir"),
        }
    }
    fn move_steps(&mut self, map: &mut Map, steps: usize) {
        for _ in 0..steps {
            let mut np = (
                self.pos.0 + DIRECTIONS[self.dir].0,
                self.pos.1 + DIRECTIONS[self.dir].1,
            );
            let mut next = map.at(np.0, np.1);
            if next == ' ' {
                println!(
                    "Wrapping: from {:?} going to {np:?} ({})",
                    self.pos, DIRNAMES[self.dir]
                );
                if self.dir == 0 {
                    // going right
                    np.1 = map.grid[np.0 as usize]
                        .iter()
                        .position(|cell| *cell != ' ')
                        .unwrap() as i64;
                } else if self.dir == 1 {
                    // going down
                    np.0 = map.wrap_col_top(np.1)
                } else if self.dir == 2 {
                    // going left
                    np.1 = (map.grid[np.0 as usize].len() - 1) as i64;
                } else {
                    // going up
                    np.0 = map.wrap_col_bottom(np.1)
                }
                next = map.at(np.0, np.1);
            }
            match next {
                '.' => {
                    map.grid[self.pos.0 as usize][self.pos.1 as usize] = DIRCHARS[self.dir];
                    self.pos = np;
                }
                '#' => {
                    println!("blocked by {np:?}");
                    return;
                }
                ' ' => panic!(
                    "bad wrapped move: [{}] ({np:?}) dir: {}",
                    next, DIRNAMES[self.dir]
                ),
                _ => panic!("bad cell"),
            }
        }
    }
    fn move_gold(&mut self, map: &mut Map, steps: usize) {
        for _ in 0..steps {
            let mut np = (
                self.pos.0 + DIRECTIONS[self.dir].0,
                self.pos.1 + DIRECTIONS[self.dir].1,
            );
            let mut next = map.at(np.0, np.1);
            let old_dir = self.dir;
            let mut new_dir: Option<usize> = None;
            if next == ' ' {
                let (wrapped, dir) = wrap_gold(np);
                np = wrapped;
                new_dir = Some(dir);
                next = map.at(np.0, np.1);
            }
            match next {
                '.' => {
                    map.grid[self.pos.0 as usize][self.pos.1 as usize] = DIRCHARS[old_dir];
                    self.pos = np;
                    if let Some(d) = new_dir {
                        self.dir = d;
                    }
                }
                '#' => {
                    println!("blocked by {np:?}");
                    return;
                }
                ' ' => panic!(
                    "bad wrapped move: [{}] ({np:?}) dir: {}",
                    next, DIRNAMES[self.dir]
                ),
                _ => panic!("bad cell"),
            }
        }
    }
    fn extract(&self) -> i64 {
        println!("h row: {}", self.pos.0 + 1);
        println!("h col: {}", self.pos.1 + 1);
        println!("h dir: {}", self.dir);
        (self.pos.0 + 1) * 1000 + (self.pos.1 + 1) * 4 + self.dir as i64
    }
}
fn main() {
    let mut input = String::new();
    stdin().read_to_string(&mut input).unwrap();
    let (map_str, instuction_stream) = input.split_once("\n\n").unwrap();
    let mut map = Map::new(map_str);
    let mut h = Human::new(&map);
    for i_str in instuction_stream.split_inclusive(|c| c == 'L' || c == 'R') {
        let inst = Instruction::new(i_str);
        println!(
            "=> {inst:?} going {} currently at {:?}",
            DIRNAMES[h.dir], h.pos
        );
        h.move_gold(&mut map, inst.steps);
        if let Some(r) = inst.turn {
            h.rotate(r);
        }
    }
    print(&map, &h);
    println!("gold {}", h.extract()); // 34393,141231 too low
}

#[allow(unused)]
fn print(map: &Map, h: &Human) {
    for r in 0..map.grid.len() {
        for c in 0..map.grid[r].len() {
            if r == h.pos.0 as usize && c == h.pos.1 as usize {
                print!("x");
            } else {
                print!("{}", map.grid[r][c]);
            }
        }
        println!();
    }
}

#[derive(Debug)]
enum Tag {
    RedA,
    RedB,
    GreenA,
    GreenB,
    BlueA,
    BlueB,
    BrownA,
    BrownB,
    PinkA,
    PinkB,
    PurpleA,
    PurpleB,
    YellowA,
    YellowB,
}

struct Zone {
    tag: Tag,
    row_range: (i64, i64),
    col_range: (i64, i64),
    sister: usize,
    new_dir: usize,
    reversed: bool,
    entrance: i64,
}

impl Zone {
    fn contains(&self, pos: (i64, i64)) -> bool {
        self.row_range.0 <= pos.0
            && pos.0 < self.row_range.1
            && self.col_range.0 <= pos.1
            && pos.1 < self.col_range.1
    }
    fn is_horizontal(&self) -> bool {
        self.row_range.1 - self.row_range.0 == 1
    }
    fn map_to_zone(&self, pos: (i64, i64)) -> i64 {
        if self.is_horizontal() {
            if self.reversed {
                self.col_range.1 - 1 - pos.1
            } else {
                pos.1 - self.col_range.0
            }
        } else {
            if self.reversed {
                self.row_range.1 - 1 - pos.0
            } else {
                pos.0 - self.row_range.0
            }
        }
    }
    fn map_from_zone(&self, zone_pos: i64) -> (i64, i64) {
        if self.is_horizontal() {
            let row = self.entrance;
            let col = if self.reversed {
                self.col_range.1 - 1 - zone_pos
            } else {
                self.col_range.0 + zone_pos
            };
            (row, col)
        } else {
            let col = self.entrance;
            let row = if self.reversed {
                self.row_range.1 - 1 - zone_pos
            } else {
                self.row_range.0 + zone_pos
            };
            (row, col)
        }
    }
}

const RIGHT: usize = 0;
const DOWN: usize = 1;
const LEFT: usize = 2;
const UP: usize = 3;

const ZONE_: [Zone; 14] = [
    Zone {tag: Tag::RedA, row_range: (-1, 0), col_range: (8, 12), entrance: 0, new_dir: DOWN, sister: 1, reversed: false},
    Zone {tag: Tag::RedB, row_range: (3, 4), col_range: (0, 3), entrance: 4, new_dir: DOWN, sister: 0, reversed: true},
    Zone {tag: Tag::GreenA, row_range: (0, 4), col_range: (12, 13), entrance: 11, new_dir: LEFT, sister: 3, reversed: false},
    Zone {tag: Tag::GreenB, row_range: (8, 12), col_range: (16, 17), entrance: 15, new_dir: LEFT, sister: 2, reversed: true},
    Zone {tag: Tag::BlueA, row_range: (0, 4), col_range: (7, 8), entrance: 8, new_dir: RIGHT, sister: 5, reversed: false},
    Zone {tag: Tag::BlueB, row_range: (3, 4), col_range: (4, 8), entrance: 4, new_dir: DOWN, sister: 4, reversed: false},
    Zone {tag: Tag::PinkA, row_range: (4, 8), col_range: (-1, 0), entrance: 0, new_dir: RIGHT, sister: 7, reversed: false},
    Zone {tag: Tag::PinkB, row_range: (12, 13), col_range: (12, 16), entrance: 11, new_dir: UP, sister: 6, reversed: true},
    Zone {tag: Tag::YellowA, row_range: (8, 9), col_range: (0, 4), entrance: 7, new_dir: UP, sister: 9, reversed: false},
    Zone {tag: Tag::YellowB, row_range: (12, 13), col_range: (8, 12), entrance: 11, new_dir: UP, sister: 8, reversed: true},
    Zone {tag: Tag::BrownA, row_range: (8, 9), col_range: (4, 8), entrance: 7, new_dir: UP, sister: 11, reversed: false},
    Zone {tag: Tag::BrownB, row_range: (8, 12), col_range: (8,9), entrance: 8, new_dir: RIGHT, sister: 10, reversed: true},
    Zone {tag: Tag::PurpleA, row_range: (4, 8), col_range: (12, 13), entrance: 11, new_dir: LEFT, sister: 13, reversed: false},
    Zone {tag: Tag::PurpleB, row_range: (7, 8), col_range: (12, 16), entrance: 8, new_dir: DOWN, sister: 12, reversed: true}
];

const ZONE: [Zone; 14] = [
    Zone {
        entrance: 0,
        reversed: false,
        tag: Tag::RedA,
        row_range: (-1, 0),
        col_range: (50, 100),
        sister: 1,
        new_dir: DOWN,
    },
    Zone {
        entrance: 0,
        reversed: false,
        tag: Tag::RedB,
        row_range: (150, 200),
        col_range: (-1, 0),
        sister: 0,
        new_dir: RIGHT,
    },
    Zone {
        entrance: 0,
        reversed: false,
        tag: Tag::GreenA,
        row_range: (-1, 0),
        col_range: (100, 150),
        sister: 3,
        new_dir: DOWN,
    },
    Zone {
        entrance: 199,
        reversed: false,
        tag: Tag::GreenB,
        row_range: (200, 201),
        col_range: (0, 50),
        sister: 2,
        new_dir: UP,
    },
    Zone {
        entrance: 50,
        reversed: false,
        tag: Tag::BlueA,
        row_range: (0, 50),
        col_range: (49, 50),
        sister: 5,
        new_dir: RIGHT,
    },
    Zone {
        entrance: 0,
        reversed: true,
        tag: Tag::BlueB,
        row_range: (100, 150),
        col_range: (-1, 0),
        sister: 4,
        new_dir: RIGHT,
    },
    Zone {
        entrance: 149,
        reversed: false,
        tag: Tag::PurpleA,
        row_range: (0, 50),
        col_range: (150, 151),
        sister: 7,
        new_dir: LEFT,
    },
    Zone {
        entrance: 99,
        reversed: true,
        tag: Tag::PurpleB,
        row_range: (100, 150),
        col_range: (100, 101),
        sister: 6,
        new_dir: LEFT,
    },
    Zone {
        entrance: 49,
        reversed: false,
        tag: Tag::BrownA,
        row_range: (50, 51),
        col_range: (100, 150),
        sister: 9,
        new_dir: UP,
    },
    Zone {
        entrance: 99,
        reversed: false,
        tag: Tag::BrownB,
        row_range: (50, 100),
        col_range: (100, 101),
        sister: 8,
        new_dir: LEFT,
    },
    Zone {
        entrance: 50,
        reversed: false,
        tag: Tag::PinkA,
        row_range: (50, 100),
        col_range: (49, 50),
        sister: 11,
        new_dir: RIGHT,
    },
    Zone {
        entrance: 100,
        reversed: false,
        tag: Tag::PinkB,
        row_range: (99, 100),
        col_range: (0, 50),
        sister: 10,
        new_dir: DOWN,
    },
    Zone {
        entrance: 149,
        reversed: false,
        tag: Tag::YellowA,
        row_range: (150, 151),
        col_range: (50, 100),
        sister: 13,
        new_dir: UP,
    },
    Zone {
        entrance: 49,
        reversed: false,
        tag: Tag::YellowB,
        row_range: (150, 200),
        col_range: (50, 51),
        sister: 12,
        new_dir: LEFT,
    },
];

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn contains() {
        assert!(ZONE[0].contains((-1, 50)));
        assert!(ZONE[0].contains((-1, 99)));
        assert!(ZONE[0].contains((-1, 70)));
        assert!(!ZONE[0].contains((0, 50)));
        assert!(!ZONE[0].contains((-1, 100)));
        assert!(!ZONE[0].contains((-1, 49)));

        assert!(ZONE[4].contains((0, 49)));
        assert!(ZONE[4].contains((49, 49)));
        assert!(ZONE[4].contains((30, 49)));
        assert!(!ZONE[4].contains((30, 50)));
        assert!(!ZONE[4].contains((30, 48)));
    }

    #[test]
    fn map_to_zone() {
        assert_eq!(ZONE[0].map_to_zone((-1, 50)), 0);
        assert_eq!(ZONE[0].map_to_zone((-1, 99)), 49);

        assert_eq!(ZONE[1].map_to_zone((150, -1)), 0);
        assert_eq!(ZONE[1].map_to_zone((199, -1)), 49);

        assert_eq!(ZONE[5].map_to_zone((100, -1)), 49);
        assert_eq!(ZONE[5].map_to_zone((149, -1)), 0);
    }

    #[test]
    fn map_from_zone() {
        assert_eq!(ZONE[0].map_from_zone(0), (0, 50));
        assert_eq!(ZONE[0].map_from_zone(49), (0, 99));

        assert_eq!(ZONE[1].map_from_zone(0), (150, 0));
        assert_eq!(ZONE[1].map_from_zone(49), (199, 0));

        assert_eq!(ZONE[5].map_from_zone(49), (100, 0));
        assert_eq!(ZONE[5].map_from_zone(0), (149, 0));
    }

    #[test]
    fn wrap_gold() {
        assert_eq!(super::wrap_gold((-1, 50)), ((150, 0), RIGHT)); // Red A
        assert_eq!(super::wrap_gold((199, -1)), ((0, 99), DOWN)); // Red B
        assert_eq!(super::wrap_gold((-1, 110)), ((199, 10), UP)); // Green A
        assert_eq!(super::wrap_gold((200, 49)), ((0, 149), DOWN)); // Green B
        assert_eq!(super::wrap_gold((30, 49)), ((119, 0), RIGHT)); // Blue A
        assert_eq!(super::wrap_gold((149, -1)), ((0, 50), RIGHT)); // Blue B
        assert_eq!(super::wrap_gold((0, 150)), ((149,99), LEFT)); // Purple A
        assert_eq!(super::wrap_gold((100, 100)), ((49,149), LEFT)); // Purple B
        assert_eq!(super::wrap_gold((50, 100)), ((50, 99), LEFT)); // Brown A
        assert_eq!(super::wrap_gold((99, 100)), ((49, 149), UP)); // Brown B
        assert_eq!(super::wrap_gold((74, 49)), ((100, 24), DOWN)); // Pink A
        assert_eq!(super::wrap_gold((99, 26)), ((76, 50), RIGHT)); // Pink B
        assert_eq!(super::wrap_gold((150, 50)), ((150, 49), LEFT)); // Yellow A
        assert_eq!(super::wrap_gold((199, 50)), ((149, 99), UP)); // Yellow A
        //
    }
}

fn wrap_gold(pos: (i64, i64)) -> ((i64, i64), usize) {
    println!("WRAPPING {pos:?}");
    let zone = &ZONE
        .iter()
        .find(|z| z.contains(pos))
        .expect("no wraping zone!");
    let sister_zone = &ZONE[zone.sister];
    let zone_pos = zone.map_to_zone(pos);
    let wrapped_pos = sister_zone.map_from_zone(zone_pos);
    let direction = sister_zone.new_dir;
    println!(
        "Wrapped {pos:?} to {wrapped_pos:?} ({:?}/{zone_pos} to {:?})",
        zone.tag, sister_zone.tag
    );
    assert!(zone_pos < 50, "zone_pos: {zone_pos}");
    assert!(zone_pos >= 0);
    (wrapped_pos, direction)
}

/*
*
* This version contains the solution for the GOLD STAR of day11,
* For the solution to the SILVER STAR, check the commit history of this file
*
* */
#[derive(Debug)]
enum Op {
    Add(usize),
    Mul(usize),
    Square,
}

struct Monkey {
    items: Vec<Item>,
    op: Op,
    test: usize,
    targets: [usize; 2],
    inspections: usize,
}

#[derive(PartialEq, Debug)]
struct Item {
    remains: [u8; 24],
    worry: usize,
}

impl Item {
    fn from(n: usize) -> Self {
        let mut remains = [0; 24];
        for i in [2, 3, 5, 7, 11, 13, 17, 19, 23] {
            remains[i] = (n % i) as u8;
        }
        Item { remains, worry: n }
    }

    fn apply_gold(&mut self, op: &Op) {
        for i in [2, 3, 5, 7, 11, 13, 17, 19, 23] {
            let r = &mut self.remains[i];
            match *op {
                Op::Add(p) => *r = ((*r as usize + p) % i) as u8,
                Op::Mul(p) => *r = ((*r as usize * p) % i) as u8,
                Op::Square => *r = ((*r as usize * *r as usize) % i) as u8,
            }
        }
    }
}

fn solve(monkeys: &mut [Monkey], rounds: usize) -> usize {
    let mut history = vec![vec![]; monkeys.len()];
    for r in 1..rounds + 1 {
        for i in 0..monkeys.len() {
            history[i].push(monkeys[i].inspections);
            while let Some(item) = monkeys[i].items.pop() {
                monkeys[i].inspections += 1;
                let mut worry = item;
                worry.apply_gold(&monkeys[i].op);
                let test = if worry.remains[monkeys[i].test] == 0 {
                    0
                } else {
                    1
                };
                let target = monkeys[i].targets[test];
                monkeys[target].items.insert(0, worry);
            }
        }
    }
    let mut activity: Vec<usize> = monkeys.iter().map(|m| m.inspections).collect();
    activity.sort();
    dbg!(activity[monkeys.len() - 1]) * dbg!(activity[monkeys.len() - 2])
}

fn main() {
    let test_monkeys = [
        // 0
        Monkey {
            items: vec![79, 98]
                .iter_mut()
                .rev()
                .map(|i| Item::from(*i))
                .collect(),
            op: Op::Mul(19),
            test: 23,
            targets: [2, 3],
            inspections: 0,
        },
        // 1
        Monkey {
            items: vec![54, 65, 75, 74]
                .iter_mut()
                .map(|i| Item::from(*i))
                .collect(),
            op: Op::Add(6),
            test: 19,
            targets: [2, 0],
            inspections: 0,
        },
        // 2
        Monkey {
            items: vec![79, 60, 97]
                .iter_mut()
                .map(|i| Item::from(*i))
                .collect(),
            op: Op::Square,
            test: 13,
            targets: [1, 3],
            inspections: 0,
        },
        // 3
        Monkey {
            items: vec![74].iter_mut().rev().map(|i| Item::from(*i)).collect(),
            op: Op::Add(3),
            test: 17,
            targets: [0, 1],
            inspections: 0,
        },
    ];
    let mut monkeys = [
        // 0
        Monkey {
            items: vec![93, 54, 69, 66, 71]
                .iter_mut()
                .map(|i| Item::from(*i))
                .collect(),
            op: Op::Mul(3),
            test: 7,
            targets: [7, 1],
            inspections: 0,
        },
        // 1
        Monkey {
            items: vec![89, 51, 80, 66]
                .iter_mut()
                .map(|i| Item::from(*i))
                .collect(),
            op: Op::Mul(17),
            test: 19,
            targets: [5, 7],
            inspections: 0,
        },
        // 2
        Monkey {
            items: vec![90, 92, 63, 91, 96, 63, 64]
                .iter_mut()
                .map(|i| Item::from(*i))
                .collect(),
            op: Op::Add(1),
            test: 13,
            targets: [4, 3],
            inspections: 0,
        },
        // 3
        Monkey {
            items: vec![65, 77]
                .iter_mut()
                .rev()
                .map(|i| Item::from(*i))
                .collect(),
            op: Op::Add(2),
            test: 3,
            targets: [4, 6],
            inspections: 0,
        },
        // 4
        Monkey {
            items: vec![76, 68, 94]
                .iter_mut()
                .map(|i| Item::from(*i))
                .collect(),
            op: Op::Square,
            test: 2,
            targets: [0, 6],
            inspections: 0,
        },
        // 5
        Monkey {
            items: vec![86, 65, 66, 97, 73, 83]
                .iter_mut()
                .map(|i| Item::from(*i))
                .collect(),
            op: Op::Add(8),
            test: 11,
            targets: [2, 3],
            inspections: 0,
        },
        // 6
        Monkey {
            items: vec![78].iter_mut().rev().map(|i| Item::from(*i)).collect(),
            op: Op::Add(6),
            test: 17,
            targets: [0, 1],
            inspections: 0,
        },
        // 7
        Monkey {
            items: vec![89, 57, 59, 61, 87, 55, 55, 88]
                .iter_mut()
                .map(|i| Item::from(*i))
                .collect(),
            op: Op::Add(7),
            test: 5,
            targets: [2, 5],
            inspections: 0,
        },
    ];
    // println!("{}", solve(&mut monkeys, 3, 20));
    println!("{}", solve(&mut monkeys, 10_000));
    // println!("{}", solve(&mut test_monkeys, 10_000));
    // println!("{}", silver(&mut test_monkeys));
}

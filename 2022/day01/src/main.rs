use std::io::{stdin, BufRead};

fn main() {
    println!("{:?}", solve_part_1_and_2(stdin().lock()));
}

fn solve_part_1_and_2(input: impl BufRead) -> (i64, i64) {
    let mut cals = 0;
    let mut max_cals = 0;
    let mut elves = Vec::new();
    for line in input.lines().flatten() {
        if line.is_empty() {
            max_cals = max_cals.max(cals);
            elves.push(cals);
            cals = 0;
        } else {
            cals += line.parse::<i64>().unwrap();
        }
    }
    elves.sort();
    (max_cals, elves.iter().rev().take(3).sum())
}

use std::collections::HashSet;

const INPUT: &'static str = include_str!("input.txt");

fn main() {
    println!("{}", silver(INPUT));
    println!("{}", gold(INPUT));
}

fn priority(item: u8) -> usize {
    (match item {
        b'a'..=b'z' => item + 1 - b'a',
        b'A'..=b'Z' => item + 27 - b'A',
        _ => panic!("bad char"),
    }) as usize
}

fn silver(input: &str) -> usize {
    input
        .lines()
        .map(|l| {
            let (left, right) = l.as_bytes().split_at(l.len() / 2);
            priority(*left.iter().find(|e| right.contains(e)).unwrap())
        })
        .sum()
}

fn gold(input: &str) -> usize {
    let rucksacks = input.lines().map(str::as_bytes).collect::<Vec<&[u8]>>();
    rucksacks
        .chunks_exact(3)
        .map(|w| {
            w.iter()
                .map(|r| HashSet::from_iter(r.iter().copied()))
                .reduce(|acc, r| HashSet::<u8>::from_iter(acc.intersection(&r).copied()))
                .map(|h| priority(h.into_iter().next().unwrap()))
                .unwrap()
        })
        .sum()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_silver() {
        assert_eq!(silver(TEST_INPUT), 157);
    }

    #[test]
    fn test_gold() {
        assert_eq!(gold(TEST_INPUT), 70);
    }

    #[test]
    fn input_silver() {
        assert_eq!(silver(INPUT), 8072);
    }

    #[test]
    fn input_gold() {
        assert_eq!(gold(INPUT), 2570);
    }

    const TEST_INPUT: &'static str = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";
}

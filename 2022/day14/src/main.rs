use std::io::{stdin, BufRead};

use inpt::{inpt, Inpt};

type Scalar = usize;

#[derive(Inpt, Debug, PartialEq)]
#[inpt(trim = r"[^\d]")]
struct Point {
    x: Scalar,
    y: Scalar,
}

#[derive(Inpt, Debug)]
struct Line {
    points: Vec<Point>,
}

fn make_grid<'a>(
    lines: impl Iterator<Item = &'a Line> + Clone,
) -> (Vec<Vec<char>>, usize, usize, usize) {
    let depth = lines
        .clone()
        .flat_map(|l| l.points.iter())
        .map(|p| p.y)
        .max()
        .unwrap();
    let floor = depth + 2;
    let max_x = 500 + depth + 2;
    let min_x = 500 - depth - 2;
    let mut grid = vec![vec!['.'; max_x + 1]; floor];
    for line in lines {
        let mut start = &line.points[0];
        for end in line.points[1..].iter() {
            if start.x == end.x {
                let start_y = start.y.min(end.y);
                let end_y = start.y.max(end.y);
                for y in start_y..=end_y {
                    grid[y][start.x] = '#';
                }
            }
            if start.y == end.y {
                let start_x = start.x.min(end.x);
                let end_x = start.x.max(end.x);
                for x in start_x..=end_x {
                    grid[start.y][x] = '#';
                }
            }
            start = end;
        }
    }
    grid.push(vec!['#'; max_x + 1]);
    (grid, min_x, max_x, depth)
}

fn silver(mut grid: Vec<Vec<char>>, depth: usize) -> usize {
    let mut units = 0;
    'outer: loop {
        let mut sand = Point { x: 500, y: 0 };
        loop {
            if sand.y >= depth {
                break 'outer;
            } else if grid[sand.y + 1][sand.x] == '.' {
                sand.y += 1;
            } else if grid[sand.y + 1][sand.x - 1] == '.' {
                sand.y += 1;
                sand.x -= 1;
            } else if grid[sand.y + 1][sand.x + 1] == '.' {
                sand.y += 1;
                sand.x += 1;
            } else {
                grid[sand.y][sand.x] = 'o';
                break;
            }
        }
        units += 1;
    }
    units
}

fn gold(mut grid: Vec<Vec<char>>) -> usize {
    let mut units = 0;
    'outer: loop {
        let mut sand = Point { x: 500, y: 0 };
        units += 1;
        loop {
            if grid[sand.y + 1][sand.x] == '.' {
                sand.y += 1;
            } else if grid[sand.y + 1][sand.x - 1] == '.' {
                sand.y += 1;
                sand.x -= 1;
            } else if grid[sand.y + 1][sand.x + 1] == '.' {
                sand.y += 1;
                sand.x += 1;
            } else {
                grid[sand.y][sand.x] = 'o';
                if sand.x == 500 && sand.y == 0 {
                    break 'outer;
                } else {
                    break;
                }
            }
        }
    }
    units
}

fn main() {
    let lines: Vec<Line> = stdin()
        .lock()
        .lines()
        .flatten()
        .map(|l| inpt::<Line>(&l).unwrap())
        .collect();
    let (grid, _min_x, _max_x, depth) = make_grid(lines.iter());
    println!("{}", silver(grid.clone(), depth));
    println!("{}", gold(grid));
}

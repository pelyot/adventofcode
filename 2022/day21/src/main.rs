use inpt::{inpt, Inpt};
use std::collections::HashMap;
use std::io::{stdin, BufRead};
use std::rc::Rc;

#[derive(Inpt, Debug, PartialEq)]
enum MonkeyOp {
    Shout(i64),
    #[inpt(regex = r"([a-z]+) ([\-\+\*/]) ([a-z]+)")]
    Op(String, char, String),
}

#[derive(Inpt, Debug)]
#[inpt(regex = r"([a-z]+): (.*)")]
struct Monkey {
    name: String,
    op: MonkeyOp,
}

impl Monkey {
    fn eval(self: Rc<Self>, m: &HashMap<String, Rc<Monkey>>) -> i64 {
        match self.op {
            MonkeyOp::Shout(n) => n,
            MonkeyOp::Op(ref l, op, ref r) => {
                let l = m.get(l).unwrap().clone().eval(m);
                let r = m.get(r).unwrap().clone().eval(m);
                match op {
                    '-' => l - r,
                    '+' => l + r,
                    '*' => l * r,
                    '/' => l / r,
                    _ => panic!("bad operation"),
                }
            }
        }
    }
    fn children(
        self: Rc<Self>,
        m: &HashMap<String, Rc<Monkey>>,
    ) -> Option<(Rc<Monkey>, Rc<Monkey>)> {
        match self.op {
            MonkeyOp::Shout(_) => None,
            MonkeyOp::Op(ref l, _, ref r) => {
                Some((m.get(l).unwrap().clone(), m.get(r).unwrap().clone()))
            }
        }
    }
    fn has_human(self: Rc<Self>, m: &HashMap<String, Rc<Monkey>>) -> bool {
        match &self.op {
            MonkeyOp::Shout(_) => false,
            MonkeyOp::Op(ref l, _, ref r) => {
                if l == "humn" || r == "humn" {
                    true
                } else {
                    let (l, r) = self.clone().children(m).unwrap();
                    l.has_human(m) || r.has_human(m)
                }
            }
        }
    }
    fn new_target_left_human(old_target: i64, op: char, right: i64) -> i64 {
        match op {
            '-' => old_target + right,
            '+' => old_target - right,
            '/' => old_target * right,
            '*' => old_target / right,
            _ => panic!("bad op"),
        }
    }
    fn new_target_right_human(old_target: i64, op: char, left: i64) -> i64 {
        match op {
            '-' => left - old_target,
            '+' => old_target - left,
            '/' => left / old_target,
            '*' => old_target / left,
            _ => panic!("bad op"),
        }
    }
    fn solve_gold(self: Rc<Self>, target: i64, m: &HashMap<String, Rc<Monkey>>) -> i64 {
        match self.op {
            MonkeyOp::Shout(_) => target,
            MonkeyOp::Op(ref l, op, ref r) => {
                let lm = m.get(l).unwrap().clone();
                let rm = m.get(r).unwrap().clone();
                if l == "humn" {
                    Self::new_target_left_human(target, op, rm.eval(m))
                } else if r == "humn" {
                    Self::new_target_right_human(target, op, lm.eval(m))
                } else {
                    if lm.clone().has_human(m) {
                        let nt = Self::new_target_left_human(target, op, rm.eval(m));
                        lm.solve_gold(nt, m)
                    } else {
                        let nt = Self::new_target_right_human(target, op, lm.eval(m));
                        rm.solve_gold(nt, m)
                    }
                }
            }
        }
    }
}

fn main() {
    let m = stdin()
        .lock()
        .lines()
        .flatten()
        .map(|l| {
            let m = inpt::<Monkey>(&l).unwrap();
            (m.name.clone(), Rc::new(m))
        })
        .collect::<HashMap<String, Rc<Monkey>>>();

    println!("silver: {:?}", m.get("root").unwrap().clone().eval(&m));

    let (l, r) = m.get("root").unwrap().clone().children(&m).unwrap();
    let gold = if l.clone().has_human(&m) {
        let target = r.eval(&m);
        l.solve_gold(target, &m)
    } else {
        let target = l.eval(&m);
        r.solve_gold(target, &m)
    };
    println!("gold: {:?}", gold);
}

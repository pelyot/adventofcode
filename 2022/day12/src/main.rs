use std::io::{stdin, BufRead};

fn input() -> (Vec<Vec<u8>>, (usize, usize), (usize, usize)) {
    let mut grid: Vec<Vec<u8>> = stdin()
        .lock()
        .lines()
        .flatten()
        .map(|l| l.as_bytes().to_vec())
        .collect();
    let w = grid[0].len();
    let h = grid.len();
    let limit = b'z' + 2;
    for r in 0..h {
        grid[r].insert(0, limit);
        grid[r].push(limit);
    }
    grid.insert(0, vec![limit; w + 2]);
    grid.push(vec![limit; w + 2]);

    let w = w + 2;
    let h = h + 2;
    let mut start = (0, 0);
    let mut end = (0, 0);
    for r in 0..h {
        for c in 0..w {
            if grid[r][c] == b'S' {
                start = (r, c);
                grid[r][c] = b'a';
            }
            if grid[r][c] == b'E' {
                end = (r, c);
                grid[r][c] = b'z';
            }
        }
    }
    (grid, start, end)
}

fn silver(grid: &[Vec<u8>], start: (usize, usize), end: (usize, usize)) -> usize {
    let w = grid[0].len();
    let h = grid.len();
    let mut open = vec![(start.0, start.1, 0)];
    let mut visited = vec![vec![usize::MAX; w]; h];
    let mut min_d = usize::MAX;
    while let Some((r, c, d)) = open.pop() {
        visited[r][c] = d;
        if (r, c) == end {
            min_d = min_d.min(d);
            continue;
        }
        for (nr, nc) in [
            (r + 1, c),
            (r - 1, c),
            (r, c + 1),
            (r, c - 1),
        ] {
            if visited[nr][nc] > d + 1 && grid[nr][nc] <= grid[r][c] + 1 {
                open.push((nr, nc, d + 1));
            }
        }
    }
    min_d
}

fn gold(grid: &[Vec<u8>], end: (usize, usize)) -> usize {
    let mut res = usize::MAX;
    for r in 0..grid.len() {
        for c in 0..grid[0].len() {
            if grid[r][c] == b'a' {
                res = res.min(silver(grid, (r, c), end));
            }
        }
    }
    res
}

fn main() {
    let (grid, start, end) = input();
    println!("{}", silver(&grid, start, end));
    println!("{}", gold(&grid, end));
}


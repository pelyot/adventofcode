use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, newline, u64},
    combinator::map,
    error::VerboseError,
    multi::{separated_list0, separated_list1},
    sequence::{preceded, terminated},
    IResult,
};
use std::{
    cmp::Ordering,
    io::{stdin, Read},
};

type ParseResult<'a, T> = IResult<&'a str, T, VerboseError<&'a str>>;

#[derive(Debug, Clone)]
enum Value {
    Integer(u64),
    List(Vec<Value>),
}

fn parse_inner(input: &str) -> ParseResult<Vec<Value>> {
    separated_list0(tag(","), parse_value)(input)
}

fn parse_value(input: &str) -> ParseResult<Value> {
    alt((
        map(
            preceded(char('['), terminated(parse_inner, char(']'))),
            Value::List,
        ),
        map(u64, Value::Integer),
    ))(input)
}

fn parse_pair(input: &str) -> ParseResult<(Value, Value)> {
    let (input, left) = parse_value(input)?;
    let (input, _) = newline(input)?;
    let (input, right) = parse_value(input)?;
    Ok((input, (left, right)))
}


fn compare_lists(left: &[Value], right: &[Value]) -> Ordering {
    for i in 0..left.len() {
        if i >= right.len() {
            return Ordering::Greater;
        }
        match compare(&left[i], &right[i]) {
            Ordering::Greater => {
                return Ordering::Greater;
            }
            Ordering::Less => {
                return Ordering::Less;
            }
            _ => (),
        }
    }
    if left.len() < right.len() {
        Ordering::Less
    } else {
        Ordering::Equal
    }
}

fn compare(left: &Value, right: &Value) -> Ordering {
    match (left, right) {
        (Value::Integer(l), Value::Integer(r)) => {
            if l < r {
                Ordering::Less
            } else if l == r {
                Ordering::Equal
            } else {
                Ordering::Greater
            }
        }
        (Value::List(l), Value::List(r)) => compare_lists(&l, &r),
        (l @ Value::Integer(_), Value::List(lr)) => compare_lists(&[l.clone()], &lr),
        (Value::List(ll), r @ Value::Integer(_)) => compare_lists(&ll, &[r.clone()]),
    }
}

fn is_list_list_n(v: &Value, n: u64) -> bool {
    match v {
        Value::List(l) => {
            if l.len() != 1 {
                false
            } else {
                match &l[0] {
                    Value::List(ll) => {
                        if ll.len() != 1 {
                            false
                        } else {
                            match ll[0] {
                                Value::Integer(i) if i == n => true,
                                _ => false,
                            }
                        }
                    }
                    _ => false,
                }
            }
        }
        _ => false,
    }
}

fn gold(input: &str) -> usize {
    let mut values: Vec<Value> = input
        .lines()
        .filter(|l| !l.is_empty())
        .map(|l| parse_value(&l).unwrap().1)
        .collect();
    values.push(Value::List(vec![Value::List(vec![Value::Integer(2)])]));
    values.push(Value::List(vec![Value::List(vec![Value::Integer(6)])]));
    values.sort_unstable_by(compare);
    let p2 = values.iter().position(|v| is_list_list_n(&v, 2)).unwrap();
    let p6 = values.iter().position(|v| is_list_list_n(&v, 6)).unwrap();
    (p2 + 1) * (p6 + 1)
}

fn silver(input: &str) -> usize {
    let pairs = separated_list1(tag("\n\n"), parse_pair)(input).unwrap().1;
    let mut res = 0;
    for (i, (left, right)) in pairs.iter().enumerate() {
        match compare(&left, &right) {
            Ordering::Less | Ordering::Equal => {
                res += i + 1;
            }
            _ => (),
        }
    }
    res
}

fn main() {
    let mut input = String::new();
    stdin().read_to_string(&mut input).unwrap();

    println!("{:?}", silver(&input));
    println!("{:?}", gold(&input));
}


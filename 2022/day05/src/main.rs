use inpt::{inpt, Inpt};

#[derive(Inpt)]
#[inpt(regex = r"move (\d+) from (\d+) to (\d+)")]
struct Move {
    count: usize,
    from: usize,
    to: usize,
}

fn parse_input(input: &str) -> (Vec<Vec<char>>, Vec<Move>) {
    let (stacks_str, moves_str) = input.split_once("\n\n").unwrap();
    (parse_stacks(stacks_str), parse_moves(moves_str))
}
fn parse_moves(input: &str) -> Vec<Move> {
    input.lines().map(|l| inpt::<Move>(l).unwrap()).collect()
}

fn parse_stacks(input: &str) -> Vec<Vec<char>> {
    let mut lines = input.lines().rev();
    let stack_count: usize = lines
        .next()
        .unwrap()
        .split_ascii_whitespace()
        .last()
        .unwrap()
        .parse()
        .unwrap();
    let mut stacks = vec![Vec::new(); stack_count];
    for line in lines {
        for (i, letter) in line
            .chars()
            .skip(1)
            .step_by(4)
            .take(stack_count)
            .enumerate()
        {
            match letter {
                ' ' => (),
                _ => stacks[i].push(letter),
            }
        }
    }
    stacks
}

fn silver(mut stacks: Vec<Vec<char>>, moves: &[Move]) -> String {
    for m in moves {
        for _ in 0..m.count {
            let c = stacks[m.from - 1].pop().unwrap();
            stacks[m.to - 1].push(c);
        }
    }
    stacks.iter().map(|s| s.last().unwrap()).collect()
}

fn gold(mut stacks: Vec<Vec<char>>, moves: &[Move]) -> String {
    for m in moves {
        let from_len = stacks[m.from - 1].len();
        for c in from_len - m.count..from_len {
            let l = stacks[m.from - 1][c];
            stacks[m.to - 1].push(l);
        }
        let from = &mut stacks[m.from - 1];
        from.truncate(from.len() - m.count);
    }
    stacks.iter().map(|s| s.last().unwrap()).collect()
}

fn main() {
    use std::io::{stdin, Read};
    let mut input = String::new();
    stdin().lock().read_to_string(&mut input).unwrap();

    let (stacks, moves) = parse_input(&input);
    println!("{}", silver(stacks.clone(), &moves));
    println!("{}", gold(stacks, &moves));
}

use inpt::{inpt, Inpt};
use std::collections::HashSet;
use std::io::{stdin, BufRead};

type Scalar = i64;

#[derive(Inpt, Hash, Debug, Eq, Clone, Copy, PartialEq)]
#[inpt(regex = r"x=(-?\d+), y=(-?\d+)")]
struct Point {
    x: Scalar,
    y: Scalar,
}

impl Point {
    fn manhattan(&self, other: &Point) -> Scalar {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
    fn distance_to_line(&self, line: Scalar) -> Scalar {
        (self.y - line).abs()
    }
}

#[derive(Inpt, Debug, PartialEq)]
#[inpt(regex = r"Sensor at (.+): closest beacon is at (.+)")]
struct SensorInput {
    sensor: Point,
    beacon: Point,
}

#[derive(Clone, Copy, Debug)]
struct LineRange {
    from: Scalar,
    to: Scalar,
}

impl LineRange {
    fn union(self, other: LineRange) -> (LineRange, Option<LineRange>) {
        if self.from > other.from {
            other.union(self)
        } else {
            if self.to < other.from - 1 {
                (self, Some(other))
            } else {
                (
                    LineRange {
                        from: self.from,
                        to: self.to.max(other.to),
                    },
                    None,
                )
            }
        }
    }

    fn intersection_len(self, other: LineRange) -> Scalar {
        let res = if self.to < other.from {
            0
        } else if self.from > other.to {
            0
        } else {
            self.to.min(other.to) - self.from.max(other.from) + 1
        };
        res
    }

    fn len(&self) -> Scalar {
        self.to - self.from + 1
    }
}

impl SensorInput {
    fn exclusion_range(&self) -> Scalar {
        self.sensor.manhattan(&self.beacon)
    }

    fn intersection_with_line(&self, line: Scalar) -> Option<LineRange> {
        let d = self.sensor.distance_to_line(line);
        let e = self.exclusion_range();
        if d > e {
            None
        } else {
            let extra_range = e - d;
            Some(LineRange {
                from: self.sensor.x - extra_range,
                to: self.sensor.x + extra_range,
            })
        }
    }
}

fn insert_range(ranges: Vec<LineRange>, line_range: LineRange) -> Vec<LineRange> {
    let mut merged = Vec::new();
    let mut current = line_range;
    for lr in ranges {
        match current.union(lr) {
            (first, Some(second)) => {
                merged.push(first);
                current = second;
            }
            (first, None) => current = first,
        }
    }
    merged.push(current);
    merged
}

fn silver(sensors: &[SensorInput], line: Scalar) -> Scalar {
    let res: i64 = sensors
        .iter()
        .map(|s| s.intersection_with_line(line))
        .flatten()
        .fold(Vec::new(), |acc, lr| insert_range(acc, lr))
        .iter()
        .map(|lr| lr.len())
        .sum();
    let beacons_on_line = sensors
        .iter()
        .filter(|s| s.beacon.y == line)
        .map(|s| s.beacon)
        .collect::<HashSet<Point>>()
        .len();
    res - beacons_on_line as i64
}

fn gold(sensors: &[SensorInput]) -> (Scalar, Vec<LineRange>) {
    let range = LineRange {
        from: 0,
        to: 4_000_000,
    };
    for line in 0..=range.to {
        let line_ranges = sensors
            .iter()
            .map(|s| s.intersection_with_line(line))
            .flatten()
            .fold(Vec::new(), |acc, lr| insert_range(acc, lr));
        let line_fill: Scalar = line_ranges
            .iter()
            .map(|lr| lr.intersection_len(range))
            .sum();
        if line_fill != range.len() {
            return (line, line_ranges);
        }
    }
    panic!("not found")
}

fn main() {
    let input: Vec<SensorInput> = stdin()
        .lock()
        .lines()
        .flatten()
        .map(|l| inpt::<SensorInput>(&l).unwrap())
        .collect();
    println!("{:?}", silver(&input, 2_000_000));
    let (y, range) = gold(&input);
    if range.len() == 2 {
        println!("Gold star: {}", 4_000_000 * (range[0].to + 1) + y);
    } else {
        println!("Gold star: find the x coord by hand.\ny = {y}\n{range:?}");
    }
}

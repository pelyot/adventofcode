use inpt::{inpt, Inpt};
use std::collections::HashSet;
use std::io::{stdin, BufRead};


type Point = (i32, i32);

fn get_move(head: Point, tail: Point) -> Point {
    let dx = head.0 - tail.0;
    let dy = head.1 - tail.1;
    match (dx, dy) {
        (-2, 0) => (-1, 0),
        (2, 0) => (1, 0),
        (0, -2) => (0, -1),
        (0, 2) => (0, 1),
        (-2, -1) => (-1, -1),
        (-2, 1) => (-1, 1),
        (2, -1) => (1, -1),
        (2, 1) => (1, 1),
        (-1, -2) => (-1, -1),
        (1, -2) => (1, -1),
        (-1, 2) => (-1, 1),
        (1, 2) => (1, 1),
        (-2, -2) => (-1, -1),
        (-2, 2) => (-1, 1),
        (2, -2) => (1, -1),
        (2, 2) => (1, 1),
        _ => (0, 0)
    }
}

fn move_point(left: &mut Point, right: Point) {
    left.0 += right.0;
    left.1 += right.1;
}

#[derive(Inpt, Debug)]
#[inpt(regex = r"(.) (\d+)")]
struct Move {
    direction: char,
    distance: usize,
}

fn solve(input: &[Move]) -> (usize, usize) {
    let mut visited_silver = HashSet::<(i32, i32)>::new();
    let mut visited_gold = HashSet::<(i32, i32)>::new();
    let mut rope = vec![(0, 0); 10];
    visited_silver.insert(rope[1]);
    visited_gold.insert(*rope.last().unwrap());
    for m in input {
        let head_move = match m.direction {
            'U' => (0, 1),
            'D' => (0, -1),
            'L' => (-1, 0),
            'R' => (1, 0),
            _ => panic!("bad input"),
        };
        for _ in 0..m.distance {
            let mut curr_move = head_move;
            for i in 0..rope.len() - 1 {
                move_point(&mut rope[i], curr_move);
                curr_move = get_move(rope[i], rope[i+1]);
                
            }
            move_point(&mut rope.last_mut().unwrap(), curr_move);
            visited_silver.insert(rope[1]);
            visited_gold.insert(*rope.last().unwrap());
        }
    }
    (visited_silver.len(), visited_gold.len())
}

fn main() {
    let input: Vec<Move> = stdin()
        .lock()
        .lines()
        .flatten()
        .map(|l| inpt::<Move>(&l).unwrap())
        .collect();
    println!("{:?}", solve(&input));
}

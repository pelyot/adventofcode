const INPUT: &'static str = include_str!("input");

fn main() {
    println!("Silver: {}", solve_silver(INPUT));
    println!("Gold: {}", solve_gold(INPUT));
}

#[derive(Debug)]
enum Bracket {
    Parenthesis = 3,
    Square = 57,
    Curly = 1197,
    Angle = 25137,
}

impl Bracket {
    fn from_char(c: char) -> Bracket {
        match c {
            '(' | ')' => Bracket::Parenthesis,
            '[' | ']' => Bracket::Square,
            '{' | '}' => Bracket::Curly,
            '<' | '>' => Bracket::Angle,
            _ => unreachable!("bad input char"),
        }
    }

    fn points_silver(&self) -> usize {
        match *self {
            Bracket::Parenthesis => 3,
            Bracket::Square => 57,
            Bracket::Curly => 1197,
            Bracket::Angle => 25137,
        }
    }

    fn points_gold(&self) -> usize {
        match *self {
            Bracket::Parenthesis => 1,
            Bracket::Square => 2,
            Bracket::Curly => 3,
            Bracket::Angle => 4,
        }
    }
}

fn solve_silver(input: &str) -> usize {
    let mut score = 0;
    'outer: for line in input.split('\n').map(|l| l.trim()) {
        let mut stack = Vec::new();
        for c in line.chars() {
            match c {
                '(' | '[' | '{' | '<' => stack.push(Bracket::from_char(c)),
                ')' | ']' | '}' | '>' => {
                    if let Some(opening) = stack.pop() {
                        let closing = Bracket::from_char(c);
                        if std::mem::discriminant(&opening) != std::mem::discriminant(&closing) {
                            score += closing.points_silver();
                        }
                    } else {
                        continue 'outer;
                    }
                }
                _ => unreachable!("bad char in input"),
            }
        }
    }
    score
}

fn solve_gold(input: &str) -> usize {
    let mut scores = Vec::new();
    'outer: for line in input.split('\n').map(|l| l.trim()) {
        let mut stack = Vec::new();
        for c in line.chars() {
            match c {
                '(' | '[' | '{' | '<' => stack.push(Bracket::from_char(c)),
                ')' | ']' | '}' | '>' => {
                    if let Some(opening) = stack.pop() {
                        let closing = Bracket::from_char(c);
                        if std::mem::discriminant(&opening) != std::mem::discriminant(&closing) {
                            continue 'outer;
                        }
                    }
                }
                _ => unreachable!("bad char in input"),
            }
        }
        scores.push(
            stack
                .iter()
                .rev()
                .fold(0, |acc, b| acc * 5 + b.points_gold()),
        );
    }
    scores.sort();
    scores[scores.len() / 2]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_silver() {
        assert_eq!(solve_silver(SAMPLE), 26397);
    }

    #[test]
    fn sample_gold() {
        assert_eq!(solve_gold(SAMPLE), 288957);
    }

    const SAMPLE: &'static str = "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";
}

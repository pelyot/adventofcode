#![feature(test)]
extern crate test;

#[derive(Debug)]
struct Board {
    marks_rows: [u8; 5],
    marks_columns: [u8; 5],
    marks: Vec<bool>,
    numbers: Vec<u8>,
    won: bool
}

impl Board {
    fn new(s: &str) -> Board {
        let numbers: Vec<u8> = s
            .split_ascii_whitespace()
            .flat_map(|w| w.parse())
            .collect();
        Board {
            marks_rows: [5, 5, 5, 5, 5],
            marks_columns: [5, 5, 5, 5, 5],
            marks: vec![false; numbers.len()],
            numbers: numbers,
            won: false
        }
    }
    fn mark(&mut self, number: u8) -> Option<usize> {
        if self.won {
            return None
        }
        self.numbers.iter().position(|&nb| nb == number).map( |pos| {
            let row = pos % 5;
            let col = pos / 5;
            self.marks_rows[row] -= 1;
            self.marks_columns[col] -= 1;
            self.marks[pos] = true;
            if self.marks_rows[row] == 0 || self.marks_columns[col] == 0 {
                self.won = true;
                Some(number as usize * self.sum_unmarked())
            } else {
                None
            }
        }).flatten()
    }
    fn sum_unmarked(&self) -> usize {
        self.marks
            .iter()
            .zip(self.numbers.iter())
            .filter(|(&marked, _)| !marked)
            .map(|(_, &nb)| nb as usize)
            .sum()
    }
}

fn parse_input(input: &str) -> (Vec<u8>, Vec<Board>) {
    let mut chunks = input.split("\n\n");
    let draws = chunks
        .next()
        .unwrap()
        .split(',')
        .flat_map(|w| w.parse::<u8>())
        .collect();
    let boards = chunks.map(|c| Board::new(c)).collect();
    (draws, boards)
}

const INPUT: &'static str = include_str!("input");

fn main() {
    solve_a(INPUT);
    solve_b(INPUT);
}

fn solve_a(input: &str) {
    let (draws, mut boards) = parse_input(&input);
    for &d in &draws {
        for b in &mut boards {
            if let Some(score) = b.mark(d) {
                println!("Result A: {}", score);
                return;
            }
        }
    }
}

fn solve_b(input: &str) {
    let (draws, mut boards) = parse_input(&input);
    let mut last_score = 0usize;
    for &d in &draws {
        for b in &mut boards {
            if let Some(score) = b.mark(d) {
                last_score = score;
            }
        }
    }
    println!("Result b: {}", last_score);
}

#[cfg(test)]
mod unittests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn input_a(b: &mut Bencher) {
        b.iter(|| solve_a(INPUT));
    }

    #[bench]
    fn input_b(b: &mut Bencher) {
        b.iter(|| solve_b(INPUT));
    }
}
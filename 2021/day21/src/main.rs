fn main() {
    // Player 1 starting position: 8
    // Player 2 starting position: 3
    println!("Silver {}", silver(8, 3));
}

fn silver(init_pos_1: usize, init_pos_2: usize) -> usize {
    let mut dice = 0;
    let mut score1 = 0;
    let mut score2 = 0;
    let mut pos1 = init_pos_1 - 1;
    let mut pos2 = init_pos_2 - 1;
    loop {
        let advance1 = 3 + dice % 100 + (dice + 1) % 100 + (dice + 2) % 100;
        dice += 3;
        pos1 += advance1;
        pos1 %= 10;
        score1 += pos1 + 1;
        if score1 >= 1000 {
            return dice * score2;
        }
        let advance2 = 3 + dice % 100 + (dice + 1) % 100 + (dice + 2) % 100;
        dice += 3;
        pos2 += advance2;
        pos2 %= 10;
        score2 += pos2 + 1;
        if score2 >= 1000 {
            return dice * score1;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn silver_sample() {
        assert_eq!(silver(4, 8), 739785);
    }
}

use std::io::{BufRead, BufReader};

fn main() {
    let input = include_str!("input");
    let reader = BufReader::new(input.as_bytes());
    let depths = reader
        .lines()
        .flatten()
        .map(|l| l.trim().parse::<isize>().unwrap())
        .collect::<Vec<_>>();
    let res_a = &depths.windows(2).filter(|w| w[0] < w[1]).count();
    println!("result A: {}", res_a);
    let three_meas = &depths
        .windows(3)
        .map(|w| w.iter().sum())
        .collect::<Vec<isize>>();
    let res_b = &three_meas.windows(2).filter(|w| w[0] < w[1]).count();
    println!("result B: {}", res_b);
}

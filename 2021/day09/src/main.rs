#![feature(test)]
extern crate test;

fn main() {
    let input = parse_input(INPUT);
    println!("Silver: {}", solve_silver(&input));
    println!("Gold: {}", solve_gold(&input));
}

const INPUT: &'static str = include_str!("input");

fn parse_input(input: &str) -> Vec<Vec<i8>> {
    let mut grid: Vec<Vec<i8>> = input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| format!("9{}9", s))
        .map(|l| l.chars().map(|c| ((c as u8) - b'0') as i8).collect())
        .collect();
    let padding = vec![9i8; grid[0].len()];
    grid.insert(0, padding.clone());
    grid.push(padding);
    grid
}

fn basin_size(grid: &Vec<Vec<i8>>, r: usize, c: usize) -> usize {
    let mut basin = Vec::new();
    basin_rec(grid, r, c, &mut basin);
    basin.len()
}

fn basin_rec(grid: &Vec<Vec<i8>>, r: usize, c: usize, basin: &mut Vec<(usize, usize)>) {
    if basin.contains(&(r, c)) {
        return;
    }
    if grid[r][c] == 9 {
        return;
    }
    basin.push((r, c));
    basin_rec(grid, r + 1, c, basin);
    basin_rec(grid, r - 1, c, basin);
    basin_rec(grid, r, c - 1, basin);
    basin_rec(grid, r, c + 1, basin);
}

struct BasinIter<'a> {
    grid: &'a Vec<Vec<i8>>,
    current_row_idx: usize,
    current_column_idx: usize,
    max_row: usize,
    max_col: usize,
}

impl<'a> BasinIter<'a> {
    fn new(grid: &'a Vec<Vec<i8>>) -> BasinIter {
        BasinIter {
            grid,
            current_row_idx: 1,
            current_column_idx: 1,
            max_row: grid.len() - 1,
            max_col: grid[0].len() - 1,
        }
    }
}

impl<'a> Iterator for BasinIter<'a> {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        while self.current_row_idx < self.max_row {
            while self.current_column_idx < self.max_col {
                let c = self.grid[self.current_row_idx][self.current_column_idx];
                if c < self.grid[self.current_row_idx - 1][self.current_column_idx]
                    && c < self.grid[self.current_row_idx + 1][self.current_column_idx]
                    && c < self.grid[self.current_row_idx][self.current_column_idx - 1]
                    && c < self.grid[self.current_row_idx][self.current_column_idx + 1]
                {
                    let value_to_yield = (self.current_row_idx, self.current_column_idx);
                    self.current_column_idx += 1;
                    return Some(value_to_yield);
                }
                self.current_column_idx += 1;
            }
            self.current_row_idx += 1;
            self.current_column_idx = 1;
        }
        None
    }
}

fn solve_gold(grid: &Vec<Vec<i8>>) -> usize {
    let mut basins: Vec<usize> = BasinIter::new(grid)
        .map(|(i, j)| basin_size(grid, i, j))
        .collect();
    basins.sort();
    basins.iter().rev().take(3).product()
}

fn solve_silver(grid: &Vec<Vec<i8>>) -> usize {
    BasinIter::new(grid)
        .map(|(i, j)| grid[i][j] as usize + 1)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn sample_silver() {
        let input = parse_input(TEST_INPUT);
        assert_eq!(15, solve_silver(&input));
    }

    #[test]
    fn sample_gold() {
        let input = parse_input(TEST_INPUT);
        assert_eq!(1134, solve_gold(&input));
    }

    #[bench]
    fn bench_gold(b: &mut Bencher) {
        b.iter(|| solve_gold(&parse_input(INPUT)));
    }

    const TEST_INPUT: &'static str = "2199943210
3987894921
9856789892
8767896789
9899965678";
}

const INPUT: &'static str = "\
6111821767
1763611615
3512683131
8582771473
8214813874
2325823217
2222482823
5471356782
3738671287
8675226574";

fn parse_input(input: &str) -> Vec<Vec<i32>> {
    input
        .split('\n')
        .map(|l| {
            l.chars()
                .map(|c| c.to_digit(10).expect("bad char") as i32)
                .collect()
        })
        .collect()
}

const AROUND: [(i32, i32); 8] = [
    (-1, -1),
    (-1, 0),
    (-1, 1),
    (1, -1),
    (1, 0),
    (1, 1),
    (0, 1),
    (0, -1),
];

fn flash(grid: &mut Vec<Vec<i32>>) -> usize {
    for row in grid.iter_mut() {
        for c in row.iter_mut() {
            *c += 1
        }
    }
    let mut flashed = Vec::new();
    let mut open = Vec::new();
    for r in 0..grid.len() {
        for c in 0..grid[0].len() {
            if grid[r][c] > 9 {
                open.push((r as i32, c as i32));
            }
        }
    }
    while let Some((r, c)) = open.pop() {
        flashed.push((r, c));
        grid[r as usize][c as usize] = 0;
        for (dr, dc) in &AROUND {
            let (nr, nc) = (r + dr, c + dc);
            if nr < 0 || nc < 0 || nr >= grid.len() as i32 || nc >= grid[0].len() as i32 {
                continue;
            }
            if flashed.contains(&(nr, nc)) {
                continue;
            }
            if !open.contains(&(nr, nc)) {
                grid[nr as usize][nc as usize] += 1;
                if grid[nr as usize][nc as usize] > 9 {
                    open.push((nr, nc));
                }
            }
        }
    }
    flashed.len()
}

fn silver(mut grid: Vec<Vec<i32>>) -> usize {
    let mut flash_count = 0;
    for _step in 0..100 {
        flash_count += flash(&mut grid);
    }
    flash_count
}

fn gold(mut grid: Vec<Vec<i32>>) -> usize {
    let mut step = 0;
    loop {
        step += 1;
        if flash(&mut grid) == 100 {
            return step;
        }
    }
}

fn main() {
    println!("Silver {}", silver(parse_input(INPUT)));
    println!("Gold {}", gold(parse_input(INPUT)));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn silver_sample() {
        assert_eq!(1656, silver(parse_input(SAMPLE_INPUT)));
    }
    #[test]
    fn gold_sample() {
        assert_eq!(195, gold(parse_input(SAMPLE_INPUT)));
    }

    #[test]
    fn silver_sample_small() {
        silver(parse_input(SAMPLE_SMALL));
    }
    const SAMPLE_INPUT: &'static str = "\
5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";

    const SAMPLE_SMALL: &'static str = "\
11111
19991
19191
19991
11111";
}

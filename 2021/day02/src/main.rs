use std::io::{BufRead, BufReader};

fn main() {
    let input = include_str!("input");
    let reader = BufReader::new(input.as_bytes());
    let instructions = reader
        .lines()
        .flatten()
        .map(|l| {
            let mut words = l.split_ascii_whitespace();
            let dir = words.next().unwrap().chars().next().unwrap();
            let dist = words.next().unwrap().parse::<isize>().unwrap();
            (dir, dist)
        })
        .collect::<Vec<_>>();

    let pos_a = instructions
        .iter()
        .fold((0isize, 0isize), |acc, (dir, dist)| match dir {
            'd' => (acc.0, acc.1 + dist),
            'u' => (acc.0, acc.1 - dist),
            'f' => (acc.0 + dist, acc.1),
            _ => unreachable!(),
        });
    println!("Result A: {:?}", pos_a.0 * pos_a.1);

    let mut x = 0isize;
    let mut depth = 0isize;
    let mut aim = 0isize;
    instructions.iter().for_each(|(dir, dist)| match dir {
        'd' => aim += dist,
        'u' => aim -= dist,
        'f' => {
            x += dist;
            depth += aim * dist
        }
        _ => unreachable!(),
    });
    println!("Result B: {:?}", x * depth);
}

#![feature(test)]
extern crate test;

use std::str::FromStr;

const INPUT: &'static str = include_str!("input");

fn solve(input: &str, nb_iter: u32) -> usize {
    let mut gen = [0usize; 9];
    input
        .split(',')
        .map(str::trim)
        .map(usize::from_str)
        .map(|nb| nb.expect("parsing"))
        .for_each(|nb| gen[nb] += 1);
    for _ in 0..nb_iter {
        let mut new_gen = [0usize; 9];
        new_gen[8] = gen[0];
        new_gen[6] = gen[0];
        for g in 1..gen.len() {
            new_gen[g - 1] += gen[g];
        }
        gen = new_gen;
    }
    gen.iter().sum()
}

fn solve_a(input: &str) -> usize {
    solve(input, 80)
}

fn solve_b(input: &str) -> usize {
    solve(input, 256)
}

fn main() {
    println!("Result A: {}", solve_a(INPUT)); // 350154 too low
    println!("Result B: {}", solve_b(INPUT)); // 350154 too low
}

#[cfg(test)]
mod unittests {
    use super::*;
    use test::Bencher;
    
    const SAMPLE: &'static str = "3,4,3,1,2";

    #[test]
    fn test_sample() {
        assert_eq!(solve_a(SAMPLE), 5934);
    }

    #[bench]
    fn bench_b(b: &mut Bencher) {
        b.iter(|| solve_b(INPUT));
    }
}

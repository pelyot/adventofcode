use std::collections::HashSet;

fn main() {
    println!("Silver: {}", silver(&TARGET));
    println!("Gold: {}", gold(&TARGET));
}

struct Area {
    x_min: i32,
    x_max: i32,
    y_min: i32,
    y_max: i32,
}

impl Area {
    fn x_is_within(&self, x: i32) -> bool {
        x >= self.x_min && x <= self.x_max
    }
    fn y_is_within(&self, y: i32) -> bool {
        y >= self.y_min && y <= self.y_max
    }
}

fn silver(target: &Area) -> i32 {
    let mut ymax_global = 0;
    for vxi in 1..=target.x_max {
        let mut x = 0;
        let mut vx = vxi;
        let mut steps = 0;
        while x <= target.x_max {
            x += vx;
            steps += 1;
            if target.x_is_within(x) {
                println!("{}", x);
                if vx == 0 {
                    let vyi = -target.y_min - 1;
                    let ymax = vyi * (vyi + 1) / 2;
                    if ymax > ymax_global {
                        ymax_global = ymax;
                    }
                    break;
                } else {
                    let vyi = (target.y_max + steps * (steps + 1)) / steps / 2;
                    let ymax = vyi * (vyi + 1) / 2;
                    if ymax > ymax_global {
                        ymax_global = ymax;
                    }
                }
            }
            if vx > 0 {
                vx -= 1;
            } else if vx < 0 {
                vx += 1;
            } else {
                break;
            }
        }
    }
    ymax_global
}

fn try_vyi(target: &Area, vyi: i32, min_steps: i32) -> bool {
    let mut steps = 0;
    let mut y = 0;
    let mut vy = vyi;
    while y >= target.y_min {
        y += vy;
        steps += 1;
        vy -= 1;
        if target.y_is_within(y) {
            if steps >= min_steps {
                return true;
            }
        }
    }
    false
}

fn gold(target: &Area) -> usize {
    let mut velocities = HashSet::new();
    for vxi in 1..=target.x_max {
        let mut x = 0;
        let mut vx = vxi;
        let mut steps = 0;
        while x <= target.x_max {
            x += vx;
            steps += 1;
            if target.x_is_within(x) {
                if vx == 0 {
                    let vyi_min = target.y_min;
                    let vyi_max = -target.y_min;
                    for vyi in vyi_min..=vyi_max {
                        if try_vyi(target, vyi, steps + 1) {
                            velocities.insert((vxi, vyi));
                        }
                    }
                    break;
                } else {
                    let y_range_min = (target.y_min as f32 / steps as f32
                        + (steps as f32 - 1f32) / 2f32)
                        .ceil() as i32;
                    let y_range_max = (target.y_max as f32 / steps as f32
                        + (steps as f32 - 1f32) / 2f32)
                        .floor() as i32;
                    for y in (y_range_min)..=(y_range_max) {
                        velocities.insert((vxi, y));
                    }
                }
            }
            if vx > 0 {
                vx -= 1;
            } else {
                break;
            }
        }
    }
    velocities.len()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn silver_sample() {
        assert_eq!(45, silver(&SAMPLE));
    }

    #[test]
    fn gold_sample() {
        assert_eq!(112, gold(&SAMPLE));
    }

    #[test]
    fn gold_test() {
        assert_eq!(3540, gold(&TARGET));
    }

    const SAMPLE: Area = Area {
        x_min: 20,
        x_max: 30,
        y_min: -10,
        y_max: -5,
    };
}
const TARGET: Area = Area {
    x_min: 81,
    x_max: 129,
    y_min: -150,
    y_max: -108,
};

#![feature(test)]
extern crate test;

use std::str::FromStr;

const INPUT: &'static str = include_str!("input");

fn main() {
    println!("Result A: {}", solve_a(INPUT)); // 340946 too high
    println!("Result B: {}", solve_b(INPUT)); // 89450056 too low
}

fn median(positions: &mut [i64]) -> i64 {
    positions.sort();
    positions[positions.len() / 2]
}

fn parse_input(input: &str) -> Vec<i64> {
    input
        .trim()
        .split(',')
        .map(i64::from_str)
        .map(|p| p.expect("parse error"))
        .collect()
}

fn solve_a(input: &str) -> i64 {
    let mut positions = parse_input(input);
    let med = median(&mut positions);
    let diff: i64 = positions.iter().map(|p| (*p - med).abs()).sum();
    diff
}

fn cost_b(positions: &[i64], position: i64) -> i64 {
    positions
        .iter()
        .map(|p| {
            let dist = (p - position).abs();
            dist * (dist + 1) / 2
        })
        .sum()
}

fn solve_b(input: &str) -> i64 {
    let positions = parse_input(input);
    let min_pos = *positions.iter().min().unwrap();
    let max_pos = *positions.iter().max().unwrap();
    (min_pos..=max_pos)
        .map(|p| cost_b(&positions, p))
        .min()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    const TEST_INPUT: &'static str = "16,1,2,0,4,2,7,1,2,14";

    #[test]
    fn test_a() {
        assert_eq!(solve_a(TEST_INPUT), 37);
    }

    #[test]
    fn test_b() {
        assert_eq!(solve_b(TEST_INPUT), 168);
    }

    #[bench]
    fn bench_a(b: &mut Bencher) {
        b.iter(|| solve_a(INPUT));
    }

    #[bench]
    fn bench_b(b: &mut Bencher) {
        b.iter(|| solve_b(INPUT));
    }
}

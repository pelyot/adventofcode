#![feature(test)]
extern crate test;

use std::num::ParseIntError;
use std::str::FromStr;

fn main() {
    solve_a(INPUT);
    solve_b(INPUT);
}

const INPUT: &'static str = include_str!("input");

#[derive(Debug)]
struct Line {
    begin: (i32, i32),
    end: (i32, i32),
}

impl Line {
    fn draw_on_grid_a(&self, grid: &mut [Vec<u8>]) {
        use core::cmp::{max, min};
        if self.begin.0 == self.end.0 {
            let x = self.begin.0;
            let beg = min(self.begin.1, self.end.1);
            let end = max(self.begin.1, self.end.1);
            for y in beg..=end {
                grid[x as usize][y as usize] += 1;
            }
        } else if self.begin.1 == self.end.1 {
            let y = self.begin.1;
            let beg = min(self.begin.0, self.end.0);
            let end = max(self.begin.0, self.end.0);
            for x in beg..=end {
                grid[x as usize][y as usize] += 1;
            }
        }
    }
    fn draw_on_grid_b(&self, grid: &mut [Vec<u8>]) {
        use core::cmp::{max, min};
        if self.begin.0 == self.end.0 {
            let x = self.begin.0;
            let beg = min(self.begin.1, self.end.1);
            let end = max(self.begin.1, self.end.1);
            for y in beg..=end {
                grid[x as usize][y as usize] += 1;
            }
        } else if self.begin.1 == self.end.1 {
            let y = self.begin.1;
            let beg = min(self.begin.0, self.end.0);
            let end = max(self.begin.0, self.end.0);
            for x in beg..=end {
                grid[x as usize][y as usize] += 1;
            }
        } else {
            let line_len = (self.end.0 - self.begin.0).abs();
            let dx = (self.end.0 - self.begin.0) / line_len;
            let dy = (self.end.1 - self.begin.1) / line_len;
            for d in 0..=line_len {
                grid[(self.begin.0 + d * dx) as usize][(self.begin.1 + d * dy) as usize] += 1;
            }
        }
    }
}
impl FromStr for Line {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut coords = s
            .split(" -> ")
            .flat_map(|p| p.split(','))
            .map(i32::from_str)
            .flatten();
        Ok(Line {
            begin: (coords.next().unwrap(), coords.next().unwrap()),
            end: (coords.next().unwrap(), coords.next().unwrap()),
        })
    }
}

fn parse_lines(input: &str) -> Vec<Line> {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(Line::from_str)
        .flatten()
        .collect()
}

fn solve_a(input: &str) -> usize {
    const N: usize = 1000;
    let lines = parse_lines(input);
    let mut grid = vec![vec![0u8; N]; N];
    for line in lines {
        line.draw_on_grid_a(&mut grid);
    }
    let count: usize = grid
        .iter()
        .map(|row| row.iter().filter(|&&c| c > 1).count())
        .sum();
    println!("Result A: {}", count); // 11602 too high
    return count;
}

fn solve_b(input: &str) -> usize {
    const N: usize = 1000;
    let lines = parse_lines(input);
    let mut grid = vec![vec![0u8; N]; N];
    for line in lines {
        line.draw_on_grid_b(&mut grid);
    }
    let count: usize = grid
        .iter()
        .map(|row| row.iter().filter(|&&c| c > 1).count())
        .sum();
    println!("Result B: {}", count); // 11602 too high
    return count;
}

#[cfg(test)]
mod unittests {
    use super::*;
    use test::Bencher;

    #[test]
    fn tests_sample() {
        let input = "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
";
        assert_eq!(5, solve_a(input));
        assert_eq!(12, solve_b(input));
    }
    #[bench]
    fn input_a(b: &mut Bencher) {
        b.iter(|| solve_a(INPUT));
    }

    #[bench]
    fn input_b(b: &mut Bencher) {
        b.iter(|| solve_b(INPUT));
    }
}

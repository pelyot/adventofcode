use std::collections::HashMap;

fn main() {
    let (poly, rules) = parse(INPUT);
    println!("Silver {}", silver(&poly, &rules));
    println!("Gold {}", gold(&poly, &rules));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn silver_sample() {
        let (poly, rules) = parse(TEST_INPUT);
        assert_eq!("NCNBCHB", print(&expand_n_times(&poly, &rules, 1)));
        assert_eq!("NBCCNBBBCBHCB", print(&expand_n_times(&poly, &rules, 2)));
        assert_eq!(
            "NBBBCNCCNBBNBNBBCHBHHBCHB",
            print(&expand_n_times(&poly, &rules, 3))
        );
        assert_eq!(
            "NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB",
            print(&expand_n_times(&poly, &rules, 4))
        );
        assert_eq!(1588, silver(&poly, &rules));
    }

    #[test]
    fn gold_sample() {
        let (poly, rules) = parse(TEST_INPUT);
        assert_eq!(2188189693529, gold(&poly, &rules));
    }

    fn print(polymer: &[u8]) -> String {
        polymer.iter().map(|&p| p as char).collect::<String>()
    }

    const TEST_INPUT: &'static str = "\
NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";
}

fn silver(polymer: &[u8], rules: &[Rule]) -> usize {
    let expanded = expand_n_times(polymer, rules, 10);
    let mut hist = [0; 128];
    for c in expanded {
        hist[c as usize] += 1;
    }
    hist.iter().max().unwrap() - hist.iter().filter(|&&e| e != 0).min().unwrap()
}

fn gold(polymer: &[u8], rules: &[Rule]) -> usize {
    let mut pairs = HashMap::new();
    for w in polymer.windows(2) {
        *pairs.entry((w[0], w[1])).or_insert(0) += 1;
    }
    for _ in 0..40 {
        pairs = expand2(pairs, rules);
    }
    let mut hist = [0; 128];
    for (pair, nb) in pairs {
        hist[pair.0 as usize] += nb;
        hist[pair.1 as usize] += nb;
    }
    hist[polymer[0] as usize] += 1;
    hist[*polymer.last().unwrap() as usize] += 1;
    (hist.iter().max().unwrap() - hist.iter().filter(|&&e| e != 0).min().unwrap()) / 2
}

fn expand2(polymer: HashMap<(u8, u8), usize>, rules: &[Rule]) -> HashMap<(u8, u8), usize> {
    let mut expanded = HashMap::new();
    for (pair, nb) in polymer {
        if let Some(rule) = rules.iter().find(|r| r.0 .0 == pair.0 && r.0 .1 == pair.1) {
            *expanded.entry((pair.0, rule.1)).or_insert(0) += nb;
            *expanded.entry((rule.1, pair.1)).or_insert(0) += nb;
        } else {
            *expanded.entry(pair).or_insert(0) += nb;
        }
    }
    expanded
}

fn expand(polymer: &[u8], rules: &[Rule]) -> Vec<u8> {
    let mut output = Vec::new();
    for w in polymer.windows(2) {
        output.push(w[0]);
        if let Some(rule) = rules.iter().find(|r| r.0 .0 == w[0] && r.0 .1 == w[1]) {
            output.push(rule.1);
        }
    }
    output.push(*polymer.last().unwrap());
    output
}

fn expand_n_times(polymer: &[u8], rules: &[Rule], times: usize) -> Vec<u8> {
    let mut expanded = polymer.to_vec();
    for _t in 0..times {
        expanded = expand(&expanded, rules);
    }
    expanded
}

struct Rule((u8, u8), u8);

fn parse(input: &str) -> (Vec<u8>, Vec<Rule>) {
    let (first_line, tail) = input.split_once("\n\n").expect("bad input");
    let template = first_line.trim().chars().map(|c| c as u8).collect();
    let rules = tail
        .split('\n')
        .filter(|l| !l.is_empty())
        .map(|l| {
            let (w, i) = l.split_once(" -> ").expect("bad rule");
            Rule(
                (
                    w.chars().nth(0).expect("bad char") as u8,
                    w.chars().nth(1).expect("bad char") as u8,
                ),
                i.chars().nth(0).expect("bad char") as u8,
            )
        })
        .collect();
    (template, rules)
}

const INPUT: &'static str = include_str!("input");

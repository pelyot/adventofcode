extern crate num_integer;

use num_integer::Integer;
use num_traits::{One, Zero};
use std::ops::{AddAssign, ShlAssign};

fn main() {
    println!("Silver {}", silver(INPUT));
    println!("Gold {}", gold(INPUT));
}

enum Packet {
    Literal(usize),
    Sum(Vec<Packet>),
    Product(Vec<Packet>),
    Minimum(Vec<Packet>),
    Maximum(Vec<Packet>),
    GreaterThan(Vec<Packet>),
    LessThan(Vec<Packet>),
    Equals(Vec<Packet>),
}

fn input_to_bits(input: &str) -> Vec<u8> {
    let trimmed = input.trim().as_bytes();
    let mut buff = Vec::with_capacity(trimmed.len() * 4);
    b"0120";
    for b in trimmed {
        match b {
            b'0' => buff.extend_from_slice(b"0000"),
            b'1' => buff.extend_from_slice(b"0001"),
            b'2' => buff.extend_from_slice(b"0010"),
            b'3' => buff.extend_from_slice(b"0011"),
            b'4' => buff.extend_from_slice(b"0100"),
            b'5' => buff.extend_from_slice(b"0101"),
            b'6' => buff.extend_from_slice(b"0110"),
            b'7' => buff.extend_from_slice(b"0111"),
            b'8' => buff.extend_from_slice(b"1000"),
            b'9' => buff.extend_from_slice(b"1001"),
            b'A' => buff.extend_from_slice(b"1010"),
            b'B' => buff.extend_from_slice(b"1011"),
            b'C' => buff.extend_from_slice(b"1100"),
            b'D' => buff.extend_from_slice(b"1101"),
            b'E' => buff.extend_from_slice(b"1110"),
            b'F' => buff.extend_from_slice(b"1111"),
            _ => unreachable!("bad char"),
        }
    }
    buff
}

fn to_int<T: Integer + Zero + ShlAssign + AddAssign>(bits: &[u8]) -> T {
    let mut acc: T = Zero::zero();
    for &b in bits {
        acc <<= One::one();
        acc += if b == b'1' { One::one() } else { Zero::zero() };
    }
    acc
}

fn parse_packet(bits: &[u8], idx: &mut usize) -> Packet {
    let _version = to_int::<u8>(&bits[*idx..*idx + 3]);
    *idx += 3;

    let op_type = to_int::<u8>(&bits[*idx..*idx + 3]);
    *idx += 3;

    match op_type {
        4 => {
            let mut value = 0usize;
            loop {
                value <<= 4;
                let pack = to_int::<usize>(&bits[*idx + 1..*idx + 5]);
                value |= pack;
                let is_last = bits[*idx] == b'0';
                *idx += 5;
                if is_last {
                    break;
                }
            }
            Packet::Literal(value)
        }
        0 | 1 | 2 | 3 | 5 | 6 | 7 => {
            let sub_packets = parse_sub_packets(bits, idx);
            match op_type {
                0 => Packet::Sum(sub_packets),
                1 => Packet::Product(sub_packets),
                2 => Packet::Minimum(sub_packets),
                3 => Packet::Maximum(sub_packets),
                5 => Packet::GreaterThan(sub_packets),
                6 => Packet::LessThan(sub_packets),
                7 => Packet::Equals(sub_packets),
                _ => unreachable!("bad operation"),
            }
        }
        _ => unreachable!(),
    }
}

fn parse_sub_packets(bits: &[u8], idx: &mut usize) -> Vec<Packet> {
    let mut packets = Vec::new();
    let type_id = bits[*idx];
    *idx += 1;
    match type_id {
        b'0' => {
            let bit_len = to_int::<usize>(&bits[*idx..*idx + 15]);
            *idx += 15;
            let idx_max = *idx + bit_len;
            while *idx < idx_max {
                packets.push(parse_packet(bits, idx));
            }
        }
        b'1' => {
            let packet_len = to_int::<u16>(&bits[*idx..*idx + 11]);
            *idx += 11;
            for _ in 0..packet_len {
                packets.push(parse_packet(bits, idx));
            }
        }
        _ => unreachable!("bad bit"),
    }
    packets
}

fn eval(packet: &Packet) -> usize {
    match packet {
        Packet::Literal(value) => *value,
        Packet::Sum(values) => values.into_iter().map(eval).sum(),
        Packet::Product(values) => values.into_iter().map(eval).product(),
        Packet::Minimum(values) => values.into_iter().map(eval).min().unwrap(),
        Packet::Maximum(values) => values.into_iter().map(eval).max().unwrap(),
        Packet::GreaterThan(values) => {
            let v1 = eval(&values[0]);
            let v2 = eval(&values[1]);
            (v1 > v2) as usize
        }
        Packet::LessThan(values) => {
            let v1 = eval(&values[0]);
            let v2 = eval(&values[1]);
            (v1 < v2) as usize
        }
        Packet::Equals(values) => {
            let v1 = eval(&values[0]);
            let v2 = eval(&values[1]);
            (v1 == v2) as usize
        }
    }
}

fn gold(input: &str) -> usize {
    let packet = parse_packet(&input_to_bits(input), &mut 0);
    eval(&packet)
}

fn silver(input: &str) -> usize {
    let bits = input_to_bits(input);
    let mut idx = 0;
    let mut acc = 0;
    while idx < bits.len() - 7 {
        let version = to_int::<u8>(&bits[idx..idx + 3]);
        acc += version as usize;
        idx += 3;
        let type_ = to_int::<u8>(&bits[idx..idx + 3]);
        idx += 3;
        if type_ == 4 {
            let mut _value = 0usize;
            loop {
                _value <<= 4;
                let pack = to_int::<usize>(&bits[idx + 1..idx + 5]);
                _value |= pack;
                let is_last = bits[idx] == b'0';
                idx += 5;
                if is_last {
                    break;
                }
            }
        } else {
            let type_id = bits[idx];
            idx += 1;
            match type_id {
                b'0' => {
                    let _bit_len = to_int::<u16>(&bits[idx..idx + 15]);
                    idx += 15;
                }
                b'1' => {
                    let _packet_len = to_int::<u16>(&bits[idx..idx + 11]);
                    idx += 11;
                }
                _ => unreachable!("bad bit"),
            }
        }
    }
    acc
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn silver_sample() {
        assert_eq!(16, silver("8A004A801A8002F478"));
        assert_eq!(12, silver("620080001611562C8802118E34"));
        assert_eq!(23, silver("C0015000016115A2E0802F182340"));
        assert_eq!(31, silver("A0016C880162017C3686B18A3D4780"));
    }

    #[test]
    fn gold_sample() {
        assert_eq!(3, gold("C200B40A82"));
        assert_eq!(54, gold("04005AC33890"));
        assert_eq!(7, gold("880086C3E88112"));
        assert_eq!(9, gold("CE00C43D881120"));
        assert_eq!(1, gold("D8005AC2A8F0"));
        assert_eq!(0, gold("F600BC2D8F"));
        assert_eq!(1, gold("9C0141080250320F1802104A08"));
    }

    #[test]
    fn input_to_bits_test() {
        assert_eq!(&input_to_bits("D2FE28"), b"110100101111111000101000");
        assert_eq!(
            &input_to_bits("EE00D40C823060"),
            b"11101110000000001101010000001100100000100011000001100000"
        );
    }
}

const INPUT: &'static str = "60552F100693298A9EF0039D24B129BA56D67282E600A4B5857002439CE580E5E5AEF67803600D2E294B2FCE8AC489BAEF37FEACB31A678548034EA0086253B183F4F6BDDE864B13CBCFBC4C10066508E3F4B4B9965300470026E92DC2960691F7F3AB32CBE834C01A9B7A933E9D241003A520DF316647002E57C1331DFCE16A249802DA009CAD2117993CD2A253B33C8BA00277180390F60E45D30062354598AA4008641A8710FCC01492FB75004850EE5210ACEF68DE2A327B12500327D848028ED0046661A209986896041802DA0098002131621842300043E3C4168B12BCB6835C00B6033F480C493003C40080029F1400B70039808AC30024C009500208064C601674804E870025003AA400BED8024900066272D7A7F56A8FB0044B272B7C0E6F2392E3460094FAA5002512957B98717004A4779DAECC7E9188AB008B93B7B86CB5E47B2B48D7CAD3328FB76B40465243C8018F49CA561C979C182723D769642200412756271FC80460A00CC0401D8211A2270803D10A1645B947B3004A4BA55801494BC330A5BB6E28CCE60BE6012CB2A4A854A13CD34880572523898C7EDE1A9FA7EED53F1F38CD418080461B00440010A845152360803F0FA38C7798413005E4FB102D004E6492649CC017F004A448A44826AB9BFAB5E0AA8053306B0CE4D324BB2149ADDA2904028600021909E0AC7F0004221FC36826200FC3C8EB10940109DED1960CCE9A1008C731CB4FD0B8BD004872BC8C3A432BC8C3A4240231CF1C78028200F41485F100001098EB1F234900505224328612AF33A97367EA00CC4585F315073004E4C2B003530004363847889E200C45985F140C010A005565FD3F06C249F9E3BC8280804B234CA3C962E1F1C64ADED77D10C3002669A0C0109FB47D9EC58BC01391873141197DCBCEA401E2CE80D0052331E95F373798F4AF9B998802D3B64C9AB6617080";

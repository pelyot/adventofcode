use std::collections::HashMap;
use std::hash::Hash;

fn main() {
    println!("Silver {}", silver(INPUT));
    println!("Gold {}", gold(INPUT));
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
struct Cave {
    name: String,
    small: bool,
}

impl Cave {
    fn new(s: &str) -> Cave {
        Cave {
            name: s.to_owned(),
            small: s
                .chars()
                .nth(0)
                .expect("bad cave")
                .is_ascii_lowercase(),
        }
    }
}

fn explore(
    graph: &HashMap<Cave, Vec<Cave>>,
    current: Cave,
    visited: &mut Vec<Cave>,
) -> usize {
    if current.name == "end" {
        return 1;
    }
    if current.small {
        if visited.contains(&current) {
            return 0;
        } else {
            visited.push(current.clone());
        }
    }
    let mut acc = 0;
    let nexts = graph.get(&current).unwrap();
    for c in nexts.iter() {
        acc += explore(graph, c.clone(), visited);
    }
    if current.small {
        visited.pop();
    }
    acc
}

fn explore2(
    graph: &HashMap<Cave, Vec<Cave>>,
    current: Cave,
    visited: &mut Vec<Cave>,
    mut second_visit: Option<String>,
    depth: usize
) -> usize {
    if current.name == "end" {
        return 1;
    }
    if current.small {
        if visited.contains(&current) {
            if let Some(_) = second_visit {
                return 0;
            } else {
                second_visit = Some(current.name.to_owned());
            }
        } else {
            visited.push(current.clone());
        }
    }
    let mut acc = 0;
    let nexts = graph.get(&current).unwrap();
    for c in nexts.iter() {
        acc += explore2(graph, c.clone(), visited, second_visit.clone(), depth + 1);
    }
    if current.small {
        if let Some(name) = second_visit {
            if name != current.name {
                visited.pop();
            }
        } else {
            visited.pop();
        }
    }
    acc
}

fn silver(input: &str) -> usize {
    let graph = parse_input(input);
    explore(
        &graph,
        Cave::new("start"),
        &mut Vec::new(),
    )
}

fn gold(input: &str) -> usize {
    let graph = parse_input(input);
    explore2(
        &graph,
        Cave::new("start"),
        &mut Vec::new(),
        None,
        0
    )
}


fn parse_input(input: &str) -> HashMap<Cave, Vec<Cave>> {
    let mut graph = HashMap::new();
    let parsing = input
        .split('\n')
        .map(|l| l.split_once('-').expect("bad input"));
    for (cave1_str, cave2_str) in parsing {
        let cave1 = Cave::new(cave1_str);
        let cave2 = Cave::new(cave2_str);
        if cave1.name == "start" || cave2.name == "end" {
            graph.entry(cave1).or_insert(Vec::new()).push(cave2);
        } else if cave2.name == "start" || cave1.name == "end" {
            graph.entry(cave2).or_insert(Vec::new()).push(cave1);
        } else {
            graph.entry(cave2.clone()).or_insert(Vec::new()).push(cave1.clone());
            graph.entry(cave1).or_insert(Vec::new()).push(cave2);
        }
    }
    graph
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn samples_silver() {
        assert_eq!(10, silver(TEST_INPUT_SMALL));
        assert_eq!(19, silver(TEST_INPUT_MEDIUM));
        assert_eq!(226, silver(TEST_INPUT_LARGE));
    }

    #[test]
    fn samples_gold() {
        assert_eq!(36, gold(TEST_INPUT_SMALL));
        assert_eq!(103, gold(TEST_INPUT_MEDIUM));
        assert_eq!(3509, gold(TEST_INPUT_LARGE));
    }

    const TEST_INPUT_SMALL: &'static str = "\
start-A
start-b
A-c
A-b
b-d
A-end
b-end";

    const TEST_INPUT_MEDIUM: &'static str = "\
dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc";

    const TEST_INPUT_LARGE: &'static str = "\
fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW";
}
const INPUT: &'static str = "\
TR-start
xx-JT
xx-TR
hc-dd
ab-JT
hc-end
dd-JT
ab-dd
TR-ab
vh-xx
hc-JT
TR-vh
xx-start
hc-ME
vh-dd
JT-bm
end-ab
dd-xx
end-TR
hc-TR
start-vh";

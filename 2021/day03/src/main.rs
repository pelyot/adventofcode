use std::io::{BufRead, BufReader};

fn count_bit(report: &[&Vec<usize>], position: usize) -> usize {
    report.iter().map(|bits| bits[position]).sum()
}

fn most_common_bit(report: &[&Vec<usize>], position: usize) -> usize {
    (2 * count_bit(report, position) >= report.len()) as usize
}

fn least_common_bit(report: &[&Vec<usize>], position: usize) -> usize {
    (report.len() > 2 * count_bit(report, position)) as usize
}

fn calculate_rating(report: &[Vec<usize>], bit_criteria: fn(&[&Vec<usize>], usize) -> usize) -> usize {
    let mut position = 0;
    let mut remaining: Vec<&Vec<usize>> = report.iter().map(|v| v).collect();
    while remaining.len() != 1 {
        let mcb = bit_criteria(&remaining, position);
        remaining = remaining.iter().filter(|bits| bits[position] == mcb).cloned().collect();
        position += 1;
    }
    remaining[0].iter().fold(0, |mut acc, bit| { acc <<= 1; acc += *bit; acc})
}

fn oxygen_generator_rating(report: &[Vec<usize>]) -> usize {
    calculate_rating(report, most_common_bit)
}

fn co2_scrubber_rating(report: &[Vec<usize>]) -> usize {
    calculate_rating(report, least_common_bit)
}

fn parse_report(input: &str) -> Vec<Vec<usize>> {
    let reader = BufReader::new(input.as_bytes());
    reader
        .lines()
        .flatten()
        .map(|l| {
            l.chars()
                .map(|c| match c {
                    '1' => 1,
                    '0' => 0,
                    _ => unreachable!(),
                })
                .collect()
        })
        .collect()
}

fn part_a(report: &[Vec<usize>]) {
    let mut counts = vec![0; report[0].len()];
    for line in report {
        for i in 0..line.len() {
            counts[i] += line[i];
        }
    }
    let total = report.len();
    let mut gamma = 0usize;
    let mut epsilon = 0usize;
    for n in counts {
        gamma <<= 1;
        epsilon <<= 1;
        if 2 * n > total {
            gamma += 1;
        } else {
            epsilon += 1;
        }
    }
    println!("Result A: {}", gamma * epsilon);
}

fn part_b(report: &[Vec<usize>]) {
    let o2 = oxygen_generator_rating(&report);
    let co2 = co2_scrubber_rating(&report);
    println!("Result B: {}", o2 * co2);
}

fn main() {
    let input = include_str!("input");
    let report = parse_report(input);
    part_a(&report);
    part_b(&report);
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test() {
        let input = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";
        let report = parse_report(input);
        let o2 = oxygen_generator_rating(&report);
        let co2 =co2_scrubber_rating(&report);
        assert_eq!(o2, 23);
        assert_eq!(co2, 10);
    }
}

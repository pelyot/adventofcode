fn main() {
    println!("Silver {}", silver(INPUT));
    println!("Gold {}", gold(INPUT));
}

#[derive(PartialEq, Debug, Clone)]
enum Token {
    Open,
    Number(u32),
    Sep,
    Close,
}

impl Token {
    fn number(&self) -> Option<u32> {
        match self {
            Token::Number(n) => Some(*n),
            _ => None,
        }
    }
    fn is_number(&self) -> bool {
        match self {
            Token::Number(_) => true,
            _ => false,
        }
    }
    fn add_number(&mut self, other: u32) {
        let self_num = self.number().expect("self not a number");
        *self = Token::Number(self_num + other);
    }
}
fn parse(input: &str) -> Vec<Token> {
    let mut tokens = Vec::new();
    for c in input.chars() {
        match c {
            '[' => tokens.push(Token::Open),
            ']' => tokens.push(Token::Close),
            ',' => tokens.push(Token::Sep),
            c => tokens.push(Token::Number(char::to_digit(c, 10).unwrap() as u32)),
        }
    }
    tokens
}

fn explode(tokens: &mut Vec<Token>) -> bool {
    let mut level = 0;
    // let mut idx: usize = 0;
    let mut prev_num_idx = None;
    for idx in 0..tokens.len() {
        match tokens[idx] {
            Token::Open => {
                if level >= 4 && tokens[idx + 1].is_number() {
                    // explode the pair: left
                    if let Some(prev) = prev_num_idx {
                        let left_num = tokens[idx + 1].number().expect("left not a number");
                        let prev_tok: &mut Token = &mut tokens[prev];
                        prev_tok.add_number(left_num);
                    }
                    {
                        // explode to the right
                        let right_num = tokens[idx + 3].number().expect("right not a number");
                        let mut next_num_idx = idx + 5;
                        loop {
                            if next_num_idx >= tokens.len() {
                                break;
                            }
                            if tokens[next_num_idx].is_number() {
                                tokens[next_num_idx].add_number(right_num);
                                break;
                            }
                            next_num_idx += 1;
                        }
                    }
                    // Remove the pair we just exploded
                    tokens.remove(idx);
                    tokens.remove(idx);
                    tokens.remove(idx);
                    tokens.remove(idx);
                    tokens[idx] = Token::Number(0);
                    return true;
                }
                level += 1
            }
            Token::Close => level -= 1,
            Token::Number(_) => prev_num_idx = Some(idx),
            Token::Sep => (),
        }
    }
    false
}

fn to_string(tokens: &[Token]) -> String {
    tokens
        .iter()
        .map(|tok| match tok {
            Token::Open => '[',
            Token::Close => ']',
            Token::Sep => ',',
            Token::Number(c) => char::from_digit(*c, 10).unwrap_or('X'),
        })
        .collect()
}

fn split(tokens: &mut Vec<Token>) -> bool {
    for idx in 0..tokens.len() {
        match tokens[idx] {
            Token::Number(n) => {
                if n >= 10 {
                    // Split number
                    let left = ((n as f64) / 2f64).floor() as u32;
                    let right = ((n as f64) / 2f64).ceil() as u32;
                    tokens[idx] = Token::Close;
                    tokens.insert(idx, Token::Number(right));
                    tokens.insert(idx, Token::Sep);
                    tokens.insert(idx, Token::Number(left));
                    tokens.insert(idx, Token::Open);
                    return true;
                }
            }
            _ => {}
        }
    }
    false
}

fn add(mut left: Vec<Token>, mut right: Vec<Token>) -> Vec<Token> {
    let mut res = Vec::with_capacity(left.len() + right.len() + 3);
    res.push(Token::Open);
    res.append(&mut left);
    res.push(Token::Sep);
    res.append(&mut right);
    res.push(Token::Close);
    res
}

fn add_and_reduce(left: Vec<Token>, right: Vec<Token>) -> Vec<Token> {
    let mut res = add(left, right);
    loop {
        let explode_done = explode(&mut res);
        if explode_done {
            continue;
        };
        let split_done = split(&mut res);
        if split_done == false && explode_done == false {
            break;
        }
    }
    res
}

fn sum(input: &str) -> Vec<Token> {
    input
        .split('\n')
        .filter(|l| !l.is_empty())
        .map(|l| parse(l))
        .reduce(|acc, tokens| add_and_reduce(acc, tokens))
        .unwrap()
}

fn magnitude(tokens: &[Token], idx: &mut usize) -> usize {
    match tokens[*idx] {
        Token::Open => {
            *idx += 1;
            let left = magnitude(tokens, idx);
            assert_eq!(tokens[*idx], Token::Sep);
            *idx += 1;
            let right = magnitude(tokens, idx);
            assert_eq!(tokens[*idx], Token::Close);
            *idx += 1;
            3 * left + 2 * right
        }
        Token::Number(n) => {
            *idx += 1;
            n as usize
        }
        _ => unreachable!(),
    }
}

fn silver(input: &str) -> usize {
    magnitude(&*sum(input), &mut 0)
}

fn gold(input: &str) -> usize {
    let numbers: Vec<Vec<Token>> = input
        .split('\n')
        .filter(|l| !l.is_empty())
        .map(|l| parse(l))
        .collect();
    let mut max_m = usize::MIN;
    for i in 0..numbers.len() {
        for j in 0..i {
            let m = magnitude(
                &*add_and_reduce(numbers[i].to_vec(), numbers[j].to_vec()),
                &mut 0,
            );
            if m > max_m {
                max_m = m;
            }
        }
    }
    max_m
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn silver_sample() {
        assert_eq!(silver(SAMPLE_INPUT), 4140);
    }

    #[test]
    fn gold_sample() {
        assert_eq!(gold(SAMPLE_INPUT), 3993);
    }

    #[test]
    fn magnitude_test() {
        assert_eq!(magnitude(&parse("[9,1]"), &mut 0), 29);
        assert_eq!(magnitude(&parse("[1,9]"), &mut 0), 21);
        assert_eq!(magnitude(&parse("[[1,2],[[3,4],5]]"), &mut 0), 143);
    }
    #[test]
    fn sum_test_1() {
        let input = "\
[1,1]
[2,2]
[3,3]
[4,4]";
        assert_eq!(to_string(&sum(input)), "[[[[1,1],[2,2]],[3,3]],[4,4]]");
    }

    #[test]
    fn sum_test_2() {
        let input = "\
[1,1]
[2,2]
[3,3]
[4,4]
[5,5]";
        assert_eq!(to_string(&sum(input)), "[[[[3,0],[5,3]],[4,4]],[5,5]]");
    }

    #[test]
    fn sum_test_3() {
        let input = "\
[1,1]
[2,2]
[3,3]
[4,4]
[5,5]
[6,6]";
        assert_eq!(to_string(&sum(input)), "[[[[5,0],[7,4]],[5,5]],[6,6]]");
    }

    #[test]
    fn sum_test_4() {
        let input = "\
[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]]";
        assert_eq!(
            to_string(&sum(input)),
            "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]"
        );
    }

    #[test]
    fn add_test() {
        let left = parse("[[[[4,3],4],4],[7,[[8,4],9]]]");
        let right = parse("[1,1]");
        let mut tokens = add(left, right);
        assert_eq!(to_string(&tokens), "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]");
        assert!(explode(&mut tokens));
        assert_eq!(to_string(&tokens), "[[[[0,7],4],[7,[[8,4],9]]],[1,1]]");
        assert!(explode(&mut tokens));
        // assert_eq!(to_string(&tokens), "[[[[0,7],4],[15,[0,13]]],[1,1]]");
        assert!(split(&mut tokens));
        // assert_eq!(to_string(&tokens), "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]");
        assert!(split(&mut tokens));
        assert_eq!(to_string(&tokens), "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]");
        assert!(explode(&mut tokens));
        assert_eq!(to_string(&tokens), "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]");
    }

    #[test]
    fn split_test() {
        split_test_impl(vec![Token::Number(10)], "[5,5]");
        split_test_impl(vec![Token::Number(11)], "[5,6]");
    }

    fn split_test_impl(mut tokens: Vec<Token>, expected: &str) {
        assert!(split(&mut tokens));
        assert_eq!(to_string(&tokens), expected);
    }

    #[test]
    fn explode_test() {
        explode_test_impl("[[[[[9,8],1],2],3],4]", "[[[[0,9],2],3],4]");
        explode_test_impl("[7,[6,[5,[4,[3,2]]]]]", "[7,[6,[5,[7,0]]]]");
        explode_test_impl("[[6,[5,[4,[3,2]]]],1]", "[[6,[5,[7,0]]],3]");
        explode_test_impl(
            "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]",
            "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]",
        );
        explode_test_impl(
            "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]",
            "[[3,[2,[8,0]]],[9,[5,[7,0]]]]",
        );
    }

    fn explode_test_impl(input: &str, expected: &str) {
        let mut toks = parse(input);
        assert!(explode(&mut toks));
        assert_eq!(to_string(&toks), expected);
    }

    #[test]
    fn parse_test() {
        for input in TEST_INPUT.split('\n') {
            parse_test_impl(input);
        }
    }

    fn parse_test_impl(input: &str) {
        assert_eq!(to_string(&parse(input)), input);
    }

    const TEST_INPUT: &'static str = "\
[1,2]
[[1,2],3]
[9,[8,7]]
[[1,9],[8,5]]
[[[[1,2],[3,4]],[[5,6],[7,8]]],9]
[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]
[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]";

    const SAMPLE_INPUT: &'static str = "\
[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]";
}

const INPUT: &'static str = include_str!("input");

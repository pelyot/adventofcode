use crate::NodeInner::Uninit;
use std::cell::{Ref, RefCell};
use std::iter::repeat;
use std::rc::{Rc, Weak};

#[derive(Debug)]
enum Color {
    Left,
    Right,
    Root,
}

type NodeRef = Rc<RefCell<Node>>;
type ParentRef = Weak<RefCell<Node>>;

struct Children {
    left: NodeRef,
    right: NodeRef,
}

enum NodeInner {
    Children(Children),
    Uninit,
    Leaf(i32),
}

struct Node {
    parent: ParentRef,
    color: Color,
    inner: NodeInner,
}

impl Node {
    fn is_leaf(&self) -> bool {
        match &self.inner {
            NodeInner::Leaf(_) => true,
            _ => false,
        }
    }
    fn is_leaf_pair(&self) -> bool {
        match &self.inner {
            NodeInner::Children(Children { left, right }) => {
                left.borrow().is_leaf() && right.borrow().is_leaf()
            }
            _ => false,
        }
    }
    fn leaf_value(&self) -> Option<i32> {
        match &self.inner {
            NodeInner::Leaf(value) => Some(*value),
            _ => None
        }
    }
    fn leaf_values(&self) -> Option<(i32, i32)> {
        match &self.inner {
            NodeInner::Children(Children{left, right}) => {
                let lvo = left.borrow().leaf_value();
                let rvo = right.borrow().leaf_value();
                match (lvo, rvo) {
                    (Some(lv), Some(rv)) => Some((lv, rv)),
                    _ => None
                }
            },
            _ => None
        }
    }
}

fn parse(input: &str) -> NodeRef {
    parse_node(input.as_bytes(), &mut 0, Weak::new(), Color::Root)
}

fn parse_node(input: &[u8], idx: &mut usize, parent: ParentRef, color: Color) -> NodeRef {
    assert_eq!(input[*idx], b'[');
    *idx += 1;
    let node = Rc::new(RefCell::new(Node {
        parent,
        inner: Uninit,
        color,
    }));
    let left = parse_child(input, idx, Rc::downgrade(&node), Color::Left);
    assert_eq!(input[*idx], b',');
    *idx += 1;
    let right = parse_child(input, idx, Rc::downgrade(&node), Color::Right);
    assert_eq!(input[*idx], b']');
    *idx += 1;
    node.borrow_mut().inner = NodeInner::Children(Children { left, right });
    node
}

fn parse_child(input: &[u8], idx: &mut usize, parent: ParentRef, color: Color) -> NodeRef {
    match input[*idx] {
        c @ b'0'..=b'9' => {
            *idx += 1;
            Rc::new(RefCell::new(Node {
                parent,
                inner: NodeInner::Leaf((c - b'0') as i32),
                color,
            }))
        }
        b'[' => parse_node(input, idx, parent, color),
        _ => unreachable!(),
    }
}

fn indent(level: usize) -> String {
    repeat(' ').take(level * 4).collect()
}

fn print_node(node: &NodeRef, level: usize) {
    let actual_node: Ref<Node> = node.borrow();
    let indentation = indent(level);
    print!("{}{:?}:", indentation, actual_node.color);
    match &actual_node.inner {
        NodeInner::Children(Children { left, right }) => {
            println!("[");
            print_node(left, level + 1);
            print_node(right, level + 1);
            println!("{}]", indentation);
        }
        NodeInner::Leaf(value) => println!("{}", value),
        _ => unreachable!(),
    }
}

fn check_explode_node(nr: &NodeRef, level: i32) -> Option<NodeRef> {
    if nr.borrow().is_leaf_pair() && level >= 4 {
        Some(nr.clone())
    } else {
        match &nr.borrow().inner {
            NodeInner::Children(Children { left, right }) => {
                check_explode_node(left, level + 1).or_else(|| check_explode_node(right, level + 1))
            },
            _ => None
        }
    }
}
//
// fn add_left_of(nr: NodeRef, value: i32) {
//     if let Some(pr) = nr.borrow().parent.upgrade() {
//         let parent = pr.borrow();
//         if parent.color == Color::Right {
//         } else {
//             add_left_of(pr, value);
//         }
//     }
// }

/*
         A

      B      C

   D    E  F   G

explode_left(E):
- E is Color::Right
- E has a parent (B)

*/

fn explode(nr: &NodeRef) {
    let (lv, rv) = nr.borrow().leaf_values().unwrap();
}

fn main() {
    println!("Hello, world!");
}

#[cfg(test)]
mod tests {
    use super::*;

    fn assert_pair(nr: &NodeRef, expected: (i32, i32)) {
        match &nr.borrow().inner {
            NodeInner::Children(Children{left, right}) => {
                assert_eq!(expected, (left.borrow().leaf_value().unwrap(), right.borrow().leaf_value().unwrap()))
            }
            _ => {
                print_node(nr, 0);
                assert!(false, "Nodes are different");
            }
        }
    }

    #[test]
    fn explode_test() {
        let root = parse("[[[[[9,8],1],2],3],4]");
        let to_explode = check_explode_node(&root, 0).unwrap();
        explode(&to_explode);
        print_node(&root, 0);
    }

    #[test]
    fn check_explode_test() {
        assert_pair(&check_explode_node(&parse("[[[[[9,8],1],2],3],4]"), 0).unwrap(), (9, 8));
        assert_pair(&check_explode_node(&parse("[7,[6,[5,[4,[3,2]]]]]"), 0).unwrap(), (3, 2));
        assert_pair(&check_explode_node(&parse("[[6,[5,[4,[3,2]]]],1]"), 0).unwrap(), (3, 2));
        assert_pair(&check_explode_node(&parse("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]"), 0).unwrap(), (7, 3));
        assert_pair(&check_explode_node(&parse("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"), 0).unwrap(), (3, 2));
    }

    #[test]
    fn parse_test() {
        for input in TEST_INPUT.split('\n') {
            parse_test_impl(input);
        }
    }

    fn parse_test_impl(input: &str) {
        let n = parse(input);
        print_node(&n, 0);
    }

    const TEST_INPUT: &'static str = "\
[1,2]
[[1,2],3]
[9,[8,7]]
[[1,9],[8,5]]
[[[[1,2],[3,4]],[[5,6],[7,8]]],9]
[[[9,[3,8]],[[0,9],6]],[[[3,7],[4,9]],3]]
[[[[1,3],[5,3]],[[1,3],[8,7]]],[[[4,9],[6,9]],[[8,2],[7,3]]]]";
}

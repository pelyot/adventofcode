use std::str::FromStr;

fn main() {
    solve_gold(parse_input(INPUT));
}
const INPUT: &'static str = include_str!("input");

enum Fold {
    FoldX(i32),
    FoldY(i32),
}

struct Input {
    points: Vec<(i32, i32)>,
    folds: Vec<Fold>,
}

fn parse_input(input: &str) -> Input {
    let (points_str, folds_str) = input.split_once("\n\n").expect("bad input");
    let points = points_str
        .split('\n')
        .filter(|l| !l.is_empty())
        .map(|l| l.split_once(',').expect("bad point"))
        .map(|(x_str, y_str)| {
            (
                i32::from_str(x_str).expect("bad int"),
                i32::from_str(y_str).expect("bad int"),
            )
        })
        .collect();
    let folds = folds_str
        .split('\n')
        .filter(|l| !l.is_empty())
        .map(|l| l.split_once('=').expect("bad fold"))
        .map(|(b, e)| {
            let nb = i32::from_str(e).expect("bad fold");
            if b.chars().last().unwrap() == 'y' {
                Fold::FoldY(nb)
            } else {
                Fold::FoldX(nb)
            }
        })
        .collect();
    Input { points, folds }
}

fn fold(points: &mut Vec<(i32, i32)>, fold: &Fold) {
    for p in points.iter_mut() {
        match *fold {
            Fold::FoldX(x) => {
                if p.0 > x {
                    p.0 = x - (p.0 - x);
                }
            }
            Fold::FoldY(y) => {
                if p.1 > y {
                    p.1 = y - (p.1 - y);
                }
            }
        }
    }
    points.sort();
    points.dedup();
}

fn solve_silver(mut input: Input) -> usize {
    fold(&mut input.points, &input.folds[0]);
    input.points.len()
}

fn solve_gold(mut input: Input) {
    for f in input.folds.iter() {
        fold(&mut input.points, f);
    }
    let max_x = input.points.iter().max_by_key(|p| p.0).expect("no max").0;
    let max_y = input.points.iter().max_by_key(|p| p.1).expect("no max").1;
    let mut grid = vec![vec!['.'; max_x as usize + 1]; max_y as usize + 1];
    for p in input.points {
        grid[p.1 as usize][p.0 as usize] = '#';
    }
    for row in grid {
        for c in row {
            print!("{}", c);
        }
        println!();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_silver() {
        assert_eq!(17, solve_silver(parse_input(SAMPLE)));
    }

    #[test]
    fn sample_gold() {
        solve_gold(parse_input(SAMPLE));
    }
    const SAMPLE: &'static str = "\
6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5";
}

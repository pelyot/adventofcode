fn main() {
    println!("Silver: {}", silver(INPUT));
    println!("Gold: {}", gold(INPUT));
}

#[derive(Copy, Clone, PartialEq, Debug)]
struct Segment {
    start: i32,
    end: i32,
}

impl Segment {
    fn new(start: i32, end: i32) -> Segment {
        assert!(start <= end);
        Segment { start, end }
    }
    fn new_with_size(start: i32, size: i32) -> Segment {
        assert!(size > 0);
        Segment {
            start,
            end: start + size - 1,
        }
    }
    fn from_str(s: &str) -> Segment {
        use std::str::FromStr;
        let mut it = s.split("..").map(|c| i32::from_str(c).unwrap());
        Segment {
            start: it.next().unwrap(),
            end: it.next().unwrap(),
        }
    }
    fn intersection(&self, other: &Segment) -> Option<Segment> {
        let a = self.start;
        let b = self.end;
        let c = other.start;
        let d = other.end;
        assert!(a <= b);
        assert!(c <= d);
        // c d a b
        if d < a {
            None
        } else if d == a {
            Some(Segment { start: a, end: a })
        }
        // a b c d
        else if b < c {
            None
        } else if b == c {
            Some(Segment { start: b, end: c })
        }
        // c a b d
        else if c <= a && b <= d {
            Some(Segment { start: a, end: b })
        }
        // a c d b
        else if a <= c && d <= b {
            Some(Segment { start: c, end: d })
        }
        // c a d b
        else if c <= a && a <= d && d <= b {
            Some(Segment { start: a, end: d })
        }
        // a c b d
        else if a <= c && c <= b && b <= d {
            Some(Segment { start: c, end: b })
        } else {
            unreachable!();
        }
    }
    fn split(&self, other: &Segment) -> Vec<Segment> {
        let a = self.start;
        let b = self.end;
        let c = other.start;
        let d = other.end;
        assert!(a <= b);
        assert!(c <= d);
        // c d a b
        if d < a {
            vec![Segment::new(a, b)]
        } else if d == a {
            if a < b {
                vec![Segment::new(a, a), Segment::new(a + 1, b)]
            } else {
                vec![Segment::new(a, b)]
            }
        }
        // a b c d
        else if b < c {
            vec![Segment::new(a, b)]
        } else if b == c {
            if a < b {
                vec![Segment::new(a, b - 1), Segment::new(b, b)]
            } else {
                vec![Segment::new(a, b)]
            }
        }
        // c a b d
        else if c <= a && b <= d {
            vec![Segment::new(a, b)]
        }
        // a c d b
        else if a <= c && d <= b {
            let mut res = Vec::new();
            if a < c {
                res.push(Segment::new(a, c - 1));
            }
            res.push(Segment::new(c, d));
            if d < b {
                res.push(Segment::new(d + 1, b))
            }
            res
        }
        // c a d b
        else if c <= a && a <= d && d <= b {
            let mut res = Vec::new();
            res.push(Segment::new(a, d));
            if d < b {
                res.push(Segment::new(d + 1, b))
            }
            res
        }
        // a c b d
        else if a <= c && c <= b && b <= d {
            let mut res = Vec::new();
            if a < c {
                res.push(Segment::new(a, c - 1));
            }
            res.push(Segment::new(c, b));
            res
        } else {
            unreachable!();
        }
    }
    fn length(&self) -> usize {
        (self.end - self.start + 1) as usize
    }
    fn contains(&self, other: &Segment) -> bool {
        self.start <= other.start && self.end >= other.end
    }
}

#[derive(Debug, PartialEq, Clone)]
struct Cube {
    x: Segment,
    y: Segment,
    z: Segment,
}

impl Cube {
    fn from_coords(x1: i32, x2: i32, y1: i32, y2: i32, z1: i32, z2: i32) -> Cube {
        Cube {
            x: Segment::new(x1, x2),
            y: Segment::new(y1, y2),
            z: Segment::new(z1, z2),
        }
    }
    fn from_str(line: &str) -> Cube {
        let mut it = line
            .split(',')
            .map(|s| s.split('=').skip(1).next().unwrap())
            .map(|s| Segment::from_str(s));
        Cube {
            x: it.next().unwrap(),
            y: it.next().unwrap(),
            z: it.next().unwrap(),
        }
    }
    fn regular(pos: i32, size: i32) -> Cube {
        Cube {
            x: Segment::new_with_size(pos, size),
            y: Segment::new_with_size(pos, size),
            z: Segment::new_with_size(pos, size),
        }
    }
    fn flat(pos: i32, size: i32) -> Cube {
        Cube {
            x: Segment::new_with_size(pos, size),
            y: Segment::new_with_size(pos, size),
            z: Segment::new(0, 0),
        }
    }
    fn intersection(&self, other: &Cube) -> Option<Cube> {
        let inter = (
            self.x.intersection(&other.x),
            self.y.intersection(&other.y),
            self.z.intersection(&other.z),
        );
        match inter {
            (Some(x), Some(y), Some(z)) => Some(Cube { x, y, z }),
            _ => None,
        }
    }
    fn volume(&self) -> usize {
        let volume = self.x.length() * self.y.length() * self.z.length();
        volume
    }
    fn split(&self, other: &Cube) -> Vec<Cube> {
        let mut res = Vec::new();
        for x in self.x.split(&other.x) {
            for y in self.y.split(&other.y) {
                for z in self.z.split(&other.z) {
                    res.push(Cube { x, y, z })
                }
            }
        }
        res
    }
    fn contains(&self, other: &Cube) -> bool {
        self.x.contains(&other.x) && self.y.contains(&other.y) && self.z.contains(&other.z)
    }
}

#[derive(Debug)]
struct Instruction {
    on: bool,
    cube: Cube,
}

impl Instruction {
    fn from_str(l: &str) -> Instruction {
        let mut it = l.split(' ');
        Instruction {
            on: it.next().unwrap() == "on",
            cube: Cube::from_str(it.next().unwrap()),
        }
    }
}

fn parse(input: &str) -> Vec<Instruction> {
    input
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(|s| Instruction::from_str(s))
        .collect()
}

fn split_cubes(cubes: &[Cube], new_cube: &Cube) -> Vec<Cube> {
    cubes.iter().flat_map(|c| c.split(new_cube)).collect()
}

fn silver(input: &str) -> usize {
    let stencil = Cube::from_coords(-50, 50, -50, 50, -50, 50);
    let instructions = parse(input);
    let mut cubes_on = Vec::new();
    for i in 0..instructions.len() {
        let inst = &instructions[i];
        if let Some(cube) = inst.cube.intersection(&stencil) {
            cubes_on = split_cubes(&cubes_on, &cube);
            cubes_on.retain(|c| !cube.contains(c));
            if inst.on {
                cubes_on.push(cube);
            }
        }
    }
    cubes_on.iter().map(|cube| cube.volume()).sum::<usize>()
}

fn gold(input: &str) -> usize {
    let instructions = parse(input);
    let mut cubes_on: Vec<Cube> = Vec::new();
    for i in 0..instructions.len() {
        println!("{}/{}", i, instructions.len());
        let inst = &instructions[i];
        let cube = &inst.cube;
        cubes_on = cubes_on
            .iter()
            .flat_map(|c| c.split(cube))
            .filter(|c| !cube.contains(c))
            .collect();
        if inst.on {
            cubes_on.push(cube.clone());
        }
    }
    cubes_on.iter().map(|cube| cube.volume()).sum::<usize>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn gold_sample() {
        assert_eq!(
            gold(
                "on x=-5..47,y=-31..22,z=-19..33
on x=-44..5,y=-27..21,z=-14..35
on x=-49..-1,y=-11..42,z=-10..38
on x=-20..34,y=-40..6,z=-44..1
off x=26..39,y=40..50,z=-2..11
on x=-41..5,y=-41..6,z=-36..8
off x=-43..-33,y=-45..-28,z=7..25
on x=-33..15,y=-32..19,z=-34..11
off x=35..47,y=-46..-34,z=-11..5
on x=-14..36,y=-6..44,z=-16..29
on x=-57795..-6158,y=29564..72030,z=20435..90618
on x=36731..105352,y=-21140..28532,z=16094..90401
on x=30999..107136,y=-53464..15513,z=8553..71215
on x=13528..83982,y=-99403..-27377,z=-24141..23996
on x=-72682..-12347,y=18159..111354,z=7391..80950
on x=-1060..80757,y=-65301..-20884,z=-103788..-16709
on x=-83015..-9461,y=-72160..-8347,z=-81239..-26856
on x=-52752..22273,y=-49450..9096,z=54442..119054
on x=-29982..40483,y=-108474..-28371,z=-24328..38471
on x=-4958..62750,y=40422..118853,z=-7672..65583
on x=55694..108686,y=-43367..46958,z=-26781..48729
on x=-98497..-18186,y=-63569..3412,z=1232..88485
on x=-726..56291,y=-62629..13224,z=18033..85226
on x=-110886..-34664,y=-81338..-8658,z=8914..63723
on x=-55829..24974,y=-16897..54165,z=-121762..-28058
on x=-65152..-11147,y=22489..91432,z=-58782..1780
on x=-120100..-32970,y=-46592..27473,z=-11695..61039
on x=-18631..37533,y=-124565..-50804,z=-35667..28308
on x=-57817..18248,y=49321..117703,z=5745..55881
on x=14781..98692,y=-1341..70827,z=15753..70151
on x=-34419..55919,y=-19626..40991,z=39015..114138
on x=-60785..11593,y=-56135..2999,z=-95368..-26915
on x=-32178..58085,y=17647..101866,z=-91405..-8878
on x=-53655..12091,y=50097..105568,z=-75335..-4862
on x=-111166..-40997,y=-71714..2688,z=5609..50954
on x=-16602..70118,y=-98693..-44401,z=5197..76897
on x=16383..101554,y=4615..83635,z=-44907..18747
off x=-95822..-15171,y=-19987..48940,z=10804..104439
on x=-89813..-14614,y=16069..88491,z=-3297..45228
on x=41075..99376,y=-20427..49978,z=-52012..13762
on x=-21330..50085,y=-17944..62733,z=-112280..-30197
on x=-16478..35915,y=36008..118594,z=-7885..47086
off x=-98156..-27851,y=-49952..43171,z=-99005..-8456
off x=2032..69770,y=-71013..4824,z=7471..94418
on x=43670..120875,y=-42068..12382,z=-24787..38892
off x=37514..111226,y=-45862..25743,z=-16714..54663
off x=25699..97951,y=-30668..59918,z=-15349..69697
off x=-44271..17935,y=-9516..60759,z=49131..112598
on x=-61695..-5813,y=40978..94975,z=8655..80240
off x=-101086..-9439,y=-7088..67543,z=33935..83858
off x=18020..114017,y=-48931..32606,z=21474..89843
off x=-77139..10506,y=-89994..-18797,z=-80..59318
off x=8476..79288,y=-75520..11602,z=-96624..-24783
on x=-47488..-1262,y=24338..100707,z=16292..72967
off x=-84341..13987,y=2429..92914,z=-90671..-1318
off x=-37810..49457,y=-71013..-7894,z=-105357..-13188
off x=-27365..46395,y=31009..98017,z=15428..76570
off x=-70369..-16548,y=22648..78696,z=-1892..86821
on x=-53470..21291,y=-120233..-33476,z=-44150..38147
off x=-93533..-4276,y=-16170..68771,z=-104985..-24507"
            ),
            2758514936282235
        );
    }

    #[test]
    fn silver_sample() {
        assert_eq!(silver(SAMPLE), 590784);
    }

    #[test]
    fn silver_small_sample() {
        assert_eq!(silver(SMALL_SAMPLE), 39);
    }

    #[test]
    fn cube_contains_test() {
        assert!(Cube::regular(0, 1).contains(&Cube::regular(0, 1)));
        assert!(Cube::regular(0, 2).contains(&Cube::regular(0, 1)));
        assert!(Cube::regular(0, 2).contains(&Cube::regular(1, 1)));
        assert!(!Cube::regular(0, 2).contains(&Cube::regular(1, 2)));
    }

    #[test]
    fn cube_split_test() {
        // Cube::flat(0, 2).split(&Cube::flat(1, 2));
        // dbg!(Cube::flat(0,3).split(&Cube::flat(0, 4)));
    }

    #[test]
    fn split_segment_test() {
        split_segment_test_impl(0, 0, 0, 1, vec![Segment::new(0, 0)]);
        split_segment_test_impl(0, 0, 0, 0, vec![Segment::new(0, 0)]);
        // a b c d
        split_segment_test_impl(1, 2, 3, 4, vec![Segment::new(1, 2)]);
        split_segment_test_impl(1, 2, 2, 4, vec![Segment::new(1, 1), Segment::new(2, 2)]);
        // c d a b
        split_segment_test_impl(3, 4, 1, 2, vec![Segment::new(3, 4)]);
        split_segment_test_impl(2, 4, 1, 2, vec![Segment::new(2, 2), Segment::new(3, 4)]);
        // c a b d
        split_segment_test_impl(1, 2, 0, 3, vec![Segment::new(1, 2)]);
        // a c d b
        split_segment_test_impl(
            0,
            3,
            1,
            2,
            vec![Segment::new(0, 0), Segment::new(1, 2), Segment::new(3, 3)],
        );
        split_segment_test_impl(0, 2, 1, 2, vec![Segment::new(0, 0), Segment::new(1, 2)]);
        split_segment_test_impl(0, 3, 0, 2, vec![Segment::new(0, 2), Segment::new(3, 3)]);
        // c a d b
        split_segment_test_impl(2, 4, 1, 3, vec![Segment::new(2, 3), Segment::new(4, 4)]);
        split_segment_test_impl(2, 4, 1, 4, vec![Segment::new(2, 4)]);
        // a c b d
        split_segment_test_impl(1, 3, 2, 4, vec![Segment::new(1, 1), Segment::new(2, 3)]);
        // other
        split_segment_test_impl(0, 0, 0, 0, vec![Segment::new(0, 0)]);
    }

    #[test]
    fn cube_intersection_test() {
        assert_eq!(
            Cube::regular(0, 3).intersection(&Cube::regular(1, 1)),
            Some(Cube::regular(1, 1))
        );
        assert_eq!(
            Cube::regular(0, 3).intersection(&Cube::regular(1, 3)),
            Some(Cube::regular(1, 2))
        )
    }

    #[test]
    fn segment_intersection_test() {
        // c d a b
        segment_intersection_test_impl(1, 2, 3, 4, None);
        segment_intersection_test_impl(1, 2, 2, 4, Some((2, 2)));
        // a b c d
        segment_intersection_test_impl(3, 4, 1, 2, None);
        segment_intersection_test_impl(2, 4, 1, 2, Some((2, 2)));
        // c a b d
        segment_intersection_test_impl(1, 2, 0, 3, Some((1, 2)));
        // a c d b
        segment_intersection_test_impl(0, 3, 1, 2, Some((1, 2)));
        // c a d b
        segment_intersection_test_impl(2, 4, 1, 3, Some((2, 3)));
        // a c b d
        segment_intersection_test_impl(1, 3, 2, 4, Some((2, 3)));
    }

    fn split_segment_test_impl(a: i32, b: i32, c: i32, d: i32, e: Vec<Segment>) {
        let s1 = Segment::new(a, b);
        let s2 = Segment::new(c, d);
        assert_eq!(s1.split(&s2), e);
    }
    fn segment_intersection_test_impl(a: i32, b: i32, c: i32, d: i32, e: Option<(i32, i32)>) {
        let s1 = Segment::new(a, b);
        let s2 = Segment::new(c, d);
        let expected = e.map(|(u, v)| Segment::new(u, v));
        assert_eq!(s1.intersection(&s2), expected);
    }

    const SMALL_SAMPLE: &'static str = "\
on x=10..12,y=10..12,z=10..12
on x=11..13,y=11..13,z=11..13
off x=9..11,y=9..11,z=9..11
on x=10..10,y=10..10,z=10..10";

    const SAMPLE: &'static str = "\
on x=-20..26,y=-36..17,z=-47..7
on x=-20..33,y=-21..23,z=-26..28
on x=-22..28,y=-29..23,z=-38..16
on x=-46..7,y=-6..46,z=-50..-1
on x=-49..1,y=-3..46,z=-24..28
on x=2..47,y=-22..22,z=-23..27
on x=-27..23,y=-28..26,z=-21..29
on x=-39..5,y=-6..47,z=-3..44
on x=-30..21,y=-8..43,z=-13..34
on x=-22..26,y=-27..20,z=-29..19
off x=-48..-32,y=26..41,z=-47..-37
on x=-12..35,y=6..50,z=-50..-2
off x=-48..-32,y=-32..-16,z=-15..-5
on x=-18..26,y=-33..15,z=-7..46
off x=-40..-22,y=-38..-28,z=23..41
on x=-16..35,y=-41..10,z=-47..6
off x=-32..-23,y=11..30,z=-14..3
on x=-49..-5,y=-3..45,z=-29..18
off x=18..30,y=-20..-8,z=-3..13
on x=-41..9,y=-7..43,z=-33..15
on x=-54112..-39298,y=-85059..-49293,z=-27449..7877
on x=967..23432,y=45373..81175,z=27513..53682
";
}

const INPUT: &'static str = include_str!("input");

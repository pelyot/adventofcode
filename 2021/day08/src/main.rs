#![feature(test)]
extern crate test;
use std::io::{BufRead, BufReader};
use std::str::FromStr;
use thiserror::Error;

const NB_SEGMENTS_IN_DIGIT: [usize; 10] = [6, 2, 5, 5, 4, 5, 6, 3, 7, 6];
const INPUT: &'static str = include_str!("input");

struct Entry {
    patterns: Vec<Vec<u8>>,
    digits: Vec<Vec<u8>>,
}

#[derive(Error, Debug)]
enum ParseError {
    #[error("Failed to parse entry: cannot split line on ' | ': {0}")]
    BadStructure(String),
    #[error("Failed to parse entry: Wrong signal count (actual {1} vs expected {2}) on line: {0}")]
    WrongSignalCount(String, usize, usize),
}

impl FromStr for Entry {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (patterns_str, digits_str) = s
            .split_once(" | ")
            .ok_or_else(|| ParseError::BadStructure(s.to_owned()))?;
        let parse_signals = |input: &str, expected_len: usize| -> Result<Vec<Vec<u8>>, Self::Err> {
            let signals: Vec<Vec<u8>> = input
                .split_ascii_whitespace()
                .map(|w| w.as_bytes().to_vec())
                .collect();
            if signals.len() != expected_len {
                return Err(ParseError::WrongSignalCount(
                    s.to_owned(),
                    signals.len(),
                    expected_len,
                ));
            }
            Ok(signals)
        };
        Ok(Entry {
            patterns: parse_signals(patterns_str, 10)?,
            digits: parse_signals(digits_str, 4)?,
        })
    }
}

impl Entry {
    fn patterns_signals(&self) -> Vec<Signals> {
        self.patterns
            .iter()
            .map(|digit| Signals::from_bytes(digit))
            .collect()
    }
    fn digits_signals(&self) -> Vec<Signals> {
        self.digits
            .iter()
            .map(|digit| Signals::from_bytes(digit))
            .collect()
    }
}
fn parse_input(input: &str) -> Vec<Entry> {
    let reader = BufReader::new(input.as_bytes());
    reader
        .lines()
        .map(|r| r.expect("Failed to get line"))
        .map(|l| l.parse::<Entry>())
        .map(|e| e.expect("Failed to parse"))
        .collect()
}

fn solve_silver(entries: Vec<Entry>) -> usize {
    entries
        .iter()
        .flat_map(|e| e.digits.iter().map(|d| d.len()))
        .filter(|&n| {
            n == NB_SEGMENTS_IN_DIGIT[1]
                || n == NB_SEGMENTS_IN_DIGIT[4]
                || n == NB_SEGMENTS_IN_DIGIT[7]
                || n == NB_SEGMENTS_IN_DIGIT[8]
        })
        .count()
}

struct Mapping(Vec<u8>);
struct Signals([bool; 7]);
struct Segments([bool; 7]);

impl Mapping {
    fn map_signals(&self, signals: &Signals) -> Segments {
        let mut segments = [false; 7];
        for i in 0..7 {
            let dest_segment = self.0[i] as usize;
            segments[dest_segment] = signals.0[i];
        }
        Segments(segments)
    }
    fn is_valid(&self, digits: &[Signals]) -> bool {
        digits
            .iter()
            .map(|signals| self.map_signals(signals))
            .all(|segments| segments.is_valid())
    }
    fn read_number(&self, entry: &Entry) -> usize {
        let digits = entry.digits_signals();
        let mut acc: usize = 0;
        for segments in digits.iter().map(|signals| self.map_signals(signals)) {
            acc *= 10;
            acc += segments.number() as usize;
        }
        acc
    }
}

const NUMBERS_SEGMENTS: [[bool; 7]; 10] = [
    [true, true, true, false, true, true, true],
    [false, false, true, false, false, true, false],
    [true, false, true, true, true, false, true],
    [true, false, true, true, false, true, true],
    [false, true, true, true, false, true, false],
    [true, true, false, true, false, true, true],
    [true, true, false, true, true, true, true],
    [true, false, true, false, false, true, false],
    [true, true, true, true, true, true, true],
    [true, true, true, true, false, true, true],
];

impl Segments {
    fn is_valid(&self) -> bool {
        NUMBERS_SEGMENTS.contains(&self.0)
    }
    fn number(&self) -> u8 {
        NUMBERS_SEGMENTS.iter().position(|e| *e == self.0).expect("Not a number!") as u8
    }
}

impl Signals {
    fn from_bytes(bytes: &[u8]) -> Signals {
        let mut signals = [false; 7];
        for b in bytes {
            let signal_idx = (b - b'a') as usize;
            signals[signal_idx] = true;
        }
        Signals(signals)
    }
}

fn deduce(entry: &Entry) -> usize {
    let patterns = entry.patterns_signals();
    let mapping = permutations([0u8, 1, 2, 3, 4, 5, 6].as_slice(), 7)
        .map(|p| Mapping(p))
        .find(|m| m.is_valid(&patterns))
        .expect("No correct mapping found");
    mapping.read_number(&entry)
}

fn solve_gold(entries: Vec<Entry>) -> usize {
    entries.iter().map(deduce).sum()
}

fn main() {
    println!("Silver: {}", solve_silver(parse_input(INPUT)));
    println!("Gold: {}", solve_gold(parse_input(INPUT)));
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn bench_gold(b: &mut Bencher) {
        b.iter(|| solve_gold(parse_input(INPUT)));
    }

    #[test]
    fn test_silver() {
        assert_eq!(solve_silver(parse_input(TEST_INPUT)), 26);
    }

    #[test]
    fn test_gold() {
        assert_eq!(solve_gold(parse_input(TEST_INPUT)), 61229);
    }

    #[test]
    fn test_gold_basic() {
        let entries = parse_input("acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf");
        assert_eq!(deduce(&entries[0]), 5353);
    }

    const TEST_INPUT: &'static str =
        "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";
}

pub struct Permutations<T> {
    vec: Vec<T>,
    subsize: usize,
    first: bool,
}

impl<T: Clone + Ord> Iterator for Permutations<T> {
    type Item = Vec<T>;

    fn next(&mut self) -> Option<Vec<T>> {
        let n = self.vec.len();
        let r = self.subsize;
        if n == 0 || r == 0 || r > n {
            return None;
        }
        if self.first {
            self.vec.sort();
            self.first = false;
        } else if self.vec[r - 1] < self.vec[n - 1] {
            let mut j = r;
            while self.vec[j] <= self.vec[r - 1] {
                j += 1;
            }
            self.vec.swap(r - 1, j);
        } else {
            self.vec[r..n].reverse();
            let mut j = r - 1;
            while j > 0 && self.vec[j - 1] >= self.vec[j] {
                j -= 1;
            }
            if j == 0 {
                return None;
            }
            let mut l = n - 1;
            while self.vec[j - 1] >= self.vec[l] {
                l -= 1;
            }
            self.vec.swap(j - 1, l);
            self.vec[j..n].reverse();
        }
        Some(self.vec[0..r].to_vec())
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let n = self.vec.len();
        let r = self.subsize;
        if n == 0 || r == 0 || r > n {
            (0, Some(0))
        } else {
            (1, Some(((n - r + 1)..=n).product()))
        }
    }
}

pub fn permutations<T: Clone + Ord>(s: &[T], subsize: usize) -> Permutations<T> {
    Permutations {
        vec: s.to_vec(),
        subsize,
        first: true,
    }
}

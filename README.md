# Advent of Code

http://adventofcode.com

## Links:

| Participant | Language | Repo |
|-------------|----------|------|
| me | rust | https://gitlab.com/pelyot/adventofcode |
| serans | rust | https://github.com/serans/aoc2022 |
| sebdotv| haskell | https://github.com/sebdotv/aoc22-haskell|
| Micmac2 | rust | https://github.com/Micmac2/AdventOfCode|
| farhadir | rust | https://github.com/farhadir/advent-2022|
| Mihnea27| elixir | https://github.com/Mihnea27/Advent-Of-Code-2022|
| samske | KotlinScript | https://gitlab.com/samske/aoc2022 |
| thodkatz | Julia | https://github.com/thodkatz/adventofcode |
| dave | excel | https://drive.google.com/drive/folders/19sxMbA4YRVO-HXM0HCJy2QGgEjDuHT5S?usp=sharing |
| picheryc | python | https://gitlab.com/picheryc/advent_of_code |

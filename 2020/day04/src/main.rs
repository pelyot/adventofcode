use std::io::{BufRead, BufReader};
#[macro_use] extern crate lazy_static;
use regex::Regex;

fn main() {
    let input = include_str!("input");
    println!("{}", count_valid_passports(input, Passport::process_a));
    println!("{}", count_valid_passports(input, Passport::process_b));
}

fn count_valid_passports(input : &str, processing: fn(&mut Passport, &str, &str)) -> usize {
    let reader = BufReader::new(input.as_bytes());
    let mut passport = Passport::new();
    let mut count = 0;
    lazy_static! {
        static ref RE: Regex = Regex::new(r"([^\s]{3}):([^\s]+)").unwrap();
    }

    for line in reader.lines().flatten() {
        if line.is_empty() {
            if passport.is_valid() {
                count += 1;
            }
            passport = Passport::new();
        } else {
            for captures in RE.captures_iter(&line) {
                let data = captures.get(2).unwrap().as_str();
                let field = captures.get(1).unwrap().as_str();
                processing(&mut passport, field, data);
            }
        }
    }
    if passport.is_valid() {
        count += 1;
    }
    count
}


struct Passport {
/*
    byr (Birth Year)
    iyr (Issue Year)
    eyr (Expiration Year)
    hgt (Height)
    hcl (Hair Color)
    ecl (Eye Color)
    pid (Passport ID)
    cid (Country ID)
    */
    byr: bool,
    iyr: bool,
    eyr: bool,
    hgt: bool,
    hcl: bool,
    ecl: bool,
    pid: bool,
    cid: bool

}

impl Passport {
    fn new() -> Passport {
        Passport{
            byr: false,
            iyr: false,
            eyr: false,
            hgt: false,
            hcl: false,
            ecl: false,
            pid: false,
            cid: false,
        }
    }

    fn process_a(&mut self, field: &str, _data: &str) {
        match field {
            "byr" => self.byr = true,
            "iyr" => self.iyr = true,
            "eyr" => self.eyr = true,
            "hgt" => self.hgt = true,
            "hcl" => self.hcl = true,
            "ecl" => self.ecl = true,
            "pid" => self.pid = true,
            "cid" => self.cid = true,
            _ => panic!("unknown field")
        }
    }

    fn process_b(&mut self, field: &str, data: &str) {
        match field {
            "byr" => self.byr(data),
            "iyr" => self.iyr(data),
            "eyr" => self.eyr(data),
            "hgt" => self.hgt(data),
            "hcl" => self.hcl(data),
            "ecl" => self.ecl(data),
            "pid" => self.pid(data),
            "cid" => self.cid = true,
            _ => panic!("unknown field")
        }
    }

    fn byr(&mut self, data: &str) {
        if let Ok(byr) = data.parse::<usize>() {
            if byr >= 1920 && byr <= 2002 {
                self.byr = true;
            }
        }
    }

    fn iyr(&mut self, data: &str) {
        if let Ok(iyr) = data.parse::<usize>() {
            if iyr >= 2010 && iyr <= 2020 {
                self.iyr = true;
            }
        }
    }

    fn eyr(&mut self, data: &str) {
        if let Ok(eyr) = data.parse::<usize>() {
            if eyr >= 2020 && eyr <= 2030 {
                self.eyr = true;
            }
        }
    }

    fn hgt(&mut self, data: &str) {
        lazy_static!{
            static ref RE_HGT : Regex = Regex::new(r"(\d+)(in|cm)").unwrap();
        }
        if let Some(caps) = RE_HGT.captures(data) {
            let value = caps.get(1).and_then(|v| v.as_str().parse::<usize>().ok());
            let unit = caps.get(2).map(|u| u.as_str());
            match (value, unit) {
                (Some(h), Some("cm")) if h >= 150 && h <= 193 => self.hgt = true,
                (Some(h), Some("in")) if h >= 59 && h <= 76 => self.hgt = true,
                _ => ()
            }
        }
    }

    fn hcl(&mut self, data: &str) {
        lazy_static!{
            static ref RE_HCL : Regex = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
        }
        self.hcl = RE_HCL.is_match(data);
    }

    fn ecl(&mut self, data: &str) {
        self.ecl = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].contains(&data);
    }

    fn pid(&mut self, data: &str) {
        lazy_static!{
            static ref RE_PID: Regex = Regex::new(r"^\d{9}$").unwrap();
        }
        self.pid = RE_PID.is_match(data);
    }

    fn is_valid(&self) -> bool {
        /*
        byr (Birth Year) - four digits; at least 1920 and at most 2002.
        iyr (Issue Year) - four digits; at least 2010 and at most 2020.
        eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
        hgt (Height) - a number followed by either cm or in:

            If cm, the number must be at least 150 and at most 193.
            If in, the number must be at least 59 and at most 76.

        hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
        ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
        pid (Passport ID) - a nine-digit number, including leading zeroes.
        */
        self.byr && self.iyr && self.eyr && self.hgt && self.hcl && self.ecl && self.pid
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample_a() {
        let input = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in";

        assert_eq!(count_valid_passports(&input, Passport::process_a), 2);
    }

    #[test]
    fn sample_b_valid() {
        let input = "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719";
        assert_eq!(count_valid_passports(&input, Passport::process_b), 4);
    }

    #[test]
    fn sample_b_invalid() {
        let input = "eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007";
        assert_eq!(count_valid_passports(&input, Passport::process_b), 0);
    }

    #[test]
    fn sample_b_other() {
        let input = "hgt:193cm iyr:2020 eyr:2026
        pid:0136642346 ecl:hzl hcl:#efcc98 byr:1995";
        assert_eq!(count_valid_passports(&input, Passport::process_b), 0);
    }
}
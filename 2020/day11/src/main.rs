#![feature(test)]
extern crate test;
use std::str::FromStr;

const INPUT: &str = include_str!("input");

fn main() {
    println!("{}", solve_a(INPUT.parse::<Area>().unwrap()));
    println!("{}", solve_b(INPUT.parse::<Area>().unwrap()));
}

fn solve_a(mut area: Area) -> usize {
    while area.step(4, Area::count_occupied_neighbors) {}
    area.count_occupied()
}

fn solve_b(mut area: Area) -> usize {
    while area.step(5, Area::count_occupied_visible) {}
    area.count_occupied()
}

#[derive(PartialEq, Clone, Debug)]
enum Square {
    Floor,
    Free,
    Occupied,
}

struct Area {
    squares: Vec<Vec<Square>>,
}

impl Area {
    fn get(&self, r: usize, c: usize) -> Square {
        self.squares
            .get(r)
            .and_then(|row| row.get(c))
            .unwrap_or(&Square::Floor)
            .clone()
    }

    fn count_occupied_neighbors(&self, r: usize, c: usize) -> usize {
        let min_r = if r == 0 { 0 } else { r - 1 };
        let min_c = if c == 0 { 0 } else { c - 1 };
        let mut count = 0;
        for ri in min_r..=(r + 1) {
            for ci in min_c..=(c + 1) {
                if (ri, ci) == (r, c) {
                    continue;
                }
                if self.get(ri, ci) == Square::Occupied {
                    count += 1;
                }
            }
        }
        count
    }

    fn count_occupied_visible(&self, r: usize, c: usize) -> usize {
        let steps = [
            (0, 1),
            (1, 0),
            (0, -1),
            (-1, 0),
            (1, 1),
            (-1, -1),
            (1, -1),
            (-1, 1),
        ];
        steps
            .iter()
            .map(|s| self.ray(r, c, s))
            .sum()
    }

    fn ray(&self, r: usize, c: usize, step: &(i64, i64)) -> usize {
        let max_r = self.squares.len() as i64;
        let max_c = self.squares[0].len() as i64;
        let mut r = r as i64;
        let mut c = c as i64;
        r += step.0;
        c += step.1;
        while r >= 0 && r < max_r && c >= 0 && c < max_c {
            match self.squares[r as usize][c as usize] {
                Square::Occupied => return 1,
                Square::Free => return 0,
                Square::Floor => (),
            }
            r += step.0;
            c += step.1;
        }
        0
    }

    fn count_occupied(&self) -> usize {
        self.squares
            .iter()
            .map(|row| row.iter().filter(|&sq| *sq == Square::Occupied).count())
            .sum()
    }

    fn step(
        &mut self,
        threshold: usize,
        count_neighbors: fn(&Self, usize, usize) -> usize,
    ) -> bool {
        let mut changed = false;
        self.squares = self
            .squares
            .iter()
            .enumerate()
            .map(|(r, row)| {
                row.iter()
                    .enumerate()
                    .map(|(c, square)| match square {
                        Square::Floor => Square::Floor,
                        Square::Free => {
                            if count_neighbors(self, r, c) == 0 {
                                changed = true;
                                Square::Occupied
                            } else {
                                Square::Free
                            }
                        }
                        Square::Occupied => {
                            if count_neighbors(self, r, c) >= threshold {
                                changed = true;
                                Square::Free
                            } else {
                                Square::Occupied
                            }
                        }
                    })
                    .collect()
            })
            .collect();
        changed
    }

    fn print(&self) {
        self.squares
            .iter()
            .map(|row| {
                row.iter()
                    .map(|sq| match sq {
                        Square::Floor => '.',
                        Square::Free => 'L',
                        Square::Occupied => '#',
                    })
                    .collect::<String>()
            })
            .for_each(|s| println!("{}", s));
    }
}

#[derive(Debug)]
struct ParseAreaError;

impl FromStr for Area {
    type Err = ParseAreaError;

    fn from_str(s: &str) -> Result<Area, Self::Err> {
        let squares = s
            .split_whitespace()
            .map(|l| {
                l.chars()
                    .map(|c| match c {
                        'L' => Square::Free,
                        '#' => Square::Occupied,
                        '.' => Square::Floor,
                        _ => unreachable!(),
                    })
                    .collect()
            })
            .collect();
        Ok(Area { squares })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn sample_a() {
        assert_eq!(solve_a(TEST_INPUT.parse::<Area>().unwrap()), 37)
    }

    #[test]
    fn sample_b() {
        assert_eq!(solve_a(TEST_INPUT.parse::<Area>().unwrap()), 26)
    }

    #[bench]
    fn input_a(b: &mut Bencher) {
        b.iter(|| assert_eq!(solve_a(INPUT.parse::<Area>().unwrap()), 2468));
    }

    #[bench]
    fn input_b(b: &mut Bencher) {
        b.iter(|| assert_eq!(solve_b(INPUT.parse::<Area>().unwrap()), 2214));
    }

    #[test]
    fn count_occupied_neighbors_test() {
        let mut area = TEST_INPUT.parse::<Area>().unwrap();
        area.step(4, Area::count_occupied_neighbors);
        area.print();
        assert_eq!(area.count_occupied_neighbors(0, 0), 2);
        assert_eq!(area.count_occupied_neighbors(0, 1), 5);
        assert_eq!(area.count_occupied_neighbors(0, 2), 4);
    }

    #[test]
    fn count_visible_1() {
        let area = ".......#.
                    ...#.....
                    .#.......
                    .........
                    ..#L....#
                    ....#....
                    .........
                    #........
                    ...#....."
            .parse::<Area>()
            .unwrap();
        assert_eq!(area.get(4, 8), Square::Occupied);
        assert_eq!(area.ray(4, 3, &(0, 1)), 1);
        assert_eq!(area.count_occupied_visible(4, 3), 8);
    }

    #[test]
    fn count_visible_2() {
        let area = ".............
                    .L.L.#.#.#.#.
                    ............."
            .parse::<Area>()
            .unwrap();
        assert_eq!(area.count_occupied_visible(1, 1), 0);
    }

    const TEST_INPUT: &str = "L.LL.LL.LL
                                LLLLLLL.LL
                                L.L.L..L..
                                LLLL.LL.LL
                                L.LL.LL.LL
                                L.LLLLL.LL
                                ..L.L.....
                                LLLLLLLLLL
                                L.LLLLLL.L
                                L.LLLLL.LL";
}

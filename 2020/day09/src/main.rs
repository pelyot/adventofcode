#![feature(test)]
extern crate test;

const INPUT: &str = include_str!("input");

fn main() {
    println!("{}", solve_a(parse(INPUT)));
    println!("{}", solve_b(parse(INPUT)));
}

fn parse(input: &str) -> Vec<u64> {
    input
        .split_whitespace()
        .flat_map(|l| l.parse::<u64>())
        .collect()
}

fn solve_a(input: Vec<u64>) -> u64 {
    do_solve_a(&input, 25)
}

fn solve_b(input: Vec<u64>) -> u64 {
    do_solve_b(&input, do_solve_a(&input, 25))
}

fn do_solve_a(input: &[u64], preamble_len: usize) -> u64 {
    *input[..]
        .windows(preamble_len + 1)
        .find(|w| {
            let last = w.last().unwrap();
            let w = &w[..preamble_len];
            for i in 0..preamble_len {
                for j in i..preamble_len {
                    if w[i] + w[j] == *last {
                        return false;
                    }
                }
            }
            true
        })
        .unwrap()
        .last()
        .unwrap()
}

fn do_solve_b(input: &[u64], number: u64) -> u64 {
    for i in 0..input.len() {
        let mut acc = 0;
        let mut j = i;
        while acc < number {
            acc += input[j];
            j += 1;
        }
        if acc == number {
            let range = &input[i..j];
            return range.iter().min().unwrap() + range.iter().max().unwrap();
        }
    }
    0
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    const TEST_INPUT: &str = "35
        20
        15
        25
        47
        40
        62
        55
        65
        95
        102
        117
        150
        182
        127
        219
        299
        277
        309
        576";

    #[test]
    fn sample_a() {
        assert_eq!(do_solve_a(&parse(TEST_INPUT), 5), 127);
    }

    #[test]
    fn sample_b() {
        let input = &parse(TEST_INPUT);
        assert_eq!(do_solve_b(input, do_solve_a(input, 5)), 62);
    }

    #[bench]
    fn input_a(b: &mut Bencher) {
        b.iter(|| assert_eq!(373803594, solve_a(parse(INPUT))));
    }

    #[bench]
    fn input_b(b: &mut Bencher) {
        b.iter(|| assert_eq!(51152360, solve_b(parse(INPUT))));
    }
}

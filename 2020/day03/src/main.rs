use std::io::{BufRead, BufReader};

fn main() {
    let input = parse(include_str!("input"));
    println!("{}", solve_a(&input));
    println!("{}", solve_b(&input));
}

type Input = Vec<Vec<u8>>;

fn parse(input: &str) -> Input {
    let reader = BufReader::new(input.as_bytes());
    reader
        .lines()
        .flatten()
        .map(|l| l.chars().map(|c| c as u8).collect())
        .collect()
}

fn solve_a(input: &Input) -> usize {
    let slope_r = 1;
    let slope_c = 3;
    run_through(input, slope_r, slope_c)
}

fn solve_b(input: &Input) -> usize {
    /*
    Right 1, down 1.
    Right 3, down 1. (This is the slope you already checked.)
    Right 5, down 1.
    Right 7, down 1.
    Right 1, down 2.
    */
    let mut res = 1;
    const SLOPES : &[(usize, usize)] = &[(1, 1), (1, 3), (1, 5), (1, 7), (2, 1)];
    for &(r, c) in SLOPES {
        let trees = run_through(input, r, c);
        res *= trees;
    }
    res
}

fn run_through(input: &Input, slope_r : usize, slope_c : usize) -> usize {
    let mut r = 0;
    let mut c = 0;
    let mut trees = 0;
    while r < input.len() {
        if input[r][c] == b'#' {
            trees += 1;
        }
        r += slope_r;
        c += slope_c;
        c %= input[0].len();
    }
    trees
}

#[cfg(test)]
mod test {
    use super::*;

    const TEST_INPUT : &'static str = &"..##.........##.........##.........##.........##.........##.......
#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..#...#...#..
.#....#..#..#....#..#..#....#..#..#....#..#..#....#..#..#....#..#.
..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#..#.#...#.#
.#...##..#..#...##..#..#...##..#..#...##..#..#...##..#..#...##..#.
..#.##.......#.##.......#.##.......#.##.......#.##.......#.##.....
.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#.#.#.#....#
.#........#.#........#.#........#.#........#.#........#.#........#
#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...#.##...#...
#...##....##...##....##...##....##...##....##...##....##...##....#
.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#.#..#...#.#";

    #[test]
    fn sample_a() {
        let input = parse(&TEST_INPUT);
        assert_eq!(solve_a(&input), 7);
    }

    #[test]
    fn sample_b() {
        let input = parse(&TEST_INPUT);
        assert_eq!(run_through(&input, 1, 1), 2);
        assert_eq!(run_through(&input, 1, 3), 7);
        assert_eq!(run_through(&input, 1, 5), 3);
        assert_eq!(run_through(&input, 1, 7), 4);
        assert_eq!(run_through(&input, 2, 1), 2);
        assert_eq!(solve_b(&input), 336);
    }
}

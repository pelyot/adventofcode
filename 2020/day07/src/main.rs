#![feature(test)]

use regex::Regex;
use std::str::FromStr;
#[macro_use]
extern crate lazy_static;
use std::collections::{HashMap, HashSet};

extern crate test;

fn main() {
    let input = include_str!("input");
    println!("{}", solve_a(input));
    println!("{}", solve_b(input));
}

fn solve_a(input: &str) -> usize {
    let rules = parse(input);
    let graph = make_graph_a(rules);
    let mut visited = HashSet::new();
    let mut open: Vec<String> = Vec::new();
    open.push("shiny gold".to_string());
    while !open.is_empty() {
        let current = open.pop().unwrap();
        visited.insert(current.to_string());
        if let Some(children) = graph.get(&current) {
            for child in children {
                if !visited.contains(child) {
                    open.push(child.to_string());
                }
            }
        }
    }
    visited.len() - 1 // Remove `shiny gold` itself from the results
}

fn solve_b(input: &str) -> usize {
    let rules = parse(input);
    let graph = make_graph_b(rules);
    solve_b_rec(&graph, &"shiny gold") - 1 // Remove `shiny gold` itself from the results
}

fn solve_b_rec(graph: &HashMap<String, Vec<Hold>>, starting_point: &str) -> usize {
    let mut res = 1;
    if let Some(holds) = graph.get(starting_point) {
        for hold in holds {
            res += hold.quantity * solve_b_rec(graph, &hold.bag);
        }
    }
    res
}

fn make_graph_a(rules: Vec<Rule>) -> HashMap<String, Vec<String>> {
    let mut who_holds = HashMap::new();
    for rule in rules {
        for hold in rule.holds {
            let entry = who_holds.entry(hold.bag).or_insert(Vec::new());
            entry.push(rule.bag.to_string());
        }
    }
    who_holds
}

fn make_graph_b(rules: Vec<Rule>) -> HashMap<String, Vec<Hold>> {
    let mut held_by = HashMap::new();
    for rule in &rules {
        for hold in &rule.holds {
            let entry = held_by.entry(rule.bag.to_string()).or_insert(Vec::new());
            entry.push(hold.clone());
        }
    }
    held_by
}

fn parse(input: &str) -> Vec<Rule> {
    input
        .split_terminator('\n')
        .map(|l| l.parse::<Rule>())
        .flatten()
        .collect::<Vec<_>>()
}

#[derive(Debug)]
struct Rule {
    bag: String,
    holds: Vec<Hold>,
}

#[derive(Debug, Clone)]
struct Hold {
    bag: String,
    quantity: usize,
}

struct RuleParseError;

impl FromStr for Rule {
    type Err = RuleParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut s1 = s.split(" bags contain ");
        let bag = s1.next().unwrap().to_string();
        let holds_str = s1.next().unwrap();

        lazy_static! {
            static ref RE_HOLDS: Regex = Regex::new(r"(\d+) (\w+ \w+) bags?").unwrap();
        }
        let holds = if holds_str.contains("no other bags") {
            Vec::new()
        } else {
            RE_HOLDS
                .captures_iter(holds_str)
                .map(|c| {
                    let quantity = c.get(1).unwrap().as_str().parse::<usize>().unwrap();
                    let bag = c.get(2).unwrap().as_str().to_string();
                    Hold { bag, quantity }
                })
                .collect()
        };
        Ok(Rule { bag, holds })
    }
}


#[cfg(test)]
mod unittests {
    use super::*;
    use test::Bencher;

    #[test]
    fn sample_1() {
        let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";
        assert_eq!(solve_a(input), 4);
        assert_eq!(solve_b(input), 32);
    }

    #[test]
    fn sample_2() {
        let input = "shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";
        assert_eq!(solve_b(input), 126);
    }

    const INPUT : &str = include_str!("input");

    #[bench]
    fn input_a(b: &mut Bencher) {
        b.iter(|| solve_a(INPUT));
    }

    #[bench]
    fn input_b(b: &mut Bencher) {
        b.iter(|| solve_b(INPUT));
    }
}

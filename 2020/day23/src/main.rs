const INPUT: [usize; 9] = [3, 2, 7, 4, 6, 5, 1, 8, 9];

fn main() {
    println!("{}", solve_a(&INPUT));
    println!("{}", solve_b(&INPUT));
}

fn solve_a(input: &[usize; 9]) -> String {
    let mut coc = CircleOfCups::new(input, 9);
    for _ in 0..100 {
        coc.step();
    }
    coc.result_part_1()
}

fn solve_b(input: &[usize; 9]) -> usize {
    let mut coc = CircleOfCups::new(input, 1_000_000);
    for _ in 0..10_000_000 {
        coc.step();
    }
    coc.result_part_2()
}

#[derive(Debug)]
struct Node {
    label: usize,
    next: usize,
}

impl Node {
    fn new(label: usize) -> Node {
        Node { label, next: 0 }
    }
}

struct Cursor<'a> {
    index: usize,
    circle: &'a [Node],
}

impl<'a> Cursor<'a> {
    fn next(&mut self) -> usize {
        let node = &self.circle[self.index];
        self.index = node.next;
        node.label
    }
}

#[derive(Debug)]
struct CircleOfCups {
    list: Vec<Node>,
    current: usize,
    origin: usize,
}

impl CircleOfCups {
    fn new(input: &[usize; 9], size: usize) -> CircleOfCups {
        let origin = input[0] - 1;
        let mut prev = origin;
        let mut list = (0..size).map(|i| Node::new(i + 1)).collect::<Vec<Node>>();
        for i in input.iter().skip(1).cloned().chain(10..=size) {
            let idx = i - 1;
            list[prev].next = idx;
            prev = idx;
        }
        list[prev].next = origin;
        CircleOfCups {
            list,
            current: origin,
            origin,
        }
    }
    fn dest_idx(&self, skipped: &[usize]) -> usize {
        let mut dest_value = self.list[self.current].label;
        loop {
            if dest_value == 1 {
                dest_value = self.list.len();
            } else {
                dest_value -= 1;
            }
            if !skipped.contains(&dest_value) {
                break dest_value - 1;
            }
        }
    }
    fn cursor(&self, from: usize) -> Cursor {
        Cursor {
            index: from,
            circle: &self.list,
        }
    }
    fn step(&mut self) {
        let mut cursor = self.cursor(self.current);
        cursor.next();
        let skipped_cups = [cursor.next(), cursor.next(), cursor.next()];
        std::mem::drop(cursor);
        // Skip 3 cups
        let after_skip = self.advance(self.current, 4);
        self.list[self.current].next = after_skip;
        // Paste the 3 cups after destination
        let dest_idx = self.dest_idx(&skipped_cups);
        let to_reconnect = self.list[dest_idx].next;
        self.list[dest_idx].next = skipped_cups[0] - 1;
        self.list[skipped_cups[2] - 1].next = to_reconnect;
        self.current = self.list[self.current].next;
    }
    fn advance(&self, from: usize, n: usize) -> usize {
        let mut cursor = self.cursor(from);
        for _ in 0..n {
            cursor.next();
        }
        cursor.index
    }
    fn print(&self) {
        let mut cursor = self.cursor(self.origin);
        for _ in 0..self.list.len() {
            if cursor.index == self.current {
                print!("({}) ", cursor.next());
            } else {
                print!("{} ", cursor.next());
            }
        }
        println!("");
    }
    fn result_part_1(&self) -> String {
        let mut cursor = self.cursor(1 - 1);
        cursor.next();
        let mut res = String::new();
        for _ in 0..(self.list.len() - 1) {
            res.push_str(&format!("{}", cursor.next()));
        }
        res
    }
    fn result_part_2(&self) -> usize {
        let mut cursor = self.cursor(1 - 1);
        cursor.next();
        let a = cursor.next();
        let b = cursor.next();
        a * b
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample() {
        let test_input = [3, 8, 9, 1, 2, 5, 4, 6, 7];
        assert_eq!(solve_a(&test_input), "67384529");
        assert_eq!(solve_b(&test_input), 149245887792);
    }
}

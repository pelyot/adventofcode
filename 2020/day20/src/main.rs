#[macro_use]
extern crate lazy_static;
use std::collections::HashMap;
use std::str::FromStr;
use regex::bytes::Regex;

fn main() {
    println!("{}", solve_a(INPUT).0);
    println!("{}", solve_b(INPUT));
}

const INPUT: &str = include_str!("input");
const TEST_INPUT: &str = include_str!("test_input");

fn solve_a(input: &str) -> (usize, HashMap<(i8, i8), Tile>) {
    let mut free_tiles = input
        .split_terminator("\n\n")
        .flat_map(str::parse::<Tile>)
        .collect::<Vec<_>>();

    let mut puzzle = HashMap::new();
    puzzle.insert((0, 0), free_tiles.pop().unwrap());
    let first_tile = puzzle.get_mut(&(0, 0)).unwrap();
    first_tile.orientation = Some(Orientation {
        flipped: false,
        rotation: 0,
    });

    // First column of puzzle
    advance_in_direction(0, (0, 0), &mut puzzle, &mut free_tiles);
    advance_in_direction(2, (0, 0), &mut puzzle, &mut free_tiles);

    // Find vertical bounds of puzzle
    let min_y = puzzle.keys().min_by_key(|coords| coords.1).unwrap().1;
    let max_y = puzzle.keys().max_by_key(|coords| coords.1).unwrap().1;

    // Finish puzzle:
    for y in min_y..=max_y {
        advance_in_direction(1, (0, y), &mut puzzle, &mut free_tiles);
        advance_in_direction(3, (0, y), &mut puzzle, &mut free_tiles);

        //display_puzzle(&puzzle);
    }

    //display_puzzle(&puzzle);

    // Multiply corner ids:
    let min_x = puzzle.keys().min_by_key(|coords| coords.0).unwrap().0;
    let max_x = puzzle.keys().max_by_key(|coords| coords.0).unwrap().0;
    let answer_part_1 = [
        (min_x, min_y),
        (min_x, max_y),
        (max_x, min_y),
        (max_x, max_y),
    ]
    .iter()
    .map(|c| puzzle.get(c).unwrap().id)
    .product();

    (answer_part_1, puzzle)
}

fn display_puzzle(puzzle: &HashMap<(i8, i8), Tile>) {
    // Find bounds of puzzle
    let min_y = puzzle.keys().min_by_key(|coords| coords.1).unwrap().1;
    let max_y = puzzle.keys().max_by_key(|coords| coords.1).unwrap().1;
    let min_x = puzzle.keys().min_by_key(|coords| coords.0).unwrap().0;
    let max_x = puzzle.keys().max_by_key(|coords| coords.0).unwrap().0;

    //dbg!((min_x, min_y, max_x, max_y));

    println!("\n==> Puzzle:");
    // Display puzzle
    for y in min_y..=max_y {
        for x in min_x..=max_x {
            if let Some(tile) = puzzle.get(&(x, y)) {
                print!("{} ", tile.id);
            } else {
                print!("**** ");
            }
        }
        print!("\n");
    }
    println!("");
}

fn advance_in_direction(
    direction: usize,
    from: (i8, i8),
    puzzle: &mut HashMap<(i8, i8), Tile>,
    free_tiles: &mut Vec<Tile>,
) {
    //println!("Direction: {}", direction);
    const POSITION_INCREMENTS: [(i8, i8); 4] = [(0, -1), (1, 0), (0, 1), (-1, 0)];
    let pos_inc = POSITION_INCREMENTS[direction];
    let matching_direction = (direction + 2) % 4;
    let mut current_pos = from;
    loop {
        let tile = puzzle.get(&current_pos).expect("Tile not found in puzzle");
        /*tile.display_oriented();
        println!(
            "\nCurrent: {:?} {} edge: {} -> {}",
            current_pos,
            tile.id,
            tile.get_side(direction as u8),
            tile.get_side_flipped(direction as u8)
        );*/
        match find_edge(free_tiles, tile.get_side_flipped(direction as u8)) {
            Some((matching_tile_idx, matching_edge_idx)) => {
                let matching_tile = free_tiles.get_mut(matching_tile_idx).unwrap();
                /*println!(
                    "Matched edge {} of tile {}",
                    tile.get_side_flipped(direction as u8),
                    matching_tile.id
                );
                matching_tile.display_initial();*/
                current_pos.0 += pos_inc.0;
                current_pos.1 += pos_inc.1;
                matching_tile.orientate(matching_edge_idx, matching_direction as u8);
                /*println!(
                    "setting tile orientation to {:?}",
                    matching_tile.orientation.as_ref().unwrap()
                );*/
                puzzle.insert(current_pos, free_tiles.remove(matching_tile_idx));
            }
            None => break,
        }
    }
}

fn find_edge(tiles: &mut [Tile], edge: u16) -> Option<(usize, usize)> {
    /*dbg!(tiles
    .iter()
    .filter_map(|t| t.position(edge).map(|pos| (t, pos)))
    .count());*/
    tiles
        .iter_mut()
        .enumerate()
        .filter_map(|(i, t)| t.position(edge).map(|pos| (i, pos)))
        .next()
}

#[derive(Debug)]
struct Orientation {
    flipped: bool,
    rotation: i8,
}

#[derive(Debug)]
struct Tile {
    id: usize,
    edges: [u16; 8],
    orientation: Option<Orientation>,
}

#[derive(Debug)]
struct ParseTileError;

impl Tile {
    fn position(&self, edge: u16) -> Option<usize> {
        self.edges.iter().position(|e| *e == edge)
    }

    fn orientate(&mut self, edge_idx: usize, position: u8) {
        debug_assert!(position < 4);
        let flipped = edge_idx >= 4;
        let rotation = (4 + (edge_idx % 4) as i8 - position as i8) % 4;
        self.orientation = Some(Orientation { flipped, rotation });
    }

    fn get_side(&self, position: u8) -> u16 {
        self.edges[self.get_side_idx(position)]
    }

    fn get_side_flipped(&self, position: u8) -> u16 {
        self.edges[self.rev_index(self.get_side_idx(position))]
    }

    fn get_side_idx(&self, position: u8) -> usize {
        if let Some(orientation) = self.orientation.as_ref() {
            ((position as i8 + orientation.rotation) % 4 + 4 * orientation.flipped as i8) as usize
        } else {
            position as usize
        }
    }

    fn get_edge_front(&self, idx: usize) -> u16 {
        self.edges[idx]
    }

    fn get_edge_back(&self, idx: usize) -> u16 {
        self.edges[self.rev_index(idx)]
    }

    fn rev_index(&self, idx: usize) -> usize {
        const MAP: [usize; 8] = [4, 7, 6, 5, 0, 3, 2, 1];
        MAP[idx]
    }

    fn display_oriented(&self) {
        println!("/------------ {:4} ------------\\", self.id);
        println!(
            "     {:4}              {:4}",
            self.get_side(0),
            self.get_side_flipped(0)
        );
        println!(
            "{:4}      {:4}    {:4}       {:4}",
            self.get_side(3),
            self.get_side(1),
            self.get_side_flipped(3),
            self.get_side_flipped(1)
        );
        println!(
            "     {:4}              {:4}",
            self.get_side(2),
            self.get_side_flipped(2)
        );
    }

    fn display_initial(&self) {
        println!("/------------ {:4} orig--------\\", self.id);
        println!(
            "     {:4}              {:4}",
            self.get_edge_front(0),
            self.get_edge_back(0)
        );
        println!(
            "{:4}      {:4}    {:4}       {:4}",
            self.get_edge_front(3),
            self.get_edge_front(1),
            self.get_edge_back(3),
            self.get_edge_back(1)
        );
        println!(
            "     {:4}              {:4}",
            self.get_edge_front(2),
            self.get_edge_back(2)
        );
    }
}

impl FromStr for Tile {
    type Err = ParseTileError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut lines = s.split_terminator('\n');
        let id = lines
            .next()
            .unwrap()
            .chars()
            .filter(char::is_ascii_digit)
            .collect::<String>()
            .parse::<usize>()
            .unwrap();
        let image = lines
            .map(|line| {
                line.chars()
                    .map(|c| if c == '#' { 1 } else { 0 })
                    .collect::<Vec<u8>>()
            })
            .collect::<Vec<Vec<u8>>>();
        let top = to_edge(image[0].iter());
        let top_rev = to_edge(image[0].iter().rev());
        let left = to_edge((0..10).map(|i| &image[9 - i][0]));
        let left_rev = to_edge((0..10).map(|i| &image[i][0]));
        let right = to_edge((0..10).map(|i| &image[i][9]));
        let right_rev = to_edge((0..10).map(|i| &image[9 - i][9]));
        let bottom = to_edge(image[9].iter().rev());
        let bottom_rev = to_edge(image[9].iter());
        Ok(Tile {
            id: id,
            edges: [
                top, right, bottom, left, top_rev, left_rev, bottom_rev, right_rev,
            ],
            orientation: None,
        })
    }
}

fn to_edge<'a>(iter: impl Iterator<Item = &'a u8>) -> u16 {
    iter.fold(0, |acc, &e| 2 * acc + e as u16)
}

struct FatTile {
    id: usize,
    pixels: Vec<Vec<u8>>,
}

impl FatTile {
    fn orientate(&mut self, orientation: &Orientation) {
        if orientation.flipped {
            self.flip();
        }
        self.rotate(orientation.rotation);
    }

    fn blit_at(&self, buffer: &mut [Vec<u8>], r0: usize, c0: usize) {
        for r in 0..self.pixels.len() {
            for c in 0..self.pixels[0].len() {
                buffer[r0 + r][c0 + c] = self.pixels[r][c];
            }
        }
    }

    fn rotate_1(&mut self) {
        let n_rows = self.pixels.len();
        let n_cols = self.pixels[0].len();
        let mut new = vec![vec![b' '; n_rows]; n_cols];
        for r in 0..n_cols {
            for c in 0..n_rows {
                new[r][c] = self.pixels[c][n_cols - 1 - r];
            }
        }
        self.pixels = new;
    }

    fn rotate(&mut self, rotation: i8) {
        for _ in 0..rotation {
            self.rotate_1();
        }
    }

    fn flip(&mut self) {
        let n_rows = self.pixels.len();
        let n_cols = self.pixels[0].len();
        for r in 0..n_rows {
            for c in 0..(n_cols / 2) {
                self.pixels[r].swap(c, n_cols - 1 - c);
            }
        }
    }
}

impl FromStr for FatTile {
    type Err = ParseTileError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut lines = s.split_terminator('\n');
        let id = lines
            .next()
            .unwrap()
            .chars()
            .filter(char::is_ascii_digit)
            .collect::<String>()
            .parse::<usize>()
            .unwrap();
        let pixels = lines
            .skip(1)
            .take(8)
            .map(|line| {
                line.chars()
                    .skip(1)
                    .take(8)
                    .map(|c| c as u8)
                    .collect::<Vec<u8>>()
            })
            .collect::<Vec<Vec<u8>>>();
        Ok(FatTile { id, pixels })
    }
}

fn build_image(input: &str) -> Vec<Vec<u8>> {
    let puzzle = solve_a(input).1;

    // Find bounds of puzzle
    let min_y = puzzle.keys().min_by_key(|coords| coords.1).unwrap().1;
    let max_y = puzzle.keys().max_by_key(|coords| coords.1).unwrap().1;
    let min_x = puzzle.keys().min_by_key(|coords| coords.0).unwrap().0;
    let max_x = puzzle.keys().max_by_key(|coords| coords.0).unwrap().0;
    let img_rows = (max_y - min_y + 1) as usize * 8;
    let img_cols = (max_x - min_x + 1) as usize * 8;
    let mut image = vec![vec![b' '; img_cols]; img_rows];
    let mut fat_tiles = input
        .split_terminator("\n\n")
        .filter_map(|s| s.parse::<FatTile>().ok())
        .map(|ft| (ft.id, ft))
        .collect::<HashMap<usize, FatTile>>();

    let mut x0 = 0;
    let mut y0 = 0;
    for x in min_x..=max_x {
        for y in min_y..=max_y {
            let tile = puzzle.get(&(x, y)).expect("Tile not found");
            let fat_tile = fat_tiles.get_mut(&tile.id).unwrap();
            fat_tile.orientate(&tile.orientation.as_ref().unwrap());
            fat_tile.blit_at(&mut image, y0, x0);
            y0 += 8;
        }
        x0 += 8;
        y0 = 0;
    }
    image
}

fn print_image(image: &Vec<Vec<u8>>) {
    println!("");
    image
        .iter()
        .map(|row| row.iter().map(|c| *c as char).collect::<String>())
        .for_each(|row| println!("{}", row));
}

fn replace_sea_monster(image_line: &mut [u8], sm_line_idx: usize) {
    /*  "                  # "
        "#    ##    ##    ###"
        " #  #  #  #  #  #   "  */
    let sea_monster = [
        "                  # "
            .chars()
            .map(|c| c as u8)
            .collect::<Vec<u8>>(),
        "#    ##    ##    ###"
            .chars()
            .map(|c| c as u8)
            .collect::<Vec<u8>>(),
        " #  #  #  #  #  #   "
            .chars()
            .map(|c| c as u8)
            .collect::<Vec<u8>>(),
    ];
    let sm_line = &sea_monster[sm_line_idx];
    for i in 0..sm_line.len() {
        if sm_line[i] == b'#' {
            image_line[i] = b'0';
        }
    }
}

fn find_sea_monster(image: &mut Vec<Vec<u8>>) {
    lazy_static! {
        static ref RE1: Regex = Regex::new(r"..................[#O].").unwrap();
        static ref RE2: Regex = Regex::new(r"[#O]....[#O][#O]....[#O][#O]....[#O][#O][#O]").unwrap();
        static ref RE3: Regex = Regex::new(r".[#O]..[#O]..[#O]..[#O]..[#O]..[#O]...").unwrap();
    }
    for row in 1..(image.len()-1) {
        let mut at = 0;
        while let Some(m2) = RE2.find_at(&image[row], at) {
            at = m2.start() + 1;
            //println!("m2 found at {} {}", row, m2.start());
            if let Some(m3) = RE3.find_at(&image[row + 1], m2.start()) {
                //println!("m3 found at {} {}", row, m3.start());
                if m3.start() == m2.start() {
                    if let Some(m1) = RE1.find_at(&image[row - 1], m2.start()) {
                        //println!("m1 found at {} {}", row, m1.start());
                        if m1.start() == m2.start() {
                            println!("Found at {} {}",row-1, m2.start());
                            replace_sea_monster(&mut image[row - 1][(at - 1)..], 0);
                            replace_sea_monster(&mut image[row][(at - 1)..], 1);
                            replace_sea_monster(&mut image[row + 1][(at - 1)..], 2);
                        }
                    }
                }
            }
        }
    }
}

fn solve_b(input: &str) -> usize {
    let mut image = FatTile {
        id: 0,
        pixels: build_image(input),
    };

    for i in 0..4 {
        println!("\nRotation: {}", i);
        find_sea_monster(&mut image.pixels);
        image.rotate_1();
    }
    image.flip();
    for i in 0..4 {
        println!("\nRotation (flipped): {}", i);
        find_sea_monster(&mut image.pixels);
        image.rotate_1();
    }
    print_image(&image.pixels);
    image.pixels.iter().map(|row| row.iter().filter(|c| **c == b'#').count()).sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_solve_b() {
        assert_eq!(273, solve_b(TEST_INPUT));
    }

    #[test]
    fn test_solve_a() {
        assert_eq!(20899048083289, solve_a(TEST_INPUT).0);
    }
}

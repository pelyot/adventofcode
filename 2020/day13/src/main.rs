#![feature(destructuring_assignment)]
#![feature(test)]
extern crate test;

const INPUT: &str = include_str!("input");

fn main() {
    println!("{}", solve_a(INPUT));
    println!("{}", solve_b(INPUT));
}

fn solve_a(input: &str) -> u64 {
    let mut lines = input.split_ascii_whitespace();
    let earliest = lines.next().unwrap().trim().parse::<u64>().unwrap();
    let bus = lines
        .next()
        .unwrap()
        .trim()
        .split(',')
        .filter(|&n| n != "x")
        .flat_map(|b| b.parse::<u64>())
        .min_by_key(|b| b - (earliest % b))
        .unwrap();
    bus * (bus - (earliest % bus))
}

fn solve_b(input: &str) -> u64 {
    // https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_des_restes_chinois#Algorithme
    let buses: Vec<(i64, i64)> = input
        .split_ascii_whitespace()
        .skip(1)
        .next()
        .unwrap()
        .trim()
        .split(',')
        .map(str::parse::<i64>)
        .enumerate()
        .filter(|(_, bus)| bus.is_ok())
        .map(|(i, bus)| (bus.unwrap(), i as i64))
        .collect();
    let n = buses.iter().map(|&(ni, _)| ni).product::<i64>();
    (buses
        .iter()
        .map(|&(ni, ai)| {
            let (_, _, vi) = extended_euclids_algorithm(ni, n / ni);
            let ei = vi * (n / ni);
            -ai * ei
        })
        .sum::<i64>()
        % n) as u64
}

fn extended_euclids_algorithm(a: i64, b: i64) -> (i64, i64, i64) {
    // https://fr.wikipedia.org/wiki/Algorithme_d%27Euclide_%C3%A9tendu
    let mut r = a;
    let mut rp = b;
    let mut u = 1;
    let mut up = 0;
    let mut v = 0;
    let mut vp = 1;
    while rp != 0 {
        let q = r / rp;
        (r, u, v, rp, up, vp) = (rp, up, vp, r - q * rp, u - q * up, v - q * vp);
    }
    (r, u, v)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_a() {
        let input = "939
        7,13,x,x,59,x,31,19";
        assert_eq!(295, solve_a(input));
    }

    #[test]
    fn sample_b() {
        let input = "939
        7,13,x,x,59,x,31,19";
        assert_eq!(1068781, solve_b(input));
    }

    #[bench]
    fn input_a(b: &mut test::Bencher) {
        b.iter(|| assert_eq!(4207, solve_a(INPUT)));
    }

    #[bench]
    fn input_b(b: &mut test::Bencher) {
        b.iter(|| assert_eq!(725850285300475, solve_b(INPUT)));
    }
}

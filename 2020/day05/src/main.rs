use std::io::{BufRead, BufReader};

fn main() {
    let input = include_str!("input");
    println!("{}", solve_a(input));
    println!("{}", solve_b(input));
}

fn solve_a(input: &str) -> usize {
    BufReader::new(input.as_bytes())
        .lines()
        .flatten()
        .map(|l| seat_id(seat_row_col(&l)))
        .max()
        .unwrap()
}

fn solve_b(input: &str) -> usize {
    let mut seats = [false; 127 * 8];
    BufReader::new(input.as_bytes())
        .lines()
        .flatten()
        .map(|l| seat_id(seat_row_col(&l)))
        .for_each(|id| seats[id] = true);
    seats
        .iter()
        .enumerate()
        .skip_while(|(_, e)| **e == false)
        .find(|(_, e)| **e == false)
        .unwrap()
        .0
}

fn seat_row_col(input: &str) -> (usize, usize) {
    let mut min = 0;
    let mut max = 128;
    for c in input.chars().take(7) {
        let pivot = (max + min) / 2;
        match c {
            'B' => min = pivot,
            'F' => max = pivot,
            _ => unreachable!(),
        }
    }
    let row = min;
    min = 0;
    max = 8;
    for c in input.chars().skip(7) {
        let pivot = (max + min) / 2;
        match c {
            'R' => min = pivot,
            'L' => max = pivot,
            _ => unreachable!(),
        }
    }
    let col = min;
    (row, col)
}

fn seat_id(seat: (usize, usize)) -> usize {
    seat.0 * 8 + seat.1
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn seat_row_col_test() {
        assert_eq!(seat_row_col(&"BFFFBBFRRR"), (70, 7));
        assert_eq!(seat_row_col(&"FFFBBBFRRR"), (14, 7));
        assert_eq!(seat_row_col(&"BBFFBBFRLL"), (102, 4));
    }

    #[test]
    fn seat_id_test() {
        assert_eq!(seat_id(seat_row_col(&"BFFFBBFRRR")), 567);
        assert_eq!(seat_id(seat_row_col(&"FFFBBBFRRR")), 119);
        assert_eq!(seat_id(seat_row_col(&"BBFFBBFRLL")), 820);
    }
}

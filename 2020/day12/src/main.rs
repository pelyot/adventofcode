#![feature(test)]
extern crate test;

const INPUT: &str = include_str!("input");

fn main() {
    println!("{}", solve_a(INPUT));
    println!("{}", solve_b(INPUT));
}

fn solve_a(input: &str) -> i64 {
    let mut ship = Ship::new();
    for l in input.split_ascii_whitespace() {
        let instruction = l.chars().nth(0).unwrap();
        let scalar = l
            .chars()
            .skip(1)
            .collect::<String>()
            .parse::<i64>()
            .unwrap();
        match instruction {
            'N' => ship.north(scalar),
            'S' => ship.south(scalar),
            'E' => ship.east(scalar),
            'W' => ship.west(scalar),
            'F' => ship.forward(scalar),
            'L' => ship.left(scalar),
            'R' => ship.right(scalar),
            _ => unreachable!("instruction"),
        }
    }
    ship.position.0.abs() + ship.position.1.abs()
}

fn solve_b(input: &str) -> i64 {
    let mut pos = (0i64, 0i64);
    let mut waypoint = (10i64, 1i64);
    for l in input.split_ascii_whitespace() {
        let instruction = l.chars().nth(0).unwrap();
        let scalar = l
            .chars()
            .skip(1)
            .collect::<String>()
            .parse::<i64>()
            .unwrap();
        match instruction {
            'N' => waypoint.1 += scalar,
            'S' => waypoint.1 -= scalar,
            'E' => waypoint.0 += scalar,
            'W' => waypoint.0 -= scalar,
            'F' => {
                pos.0 += waypoint.0 * scalar;
                pos.1 += waypoint.1 * scalar
            }
            'L' => match scalar {
                90 => {
                    let tmp = waypoint.0;
                    waypoint.0 = -waypoint.1;
                    waypoint.1 = tmp;
                }
                180 => {
                    waypoint.0 = -waypoint.0;
                    waypoint.1 = -waypoint.1
                }
                270 => {
                    let tmp = waypoint.0;
                    waypoint.0 = waypoint.1;
                    waypoint.1 = -tmp;
                }
                _ => unreachable!("angle L"),
            },
            'R' => match scalar {
                270 => {
                    let tmp = waypoint.0;
                    waypoint.0 = -waypoint.1;
                    waypoint.1 = tmp;
                }
                180 => {
                    waypoint.0 = -waypoint.0;
                    waypoint.1 = -waypoint.1
                }
                90 => {
                    let tmp = waypoint.0;
                    waypoint.0 = waypoint.1;
                    waypoint.1 = -tmp;
                }
                _ => unreachable!("angle L"),
            },
            _ => unreachable!("instruction"),
        }
        println!("position: {:?}, waypoint: {:?}", pos, waypoint);
    }
    pos.0.abs() + pos.1.abs()
}

struct Direction {
    angle: i64,
}

impl Direction {
    fn right(&mut self, rotation: i64) {
        self.angle += 360;
        self.angle -= rotation;
        self.angle %= 360;
    }

    fn left(&mut self, rotation: i64) {
        self.angle += rotation;
        self.angle %= 360;
    }
}

struct Ship {
    direction: Direction,
    position: (i64, i64),
}

impl Ship {
    fn new() -> Ship {
        Ship {
            direction: Direction { angle: 270 },
            position: (0, 0),
        }
    }

    /*
        Action N means to move north by the given value.
        Action S means to move south by the given value.
        Action E means to move east by the given value.
        Action W means to move west by the given value.
        Action L means to turn left the given number of degrees.
        Action R means to turn right the given number of degrees.
        Action F means to move forward by the given value in the direction the ship is currently facing.
    */

    fn north(&mut self, distance: i64) {
        self.position.1 += distance;
    }

    fn south(&mut self, distance: i64) {
        self.position.1 -= distance;
    }

    fn east(&mut self, distance: i64) {
        self.position.0 += distance;
    }

    fn west(&mut self, distance: i64) {
        self.position.0 -= distance;
    }

    fn left(&mut self, rotation: i64) {
        self.direction.left(rotation);
    }

    fn right(&mut self, rotation: i64) {
        self.direction.right(rotation);
    }
    fn forward(&mut self, distance: i64) {
        match self.direction.angle {
            0 => self.north(distance),
            90 => self.west(distance),
            180 => self.south(distance),
            270 => self.east(distance),
            _ => unreachable!("angle"),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_a() {
        let input = "F10
        N3
        F7
        R90
        F11";
        assert_eq!(solve_a(input), 25);
    }

    #[test]
    fn sample_b() {
        let input = "F10
        N3
        F7
        R90
        F11";
        assert_eq!(solve_b(input), 286);
    }

    #[bench]
    fn input_a(b: &mut test::Bencher) {
        b.iter(|| assert_eq!(362, solve_a(INPUT)));
    }

    #[bench]
    fn input_b(b: &mut test::Bencher) {
        b.iter(|| assert_eq!(29895, solve_b(INPUT)));
    }
}

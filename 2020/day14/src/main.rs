#![feature(test)]
extern crate test;
#[macro_use] extern crate lazy_static;
use std::collections::HashMap;
use std::str::FromStr;

const INPUT: &str = include_str!("input");

fn main() {
    println!("{}", solve_a(INPUT));
    println!("{}", solve_b(INPUT));
}

#[derive(Debug)]
struct Mask {
    ones: u64,
    zeros: u64,
    floats: Vec<usize>
}

impl Mask {
    fn apply(&self, value: u64) -> u64 {
        (value | self.ones) & !self.zeros
    }
}


lazy_static!{
    static ref RE : regex::Regex = regex::Regex::new(r"mem\[(\d+)\] = (\d+)").unwrap();
}

fn solve_a(input: &str) -> u64 {
    let mut memory = HashMap::<u64, u64>::new();
    let mut mask = Mask { ones: 0, zeros: 0, floats: Vec::new()};
    for line in input.split_terminator('\n') {
        if line.trim().starts_with("mask") {
            mask = line.split(" = ").skip(1).next().unwrap().parse::<Mask>().unwrap();
        } else {
            let captures = RE.captures(line).unwrap();
            let address = captures.get(1).unwrap().as_str().parse::<u64>().unwrap();
            let value = captures.get(2).unwrap().as_str().parse::<u64>().unwrap();
            memory.insert(address, mask.apply(value));
        }
    }
    memory.values().sum()
}

fn solve_b(input: &str) -> u64 {
    let mut memory = HashMap::<u64, u64>::new();
    let mut mask = Mask { ones: 0, zeros: 0, floats: Vec::new()};
    for line in input.split_terminator('\n') {
        if line.trim().starts_with("mask") {
            mask = line.split(" = ").skip(1).next().unwrap().parse::<Mask>().unwrap();
        } else {
            let captures = RE.captures(line).unwrap();
            let address = captures.get(1).unwrap().as_str().parse::<u64>().unwrap();
            let value = captures.get(2).unwrap().as_str().parse::<u64>().unwrap();
            fn float_rec(floats: &[usize], addr: u64, memory: &mut HashMap<u64, u64>, value: u64) {
                if floats.is_empty() {
                    memory.insert(addr, value);
                } else {
                    let bit = floats[0];
                    float_rec(&floats[1..], addr, memory, value);
                    float_rec(&floats[1..], addr ^ (1 << bit), memory, value);
                }
            };
            float_rec(&mask.floats, address | mask.ones, &mut memory, value);
        }
    }
    memory.values().sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_a() {
        let input = "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
        mem[8] = 11
        mem[7] = 101
        mem[8] = 0";
        assert_eq!(165, solve_a(input));
    }

    #[test]
    fn sample_b() {
        let input = "mask = 000000000000000000000000000000X1001X
        mem[42] = 100
        mask = 00000000000000000000000000000000X0XX
        mem[26] = 1";
        assert_eq!(208, solve_b(input));
    }

    #[bench]
    fn input_a(b: &mut test::Bencher) {
        b.iter(|| assert_eq!(6559449933360, solve_a(INPUT)));
    }

    #[bench]
    fn input_b(b: &mut test::Bencher) {
        b.iter(|| assert_eq!(3369767240513, solve_b(INPUT)));
    }
}

#[derive(Debug)]
struct MaskParseError;

impl FromStr for Mask {
    type Err = MaskParseError;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut ones = 0u64;
        let mut zeros = 0u64;
        let mut floats = Vec::new();
        for (i, c) in input.chars().rev().enumerate() {
            match c {
                '0' => zeros += 1 << i,
                '1' => ones += 1 << i,
                'X' => floats.push(i),
                _ => unreachable!()
            }
        }
        Ok(Mask{ones, zeros, floats})
    }
}
use regex::Regex;
use std::io::{BufReader, BufRead};

fn main() {
    let input = include_str!("input");
    let reader = BufReader::new(input.as_bytes());
    let lines = reader.lines().flatten().collect::<Vec<String>>();
    let result_a = lines.iter().filter(|l| check_password(&l)).count();
    let result_b = lines.iter().filter(|l| check_password_b(&l)).count();
    println!("Part A: {}", result_a);
    println!("Part B: {}", result_b);
}

fn check_password(line: &str) -> bool {
    let re = Regex::new(r"(\d+)-(\d+) ([a-z]): ([a-z]+)").unwrap();
    let cap = re.captures_iter(line).next().expect("failed to match regex");
    let mini = cap[1].parse::<usize>().expect("failed to parse min");
    let maxi = cap[2].parse::<usize>().expect("failed to parse max");
    let letter = cap[3].chars().next().expect("failed to get char");
    let password =& cap[4];
    let count = password.chars().filter(|&c| c == letter).count();
    return count >= mini && count <= maxi;
}

fn check_password_b(line: &str) -> bool {
    let re = Regex::new(r"(\d+)-(\d+) ([a-z]): ([a-z]+)").unwrap();
    let cap = re.captures_iter(line).next().expect("failed to match regex");
    let mini = cap[1].parse::<usize>().expect("failed to parse min");
    let maxi = cap[2].parse::<usize>().expect("failed to parse max");
    let letter = cap[3].chars().next().expect("failed to get char");
    let password =& cap[4];
    return (password.chars().nth(mini - 1).unwrap() == letter) ^ (password.chars().nth(maxi - 1).unwrap() == letter);
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample() {
        let input_1 = "1-3 a: abcde";
        let input_2 = "1-3 b: cdefg";
        let input_3 = "2-9 c: ccccccccc";
        assert_eq!(true, check_password(&input_1));
        assert_eq!(false, check_password(&input_2));
        assert_eq!(true, check_password(&input_3));
    }

    #[test]
    fn sample_2() {
        let input_1 = "1-3 a: abcde";
        let input_2 = "1-3 b: cdefg";
        let input_3 = "2-9 c: ccccccccc";
        assert_eq!(true, check_password(&input_1));
        assert_eq!(false, check_password(&input_2));
        assert_eq!(false, check_password_b(&input_3));
    }
}
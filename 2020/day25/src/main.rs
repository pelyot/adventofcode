use std::iter::Iterator;

fn main() {
    println!("{}", solve_a(PUBKEY_1, PUBKEY_2));
}

const PUBKEY_1 : usize = 2084668;
const PUBKEY_2 : usize = 3704642;

fn solve_a(pub_key_1: usize, pub_key_2: usize) -> usize {
    let loop_size_1 = loop_size(pub_key_1);
    encryption_key(pub_key_2, loop_size_1)
}

struct SubjectNumberTransformer {
    subject_number: usize,
    value: usize
}

impl SubjectNumberTransformer {
    fn new(subject_number: usize) -> Self {
        SubjectNumberTransformer { subject_number, value: 1 }
    }
}

impl Iterator for SubjectNumberTransformer {
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item> {
        self.value *= self.subject_number;
        self.value %= 20201227;
        Some(self.value)
    }
}

fn loop_size(pub_key: usize) -> usize {
    SubjectNumberTransformer::new(7).enumerate().find(|&(n, value)| value == pub_key).unwrap().0 + 1
}

fn encryption_key(pub_key: usize, loop_size_other: usize) -> usize {
    SubjectNumberTransformer::new(pub_key).skip(loop_size_other - 1).next().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_a() {
        let card_pub_key = 5764801;
        let door_pub_key = 17807724;
        let card_loop_size = loop_size(card_pub_key);
        let door_loop_size = loop_size(door_pub_key);
        assert_eq!(card_loop_size, 8);
        assert_eq!(door_loop_size, 11);
        assert_eq!(encryption_key(card_pub_key, door_loop_size), 14897079);
        assert_eq!(encryption_key(door_pub_key, card_loop_size), 14897079);
    }
}
#![feature(test)]
extern crate test;
use std::collections::HashMap;

const INPUT:&[usize] = &[0,5,4,1,10,14,7];

fn main() {
    println!("{}", solve_a(INPUT));
    println!("{}", solve_b(INPUT));
}

fn solve_b(input: &[usize]) -> usize {
    solve(input, 30000000)
}

fn solve_a(input: &[usize]) -> usize {
    solve(input, 2020)
}

fn solve(input: &[usize], iterations: usize) -> usize {
    let mut prev : HashMap::<usize, usize> = HashMap::new();
    let mut t = 1usize;
    for &num in input {
        prev.insert(num, t);
        t += 1;
    }
    let mut last_spoken = *input.last().unwrap();
    while t <= iterations {
        let age = t - 1 - prev.get(&last_spoken).unwrap_or(&(t-1));
        prev.insert(last_spoken, t-1);
        last_spoken = age;
        t += 1;
    }
    last_spoken
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_a() {
        let input = &[0, 3, 6];
        assert_eq!(436, solve_a(input));
    }

    #[test]
    fn sample_b() {
        let input = &[0, 3, 6];
        assert_eq!(175594, solve_b(input));
    }

    #[bench]
    fn input_a(b: &mut test::Bencher) {
        b.iter(|| assert_eq!(203, solve_a(INPUT)));
    }

    #[bench]
    fn input_b(b: &mut test::Bencher) {
        b.iter(|| assert_eq!(9007186, solve_b(INPUT)));
    }

}
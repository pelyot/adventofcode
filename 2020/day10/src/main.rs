#![feature(test)]
extern crate test;

const INPUT: &str = include_str!("input");

fn main() {
    let mut input = parse(INPUT);
    println!("{}", solve_a(&mut input));
    println!("{}", solve_b(&mut input));
}

fn solve_a(adapters: &mut [u64]) -> u64 {
    adapters.sort();
    let counts = &mut [0, 0];
    for w in adapters.windows(2) {
        let diff = w[1] - w[0];
        counts[diff as usize / 3] += 1;
    }
    counts[0] * counts[1]
}

fn solve_b(adapters: &mut [u64]) -> u64 {
    adapters.sort();
    let mut possibilities = 1;
    let mut count = 0;
    for e in adapters.windows(2).map(|w| w[1] - w[0]) {
        if e == 1 {
            count += 1;
        } else {
            if count > 1 {
                possibilities *= match count {
                    2 => 2,
                    3 => 4,
                    4 => 7,
                    _ => panic!("Bigger group!")
                };
            }
            count = 0;
        }
    }
    possibilities
}

fn parse(input: &str) -> Vec<u64> {
    let mut adapters: Vec<u64> = (0..1)
        .chain(input.split_whitespace().flat_map(|s| s.parse::<u64>()))
        .collect();
    adapters.push(adapters.iter().max().unwrap() + 3);
    adapters
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn sample_a_small() {
        assert_eq!(solve_a(&mut parse(TEST_INPUT_SMALL)), 35);
    }

    #[test]
    fn sample_a_medium() {
        assert_eq!(solve_a(&mut parse(TEST_INPUT_MEDIUM)), 220);
    }

    #[test]
    fn sample_b_small() {
        assert_eq!(solve_b(&mut parse(TEST_INPUT_SMALL)), 8);
    }

    #[test]
    fn sample_b_medium() {
        assert_eq!(solve_b(&mut parse(TEST_INPUT_MEDIUM)), 19208);
    }

    #[bench]
    fn input_a(b: &mut Bencher) {
        b.iter(|| assert_eq!(solve_a(&mut parse(INPUT)), 2232));
    }

    #[bench]
    fn input_b(b: &mut Bencher) {
        b.iter(|| assert_eq!(solve_b(&mut parse(INPUT)), 173625106649344));
    }

    const TEST_INPUT_SMALL: &str = "16
    10
    15
    5
    1
    11
    7
    19
    6
    12
    4";

    const TEST_INPUT_MEDIUM: &str = "28
    33
    18
    42
    31
    14
    46
    20
    48
    47
    24
    23
    49
    45
    19
    38
    39
    11
    1
    32
    25
    35
    8
    17
    7
    9
    4
    2
    34
    10
    3";
}

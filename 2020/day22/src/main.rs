use std::collections::VecDeque;

const INPUT: &str = include_str!("input");

fn main() {
    println!("{}", solve_a(INPUT));
    println!("{}", solve_b(INPUT));
}

fn solve_a(input: &str) -> usize {
    let mut decks = input
        .split("\n\n")
        .map(|s| {
            s.split('\n')
                .filter_map(|line| line.parse::<usize>().ok())
                .collect::<VecDeque<usize>>()
        })
        .take(2)
        .collect::<Vec<_>>();
    debug_assert_eq!(decks[0].len(), decks[1].len());
    loop {
        if decks[0].is_empty() {
            break score(&decks[1]);
        } else if decks[1].is_empty() {
            break score(&decks[0]);
        }
        let card0 = decks[0].pop_front().unwrap();
        let card1 = decks[1].pop_front().unwrap();
        if card0 > card1 {
            decks[0].push_back(card0);
            decks[0].push_back(card1);
        } else {
            decks[1].push_back(card1);
            decks[1].push_back(card0);
        }
    }
}

fn solve_b(input: &str) -> usize {
    let mut decks = input.split("\n\n").map(|s| {
        s.split('\n')
            .filter_map(|line| line.parse::<usize>().ok())
            .collect::<VecDeque<usize>>()
    });
    let mut deck1 = decks.next().unwrap();
    let mut deck2 = decks.next().unwrap();
    debug_assert_eq!(deck1.len(), deck2.len());

    let winner = recursive_combat(&mut deck1, &mut deck2);

    if winner == 1 {
        score(&deck1)
    } else {
        score(&deck2)
    }
}

fn hash(deck: &VecDeque<usize>) -> u64 {
    use std::collections::hash_map::DefaultHasher;
    use std::hash::Hash;
    use std::hash::Hasher;
    let mut hasher = DefaultHasher::new();
    deck.hash(&mut hasher);
    hasher.finish()
}

fn recursive_combat(deck1: &mut VecDeque<usize>, deck2: &mut VecDeque<usize>) -> usize /* winner */
{
    let mut rounds1 = vec![];
    let mut rounds2 = vec![];
    loop {
        if deck1.is_empty() {
            break 2;
        } else if deck2.is_empty() {
            break 1;
        }
        let hash1 = hash(deck1);
        let hash2 = hash(deck2);
        if rounds1.contains(&hash1) || rounds2.contains(&hash2) {
            break 1;
        }
        rounds1.push(hash1);
        rounds2.push(hash2);
        let card1 = deck1.pop_front().unwrap();
        let card2 = deck2.pop_front().unwrap();
        let winner = if deck1.len() >= card1 && deck2.len() >= card2 {
            let mut sub1 = deck1
                .iter()
                .take(card1)
                .cloned()
                .collect::<VecDeque<usize>>();
            let mut sub2 = deck2
                .iter()
                .take(card2)
                .cloned()
                .collect::<VecDeque<usize>>();
            recursive_combat(&mut sub1, &mut sub2)
        } else {
            if card1 > card2 {
                1
            } else {
                2
            }
        };
        if winner == 1 {
            deck1.push_back(card1);
            deck1.push_back(card2);
        } else {
            deck2.push_back(card2);
            deck2.push_back(card1);
        }
    }
}

fn score(deck: &VecDeque<usize>) -> usize {
    deck.iter()
        .rev()
        .enumerate()
        .map(|(i, card)| (i + 1) * card)
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_a() {
        assert_eq!(306, solve_a(TEST_INPUT));
    }
    #[test]
    fn sample_b() {
        assert_eq!(291, solve_b(TEST_INPUT));
    }

    const TEST_INPUT: &str = "Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10";
}

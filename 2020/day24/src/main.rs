#[macro_use]
extern crate lazy_static;
use regex::Regex;
use std::collections::HashMap;
use std::ops::{Add, AddAssign};

const INPUT: &str = include_str!("input");

fn main() {
    println!("{}", solve_a(INPUT));
    println!("{}", solve_b(INPUT));
}

fn create_pattern(input: &str) -> HashMap<Point, Color> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"(e|se|sw|w|nw|ne)").unwrap();
    }
    let mut tiles = HashMap::new();
    input.split_whitespace().for_each(|line| {
        let mut tile = ORIGIN;
        for caps in RE.captures_iter(&line) {
            match caps.get(0).unwrap().as_str() {
                "e" => tile += E,
                "se" => tile += SE,
                "sw" => tile += SW,
                "w" => tile += W,
                "ne" => tile += NE,
                "nw" => tile += NW,
                _ => unreachable!(),
            }
        }
        tiles
            .entry(tile)
            .and_modify(|a| *a = Color::flip(*a))
            .or_insert(Color::Black);
    });
    tiles
}

fn solve_a(input: &str) -> usize {
    let tiles = create_pattern(input);
    tiles.values().filter(|&v| *v == Color::Black).count()
}

fn pad(tile: Point, map: &mut HashMap<Point, Color>) {
    for &d in DIRECTIONS.iter() {
        map.entry(d + tile).or_insert(Color::White);
    }
}

fn pad_all(tiles: HashMap<Point, Color>) -> HashMap<Point, Color> {
    let mut padded_tiles = tiles.clone();
    for (tile, color) in tiles {
        if color == Color::Black {
            pad(tile, &mut padded_tiles);
        }
    }
    padded_tiles
}

fn count_neighbors(tile: Point, map: &HashMap<Point, Color>) -> usize {
    DIRECTIONS
        .iter()
        .map(|&d| {
            let neighbor = d + tile;
            map.get(&neighbor).cloned().unwrap_or_default()
        })
        .filter(|&c| c == Color::Black)
        .count()
}

fn solve_b(input: &str) -> usize {
    let tiles = create_pattern(input);
    let mut tiles = pad_all(tiles);
    for _ in 0..100 {
        let mut new_tiles = tiles.clone();
        for (tile, color) in &tiles {
            let nb_black_neighbor = count_neighbors(*tile, &tiles);
            match (color, nb_black_neighbor) {
                (Color::White, 2) => {
                    *new_tiles.entry(*tile).or_default() = Color::Black;
                    pad(*tile, &mut new_tiles);
                }
                (Color::Black, 1) | (Color::Black, 2) => (),
                (Color::Black, i) if i==0 || i>2 => {
                    new_tiles.entry(*tile).and_modify(|c| *c = Color::White);
                }
                _ => (),
            }
        }
        tiles = new_tiles;
    }
    tiles.values().filter(|&v| *v == Color::Black).count()
}

#[derive(Clone, Copy, Hash, PartialEq, Eq)]
struct Point(i16, i16, i16);

impl AddAssign for Point {
    fn add_assign(&mut self, other: Self) {
        self.0 += other.0;
        self.1 += other.1;
        self.2 += other.2;
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Point(self.0 + other.0, self.1 + other.1, self.2 + other.2)
    }
}
const NE: Point = Point(1, 0, -1);
const E: Point = Point(1, -1, 0);
const SE: Point = Point(0, -1, 1);
const SW: Point = Point(-1, 0, 1);
const W: Point = Point(-1, 1, 0);
const NW: Point = Point(0, 1, -1);
const ORIGIN: Point = Point(0, 0, 0);

const DIRECTIONS: [Point; 6] = [NE, E, SE, W, SW, NW];

#[derive(PartialEq, Clone, Copy)]
enum Color {
    Black,
    White,
}

impl Color {
    fn flip(c: Self) -> Self {
        match c {
            Color::Black => Color::White,
            Color::White => Color::Black,
        }
    }
}

impl Default for Color {
    fn default() -> Self {
        Color::White
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = "sesenwnenenewseeswwswswwnenewsewsw
    neeenesenwnwwswnenewnwwsewnenwseswesw
    seswneswswsenwwnwse
    nwnwneseeswswnenewneswwnewseswneseene
    swweswneswnenwsewnwneneseenw
    eesenwseswswnenwswnwnwsewwnwsene
    sewnenenenesenwsewnenwwwse
    wenwwweseeeweswwwnwwe
    wsweesenenewnwwnwsenewsenwwsesesenwne
    neeswseenwwswnwswswnw
    nenwswwsewswnenenewsenwsenwnesesenew
    enewnwewneswsewnwswenweswnenwsenwsw
    sweneswneswneneenwnewenewwneswswnese
    swwesenesewenwneswnwwneseswwne
    enesenwswwswneneswsenwnewswseenwsese
    wnwnesenesenenwwnenwsewesewsesesew
    nenewswnwewswnenesenwnesewesw
    eneswnwswnwsenenwnwnwwseeswneewsenese
    neswnwewnwnwseenwseesewsenwsweewe
    wseweeenwnesenwwwswnew";

    #[test]
    fn sample_a() {
        assert_eq!(solve_a(TEST_INPUT), 10);
    }
    #[test]
    fn sample_b() {
        assert_eq!(solve_b(TEST_INPUT), 2208);
    }
}

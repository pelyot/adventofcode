use regex::Regex;
#[macro_use]
extern crate lazy_static;
use std::collections::HashMap;

const INPUT: &str = include_str!("input");

fn main() {
    println!("{}", solve_a(INPUT));
    println!("{}", solve_b(INPUT));
}

fn solve_a(input: &str) -> usize {
    let (ranges, tickets) = parse_a(input);
    tickets
        .iter()
        .map(|t| {
            t.iter()
                .filter(|&&value| {
                    !ranges
                        .iter()
                        .any(|range| value >= range.0 && value <= range.1)
                })
                .sum::<usize>()
        })
        .sum()
}

fn solve_b(input: &str) -> usize {
    let (rules, my_ticket, mut tickets) = parse_b(input);
    tickets.retain(|t| is_valid(t, &rules));
    let mut violations = vec![vec![false; rules.len()]; tickets[0].len()];
    for ticket in &tickets {
        for (fidx, &field) in ticket.iter().enumerate() {
            for (ridx, rule) in rules.iter().enumerate() {
                violations[fidx][ridx] |= !rule.matches(field);
            }
        }
    }
    let mut assigned_rules: HashMap<String, usize> = HashMap::new();
    while assigned_rules.keys().filter(|name| name.trim().starts_with("departure")).count() < 6 {
        for fidx in 0..violations.len() {
            let ok_count = violations[fidx].iter().filter(|&&v| !v).count();
            if ok_count == 1 {
                let (ridx, _) = violations[fidx]
                    .iter()
                    .enumerate()
                    .find(|(_, v)| !**v)
                    .unwrap();
                assigned_rules.insert(rules[ridx].name.to_string(), fidx);
                for fi in 0..violations.len() {
                    violations[fi][ridx] = true;
                }
            }
        }
    }
    assigned_rules
        .iter()
        .filter(|(name, _)| name.trim().starts_with("departure"))
        .map(|(_, &fidx)| my_ticket[fidx])
        .product()
}

fn is_valid(ticket: &Ticket, rules: &[Rule]) -> bool {
    ticket
        .iter()
        .all(|&field| rules.iter().any(|rule| rule.matches(field)))
}

type Range = (usize, usize);
type Ticket = Vec<usize>;

#[derive(Debug)]
struct Rule {
    name: String,
    ranges: (Range, Range),
}

impl Rule {
    fn matches(&self, field: usize) -> bool {
        (field >= self.ranges.0 .0 && field <= self.ranges.0 .1)
            || (field >= self.ranges.1 .0 && field <= self.ranges.1 .1)
    }
}

fn parse_b(input: &str) -> (Vec<Rule>, Ticket, Vec<Ticket>) {
    let input_chunks = input.split("\n\n").collect::<Vec<&str>>();
    let rules = RE_RULE
        .captures_iter(input_chunks[0])
        .map(|cap| Rule {
            name: cap[1].to_string(),
            ranges: (
                (cap[2].parse().unwrap(), cap[3].parse().unwrap()),
                (cap[4].parse().unwrap(), cap[5].parse().unwrap()),
            ),
        })
        .collect::<Vec<Rule>>();
    let my_ticket = parse_ticket(input_chunks[1].split('\n').skip(1).next().unwrap());
    let tickets = input_chunks[2]
        .split("\n")
        .map(parse_ticket)
        .filter(|v| !v.is_empty())
        .collect::<Vec<Ticket>>();
    (rules, my_ticket, tickets)
}

fn parse_ticket(s: &str) -> Ticket {
    s.split(',').flat_map(str::parse::<usize>).collect()
}

lazy_static! {
    static ref RE_RANGE: Regex = Regex::new(r"(\d+)-(\d+)").unwrap();
    static ref RE_RULE: Regex = Regex::new(r"(.+): (\d+)-(\d+) or (\d+)-(\d+)").unwrap();
}

fn parse_a(input: &str) -> (Vec<Range>, Vec<Ticket>) {
    let input_chunks = input.split("\n\n").collect::<Vec<&str>>();
    let ranges = RE_RANGE
        .captures_iter(input_chunks[0])
        .map(|cap| {
            (
                cap[1].parse::<usize>().unwrap(),
                cap[2].parse::<usize>().unwrap(),
            )
        })
        .collect::<Vec<Range>>();
    let tickets = input_chunks[2]
        .split("\n")
        .map(|line| {
            line.split(',')
                .flat_map(str::parse::<usize>)
                .collect::<Ticket>()
        })
        .collect::<Vec<Ticket>>();
    (ranges, tickets)
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = "class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12";

    #[test]
    fn sample_a() {
        assert_eq!(71, solve_a(TEST_INPUT));
    }
    #[test]
    fn sample_b() {
        assert_eq!(71, solve_b(TEST_INPUT));
    }
}

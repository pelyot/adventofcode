#[macro_use]
extern crate itertools;

const INPUT: &str = include_str!("input");

struct Grid {
    cubes: Vec<bool>,
    dims: (usize, usize, usize, usize),
}

impl Grid {
    fn with_dims(dims: (usize, usize, usize, usize)) -> Grid {
        Grid {
            cubes: vec![false; dims.0 * dims.1 * dims.2 * dims.3],
            dims,
        }
    }

    fn index_of(&self, x: usize, y: usize, z: usize, t: usize) -> usize {
        debug_assert!(x < self.dims.0);
        debug_assert!(y < self.dims.1);
        debug_assert!(z < self.dims.2);
        debug_assert!(t < self.dims.3);
        let mut idx = x;
        idx = idx * self.dims.1 + y;
        idx = idx * self.dims.2 + z;
        idx = idx * self.dims.3 + t;
        idx
    }

    fn at(&self, x: usize, y: usize, z: usize, t: usize) -> bool {
        self.cubes[self.index_of(x, y, z, t)]
    }

    fn set(&mut self, x: usize, y: usize, z: usize, t: usize, value: bool) {
        let idx = self.index_of(x, y, z, t);
        self.cubes[idx] = value;
    }

    fn next_generation_4d(&mut self) {
        let mut next = self.cubes.clone();
        for x in 1..(self.dims.0 - 1) {
            for y in 1..(self.dims.1 - 1) {
                for z in 1..(self.dims.2 - 1) {
                    for t in 1..(self.dims.3 - 1) {
                        let neighbors = self.neighbors_4d(x, y, z, t);
                        if self.at(x, y, z, t) {
                            if neighbors != 2 && neighbors != 3 {
                                next[self.index_of(x, y, z, t)] = false;
                            }
                        } else {
                            if neighbors == 3 {
                                next[self.index_of(x, y, z, t)] = true;
                            }
                        }
                    }
                }
            }
        }
        self.cubes = next;
    }

    fn neighbors_4d(&mut self, x: usize, y: usize, z: usize, t: usize) -> usize {
        iproduct!(0..3, 0..3, 0..3, 0..3)
            .filter(|&(i, j, k, l)| {
                if i == 1 && j == 1 && k == 1 && l == 1 {
                    false
                } else {
                    self.at((x + i) - 1, (y + j) - 1, (z + k) - 1, (t + l) - 1)
                }
            })
            .count()
    }

    fn neighbors_3d(&self, x: usize, y: usize, z: usize) -> usize {
        iproduct!(0..3, 0..3, 0..3)
            .filter(|&(i, j, k)| {
                if i == 1 && j == 1 && k == 1 {
                    false
                } else {
                    self.at((x + i) - 1, (y + j) - 1, (z + k) - 1, 0)
                }
            })
            .count()
    }

    fn next_generation_3d(&mut self) {
        let mut next = self.cubes.clone();
        for x in 1..(self.dims.0 - 1) {
            for y in 1..(self.dims.1 - 1) {
                for z in 1..(self.dims.2 - 1) {
                    let neighbors = self.neighbors_3d(x, y, z);
                    if self.at(x, y, z, 0) {
                        if neighbors != 2 && neighbors != 3 {
                            next[self.index_of(x, y, z, 0)] = false;
                        }
                    } else {
                        if neighbors == 3 {
                            next[self.index_of(x, y, z, 0)] = true;
                        }
                    }
                }
            }
        }
        self.cubes = next;
    }

    fn from_str(input: &str, fourth_dim: bool) -> Grid {
        let input_grid = input
            .split_terminator('\n')
            .map(|l| l.trim().chars().map(|c| c == '#').collect::<Vec<bool>>())
            .collect::<Vec<Vec<bool>>>();
        let max_generations = 6;
        let dims = (
            input_grid[0].len() + 2 + 2 * max_generations,
            input_grid.len() + 2 + 2 * max_generations,
            2 * max_generations + 1 + 2,
            if fourth_dim {
                2 * max_generations + 1 + 2
            } else {
                1
            },
        );
        let mut grid = Grid::with_dims(dims);
        let start_x = (dims.0 - input_grid[0].len()) / 2;
        let start_y = (dims.1 - input_grid.len()) / 2;
        for i in 0..input_grid.len() {
            for j in 0..input_grid.len() {
                grid.set(
                    start_x + i,
                    start_y + j,
                    dims.2 / 2,
                    dims.3 / 2,
                    input_grid[i][j],
                );
            }
        }
        grid
    }

    fn count_alive(&self) -> usize {
        self.cubes.iter().filter(|&&alive| alive).count()
    }

    fn print_plane(&self, zi: i64, ti: i64) {
        let mut s = String::new();
        for x in 0..self.dims.0 {
            for y in 0..self.dims.1 {
                s.push(
                    if self.at(
                        x,
                        y,
                        (self.dims.2 as i64 / 2 + zi) as usize,
                        (self.dims.2 as i64 / 2 + ti) as usize,
                    ) {
                        '#'
                    } else {
                        '.'
                    },
                );
            }
            s.push('\n');
        }
        println!("{}", s);
    }

    fn print(&self) {
        let d = 6;
        for w in -d..=d {
            for z in -d..=d {
                println!("z={} w={}", z, w);
                self.print_plane(z, w);
            }
        }
    }
}

fn main() {
    println!("{:?}", solve_a(INPUT));
    println!("{:?}", solve_b(INPUT));
}

fn solve_a(input: &str) -> usize {
    let mut grid = Grid::from_str(input, false);
    for _ in 0..6 {
        grid.next_generation_3d();
    }
    grid.count_alive()
}

fn solve_b(input: &str) -> usize {
    let mut grid = Grid::from_str(input, true);
    for _ in 0..6 {
        grid.next_generation_4d();
    }
    grid.count_alive()
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = ".#.
    ..#
    ###";

    #[test]
    fn sample_a() {
        assert_eq!(112, solve_a(TEST_INPUT));
    }

    #[test]
    fn sample_b() {
        assert_eq!(848, solve_b(TEST_INPUT));
    }

    #[test]
    fn input_a() {
        assert_eq!(218, solve_a(INPUT));
    }

    #[test]
    fn input_b() {
        assert_eq!(218, solve_b(INPUT));
    }
}

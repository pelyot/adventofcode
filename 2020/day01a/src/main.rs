use std::io::{stdin, BufRead, BufReader, Read};

fn main() {
    let input = include_str!("input");
    println!("{}", solve_a(input.as_bytes()));
    println!("{}", solve_b(input.as_bytes()));
}

fn solve_a(input: impl Read) -> i64 {
    let reader = BufReader::new(input);
    let numbers = reader
        .lines()
        .flatten()
        .flat_map(|l| l.trim().parse::<i64>())
        .collect::<Vec<_>>();
    for i in 0..numbers.len() {
        for j in 0..i {
            let a = numbers[i];
            let b = numbers[j];
            if a + b == 2020 {
                return a * b;
            }
        }
    }
    return -1;
}

fn solve_b(input: impl Read) -> i64 {
    let reader = BufReader::new(input);
    let numbers = reader
        .lines()
        .flatten()
        .flat_map(|l| l.trim().parse::<i64>())
        .collect::<Vec<_>>();
    for i in 0..numbers.len() {
        for j in 0..i {
            for k in 0..j {
                let a = numbers[i];
                let b = numbers[j];
                let c = numbers[k];
                if a + b + c == 2020 {
                    return a * b * c;
                }
            }
        }
    }
    return -1;
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn sample1() {
        let input = "1721
        979
        366
        299
        675
        1456";
        assert_eq!(514579, solve_a(input.as_bytes()));
    }
}

use std::collections::{HashMap, HashSet,BTreeMap};

const INPUT: &str = include_str!("input");

fn main() {
    println!("{}", solve_a(INPUT));
}

fn solve_a(input: &str) -> usize {
    let mut problem = HashMap::<&str, Vec<HashSet<&str>>>::new();
    for (ingredients, allergens) in input.split_terminator('\n').map(|line| parse_food(line)) {
        for allergen in allergens {
            problem
                .entry(allergen)
                .or_default()
                .push(ingredients.clone());
        }
    }
    let mut possibilities = problem
        .iter()
        .map(|(&allergen, foods)| {
            let mut intersection = foods[0].clone();
            for food in &foods[1..] {
                intersection = &intersection & food;
            }
            (allergen, intersection.into_iter().collect::<Vec<_>>())
        })
        .collect::<HashMap<&str, Vec<&str>>>();

    let mut allergen_food_mapping = BTreeMap::new();

    while possibilities.len() > 0 {
        allergen_food_mapping.extend(
            possibilities
                .iter()
                .filter(|(_, ingredients)| ingredients.len() == 1)
                .map(|(&allergen, ingredients)| (allergen, ingredients[0])),
        );
        possibilities.retain(|_, ingredients| ingredients.len() > 1);
        possibilities.iter_mut().for_each(|(_, ingredients)| {
            ingredients.retain(|i| allergen_food_mapping.values().find(|e| *e == i).is_none())
        });
    }
    println!("Canonical dangerous list: {}", itertools::join(allergen_food_mapping.values(), ","));
    let allergenic_ingredients = allergen_food_mapping.values().collect::<Vec<_>>();
    input
        .split_terminator('\n')
        .map(|line| parse_food(line).0)
        .map(|ingredients| {
            ingredients
                .iter()
                .filter(|&i| !allergenic_ingredients.contains(&i))
                .count()
        })
        .sum()
}

fn parse_food(line: &str) -> (HashSet<&str>, HashSet<&str>) {
    let mut split1 = line.trim().split(" (contains ");
    let ingredients = split1.next().unwrap().split(' ').collect::<HashSet<&str>>();
    let allergens = split1
        .next()
        .unwrap()
        .strip_suffix(")")
        .unwrap()
        .split(", ")
        .collect::<HashSet<&str>>();
    (ingredients, allergens)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_a() {
        assert_eq!(solve_a(TEST_INPUT), 5);
    }

    #[test]
    fn parse_test() {
        let ingredients = ["mxmxvkd", "kfcds", "sqjhc", "nhms"]
            .iter()
            .cloned()
            .collect::<HashSet<&str>>();
        let allergens = ["dairy", "fish"].iter().cloned().collect::<HashSet<&str>>();
        assert_eq!(
            parse_food(TEST_INPUT.split('\n').next().unwrap()),
            (ingredients, allergens)
        );
    }

    const TEST_INPUT: &str = "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
    trh fvjkl sbzzf mxmxvkd (contains dairy)
    sqjhc fvjkl (contains soy)
    sqjhc mxmxvkd sbzzf (contains fish)";
}

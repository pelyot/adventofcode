#![feature(str_split_once)]
#![feature(test)]
extern crate test;

use std::str::FromStr;

fn main() {
    let input = include_str!("input");
    println!("{}", solve_a(parse_instructions(input)));
    println!("{}", solve_b(parse_instructions(input)));
}

fn solve_a(instructions: Vec<Instruction>) -> i64 {
    run_loop(&instructions).1
}

fn solve_b(mut instructions: Vec<Instruction>) -> i64 {
    let mut ip = 0;
    loop {
        while let Instruction::Acc(_) = instructions[ip] {
            ip += 1;
        }
        switch(&mut instructions, ip);
        let (infinite_loop, acc) = run_loop(&instructions);
        switch(&mut instructions, ip);
        if !infinite_loop {
            return acc;
        }
        ip += 1;
    }
}

fn switch(instructions: &mut[Instruction], ip: usize) {
    match instructions[ip] {
        Instruction::Jmp(x) => instructions[ip] = Instruction::Nop(x),
        Instruction::Nop(x) => instructions[ip] = Instruction::Jmp(x),
        _ => unreachable!(),
    }
}

fn run_loop(instructions: &[Instruction]) -> (bool, i64) {
    let mut visited = vec![false; instructions.len()];
    let mut ip = 0i64;
    let mut acc = 0i64;
    loop {
        if ip as usize == instructions.len() {
            return (false, acc);
        }
        if visited[ip as usize] {
            return (true, acc);
        }
        visited[ip as usize] = true;
        match instructions[ip as usize] {
            Instruction::Acc(x) => {
                acc += x;
                ip += 1
            }
            Instruction::Jmp(x) => ip += x,
            Instruction::Nop(_) => ip += 1,
        }
    }
}

#[derive(Debug, PartialEq)]
enum Instruction {
    Acc(i64),
    Jmp(i64),
    Nop(i64),
}

struct InstructionParseError;

impl FromStr for Instruction {
    type Err = InstructionParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some((op, value)) = s.trim().split_once(' ') {
            let v = value.parse::<i64>().expect("parse operand");
            match op {
                "acc" => Ok(Instruction::Acc(v)),
                "jmp" => Ok(Instruction::Jmp(v)),
                "nop" => Ok(Instruction::Nop(v)),
                _ => Err(InstructionParseError),
            }
        } else {
            Err(InstructionParseError)
        }
    }
}

fn parse_instructions(input: &str) -> Vec<Instruction> {
    input
        .split('\n')
        .map(|l| l.trim().parse::<Instruction>())
        .flatten()
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[test]
    fn parse_1() {
        let input = "nop +0
        acc -1
        jmp +4";
        assert_eq!(
            [
                Instruction::Nop(0),
                Instruction::Acc(-1),
                Instruction::Jmp(4)
            ][..],
            parse_instructions(input)
        );
    }

    const TEST_INPUT: &str = "nop +0
        acc +1
        jmp +4
        acc +3
        jmp -3
        acc -99
        acc +1
        jmp -4
        acc +6";

    #[test]
    fn sample_a() {
        assert_eq!(solve_a(parse_instructions(TEST_INPUT)), 5);
    }

    #[test]
    fn sample_b() {
        assert_eq!(solve_b(parse_instructions(TEST_INPUT)), 8);
    }

    const INPUT: &str = include_str!("input");

    #[bench]
    fn input_a(b: &mut Bencher) {
        b.iter(|| assert_eq!(1939, solve_a(parse_instructions(INPUT))));
    }

    #[bench]
    fn input_b(b: &mut Bencher) {
        b.iter(|| assert_eq!(2212, solve_b(parse_instructions(INPUT))));
    }
}

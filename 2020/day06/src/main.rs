use std::collections::HashSet;

fn main() {
    let input = include_str!("input");
    println!("{}", solve_a(input));
    println!("{}", solve_b(input));
}

fn solve_a(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|s| {
            s.chars()
                .filter(char::is_ascii_lowercase)
                .collect::<HashSet<_>>()
                .len()
        })
        .sum()
}

fn solve_b(input: &str) -> usize {
    input
        .split("\n\n")
        .map(|group| {
            let mut answers_iter = group
                .split_terminator("\n")
                .map(|line| line
                    .chars()
                    .collect::<HashSet<_>>());
            let mut answers = answers_iter.next().unwrap();
            answers_iter.for_each(|s| answers.retain(|a| s.contains(a)));
            answers.len()
        })
        .sum()
}

#[cfg(test)]
mod test {
    use super::*;

    const input: &str = "abc

a
b
c

ab
ac

a
a
a
a

b";

    #[test]
    fn sample_a() {
        assert_eq!(solve_a(input), 11);
    }

    #[test]
    fn sample_b() {
        assert_eq!(solve_b(input), 6);
    }
}

#[macro_use]
extern crate lazy_static;

use regex::Regex;
use std::collections::HashMap;

const INPUT: &str = include_str!("input");

fn main() {
    println!("{}", solve_a(INPUT));
    println!("{}", solve_b(INPUT));
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_INPUT: &str = r#"0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"

ababbb
bababa
abbbab
aaabbb
aaaabbb"#;
}

#[derive(Debug, Clone)]
enum Rule {
    Simple(char),
    Single(usize),
    Seq(usize, usize),
    Choice1(usize, usize),
    Choice2(usize, usize, usize, usize),
}

lazy_static! {
    static ref RE_CHAR: Regex = Regex::new(r#"^"([a-z]+)"$"#).unwrap();
    static ref RE_SINGLE: Regex = Regex::new(r#"^(\d+)$"#).unwrap();
    static ref RE_SEQ: Regex = Regex::new(r#"^(\d+) (\d+)$"#).unwrap();
    static ref RE_CHOICE_1: Regex = Regex::new(r#"^(\d+) \| (\d+)$"#).unwrap();
    static ref RE_CHOICE_2: Regex = Regex::new(r#"^(\d+) (\d+) \| (\d+) (\d+)$"#).unwrap();
}

type Rules = HashMap<usize, Rule>;

fn parse(input: &str) -> (Rules, Vec<&str>) {
    let split = input.split_terminator("\n\n").collect::<Vec<&str>>();
    let rules = split[0]
        .split_terminator('\n')
        .map(|line| {
            let rule_id = line.split(": ").next().unwrap().parse::<usize>().unwrap();
            let rule_str = line.split(": ").skip(1).next().unwrap();
            let rule = if let Some(caps) = RE_CHAR.captures(rule_str) {
                Rule::Simple(caps.get(1).unwrap().as_str().chars().next().unwrap())
            } else if let Some(caps) = RE_SINGLE.captures(rule_str) {
                Rule::Single(caps.get(1).unwrap().as_str().parse::<usize>().unwrap())
            } else if let Some(caps) = RE_SEQ.captures(rule_str) {
                Rule::Seq(
                    caps.get(1).unwrap().as_str().parse::<usize>().unwrap(),
                    caps.get(2).unwrap().as_str().parse::<usize>().unwrap(),
                )
            } else if let Some(caps) = RE_CHOICE_1.captures(rule_str) {
                let a = caps.get(1).unwrap().as_str().parse::<usize>().unwrap();
                let b = caps.get(2).unwrap().as_str().parse::<usize>().unwrap();
                Rule::Choice1(a, b)
            } else if let Some(caps) = RE_CHOICE_2.captures(rule_str) {
                let a = caps.get(1).unwrap().as_str().parse::<usize>().unwrap();
                let b = caps.get(2).unwrap().as_str().parse::<usize>().unwrap();
                let c = caps.get(3).unwrap().as_str().parse::<usize>().unwrap();
                let d = caps.get(4).unwrap().as_str().parse::<usize>().unwrap();
                Rule::Choice2(a, b, c, d)
            } else {
                unreachable!();
            };
            (rule_id, rule)
        })
        .collect::<HashMap<usize, Rule>>();
        let strings = split[1].split_terminator('\n').collect();
        (rules, strings)
}

fn solve_a(input: &str) -> usize {
    let (rules, strings) = parse(input);
    let mut cache: HashMap<usize, String> = HashMap::new();
    let regex_str = String::from("^") + &make_regex(&rules, 0, &mut cache) + "$";
    let re = Regex::new(&regex_str).unwrap();
    strings.iter().filter(|s| re.is_match(s)).count()
}

fn solve_b(input: &str) -> usize {
    let (_, strings) = parse(input);
    let re_template = "^((((((a((a(a(b|a)|bb)|b(ba))a|((ab)b|(bb|(b|a)a)a)b)|b(a((bb)b)|b((bb|aa)a|(bb)b)))a|((b(a(aa)|b(aa|ba))|a(a(bb|ab)|b(a(b|a)|ba)))b|((a(ab)|b((b|a)(b|a)))b|((aa)b|(a(b|a)|ba)a)a)a)b)a|((a(b(a(a(b|a)|ba)|b((b|a)(b|a)))|a(b((b|a)(b|a))|a(ba|bb)))|b((b(a(b|a)|bb)|a(bb|ab))b|(b(bb|aa)|a(a(b|a)|ba))a))a|((((bb)a|(bb)b)a|(b(a(b|a)|bb)|a(ba))b)a|(b(b((b|a)(b|a))|a(bb|aa))|a((bb|ab)a|(bb|aa)b))b)b)b)a|((((b((bb|(b|a)a)b|(bb)a)|a(a(aa|ba)))a|(b((bb)a|(ba)b)|a(b(a(b|a)|bb)|a(bb|ab)))b)a|(a((a(ba|bb)|b(bb))a|(a(ab)|b((b|a)(b|a)))b)|b(b(b(ba|bb)|a(ba))|a((ba)b|(a(b|a)|bb)a)))b)a|((b((a(a(b|a)|bb)|b((b|a)(b|a)))a|(a(bb|ab)|b(a(b|a)|ba))b)|a(a((ba)a|(bb)b)|b((aa)b|(ab|aa)a)))b|(a(a(a(bb)|b(ab|ba))|b(a(ba)|b(aa|ba)))|b((b((b|a)(b|a))|a(bb))b|((bb|(b|a)a)b|(aa)a)a))a)b)b)+)(((((a((a(a(b|a)|bb)|b(ba))a|((ab)b|(bb|(b|a)a)a)b)|b(a((bb)b)|b((bb|aa)a|(bb)b)))a|((b(a(aa)|b(aa|ba))|a(a(bb|ab)|b(a(b|a)|ba)))b|((a(ab)|b((b|a)(b|a)))b|((aa)b|(a(b|a)|ba)a)a)a)b)a|((a(b(a(a(b|a)|ba)|b((b|a)(b|a)))|a(b((b|a)(b|a))|a(ba|bb)))|b((b(a(b|a)|bb)|a(bb|ab))b|(b(bb|aa)|a(a(b|a)|ba))a))a|((((bb)a|(bb)b)a|(b(a(b|a)|bb)|a(ba))b)a|(b(b((b|a)(b|a))|a(bb|aa))|a((bb|ab)a|(bb|aa)b))b)b)b)a|((((b((bb|(b|a)a)b|(bb)a)|a(a(aa|ba)))a|(b((bb)a|(ba)b)|a(b(a(b|a)|bb)|a(bb|ab)))b)a|(a((a(ba|bb)|b(bb))a|(a(ab)|b((b|a)(b|a)))b)|b(b(b(ba|bb)|a(ba))|a((ba)b|(a(b|a)|bb)a)))b)a|((b((a(a(b|a)|bb)|b((b|a)(b|a)))a|(a(bb|ab)|b(a(b|a)|ba))b)|a(a((ba)a|(bb)b)|b((aa)b|(ab|aa)a)))b|(a(a(a(bb)|b(ab|ba))|b(a(ba)|b(aa|ba)))|b((b((b|a)(b|a))|a(bb))b|((bb|(b|a)a)b|(aa)a)a))a)b)b){n}(a(((b(b((aa)b|(a(b|a)|ba)a)|a((aa|ba)(b|a)))|a((a(a(b|a)|ba)|b(bb|ab))b|((b|a)(ab|ba))a))a|(b((b(bb|ab)|a(a(b|a)|bb))a|(b((b|a)(b|a))|a(bb))b)|a((a((b|a)(b|a))|b(ab|ba))b|(b(bb|aa)|a(a(b|a)|ba))a))b)b|(((((ab|aa)a|(bb)b)b|((bb|(b|a)a)b|(bb)a)a)b|((b(a(b|a)|bb)|a(ba))a|((bb|(b|a)a)b|(ab)a)b)a)a|(a(b(a(ab)|b(bb|ab))|a(a((b|a)(b|a))|b(ab)))|b(((ab|aa)b|(bb|(b|a)a)a)b|((ab)b)a))b)a)|b(a(a((b((ba)b|(ba|bb)a)|a((bb|(b|a)a)a|(ba|bb)b))b|(a((aa|ba)a|(ab|ba)b)|b(a(aa)|b(bb|(b|a)a)))a)|b((((a(b|a)|ba)a|(ab|aa)b)b|(b(bb|(b|a)a)|a(a(b|a)|ba))a)b|(((ab|aa)b|(ab|ba)a)b|((a(b|a)|ba)a|(a(b|a)|bb)b)a)a))|b((b((a(aa|ba))b|((ab|aa)a|(bb|aa)b)a)|a(a(a(ba)|b(aa))|b((ab|aa)b|(bb|(b|a)a)a)))a|(a(((aa)a)b|((a(b|a)|bb)b|(ab|aa)a)a)|b(a(a(ab|ba)|b(ba))|b((ba)a|(bb)b)))b))){n}))$";
    (1..5).map(|i| {
        let re_str = re_template.replace('n', &format!("{}", i));
        let re = Regex::new(&re_str).unwrap();
        strings.iter().filter(|s| re.is_match(s)).count()
    }).sum()
}

fn make_regex(rules: &Rules, idx: usize, cache: &mut HashMap<usize, String>) -> String {
    if let Some(regex) = cache.get(&idx) {
        return regex.to_string();
    } else {
        let rule = rules.get(&idx).unwrap();
        match rule {
            Rule::Simple(c) => String::from(*c),
            Rule::Single(a) => String::from("(") + &make_regex(rules, *a, cache) + ")",
            Rule::Seq(a, b) => {
                String::from("(")
                    + &make_regex(rules, *a, cache)
                    + &make_regex(rules, *b, cache)
                    + ")"
            }
            Rule::Choice1(a, b) => {
                String::from("(")
                    + &make_regex(rules, *a, cache)
                    + "|"
                    + &make_regex(rules, *b, cache)
                    + ")"
            }
            Rule::Choice2(a, b, c, d) => {
                String::from("(")
                    + &make_regex(rules, *a, cache)
                    + &make_regex(rules, *b, cache)
                    + "|"
                    + &make_regex(rules, *c, cache)
                    + &make_regex(rules, *d, cache)
                    + ")"
            }
        }
    }
}

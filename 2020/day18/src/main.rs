#![feature(destructuring_assignment)]

#[macro_use]
extern crate lazy_static;

const INPUT: &str = include_str!("input");

fn main() {
    println!("{}", solve_a(INPUT));
    println!("{}", solve_b(INPUT));
}

#[derive(PartialEq)]
enum Op {
    Add,
    Mul,
    Open,
}

fn evalb_regex(input: &str) -> usize {
    let mut new = input.to_string();
    let mut prev = String::new();
    while prev != new {
        prev = new.clone();
        new = replace_add(&new);
        new = replace_mul_paren(&new);
    }
    debug_assert!(!new.contains("+"));
    prev = String::new();
    while prev != new {
        prev = new.clone();
        new = replace_mul(&new);
    }
    new.parse().unwrap()
}

fn solve_b(input: &str) -> usize {
    input.split_terminator('\n').map(|l| evalb_regex(l)).sum()
}

fn solve_a(input: &str) -> usize {
    input.split_terminator('\n').map(|l| eval_a(l)).sum()
}

fn eval_a(input: &str) -> usize {
    let mut oper_stack = vec![];
    let mut num_stack = vec![];
    for c in input.as_bytes().iter().rev() {
        match c {
            &d if d >= b'1' && d <= b'9' => {
                num_stack.push((d - b'0') as usize);
            }
            b'*' => oper_stack.push(Op::Mul),
            b'+' => oper_stack.push(Op::Add),
            b')' => oper_stack.push(Op::Open),
            b'(' => {
                let mut left = num_stack.pop().unwrap();
                let mut op = oper_stack.pop().unwrap();
                while op != Op::Open {
                    let right = num_stack.pop().unwrap();
                    match op {
                        Op::Add => left += right,
                        Op::Mul => left *= right,
                        _ => unreachable!(),
                    }
                    op = oper_stack.pop().unwrap();
                }
                num_stack.push(left);
            }
            _ => (),
        }
    }
    let mut acc = num_stack.pop().unwrap();
    while let Some(op) = oper_stack.pop() {
        let right = num_stack.pop().unwrap();
        match op {
            Op::Add => acc += right,
            Op::Mul => acc *= right,
            _ => unreachable!(),
        }
    }
    acc
}

use regex::{Captures, Regex};
lazy_static! {
    static ref RE_ADD: Regex = Regex::new(r"(\d+) \+ (\d+)").unwrap();
    static ref RE_MUL: Regex = Regex::new(r"(\d+) \* (\d+)").unwrap();
    static ref RE_NUM: Regex = Regex::new(r"(\d+)").unwrap();
    static ref RE_MUL_PAREN: Regex = Regex::new(r"\((\d+)(?: \* (\d+))*\)").unwrap();
    static ref RE_PAREN: Regex = Regex::new(r"\((\d+)\)").unwrap();
}

fn replace_add(s: &str) -> String {
    RE_ADD
        .replace(s, |caps: &Captures| {
            format!(
                "{}",
                &caps[1].parse::<usize>().unwrap() + &caps[2].parse::<usize>().unwrap()
            )
        })
        .to_string()
}

fn replace_mul_paren(s: &str) -> String {
    RE_MUL_PAREN
        .replace(s, |caps: &Captures| {
            let product: usize = RE_NUM
                .captures_iter(&caps[0])
                .map(|c| c[0].parse::<usize>().unwrap())
                .product();
            format!("{}", product)
        })
        .to_string()
}

fn replace_mul(s: &str) -> String {
    RE_MUL
        .replace(s, |caps: &Captures| {
            let product: usize = RE_NUM
                .captures_iter(&caps[0])
                .map(|c| c[0].parse::<usize>().unwrap())
                .product();
            format!("{}", product)
        })
        .to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sample_a() {
        assert_eq!(6, eval_a("2 * 3"));
        assert_eq!(5, eval_a("2 + 3"));
        assert_eq!(7, eval_a("2 + 3 + 2"));
        assert_eq!(8, eval_a("2 * 3 + 2"));
        assert_eq!(12, eval_a("2 * 3 * 2"));
        assert_eq!(10, eval_a("2 + 3 * 2"));
        assert_eq!(26, eval_a("2 * 3 + (4 * 5)"));
        assert_eq!(437, eval_a("5 + (8 * 3 + 9 + 3 * 4 * 3)"));
        assert_eq!(12240, eval_a("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"));
        assert_eq!(
            13632,
            eval_a("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")
        );
    }
    #[test]
    fn sample_b() {
        assert_eq!(46, evalb_regex("2 * 3 + (4 * 5)"));
        assert_eq!(1445, evalb_regex("5 + (8 * 3 + 9 + 3 * 4 * 3)"));
        assert_eq!(
            669060,
            evalb_regex("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))")
        );
        assert_eq!(
            23340,
            evalb_regex("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2")
        );
    }
}

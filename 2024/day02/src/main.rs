use itertools::Itertools;

const INPUT: &'static str = include_str!("../input");

fn main() {
    let reports = INPUT
        .lines()
        .map(|l| {
            l.split_ascii_whitespace()
                .map(|e| e.parse::<i32>().unwrap())
                .collect_vec()
        })
        .collect_vec();
    let part_1 = reports.iter().filter(|r| check_report_1(r)).count();
    println!("Part 1: {part_1}");

    let part_2 = reports.iter().filter(|r| check_report_2(r)).count();
    println!("Part 2: {part_2}");
}

fn check_report_1(report: &Vec<i32>) -> bool {
    let diffs = report
        .iter()
        .tuple_windows()
        .map(|(a, b)| b - a)
        .collect_vec();
    diffs.iter().all(|x| x.abs() <= 3)
        && (diffs.iter().all(|x| *x > 0) || diffs.iter().all(|x| *x < 0))
}

fn check_report_2(report: &Vec<i32>) -> bool {
    if check_report_1(report) {
        return true;
    }
    for i in 0..report.len() {
        let report = {
            let mut r = report.clone();
            r.remove(i);
            r
        };
        if check_report_1(&report) {
            return true;
        }
    }
    return false;
}

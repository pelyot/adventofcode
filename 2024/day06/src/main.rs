use std::collections::HashSet;

const INPUT: &str = include_str!("../input");
const TEST_INPUT: &str = include_str!("../test_input");

fn solve_1(grid: &Vec<Vec<char>>) -> usize {
    let (pos, dir) = init(&grid);
    solve_1_inner(grid, pos, dir).len()
}

fn init(grid: &Vec<Vec<char>>) -> ((i32, i32), Dir) {
    grid.iter()
        .enumerate()
        .find_map(|(r, row)| {
            row.iter().enumerate().find_map(|(c, cell)| match cell {
                '.' | '#' => None,
                _ => Some(((r as i32, c as i32), Dir::from(*cell))),
            })
        })
        .unwrap()
}

fn solve_1_inner(grid: &Vec<Vec<char>>, mut pos: (i32, i32), mut dir: Dir) -> HashSet<(i32, i32)> {
    let mut positions = HashSet::new();
    loop {
        positions.insert(pos);
        let (nr, nc) = (pos.0 + dir.0, pos.1 + dir.1);
        if nr < 0 || nr >= grid.len() as i32 || nc < 0 || nc >= grid[0].len() as i32 {
            return positions;
        }
        let next_cell = grid[nr as usize][nc as usize];
        if next_cell == '#' {
            dir = dir.rotate();
            continue;
        } else {
            pos = (nr, nc)
        };
    }
}

fn test_loop(grid: &Vec<Vec<char>>, mut pos: (i32, i32), mut dir: Dir) -> bool {
    let mut pos_dirs = HashSet::new();
    loop {
        let (nr, nc) = (pos.0 + dir.0, pos.1 + dir.1);
        if nr < 0 || nr >= grid.len() as i32 || nc < 0 || nc >= grid[0].len() as i32 {
            return false;
        }
        let next_cell = grid[nr as usize][nc as usize];
        if next_cell == '#' {
            dir = dir.rotate();
            continue;
        } else {
            if !pos_dirs.insert((pos, dir)) {
                return true;
            }
            pos = (nr, nc)
        };
    }
}

fn solve_2(grid: &mut Vec<Vec<char>>) -> usize {
    let (init, dir) = init(&grid);
    let positions = {
        let mut positions = solve_1_inner(grid, init, dir);
        assert!(positions.remove(&init));
        positions
    };
    let mut count = 0;
    for p in &positions {
        let tmp = grid[p.0 as usize][p.1 as usize];
        grid[p.0 as usize][p.1 as usize] = '#';
        if test_loop(&grid, init, dir) {
            count += 1;
        }
        grid[p.0 as usize][p.1 as usize] = tmp;
    }
    count
}

fn main() {
    let mut grid: Vec<Vec<char>> = INPUT.lines().map(|l| l.chars().collect()).collect();

    eprintln!("Part 1: {}", solve_1(&grid));
    eprintln!("Part 2: {}", solve_2(&mut grid));
}

#[derive(PartialEq, Eq, Hash, Copy, Clone)]
struct Dir(i32, i32);

impl Dir {
    fn from(c: char) -> Self {
        match c {
            '>' => Dir(0, 1),
            'v' => Dir(1, 0),
            '<' => Dir(0, -1),
            '^' => Dir(-1, 0),
            _ => unreachable!(),
        }
    }
    fn rotate(self) -> Self {
        match (self.0, self.1) {
            (0, 1) => Dir(1, 0),
            (1, 0) => Dir(0, -1),
            (0, -1) => Dir(-1, 0),
            (-1, 0) => Dir(0, 1),
            _ => unreachable!(),
        }
    }
}

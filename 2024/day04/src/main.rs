use itertools::Itertools;

const INPUT: &str = include_str!("../input");

fn main() {
    let input: Vec<Vec<u8>> = INPUT.lines().map(|l| l.bytes().collect_vec()).collect_vec();
    println!("Part 1: {}", solve_1(&input));
    println!("Part 2: {}", solve_2(&input));
}

const XMAS: &[u8; 4] = b"XMAS";
const XMAS_REV: &[u8; 4] = b"SAMX";

fn count_in_slice(slice: &[u8]) -> usize {
    slice
        .windows(4)
        .filter(|w| {
            w.iter().zip(XMAS.iter()).all(|(l, r)| l == r)
                || w.iter().zip(XMAS_REV.iter()).all(|(l, r)| l == r)
        })
        .count()
}

struct DiagIterator<'a> {
    grid: &'a Vec<Vec<u8>>,
    n: i64,
    max_n: i64,
}

impl<'a> DiagIterator<'a> {
    fn new(grid: &'a Vec<Vec<u8>>) -> Self {
        let max_n = grid.len() as i64 - 1;
        Self {
            grid,
            max_n,
            n: -(max_n - 3),
        }
    }
}

impl<'a> Iterator for DiagIterator<'a> {
    type Item = Vec<u8>;

    fn next(&mut self) -> Option<Self::Item> {
        match self.n {
            n if n >= -(self.max_n - 3) && n < 0 => {
                self.n += 1;
                Some(
                    (-n..=self.max_n)
                        .map(|x| self.grid[x as usize][(x + n) as usize])
                        .collect(),
                )
            }
            n if n >= 0 && n <= (self.max_n - 3) => {
                self.n += 1;
                Some(
                    (n..=self.max_n)
                        .map(|y| self.grid[(y - n) as usize][y as usize])
                        .collect(),
                )
            }
            _ => None,
        }
    }
}

fn solve_1(grid: &Vec<Vec<u8>>) -> usize {
    let mut acc: usize = 0;
    // lines
    for line in grid {
        acc += count_in_slice(&line);
    }
    // columns
    for j in 0..grid[0].len() {
        let col = grid.iter().map(|row| row[j]).collect_vec();
        acc += count_in_slice(&col);
    }
    // diagonals
    acc += DiagIterator::new(&grid)
        .map(|diag| count_in_slice(&diag))
        .sum::<usize>();
    let flipped_grid = grid
        .iter()
        .map(|row| row.iter().rev().cloned().collect())
        .collect();
    acc += DiagIterator::new(&flipped_grid)
        .map(|diag| count_in_slice(&diag))
        .sum::<usize>();
    acc
}

fn mas_sam(a: u8, b: u8) -> bool {
    match (a, b) {
        (b'M', b'S') | (b'S', b'M') => true,
        _ => false,
    }
}

fn solve_2(grid: &Vec<Vec<u8>>) -> usize {
    let mut acc = 0;
    for r in 1..grid.len() - 1 {
        for c in 1..grid.len() - 1 {
            let x_mas = grid[r][c] == b'A'
                && mas_sam(grid[r - 1][c - 1], grid[r + 1][c + 1])
                && mas_sam(grid[r - 1][c + 1], grid[r + 1][c - 1]);
            acc += x_mas as usize;
        }
    }
    acc
}

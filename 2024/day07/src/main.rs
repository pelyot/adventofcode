use std::{num::ParseIntError, str::FromStr};
use thiserror::Error;

struct Equation {
    test: i64,
    numbers: Vec<i64>,
}

impl Equation {
    fn is_solveable_1(test: i64, numbers: &[i64]) -> bool {
        match numbers {
            [a] => test == *a,
            [head @ .., a] => {
                Self::is_solveable_1(test - a, head)
                    || test % a == 0 && Self::is_solveable_1(test / a, head)
            }
            _ => unreachable!(),
        }
    }

    fn is_solveable_2(test: i64, curr: i64, numbers: &[i64]) -> bool {
        match numbers {
            [] => test == curr,
            [a, tail @ ..] => {
                Self::is_solveable_2(test, curr + a, tail)
                    || Self::is_solveable_2(test, curr * a, tail)
                    || Self::is_solveable_2(
                        test,
                        (curr.to_string() + &a.to_string()).parse().unwrap(),
                        tail,
                    )
            }
        }
    }
}

fn solve_1(equations: &[Equation]) -> i64 {
    equations
        .iter()
        .filter_map(|e| Equation::is_solveable_1(e.test, &e.numbers).then_some(e.test))
        .sum()
}

fn solve_2(equations: &[Equation]) -> i64 {
    equations
        .iter()
        .filter_map(|e| Equation::is_solveable_2(e.test, 0, &e.numbers).then_some(e.test))
        .sum()
}

fn main() -> Result<(), ParseError> {
    let equations = parse(include_str!("../input"))?;
    let test_equations = parse(include_str!("../test_input"))?;
    println!("Part 1 (test): {}", solve_1(&test_equations));
    println!("Part 1: {}", solve_1(&equations));
    println!("Part 2 (test): {}", solve_2(&test_equations));
    println!("Part 2: {}", solve_2(&equations));
    Ok(())
}

fn parse(input: &str) -> Result<Vec<Equation>, ParseError> {
    input
        .lines()
        .map(|l| l.parse::<Equation>())
        .collect::<Result<Vec<Equation>, _>>()
}

#[derive(Error, Debug)]
#[error("failed to parse input")]
enum ParseError {
    BadInt(#[from] ParseIntError),
    Structure,
}

impl FromStr for Equation {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (test_s, num_s) = s.split_once(':').ok_or(ParseError::Structure)?;
        Ok(Self {
            test: test_s.parse()?,
            numbers: num_s
                .split_ascii_whitespace()
                .map(|e| e.parse::<i64>())
                .collect::<Result<_, _>>()?,
        })
    }
}

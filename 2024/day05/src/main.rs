use std::{collections::HashSet, iter::once, num::ParseIntError, str::FromStr};
use thiserror::Error;

const INPUT: &str = include_str!("../input");
const TEST_INPUT: &str = include_str!("../test_input");

type Page = i32;
struct Rule(Page, Page);
struct Update(Vec<Page>);
struct Input {
    rules: Vec<Rule>,
    updates: Vec<Update>,
}

impl Update {
    fn page_positions(&self) -> [Option<usize>; 100] {
        let mut positions = [None; 100];
        for (i, page) in self.0.iter().enumerate() {
            positions[*page as usize] = Some(i);
        }
        positions
    }

    fn fix(&self, rules: &[Rule]) -> Self {
        let order = {
            let mut rem_pages = rules
                .iter()
                .filter(|&Rule(a, b)| self.0.contains(a) && self.0.contains(b))
                .flat_map(|&Rule(a, b)| once(a).chain(once(b)))
                .collect::<HashSet<Page>>();
            let mut order = Vec::new();
            while rem_pages.len() > 0 {
                let next = never_after(rules, &rem_pages);
                rem_pages.remove(&next);
                order.push(next);
            }
            order
        };
        Update(
            order
                .iter()
                .filter(|p| self.0.contains(p))
                .cloned()
                .collect(),
        )
    }

    fn middle(&self) -> Page {
        self.0[self.0.len() / 2]
    }
}

fn check_rules(rules: &[Rule], update: &Update) -> bool {
    let positions = update.page_positions();
    rules.iter().all(
        |rule| match (positions[rule.0 as usize], positions[rule.1 as usize]) {
            (Some(pa), Some(pb)) => pa < pb,
            _ => true,
        },
    )
}

fn solve_1(input: &Input) -> i32 {
    input
        .updates
        .iter()
        .filter(|u| check_rules(&input.rules, u))
        .map(Update::middle)
        .sum()
}

fn solve_2(input: &Input) -> i32 {
    input
        .updates
        .iter()
        .filter(|u| !check_rules(&input.rules, u))
        .map(|u| u.fix(&input.rules))
        .map(|u| u.middle())
        .sum()
}

fn never_after(rules: &[Rule], include: &HashSet<Page>) -> Page {
    *include
        .iter()
        .find(|i| {
            rules
                .iter()
                .filter(|Rule(bef, _)| include.contains(bef))
                .all(|Rule(_, aft)| aft != *i)
        })
        .unwrap()
}

fn main() {
    let input = INPUT.parse::<Input>().unwrap();
    println!("Part 1: {}", solve_1(&input));
    println!("Part 2: {}", solve_2(&input));
}

#[derive(Error, Debug)]
#[error("failed to parse input")]
enum ParseError {
    Int(#[from] ParseIntError),
    Structure,
}

impl FromStr for Rule {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (a, b) = s.split_once("|").ok_or(ParseError::Structure)?;
        Ok(Rule(a.parse()?, b.parse()?))
    }
}

impl FromStr for Update {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let pages: Result<Vec<Page>, ParseIntError> =
            s.split(",").map(|e| e.parse::<Page>()).collect();
        Ok(Update(pages?))
    }
}

impl FromStr for Input {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (rules_s, updates_s) = s.split_once("\n\n").ok_or(ParseError::Structure)?;
        Ok(Input {
            rules: rules_s
                .lines()
                .map(|l| l.parse::<Rule>())
                .collect::<Result<_, _>>()?,
            updates: updates_s
                .lines()
                .map(|l| l.parse::<Update>())
                .collect::<Result<_, _>>()?,
        })
    }
}

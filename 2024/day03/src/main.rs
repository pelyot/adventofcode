const INPUT: &'static str = include_str!("../input");

use once_cell::sync::Lazy;
use regex::Regex;

static RE: Lazy<Regex> = Lazy::new(|| Regex::new(r"mul\((\d+),(\d+)\)").unwrap());

fn solve_1(input: &str) -> i32 {
    RE.captures_iter(input)
        .map(|caps| {
            let (_, [a, b]) = caps.extract();
            a.parse::<i32>().unwrap() * b.parse::<i32>().unwrap()
        })
        .sum()
}

fn solve_2(input: &str) -> i32 {
    const DONT: &str = "don't()";
    const DO: &str = "do()";
    let mut i = 0;
    let mut acc = 0;
    while i < input.len() {
        let until = i + input[i..].find(DONT).unwrap_or(input[i..].len());
        acc += solve_1(&input[i..until]);
        i = until + DONT.len();
        if i >= input.len() {
            break;
        }
        let until = i + input[i..].find(DO).unwrap_or(input[i..].len());
        i = until + DO.len();
    }
    acc
}

fn main() {
    println!("Part 1: {}", solve_1(INPUT));
    println!("Part 2: {}", solve_2(INPUT));
}

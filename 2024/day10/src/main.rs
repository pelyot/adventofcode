use std::{collections::HashSet, num::ParseIntError, str::FromStr};

struct Grid {
    rows: Vec<Vec<u8>>,
}

fn solve_1(grid: &Grid) -> usize {
    grid.trail_heads()
        .into_iter()
        .map(|(r, c)| score(grid, r, c).0)
        .sum()
}

fn solve_2(grid: &Grid) -> usize {
    grid.trail_heads()
        .into_iter()
        .map(|(r, c)| score(grid, r, c).1)
        .sum()
}

fn score(grid: &Grid, start_r: i32, start_c: i32) -> (usize, usize) {
    let mut open = Vec::new();
    let mut summits = HashSet::new();
    let mut rating = 0;
    open.push((start_r, start_c));

    while let Some((r, c)) = open.pop() {
        let e = grid.at(r, c);
        if e == 9 {
            summits.insert((r, c));
            rating += 1;
        } else {
            open.extend(
                grid.neighbors(r, c)
                    .filter_map(|(r, c)| (grid.at(r, c) == e + 1).then_some((r, c))),
            );
        }
    }
    (summits.len(), rating)
}

fn main() {
    let test_grid = include_str!("../test_input").parse().unwrap();
    let grid = include_str!("../input").parse().unwrap();
    println!("Part 1 (test): {}", solve_1(&test_grid));
    println!("Part 1: {}", solve_1(&grid));
    println!("Part 2 (test): {}", solve_2(&test_grid));
    println!("Part 2: {}", solve_2(&grid));
}

impl Grid {
    fn at(&self, r: i32, c: i32) -> u8 {
        self.rows[r as usize][c as usize]
    }

    #[allow(unused)]
    fn get(&self, r: i32, c: i32) -> Option<u8> {
        self.in_bounds(r, c)
            .then_some(self.rows[r as usize][c as usize])
    }

    #[allow(unused)]
    fn in_bounds(&self, r: i32, c: i32) -> bool {
        r >= 0 && c >= 0 && (r as usize) < self.rows.len() && (c as usize) < self.rows[0].len()
    }

    fn neighbors(&self, r: i32, c: i32) -> impl Iterator<Item = (i32, i32)> {
        let w = self.rows.len();
        let h = self.rows[0].len();
        [(0, 1), (0, -1), (1, 0), (-1, 0)]
            .iter()
            .filter_map(move |(dr, dc)| {
                let nr = dr + r;
                let nc = dc + c;
                let in_bounds = nr >= 0 && nc >= 0 && (nr as usize) < w && (nc as usize) < h;
                in_bounds.then_some((nr, nc))
            })
    }

    fn trail_heads(&self) -> Vec<(i32, i32)> {
        let mut heads = Vec::new();
        for r in 0..self.rows.len() {
            for c in 0..self.rows[0].len() {
                if self.rows[r][c] == 0 {
                    heads.push((r as i32, c as i32))
                }
            }
        }
        heads
    }
}

impl FromStr for Grid {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Grid {
            rows: s
                .lines()
                .map(|r| r.chars().map(|ch| ch as u8 - b'0').collect())
                .collect(),
        })
    }
}

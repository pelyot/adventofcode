use counter::Counter;
use itertools::Itertools;

const INPUT: &str = include_str!("../input");

fn main() {
    let (mut a, mut b): (Vec<i32>, Vec<i32>) = INPUT
        .split_ascii_whitespace()
        .map(|s| s.parse::<i32>().unwrap())
        .tuples::<(_, _)>()
        .multiunzip();
    a.sort_unstable();
    b.sort_unstable();
    let part_1: i32 = a.iter().zip(&b).map(|(ai, bi)| (ai - bi).abs()).sum();
    println!("Part 1: {:?}", part_1);

    let counts = b.into_iter().collect::<Counter<_>>();
    let part_2: i32 = a
        .into_iter()
        .map(|i| i * counts.get(&i).cloned().unwrap_or(0) as i32)
        .sum();
    println!("Part 2: {:?}", part_2);
}

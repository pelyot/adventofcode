use gcd::Gcd;
use std::ops::{Add, Neg, Sub};

#[derive(Hash, Eq, PartialEq, PartialOrd, Ord, Debug, Clone, Copy)]
pub(crate) struct Vec2(pub i64, pub i64);

impl Vec2 {
    pub fn normalize_int(self) -> Self {
        let gcd = (self.0.abs() as u64).gcd_binary(self.1.abs() as u64) as i64;
        Self(self.0 / gcd, self.1 / gcd)
    }
}

impl Add for Vec2 {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Self(self.0 + rhs.0, self.1 + rhs.1)
    }
}

impl Neg for Vec2 {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self(-self.0, -self.1)
    }
}

impl Sub for Vec2 {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Self(self.0 - rhs.0, self.1 - rhs.1)
    }
}

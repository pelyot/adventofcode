mod vec2;

use std::{array, collections::HashSet};
use vec2::Vec2;

struct World {
    columns: i64,
    rows: i64,
    antennas: [Vec<Vec2>; 128],
}

fn process_antennas_1(a1: Vec2, a2: Vec2, world: &World, antinodes: &mut HashSet<Vec2>) {
    let p = a2 - a1 + a2;
    if p.0 >= 0 && p.1 >= 0 && p.0 < world.rows && p.1 < world.columns {
        antinodes.insert(p);
    }
}

fn process_antennas_2(a1: Vec2, a2: Vec2, world: &World, antinodes: &mut HashSet<Vec2>) {
    let diff = (a2 - a1).normalize_int();
    let mut p = a2;
    while p.0 >= 0 && p.1 >= 0 && p.0 < world.rows && p.1 < world.columns {
        antinodes.insert(p);
        p = p + diff;
    }
}

type ProcessAntennas = fn(Vec2, Vec2, &World, &mut HashSet<Vec2>) -> ();

fn solve_1(world: &World) -> usize {
    solve(world, process_antennas_1)
}

fn solve_2(world: &World) -> usize {
    solve(world, process_antennas_2)
}

fn solve(world: &World, process_antennas: ProcessAntennas) -> usize {
    let mut antinodes = HashSet::new();
    for antennas in ('0'..='9')
        .chain('a'..='z')
        .chain('A'..='Z')
        .map(|ch| &world.antennas[ch as usize])
        .filter(|a| a.len() > 1)
    {
        for r in 0..antennas.len() - 1 {
            for s in (r + 1)..antennas.len() {
                let a1 = antennas[r];
                let a2 = antennas[s];
                process_antennas(a1, a2, world, &mut antinodes);
                process_antennas(a2, a1, world, &mut antinodes);
            }
        }
    }
    antinodes.len()
}

fn main() {
    let world = parse(include_str!("../input"));
    let test_world = parse(include_str!("../test_input"));
    println!("Part1 (test): {}", solve_1(&test_world));
    println!("Part1: {}", solve_1(&world));
    println!("Part2 (test): {}", solve_2(&test_world));
    println!("Part2: {}", solve_2(&world));
}

fn parse(input: &str) -> World {
    let mut world = World {
        rows: input.lines().count() as i64,
        columns: input.lines().next().unwrap().len() as i64,
        antennas: array::from_fn(|_| Vec::new()),
    };
    for (r, c, ch) in input
        .lines()
        .enumerate()
        .flat_map(|(r, l)| l.chars().enumerate().map(move |(c, ch)| (r, c, ch)))
        .filter(|(_, _, ch)| *ch != '.')
    {
        world.antennas[ch as usize].push(Vec2(r as i64, c as i64));
    }
    world
}

use std::iter::repeat_n;

type Id = u16;

fn solve_1(layout: &str) -> usize {
    let layout = layout.as_bytes();
    let mut disk: Vec<Option<Id>> = Vec::new();
    let mut id = 0;
    for i in 0..layout.len() {
        let reps = (layout[i] - b'0') as usize;
        match i % 2 {
            0 => {
                disk.extend(repeat_n(Some(id), reps));
                id += 1;
            }
            _ => {
                disk.extend(repeat_n(None, reps));
            }
        }
    }
    let mut beg = 0;
    let mut end = disk.len() - 1;
    while beg < end {
        match (disk[beg], disk[end]) {
            (Some(_), _) => {
                beg += 1;
            }
            (_, None) => {
                end -= 1;
            }
            (None, Some(_)) => {
                disk.swap(beg, end);
            }
        }
    }
    disk.iter()
        .take_while(|b| b.is_some())
        .enumerate()
        .map(|(k, id)| k as usize * (id.unwrap() as usize))
        .sum()
}

struct Block {
    size: u8,
    file: Option<Id>,
}

fn solve_2(input: &str) -> usize {
    let (mut disk, file_count) = {
        let layout = input.as_bytes();
        let mut disk: Vec<Block> = Vec::new();
        let mut id = 0;
        for i in 0..layout.len() {
            let reps = layout[i] - b'0';
            match i % 2 {
                0 => {
                    disk.push(Block {
                        size: reps,
                        file: Some(id),
                    });
                    id += 1;
                }
                _ => {
                    disk.push(Block {
                        size: reps,
                        file: None,
                    });
                }
            }
        }
        (disk, id)
    };

    for next_file_to_move in (0..file_count).rev() {
        // find index of next block to move:
        let mut file_pos = disk
            .iter()
            .rposition(|b| b.file == Some(next_file_to_move))
            .unwrap();
        let file_size = disk[file_pos].size;
        // find index of first accepting block
        if let Some(space_pos) = disk
            .iter()
            .position(|b| b.size >= file_size && b.file.is_none())
        {
            if space_pos >= file_pos {
                continue;
            }
            let space_size = disk[space_pos].size;
            // split the space block into 2 if needed
            if space_size > file_size {
                disk.insert(
                    space_pos,
                    Block {
                        size: file_size,
                        file: None,
                    },
                );
                disk[space_pos + 1].size = space_size - file_size;
                file_pos += 1;
            }
            disk.swap(file_pos, space_pos);
        }
    }
    disk.iter()
        .map(|b| repeat_n(b.file.unwrap_or(0), b.size as usize))
        .flatten()
        .enumerate()
        .map(|(i, id)| i * (id as usize))
        .sum()
}

fn main() {
    assert_eq!(solve_1("2333133121414131402"), 1928);
    assert_eq!(solve_2("2333133121414131402"), 2858);
    println!("Part 1: {}", solve_1(include_str!("../input").trim()));
    println!("Part 2: {}", solve_2(include_str!("../input").trim()));
}

use std::collections::HashMap;

fn blink(st: &str) -> (String, Option<String>) {
    let mut stones_n = Vec::new();
    match st {
        "0" => ("1".to_owned(), None),
        s if s.len() % 2 == 0 => {
            let (s1, s2) = s.split_at(s.len() / 2);
            stones_n.push(s1.to_owned());
            let s2 = s2.trim_start_matches('0');
            let s2 = {
                if s2.is_empty() {
                    "0".to_owned()
                } else {
                    s2.to_owned()
                }
            };
            (s1.to_owned(), Some(s2))
        }
        _ => {
            let s = st.parse::<u64>().unwrap().checked_mul(2024).unwrap();
            (s.to_string(), None)
        }
    }
}

fn blink_rec(stone: &str, count: usize, cache: &mut HashMap<(u64, usize), usize>) -> usize {
    let stone_int = stone.parse::<u64>().unwrap();
    if let Some(cached) = cache.get(&(stone_int, count)) {
        return *cached;
    } else {
        let res = match count {
            0 => 1,
            _ => match blink(stone) {
                (s1, None) => blink_rec(&s1, count - 1, cache),
                (s1, Some(s2)) => {
                    blink_rec(&s1, count - 1, cache) + blink_rec(&s2, count - 1, cache)
                }
            },
        };
        cache.insert((stone_int, count), res);
        res
    }
}

fn solve(stones: Vec<String>, count: usize) -> usize {
    stones
        .iter()
        .map(|st| blink_rec(st, count, &mut HashMap::new()))
        .sum()
}

fn main() {
    let test_input: Vec<String> = "125 17"
        .split_ascii_whitespace()
        .map(|s| s.to_owned())
        .collect();
    let input: Vec<String> = include_str!("../input")
        .split_ascii_whitespace()
        .map(|s| s.to_owned())
        .collect();

    assert_eq!(solve(test_input.clone(), 25), 55312);
    println!("Part 1: {}", solve(input.clone(), 25));
    println!("Part 2: {}", solve(input.clone(), 75));
}
